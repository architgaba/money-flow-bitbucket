import { NgModule, InjectionToken } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules, ActivatedRouteSnapshot } from '@angular/router';
import { LandingPageComponent } from './shared/landing-page/landing-page.component';
import { ContactUsComponent } from './shared/contact-us/contact-us.component';
import { ErrorComponent } from './error-component/error-component.component';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { SignupComponent } from './shared/signup/signup.component';
import { LoginComponent } from './shared/login/login.component';
import { IntroductionComponent } from './shared/introduction/introduction.component';
import { UserActivationComponent } from './shared/user-activation/user-activation.component';
import { FromThePresidentComponent } from './shared/from-the-president/from-the-president.component';
import { MileageTrackerComponent } from './shared/mileage-tracker/mileage-tracker.component';
import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';
import { P2pDistributorRegistrationComponent } from './shared/p2p-distributor-registration/p2p-distributor-registration.component';
import { SharedComponentAuthGuard } from './core/services/shared.component.guard';
import { HelpCenterComponent } from './shared/help-center/help-center.component';
import { AccountSetupComponent } from './shared/account-setup/account-setup.component';
import { AppFeaturesComponent } from './shared/app-features/app-features.component';
import { FaqComponent } from './shared/faq/faq.component';
import { GrowingMoneyComponent } from './shared/growing-money/growing-money.component';
import { HowDoCancelComponent } from './shared/how-do-cancel/how-do-cancel.component';
import { ResetPasswordComponent } from './shared/reset-password/reset-password.component';
const externalUrlProvider = new InjectionToken('externalUrlRedirectResolver');
const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  {
    path: 'credit-repair',
    component: FromThePresidentComponent,
  },
  { path: 'leaders-team', component: MileageTrackerComponent },
  { path: 'the-academy', component: IntroductionComponent },
  {
    path: 'help-center', component: HelpCenterComponent,
    children: [
      { path: '', redirectTo: 'contact-us', pathMatch: 'full' },
      { path: 'contact-us', component: ContactUsComponent },
      { path: 'account-setup', component: AccountSetupComponent },
      { path: 'app-features', component: AppFeaturesComponent },
      { path: 'faqs', component: FaqComponent },
      { path: 'growing-your-money', component: GrowingMoneyComponent },
      { path: 'how-do-i-cancel', component: HowDoCancelComponent }
    ]
  },
  { path: 'activate-user/:id', component: UserActivationComponent, canActivate: [SharedComponentAuthGuard] },
  { path: 'reset-password/:id', component: ResetPasswordComponent, canActivate: [SharedComponentAuthGuard] },
  {
    path: 'p2p-distributor-register',
    canActivate: [SharedComponentAuthGuard],
    component: P2pDistributorRegistrationComponent
  },
  {
    path: 'dashboard',
    canActivate: [NgxPermissionsGuard],
    loadChildren: 'src/app/dashboard/dashboard.module#DashboardModule',
    data: {
      permissions: {
        only: ['ROLE_PROSPECT', 'ROLE_SUPER_ADMIN', 'ROLE_CUSTOMER_REP', 'ROLE_CUSTOMER', 'ROLE_ADMIN'],
        redirectTo: '/home'
      }
    }
  }, {
    path: 'app-settings',
    canActivate: [NgxPermissionsGuard],
    loadChildren: 'src/app/application-setting/application-setting.module#ApplicationSettingModule',
    data: {
      permissions: {
        only: ['ROLE_SUPER_ADMIN'],
        redirectTo: '/home'
      }
    }
  },
  {
    path: 'contact-enquiries',
    canActivate: [NgxPermissionsGuard],
    loadChildren: 'src/app/contact-enquiries/contact-enquiries.module#ContactEnquiriesModule',
    data: {
      permissions: {
        only: ['ROLE_SUPER_ADMIN'],
        redirectTo: '/home'
      }
    }
  },
  {
    path: 'directory',
    canActivate: [NgxPermissionsGuard],
    loadChildren: 'src/app/directory/directory.module#DirectoryModule',
    data: {
      permissions: {
        only: ['ROLE_SUPER_ADMIN'],
        redirectTo: '/home'
      }
    }
  },
  {
    path: 'change-referral-code',
    canActivate: [NgxPermissionsGuard],
    loadChildren: 'src/app/change-referral-code/change-referral-code.module#ChangeReferralCodeModule',
    data: {
      permissions: {
        only: ['ROLE_SUPER_ADMIN'],
        redirectTo: '/home'
      }
    }
  },
  {
    path: 'manage-promotions',
    canActivate: [NgxPermissionsGuard],
    loadChildren: 'src/app/manage-promotions/manage-promotions.module#ManagePromotionsModule',
    data: {
      permissions: {
        only: ['ROLE_SUPER_ADMIN'],
        redirectTo: '/home'
      }
    }
  },
  {
    path: 'error',
    component: ErrorComponent
  },
  {
    path: 'home',
    component: LandingPageComponent
  },
  {
    path: 'externalRedirect',
    canActivate: [externalUrlProvider],
    component: PageNotFoundComponent,
  },
  {
    path: 'page-not-found',
    component: PageNotFoundComponent
  },
  {
    path: '**',
    redirectTo: 'page-not-found'
  }
];
@NgModule({
  providers: [
    {
      provide: externalUrlProvider,
      useValue: (route: ActivatedRouteSnapshot) => {
        const externalUrl = route.paramMap.get('externalUrl');
        window.open(externalUrl, '_blank');
      },
    },
  ],
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
