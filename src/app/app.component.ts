import { Component, OnInit, ViewChild, ElementRef, OnDestroy, ChangeDetectorRef, AfterViewChecked } from "@angular/core";
import { NgxRolesService, NgxPermissionsService } from "ngx-permissions";
import { AuthServerProvider } from "./core/services/auth-jwt.service";
import { AccountService } from "./core/services/account.service";
import { Principal } from "./core/services/principal.service";
import { Keepalive } from "@ng-idle/keepalive";
import { ModalDirective, MDBSpinningPreloader } from "ng-uikit-pro-standard";
import { AlertService } from "./core/services/alert.service";
import { Subscription } from "rxjs";
import { SpinnerService } from "./core/services/spinner.service";
import { RolesService } from "./core/services/roles.service";
import { SessionStorageService } from "ngx-webstorage";
import {
  Router, Event as RouterEvent,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError
} from "@angular/router";
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit, OnDestroy, AfterViewChecked {
  @ViewChild("alert") alert: ElementRef;
  @ViewChild("basicModal") modal: ModalDirective;
  idleState = "Not started.";
  timedOut = false;
  lastPing?: Date = null;
  title = "money-flow-manager-frontend";
  message: any;
  messageSubscription: Subscription;
  alertSubscription: Subscription;
  spinnerSubscription: Subscription;
  alertMsg: any;
  alertType: any;
  setType: string;
  removeAlert = false;
  public loadingSpinner = false;
  constructor(
    private sessionStorage: SessionStorageService,
    private cdRef: ChangeDetectorRef,
    private mdbSpinningPreloader: MDBSpinningPreloader,
    private keepalive: Keepalive,
    private principalService: Principal,
    private account: AccountService,
    private roleService: NgxRolesService,
    private jwtService: AuthServerProvider,
    private alertService: AlertService,
    private spinnerService: SpinnerService,
    private ngxPermissionsService: NgxPermissionsService,
    private rolesService: RolesService,
    private router: Router
  ) {
    this.router.events.subscribe((event: RouterEvent) => {
      this.navigationInterceptor(event)
    })
  }
  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.spinnerService.showLoader();
    }
    if (event instanceof NavigationEnd) {
      this.spinnerService.hideLoader();
    }
    if (event instanceof NavigationCancel) {
      this.spinnerService.hideLoader();
    }
    if (event instanceof NavigationError) {
      this.spinnerService.hideLoader();
    }
  }
  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }
  ngOnInit() {
    console.log("Loading permissions again.");
    window.scroll(0, 0);
    this.mdbSpinningPreloader.stop();
    this.spinnerSubscription = this.spinnerService
      .getLoaderValue()
      .subscribe(loader => {
        this.loading(loader);
      });
    this.alertSubscription = this.alertService
      .getAlertType()
      .subscribe(alertType => {
        this.alertType = alertType;
      });
    this.messageSubscription = this.alertService
      .getMessage()
      .subscribe(message => {
        this.message = message;
        this.closeAlert();
      });
    console.log(this.message);
    console.log(this.alertType);
    let token = this.jwtService.getToken();
    if (token) {
      this.spinnerService.showLoader();
      this.roleService.flushRoles();
      var perms = JSON.parse(this.sessionStorage.retrieve('permissions'));
      if (perms) {
        this.ngxPermissionsService.loadPermissions(Object.keys(perms))
        console.log("qualificationss", this.ngxPermissionsService.getPermissions())
        this.rolesService.clearRole();
        Object.keys(perms).forEach(element => {
          this.rolesService.setRole(element);
        });
        this.account
          .get()
          .toPromise()
          .then(response => {
            const account = response.body;
            if (account) {
              this.principalService.setUserNameForDashboard(account["name"], account["refCode"], account["refType"], account['myTeamSize'], account['customerCount'], account['isEligibleForPromotion']);
            }
          });
        this.spinnerService.hideLoader();
      } else {
        this.roleService.flushRoles();
        this.rolesService.clearRole();
        this.account
          .get()
          .toPromise()
          .then(response => {
            const account = response.body;

            if (account) {
              this.principalService.setUserNameForDashboard(account["name"], account["refCode"], account["refType"], account['myTeamSize'], account['customerCount'], account['isEligibleForPromotion']);
              let userIdentity = account["role"];
              console.log("Loading role again...");
              userIdentity.forEach(element => {
                this.roleService.addRole(element, []);
                this.rolesService.setRole(element);
              });
              this.roleService
                .addRole(account['permission'], []);
              this.rolesService.setRole(account['permission']);
              this.spinnerService.hideLoader();
            }
          })
          .catch(err => { this.spinnerService.hideLoader(); });
      }
    }
    this.keepalive.interval(15);
    this.keepalive.onPing.subscribe(() => (this.lastPing = new Date()));
    this.reset();
  }
  reset() {
    this.idleState = "Started.";
    this.timedOut = false;
  }
  loading(loader) {
    if (loader) {
      this.loadingSpinner = true;
    } else {
      this.loadingSpinner = false;
    }
  }
  closeAlert() {
    if (this.alertType === "error") {
      this.setType = "alert-danger";
    } else if (this.alertType === "success") {
      this.setType = "alert-success";
    }
    this.removeAlert = false;
    this.alert.nativeElement.classList.add("show");
    setTimeout(() => {
      this.alert.nativeElement.classList.remove("show");
      this.removeAlert = true;
    }, 3000);
  }
  dismissAlert() {
    this.alert.nativeElement.classList.remove("show");
    this.removeAlert = true;
  }
  ngOnDestroy() {
    this.messageSubscription.unsubscribe();
    this.alertSubscription.unsubscribe();
    this.spinnerSubscription.unsubscribe();
  }
}
