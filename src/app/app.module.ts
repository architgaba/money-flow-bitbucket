import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MDBBootstrapModulesPro, TooltipModule } from 'ng-uikit-pro-standard';
import { MDBSpinningPreloader } from 'ng-uikit-pro-standard';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { ApiInterceptorService } from './core/interceptors/api-interceptor.service';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ApiCommonService } from './core/services/api-common.service';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { ErrorHandlerInterceptor } from './core/interceptors/errorhandler.interceptor';
import { ErrorComponent } from './error-component/error-component.component';
import { NgxPermissionsModule } from 'ngx-permissions';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './layouts/header/header.component';
import { ModalModule, WavesModule, InputsModule, ButtonsModule } from 'ng-uikit-pro-standard';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { MomentModule } from 'angular2-moment';
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';
import { AgmCoreModule } from '@agm/core';
import { NgxStripeModule } from 'ngx-stripe';
import { ExternalUrlDirective } from './core/pipes/external-url.directive';
import { DirectoryModule } from './directory/directory.module';
// import { TooltipModule } from 'primeng/tooltip';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ErrorComponent,
    ExternalUrlDirective
  ],
  imports: [
    NgxLoadingModule.forRoot({
      animationType: ngxLoadingAnimationTypes.doubleBounce,
      backdropBackgroundColour: 'rgba(0,0,0,0.5)',
      backdropBorderRadius: '4px',
      primaryColour: '#00c851',
      secondaryColour: '#00c851',
      tertiaryColour: '#00c851'
    }),
    NgxStripeModule.forRoot('pk_live_oKsL4mOmR9fGqCy0ldsPJbbE'),
    // NgxStripeModule.forRoot('pk_test_wZ7IbrC0SLFgO0qZgWziagRM'),
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    CoreModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    TooltipModule,
    MDBBootstrapModulesPro.forRoot(),
    NgxWebstorageModule.forRoot(),
    NgxPermissionsModule.forRoot(),
    MomentModule,
    NgIdleKeepaliveModule.forRoot(),
    ModalModule, WavesModule, InputsModule, ButtonsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDRNeu5OsRcsOGm9ce2V6uBWJv0cESDRGo',
      libraries: ['geometry']
    }),
    DirectoryModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptorService,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorHandlerInterceptor,
      multi: true
    },
    ApiCommonService,
    MDBSpinningPreloader,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

