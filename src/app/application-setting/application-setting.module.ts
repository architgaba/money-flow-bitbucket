import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApplicationSettingRoutingModule } from './application-setting-routing.module';
import { ApplicationSettingComponent } from './application-setting/application-setting.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {
  ModalModule, ButtonsModule, InputsModule
} from 'ng-uikit-pro-standard';
@NgModule({
  declarations: [ApplicationSettingComponent],
  imports: [
    CommonModule,
    ApplicationSettingRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    ModalModule,
    ButtonsModule,
    InputsModule
  ]
})
export class ApplicationSettingModule { }
