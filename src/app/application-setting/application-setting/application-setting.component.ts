import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { AlertService } from 'src/app/core/services/alert.service';
import { ApplicationSettingService } from './application-setting.service';
@Component({
  selector: 'app-application-setting',
  templateUrl: './application-setting.component.html',
  styleUrls: ['./application-setting.component.scss']
})
export class ApplicationSettingComponent implements OnInit {
  commissionSettingForm: FormGroup;
  constructor(private applicationSettingService: ApplicationSettingService,
    private _fb: FormBuilder,
    private loaderService: SpinnerService,
    private alertService: AlertService) {
  }
  ngOnInit() {
    this.getCommissionSettingData();
    this.commissionSettingForm = this._fb.group({
      "commissionSettingId": [],
      "schedulerRunningDay": [null, [Validators.required]],
      "commission1stLevelAmount": [null, [Validators.required]],
      "commission2ndLevelAmount": [null, [Validators.required]]
    });
  }
  getCommissionSettingData() {
    this.applicationSettingService.getCommissionSetting().subscribe(res => {
      this.commissionSettingForm.controls.commissionSettingId.setValue(res.commissionSettingId);
      this.commissionSettingForm.controls.schedulerRunningDay.setValue(res.schedulerRunningDay, [Validators.required]);
      this.commissionSettingForm.controls.commission1stLevelAmount.setValue(res.commission1stLevelAmount, [Validators.required]);
      this.commissionSettingForm.controls.commission2ndLevelAmount.setValue(res.commission2ndLevelAmount, [Validators.required]);
    }, err => {
    }, () => {
    })
  }
  saveUpdatedCommissionSettingData() {
    if (this.commissionSettingForm.valid) {
      this.loaderService.showLoader();
      const body = this.commissionSettingForm.value;
      const commissionSettingId = this.commissionSettingForm.controls.commissionSettingId.value;
      this.applicationSettingService.saveUpdatedCommissionSetting(commissionSettingId, body).subscribe(res => {
        this.loaderService.hideLoader();
        this.alertService.clearMessage();
        this.alertService.sendMessage(res.message, 'success');
      }, err => {
      }, () => {
        this.getCommissionSettingData();
      });
    } else {
      Object.keys(this.commissionSettingForm.controls).forEach(key => {
        this.commissionSettingForm.get(key).markAsDirty();
      });
    }
  }
}
