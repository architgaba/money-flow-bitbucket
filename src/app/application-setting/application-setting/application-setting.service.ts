import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiCommonService } from 'src/app/core/services/api-common.service';
@Injectable({
  providedIn: 'root'
})
export class ApplicationSettingService {
  constructor(private apiCommonService: ApiCommonService) { }
  getCommissionSetting(): Observable<any> {
    return this.apiCommonService.get('/commission/setting');
  }
  saveUpdatedCommissionSetting(id, body): Observable<any> {
    return this.apiCommonService.put('/commission/setting/update/id/' + id, body)
  }
}
