import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChangeReferralCodeComponent } from './change-referral-code/change-referral-code.component';
import { ChangeReferralCodeRoutingModule } from './change-referral-code.routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ModalModule, ButtonsModule, InputsModule } from 'ng-uikit-pro-standard';

@NgModule({
  declarations: [ChangeReferralCodeComponent],
  imports: [
    CommonModule,
    ChangeReferralCodeRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    ModalModule,
    ButtonsModule,
    InputsModule
  ]
})
export class ChangeReferralCodeModule { }
