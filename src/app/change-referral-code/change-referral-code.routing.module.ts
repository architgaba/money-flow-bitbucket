import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChangeReferralCodeComponent } from './change-referral-code/change-referral-code.component';
const routes: Routes = [{
    path: '',
    component: ChangeReferralCodeComponent
}];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ChangeReferralCodeRoutingModule { }
