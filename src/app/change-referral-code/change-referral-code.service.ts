import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiCommonService } from 'src/app/core/services/api-common.service';
@Injectable({
    providedIn: 'root'
})
export class ChangeReferralCodeService {
    constructor(private apiCommonService: ApiCommonService) { }
    changeReferralCode(body: any): Observable<any> {
        return this.apiCommonService.post('/admin/change-referral-code', body);
    }
}
