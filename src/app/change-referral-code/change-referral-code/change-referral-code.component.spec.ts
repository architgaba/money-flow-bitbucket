import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeReferralCodeComponent } from './change-referral-code.component';

describe('ChangeReferralCodeComponent', () => {
  let component: ChangeReferralCodeComponent;
  let fixture: ComponentFixture<ChangeReferralCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeReferralCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeReferralCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
