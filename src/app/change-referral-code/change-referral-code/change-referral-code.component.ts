import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ChangeReferralCodeService } from '../change-referral-code.service';
import { AlertService } from 'src/app/core/services/alert.service';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { AccountService } from 'src/app/core/services/account.service';

@Component({
  selector: 'app-change-referral-code',
  templateUrl: './change-referral-code.component.html',
  styleUrls: ['./change-referral-code.component.scss']
})
export class ChangeReferralCodeComponent implements OnInit {
  changeReferralCodeForm: FormGroup;
  constructor(private _fb: FormBuilder, private changeReferralCodeService: ChangeReferralCodeService, private alertService: AlertService, private loaderService: SpinnerService, private accountService: AccountService) { }

  ngOnInit() {
    this.changeReferralCodeForm = this._fb.group({
      currentReferralCode: ["", Validators.required],
      newReferralCode: ["", Validators.required],
      userRegistrationId: ["", Validators.required],
    });
  }
  changeReferralCode() {
    console.log(this.changeReferralCodeForm.value);
    if (this.changeReferralCodeForm.valid) {
      this.loaderService.showLoader();
      const body = {
        "currentReferralCode": this.changeReferralCodeForm.controls.currentReferralCode.value,
        "newReferralCode": this.changeReferralCodeForm.controls.newReferralCode.value,
        "userRegistrationId": this.changeReferralCodeForm.controls.userRegistrationId.value,
      }
      this.changeReferralCodeService.changeReferralCode(this.changeReferralCodeForm.value).subscribe(res => {
        this.loaderService.hideLoader();
        this.alertService.clearMessage();
        this.alertService.sendMessage(res.message, 'success');
        this.changeReferralCodeForm.reset();
      }, (err) => {

      }, () => {

      });
    } else {
      Object.keys(this.changeReferralCodeForm.controls).forEach(key => {
        this.changeReferralCodeForm.get(key).markAsDirty();
      });
    }



  }

}
