import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CkeditorComponent } from './ckeditor/ckeditor.component';
@NgModule({
  declarations: [CkeditorComponent],
  imports: [
    CommonModule
  ],
  exports: [CkeditorComponent]
})
export class CkeditorModule { }
