import { Component } from '@angular/core';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
const ClassicEditor = require('@ckeditor/ckeditor5-build-classic');
import { SpinnerService } from 'src/app/core/services/spinner.service';
declare var require: any
declare const window: any;
declare const $: any;
@Component({
  selector: 'app-ckeditor',
  templateUrl: './ckeditor.component.html',
  styleUrls: ['./ckeditor.component.scss']
})
export class CkeditorComponent {
  constructor(private spinnerService: SpinnerService) {
  }
  ngOnInit() {
    this.spinnerService.showLoader();
    this.loadScript('https://s3.us-east-2.amazonaws.com/mfmprod-bucket/ckeditor/ckeditor.js');
    this.spinnerService.hideLoader();
    if (window.CKEDITOR) {
      window.CKEDITOR.replace('editor1');
    }
    ClassicEditor
      .create(document.querySelector('#editor'))
      .catch(error => {
        console.error(error);
      });
  }
  private loadScript(scriptUrl: string) {
    return new Promise((resolve, reject) => {
      const scriptElement = document.createElement('script');
      scriptElement.src = scriptUrl;
      scriptElement.onload = resolve;
      document.body.appendChild(scriptElement);
    })
  }
}


