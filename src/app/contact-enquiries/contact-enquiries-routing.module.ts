import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactEnquiriesComponent } from './contact-enquiries/contact-enquiries.component';
const routes: Routes = [{
  path: '',
  component: ContactEnquiriesComponent
}];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactEnquiriesRoutingModule { }
