import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactEnquiriesRoutingModule } from './contact-enquiries-routing.module';
import { ContactEnquiriesComponent } from './contact-enquiries/contact-enquiries.component';
import { PanelModule, DialogModule } from 'primeng/primeng';
import {DropdownModule} from 'primeng/dropdown';
import { DataViewModule } from 'primeng/dataview';
import { ModalModule, InputsModule } from 'ng-uikit-pro-standard';
import { FormsModule } from '@angular/forms';
import {TooltipModule} from 'primeng/tooltip';
@NgModule({
  declarations: [ContactEnquiriesComponent],
  imports: [
    CommonModule,
    ContactEnquiriesRoutingModule,
    PanelModule,
    DataViewModule,
    DialogModule,
    DropdownModule,
    ModalModule,
    FormsModule,
    InputsModule,
    TooltipModule
  ]
})
export class ContactEnquiriesModule { }
