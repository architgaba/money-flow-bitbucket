import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/primeng';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { ApiCommonService } from 'src/app/core/services/api-common.service';
import { AlertService } from 'src/app/core/services/alert.service';
@Component({
  selector: 'app-contact-enquiries',
  templateUrl: './contact-enquiries.component.html',
  styleUrls: ['./contact-enquiries.component.scss']
})
export class ContactEnquiriesComponent implements OnInit {
  displayDialog: boolean;
  contactEnquiryForm: FormGroup;
  sortOptions: SelectItem[];
  selectedEnquiry: Enquiry;
  sortKey: string;
  sortField: string;
  sortOrder: number;
  enquiries: Enquiry[];
  constructor(private _fb: FormBuilder, private spinnerService: SpinnerService, private apiCommonService: ApiCommonService, private alertService: AlertService) { }
  selectEnquiry(event: Event, enquiry: Enquiry) {
    this.selectedEnquiry = enquiry;
    this.displayDialog = true;
    event.preventDefault();
  }
  ngOnInit() {
    this.sortOptions = [
      { label: 'Newest First', value: '!date' },
      { label: 'Oldest First', value: 'date' },
    ];
    this.getContactEnquiry();
  }
  onDialogHide() {
    this.selectedEnquiry = null;
  }
  getContactEnquiry() {
    this.enquiries = [];
    this.spinnerService.showLoader();
    this.apiCommonService.get('/admin/contact-enquiries').subscribe(res => {
      this.enquiries = res;
    }, (err) => {
    }, () => {
      console.log(this.enquiries);
      this.spinnerService.hideLoader();
    });
  }
  onSortChange(event) {
    let value = event.value;
    if (value.indexOf('!') === 0) {
      this.sortOrder = -1;
      this.sortField = value.substring(1, value.length);
    }
    else {
      this.sortOrder = 1;
      this.sortField = value;
    }
  }
}
export interface Enquiry {
  emailId: string
  message: string
  name: string
  subject: string
  referredBy: string
}
