import { Injectable } from "@angular/core";
import {
  HttpInterceptor,
  HttpRequest,
  HttpErrorResponse,
  HttpHandler,
  HttpEvent
} from "@angular/common/http";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { Router, NavigationExtras } from "@angular/router";
import { AlertService } from "../services/alert.service";
import { AuthServerProvider } from "../services/auth-jwt.service";
import { SpinnerService } from "../services/spinner.service";
@Injectable()
export class ErrorHandlerInterceptor implements HttpInterceptor {
  constructor(
    private spinnerService: SpinnerService,
    private router: Router,
    private alertService: AlertService,
    private authServerProvider: AuthServerProvider
  ) { }
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      tap(
        (event: HttpEvent<any>) => { },
        (err: any) => {
          this.spinnerService.hideLoader();
          console.log("In ERROR Handler Interceptor");
          if (err instanceof HttpErrorResponse) {
            if (err.status === 400) {
              this.alertService.clearMessage();
              this.alertService.sendMessage(err.error.message, "error");
            }
            if (err.status >= 401) {
              let navigationExtras: NavigationExtras = {
                queryParams: {
                  message: JSON.stringify(err.error.message),
                  error: JSON.stringify(err.status)
                }
              };
              this.router.navigate(["error"], navigationExtras);
              window.scroll(0, 0);
              if (err.status === 401) {
                console.log("Inside logout method");
                this.authServerProvider.logout().subscribe();
              }
            }
            if (err.status === 0) {
              this.authServerProvider.logout().subscribe();
              let navigationExtras: NavigationExtras = {
                queryParams: {
                  message: "Server Down Please Try Later",
                  error: 503
                }
              };
              this.router.navigate(["error"], navigationExtras);
            }
            console.log(err);
          }
        }
      )
    );
  }
}
