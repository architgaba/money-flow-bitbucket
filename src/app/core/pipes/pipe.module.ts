import { NgModule } from "@angular/core";
import { BooleanPipe } from "./boolean.pipe";
import { CommonModule } from "@angular/common";
import { PhoneMaskDirective } from "./phone-mask.directive";
@NgModule({
  imports: [CommonModule],
  declarations: [BooleanPipe,PhoneMaskDirective],
  exports: [BooleanPipe,PhoneMaskDirective]   
})
export class PipeModule {}
