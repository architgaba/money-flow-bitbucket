import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
@Injectable({ providedIn: 'root' })
export class AccountService {
    constructor(private http: HttpClient) { }
    get(): Observable<HttpResponse<Account>> {
        return this.http.get<Account>(environment.baseUrl + '/user/authorize', { observe: 'response' },
        );
    }
}
  