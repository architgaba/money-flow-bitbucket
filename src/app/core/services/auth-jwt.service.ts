import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { NgxPermissionsService, NgxRolesService } from 'ngx-permissions';
import { AlertService } from './alert.service';
@Injectable({ providedIn: 'root' })
export class AuthServerProvider {
    constructor(private permissionsService: NgxPermissionsService,
        private rolesService: NgxRolesService, private http: HttpClient,
        private $localStorage: LocalStorageService,
        private $sessionStorage: SessionStorageService,
        private alertService: AlertService) {
    }
    getToken() {
        return this.$localStorage.retrieve('authenticationToken') || this.$sessionStorage.retrieve('authenticationToken');
    }
    login(credentials): Observable<any> {
        console.log("clearing permission");
        const data = {
            userName: credentials.userName,
            password: credentials.password
        };
        return this.http.post(environment.baseUrl + '/user/authenticate', data, { observe: 'response' }).pipe(map(authenticateSuccess.bind(this)));
        function authenticateSuccess(resp) {
            console.log(resp.headers.get('Authorization'));
            const bearerToken = resp.headers.get('Authorization');
            if (bearerToken && bearerToken.slice(0, 7) === 'Bearer ') {
                const jwt = bearerToken.slice(7, bearerToken.length);
                this.storeAuthenticationToken(jwt, credentials.rememberMe);
                return jwt;
            }
        }
    }
    loginWithToken(jwt, rememberMe) {
        if (jwt) {
            return Promise.resolve(jwt);
        } else {
            return Promise.reject('auth-jwt-service Promise reject'); // Put appropriate error message here
        }
    }
    storeAuthenticationToken(jwt, rememberMe) {
        this.$sessionStorage.store('authenticationToken', jwt);
    }
    logout(): Observable<any> {
        this.rolesService.flushRoles();
        return new Observable(observer => {
            this.$localStorage.clear('authenticationtoken');
            this.$localStorage.clear('permissions');
            this.$sessionStorage.clear('permissions');
            window.localStorage.clear();
            this.permissionsService.flushPermissions();
            this.$sessionStorage.clear('authenticationtoken');
            observer.complete();
            this.alertService.clearMessage();
            this.alertService.sendMessage('You have logged out successfully.', 'success');
        });
    }
    getRoles() {
        let roles = this.rolesService.getRoles();
        console.log(roles)
        this.rolesService.roles$.subscribe((data) => {
            console.log(data);
        })
    }
}