import { Injectable } from "@angular/core";
@Injectable({ providedIn: "root" })
export class DateFormatterService {
  getFormattedDate(date: Date) {
    var dd;
    var mm;
    dd = date.getDate();
    mm = date.getMonth() + 1;
    var yyyy = date.getFullYear();
    if (dd < 10) {
      dd = "0" + dd;
    }
    if (mm < 10) {
      mm = "0" + mm;
    }
    return mm + "-" + dd + "-" + yyyy;
  }
  getFormattedTime(timeData) {
    const time = new Date(timeData);
    let hr: any = time.getHours();
    let min: any = time.getMinutes();
    if (hr < 10) {
      hr = "0" + hr;
    }
    if (min < 10) {
      min = "0" + min;
    }
    return hr + ":" + min;
  }
}
