import { Injectable } from '@angular/core';
import { SessionStorageService } from 'ngx-webstorage';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { AccountService } from './account.service';
import { NgxRolesService } from 'ngx-permissions';
import { SpinnerService } from './spinner.service';
import { RolesService } from './roles.service';
@Injectable({ providedIn: 'root' })
export class Principal {
    private userIdentity: any;
    public loggedInUser = new BehaviorSubject('');
    public referrerCode = new BehaviorSubject('');
    public referenceType = new BehaviorSubject('');
    public p2pLink = new BehaviorSubject('');
    public isEligibleForPromotion = new BehaviorSubject<Boolean>(false);
    public customerCount = new BehaviorSubject('');
    public currentUser = this.loggedInUser.asObservable();
    public currentUserReferrerCode = this.referrerCode.asObservable();
    public currentUserReferenceType = this.referenceType.asObservable();
    public currentp2plink = this.p2pLink.asObservable();
    public currentcustomerCount = this.customerCount.asObservable();
    private authenticated = false;
    private authenticationState = new Subject<any>();
    private messageSource = new BehaviorSubject(false);
    currentMessage = this.messageSource.asObservable();
    changeMessage(val) {
        this.messageSource.next(val)
    }
    constructor(
        private spinnerService: SpinnerService,
        private sessionStorage: SessionStorageService,
        private account: AccountService,
        private roleService: NgxRolesService,
        private rolesService: RolesService
    ) {
    }
    authenticate(identity) {
        this.userIdentity = identity;
        this.authenticated = identity !== null;
        this.authenticationState.next(this.userIdentity);
    }
    hasAnyAuthority(authorities: string[]): Promise<boolean> {
        return Promise.resolve(this.hasAnyAuthorityDirect(authorities));
    }
    hasAnyAuthorityDirect(authorities: string[]): boolean {
        if (!this.authenticated || !this.userIdentity || !this.userIdentity.authorities) {
            return false;
        }
        for (let i = 0; i < authorities.length; i++) {
            if (this.userIdentity.authorities.includes(authorities[i])) {
                return true;
            }
        }
        return false;
    }
    setUserNameForDashboard(userName: string, referralCode: string, referenceType: string, p2pLink: string, customerCount: string, isEligibleForPromotion: Boolean) {
        this.loggedInUser.next(userName);
        this.referrerCode.next(referralCode);
        this.referenceType.next(referenceType)
        this.p2pLink.next(p2pLink);
        this.isEligibleForPromotion.next(isEligibleForPromotion);
        this.customerCount.next(customerCount)
        this.spinnerService.hideLoader();
    }
    hasAuthority(authority: string): Promise<boolean> {
        if (!this.authenticated) {
            return Promise.resolve(false);
        }
        return this.identity().then(
            id => {
                return Promise.resolve(id.authorities && id.authorities.includes(authority));
            },
            () => {
                return Promise.resolve(false);
            }
        );
    }
    identity(force?: boolean): Promise<any> {
        this.roleService.flushRoles();
        this.rolesService.clearRole();
        return this.account
            .get()
            .toPromise()
            .then(response => {
                const account = response.body;
                if (account) {
                    console.log("geting roles");
                    console.log(account);
                    this.loggedInUser.next(account['name'])
                    this.referrerCode.next(account['refCode'])
                    this.referenceType.next(account['refType'])
                    this.p2pLink.next(account['myTeamSize'])
                    this.isEligibleForPromotion.next(account['isEligibleForPromotion']);
                    this.customerCount.next(account['customerCount'])
                    this.userIdentity = account['role'];
                    this.userIdentity.forEach(element => {
                        this.roleService
                            .addRole(element, []);
                        this.rolesService.setRole(element);
                    });
                    this.roleService
                        .addRole(account['permission'], []);
                    this.rolesService.setRole(account['permission']);
                    this.roleService.roles$.subscribe((data) => {
                        this.sessionStorage.store('permissions', JSON.stringify(data));
                    })
                    this.authenticated = true;
                } else {
                    this.userIdentity = null;
                    this.authenticated = false;
                }
                this.authenticationState.next(this.userIdentity);
                return this.userIdentity;
            })
            .catch(err => {
                this.userIdentity = null;
                this.authenticated = false;
                this.authenticationState.next(this.userIdentity);
                return null;
            });
    }
    isAuthenticated(): boolean {
        return this.authenticated;
    }
    isIdentityResolved(): boolean {

        return this.userIdentity !== undefined;
    }
    getAuthenticationState(): Observable<any> {
        return this.authenticationState.asObservable();
    }
    getImageUrl(): string {
        return this.isIdentityResolved() ? this.userIdentity.imageUrl : null;
    }
}
