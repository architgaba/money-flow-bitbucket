import { Injectable } from '@angular/core';
@Injectable({
    providedIn: 'root'
})
export class RolesService {
    private role = [];
    setRole(role: any) {
        this.role.push(role);
        console.log("after setting roles..");
        console.log(this.role);
    }
    clearRole() {
        this.role = [];
    }
    getRole() {
        return this.role;
    }
}