import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { AllCustomersComponent } from './all-customers/all-customers.component';

const routes: Routes = [{
  path: '',
  canActivate: [NgxPermissionsGuard],
  component: AllCustomersComponent,
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AllCustomersRoutingModule { }
