import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllCustomersRoutingModule } from './all-customers-routing.module';
import { AllCustomersComponent } from './all-customers/all-customers.component';
import { TableModule } from 'primeng/table';
import { IconsModule, InputsModule, WavesModule, ModalModule, ButtonsModule, TooltipModule } from 'ng-uikit-pro-standard';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { NgxPermissionsModule } from 'ngx-permissions';
import { MultiSelectModule } from 'primeng/primeng';
import { PipeModule } from 'src/app/core/pipes/pipe.module';

@NgModule({
  declarations: [AllCustomersComponent],
  imports: [
    CommonModule,
    PipeModule, 
    IconsModule,
    TableModule,
    ModalModule,
    WavesModule,
    InputsModule,
    ButtonsModule,
    TooltipModule,
    FormsModule,
    ReactiveFormsModule,
    BreadcrumbModule,
    MultiSelectModule,
    NgxPermissionsModule.forChild(),
    AllCustomersRoutingModule
  ]
})
export class AllCustomersModule { }
