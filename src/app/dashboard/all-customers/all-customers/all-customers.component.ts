import { Component, OnInit, ViewChild } from "@angular/core";
import { AlertService } from "src/app/core/services/alert.service";
import { SpinnerService } from "src/app/core/services/spinner.service";
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { ModalDirective } from "ng-uikit-pro-standard";
import { SelectItem } from "primeng/api";
import { DataTable } from "primeng/primeng";
import { DashboardService } from "../../dashboard.service";
import { element } from "@angular/core/src/render3";
import { ReferrerService } from "../../referrers/view-referrers/referrer.service";
@Component({
  selector: 'app-all-customers',
  templateUrl: './all-customers.component.html',
  styleUrls: ['./all-customers.component.scss']
})
export class AllCustomersComponent implements OnInit {
  @ViewChild("addframe") frame: ModalDirective;
  @ViewChild("dt1") dt: DataTable;
  @ViewChild("dt2") dt2: DataTable;
  @ViewChild('actionFrame') actionFrame: ModalDirective;
  @ViewChild('updateEmail') updateEmail: ModalDirective;
  @ViewChild('deletionFrame') deletionFrame: ModalDirective;

  statusList: SelectItem[] = [];
  showCustomers: any = false;
  userIdToDelete = null;
  selectedId: any;
  selectedReferrerName: any;
  addReferrerForm: FormGroup;
  tableColumns = [
    { field: "name", header: "Customer Name", width: "120px" },
    { field: "email", header: "Email ID", width: "120px" },
    { field: "contactNumber", header: "Phone Number", width: "120px" },
    { field: "status", header: "Account Status", width: "120px" },
    { field: "accountType", header: "Account Type", width: "100px" },
    { field: "userType", header: "User Type", width: "120px" },
    { field: "referrerCode", header: "Sponsor Code", width: "120px" },
    { field: "referrerName", header: "Sponsor Name", width: "120px" },
    { field: "action", header: "Actions", width: "220px" }
  ];


  referrerList = [];
  customerList = [];
  selectedUser: any;
  toggleAction: any;
  currentReferrer = [];
  selectedReferrerId: any;
  accountType: SelectItem[];
  email_id = new FormControl('');
  userId: any;
  showReferralCodeField: boolean;
  constructor(
    private _fb: FormBuilder,
    private alertService: AlertService,
    private spinnerService: SpinnerService,
    private referrerService: ReferrerService,
    private dashboardService: DashboardService
  ) {
  }
  ngOnInit() {
    this.showReferralCodeField = false;
    this.dashboardService.clearMessage();
    this.dashboardService.sendMessage(11);
    this.email_id.setValidators([Validators.required, Validators.email]);
    this.statusList = [
      { label: 'Active', value: 'ACTIVE' },
      { label: 'Inactive', value: 'INACTIVE' },
      { label: 'Archived', value: 'ARCHIVED' },
      { label: 'Activation Pending', value: 'ACTIVATION PENDING' },
      { label: 'Subscription Pending', value: 'SUBSCRIPTION PENDING' },
      { label: 'Subscription Expired', value: 'SUBSCRIPTION EXPIRED' },
      { label: 'Subscription Canceled', value: 'SUBSCRIPTION CANCELLED' }
    ];
    this.accountType = [
      { label: 'Free', value: 'FREE' },
      { label: 'Paid', value: 'PAID' }
    ];
    this.getAllCustomers();
  }
  setUserToDelete(rowData: any) {
    this.userIdToDelete = rowData.userId;
    this.selectedUser = rowData
  }
  deleteUser() {
    this.spinnerService.showLoader();
    this.referrerService.deleteReferrers(this.userIdToDelete).subscribe(res => {
      this.spinnerService.hideLoader();
      this.alertService.clearMessage();
      this.alertService.sendMessage(res.message, 'success');
    }, (err) => {
      this.alertService.clearMessage();
      this.alertService.sendMessage(err.message, 'error');
    }, () => {
      this.deletionFrame.hide();
      this.userIdToDelete = null;
      this.getAllCustomers();
      this.spinnerService.hideLoader();
    });
  }
  updateReferrerEmail() {
    if (this.email_id.valid) {
      this.spinnerService.showLoader();
      const body = {
        "emailId": this.email_id.value,
        "userId": this.selectedReferrerId
      };
      this.referrerService.updateReferrerEmail(body).subscribe(res => {
        this.alertService.clearMessage();
        this.alertService.sendMessage(res.message, "success");
      }, () => {
        this.email_id = new FormControl('');
        this.updateEmail.hide();
        this.getAllCustomers();
        this.spinnerService.hideLoader();
      });
    }
    else {
      this.email_id.markAsDirty();
    }
  }
  showUserDetails(data: any, action: any) {
    this.selectedUser = data;
    this.toggleAction = action;
    this.selectedId = data.userId;
  }
  resendMail(rowData: any) {
    this.spinnerService.showLoader();
    this.referrerService.resendMail(rowData.userId).subscribe(
      res => {
        console.log(res);
        this.alertService.clearMessage();
        this.alertService.sendMessage(res.message, "success");
      },
      err => { },
      () => {
        this.spinnerService.hideLoader();
      }
    );
  }
  updateReferrerAccount() {
    this.spinnerService.showLoader();
    this.referrerService.updateReferrerAccount(this.toggleAction, this.selectedId).subscribe(res => {
      console.log(res);
    }, (err) => {
    }, () => {
      this.getAllCustomers();
      this.spinnerService.hideLoader();
      this.actionFrame.hide();
    });
  }
  getAllCustomers() {
    this.referrerList = [];
    this.spinnerService.showLoader();
    this.referrerService.getAllCustomers().subscribe(
      res => {
        console.log(res);
        res.forEach(element => {
          this.referrerList.push(element);
        });
      },
      err => { },
      () => {
        this.dt.reset();
        this.spinnerService.hideLoader();
      }
    );
  }

}







