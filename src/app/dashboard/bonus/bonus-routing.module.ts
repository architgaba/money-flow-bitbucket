import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BonusViewAdminComponent } from './bonus-view-admin/bonus-view-admin.component';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { BonusViewCustomerRepComponent } from './bonus-view-customer-rep/bonus-view-customer-rep.component';

const routes: Routes = [  {
  path: 'admin',
  component: BonusViewAdminComponent,
  canActivate: [NgxPermissionsGuard]
}, {
  path: 'view',
  component: BonusViewCustomerRepComponent,
  canActivate: [NgxPermissionsGuard]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BonusRoutingModule { }
