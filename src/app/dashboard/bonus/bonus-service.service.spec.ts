import { TestBed } from '@angular/core/testing';

import { BonusServiceService } from './bonus-service.service';

describe('BonusServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BonusServiceService = TestBed.get(BonusServiceService);
    expect(service).toBeTruthy();
  });
});
