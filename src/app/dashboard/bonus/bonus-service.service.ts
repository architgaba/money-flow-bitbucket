import { Injectable } from '@angular/core';
import { ApiCommonService } from 'src/app/core/services/api-common.service';

@Injectable({
  providedIn: 'root'
})
export class BonusServiceService {
  getBonusPayoutsForCustomerReps(fromDate,toDate) {
      
    return this.apiCommonService.get('/admin/my-bonus'+'?fromDate=' + fromDate + '&toDate=' + toDate);
  }
  getBonusPayouts(fromDate,toDate) {
    
    return this.apiCommonService.get('/admin/bonus-list'+'?fromDate=' + fromDate + '&toDate=' + toDate);
  }

  constructor(private apiCommonService: ApiCommonService) { }
}
