import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BonusViewAdminComponent } from './bonus-view-admin.component';

describe('BonusViewAdminComponent', () => {
  let component: BonusViewAdminComponent;
  let fixture: ComponentFixture<BonusViewAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BonusViewAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BonusViewAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
