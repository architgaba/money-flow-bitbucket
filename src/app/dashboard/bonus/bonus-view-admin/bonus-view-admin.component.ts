import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTable, SelectItem } from 'primeng/primeng';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { DashboardService } from '../../dashboard.service';
import { BonusServiceService } from '../bonus-service.service';
import { DateFormatterService } from 'src/app/core/services/date-formatter.service';
import * as moment from 'moment';
import { Principal } from 'src/app/core/services/principal.service';
@Component({
  selector: 'app-bonus-view-admin',
  templateUrl: './bonus-view-admin.component.html',
  styleUrls: ['./bonus-view-admin.component.scss']
})
export class BonusViewAdminComponent implements OnInit {
  values=[]
  showCustomers: boolean = false;
  @ViewChild("dt1") dt1: DataTable;
  selectedAAP=''
  totalBonus=0;
  subTotal=0;
  customerCount:any
  namesFilter: SelectItem[] = [];
  namesFilterBreakupList: SelectItem[] = [];
  tableColumns = [
    { field: "name", header: "Sponsor Name", width: "120px" },
    { field: "customerCount", header: "Total Customers", width: "120px" },
    { field: "bonuseAmount", header: "Total Amount", width: "120px" },
    { field: "actions", header: "Actions", width: "120px" },
  ];
  tableColumnsBonusBreakup = [
    { field: "bonusEarnedFrom", header: "Bonus Earned From", width: "120px" },
    { field: "disbursementDate", header: "Transaction Date", width: "100px" },
    { field: "bonuseAmount", header: "Total Amount", width: "120px" }
  ];
  bonusList = [];
  bonusBreakup=[];
  totalBonusCopy: number;
  subTotalBonusCopy:number;
  constructor(
    private spinnerService: SpinnerService,
    private bonusService:BonusServiceService ,
    private dashboardService: DashboardService,
    private dateFormatter: DateFormatterService,
    private principal:Principal
  ) {
    this.select(new Date())
  }
  ngOnInit() {
    this.dashboardService.clearMessage();
    this.dashboardService.sendMessage(12);
    this.principal.customerCount.subscribe(message => { this.customerCount = message });
    this.getBonusPayouts();
  }
  select(evt) {
    this.values[0]= new Date(moment(evt).startOf("isoWeek").toDate())    
    this.values[1]= new Date(moment(evt).endOf("isoWeek").toDate())
    console.log(this.values)
    console.log(new Date())
  }

  calculateTotal(event){
    console.log(event);
    this.totalBonus=0;
    event.forEach(el1=>{
      this.bonusList.forEach(el2=>{
        if(el2.name===el1){
          this.totalBonus=this.totalBonus+ (+el2.bonuseAmount)
        }
      })
    })
    if(this.totalBonus==0)
      this.totalBonus=this.totalBonusCopy
  }
  calculateSubTotal(event){
    console.log(event);
    this.subTotal=0;
    event.forEach(el1=>{
      this.bonusBreakup.forEach(el2=>{
        if(el2.bonusEarnedFrom===el1){
          this.subTotal=this.subTotal+ (+el2.bonuseAmount)
        }
      })
    })
    if(+this.subTotal==0)
      this.subTotal=this.subTotalBonusCopy
  }
  getBonusPayouts(){
    var fromDate = this.dateFormatter.getFormattedDate(this.values[0]);
    var toDate = this.dateFormatter.getFormattedDate(this.values[1]);
    this.bonusList = [];
    this.namesFilter=[];
    this.totalBonus=0;
    this.totalBonusCopy=0;
    this.spinnerService.showLoader();
    this.bonusService.getBonusPayouts(fromDate,toDate).subscribe(
      res => {
        console.log(res);
        res.forEach((element,index) => {
          this.bonusList.push(element);
          this.totalBonus = this.totalBonus + +element.bonuseAmount
          this.totalBonusCopy=this.totalBonus;
          if(!this.namesFilter.some(x => x.value ===  this.bonusList[index].name)) {
                this.namesFilter.push({label:this.bonusList[index].name,value:this.bonusList[index].name})
          }
        });
      },
      err => { },
      () => {
        this.dt1.reset();
        this.spinnerService.hideLoader();
      }
    );
  }
  getBonusBreakup(row){
    this.bonusBreakup=[]
    this.subTotal=0;
    this.selectedAAP=''
    this.selectedAAP=row.name
    this.bonusBreakup=row.bonusBreakDownVoList
    this.bonusBreakup.forEach((element,index)=>{
      this.subTotal=this.subTotal+element.bonuseAmount
      this.subTotalBonusCopy=this.subTotal;
      if(!this.namesFilterBreakupList.some(x => x.value ===  this.bonusBreakup[index].bonusEarnedFrom)) {
        this.namesFilterBreakupList.push({label:this.bonusBreakup[index].bonusEarnedFrom,value:this.bonusBreakup[index].bonusEarnedFrom})
  }
    })


  
  }
  }

