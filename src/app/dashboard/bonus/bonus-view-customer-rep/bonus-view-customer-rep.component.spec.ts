import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BonusViewCustomerRepComponent } from './bonus-view-customer-rep.component';

describe('BonusViewCustomerRepComponent', () => {
  let component: BonusViewCustomerRepComponent;
  let fixture: ComponentFixture<BonusViewCustomerRepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BonusViewCustomerRepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BonusViewCustomerRepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
