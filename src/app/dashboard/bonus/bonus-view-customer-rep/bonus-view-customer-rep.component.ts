import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTable } from 'primeng/primeng';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { BonusServiceService } from '../bonus-service.service';
import { DashboardService } from '../../dashboard.service';
import { DateFormatterService } from 'src/app/core/services/date-formatter.service';
import * as moment from 'moment';
import { Principal } from 'src/app/core/services/principal.service';
@Component({
  selector: 'app-bonus-view-customer-rep',
  templateUrl: './bonus-view-customer-rep.component.html',
  styleUrls: ['./bonus-view-customer-rep.component.scss']
})
export class BonusViewCustomerRepComponent implements OnInit {
  @ViewChild("dt1") dt: DataTable;
  totalBonus=0;
  values=[]
  customerCount:any
  tableColumns = [
    { field: "name", header: "Sponsor Name", width: "120px" },
    { field: "bonusEarnedFrom", header: "Bonus Earned From", width: "120px" },
    { field: "disbursementDate", header: "Transaction Date", width: "100px" },
    { field: "bonuseAmount", header: "Amount", width: "120px" },
  ];
  bonusList = [];
  constructor(
    private spinnerService: SpinnerService,
    private bonusService:BonusServiceService ,
    private dashboardService: DashboardService,
    private dateFormatter: DateFormatterService,
    private principal:Principal
  ) {
    this.select(new Date())
  }
  ngOnInit() {
    this.dashboardService.clearMessage();
    this.dashboardService.sendMessage(12);
    this.principal.customerCount.subscribe(message => { this.customerCount = message });
    this.getBonusPayouts();
  }
  select(evt) {
    // let start = new Date(evt);
    // start.setDate(start.getDate() - start.getDay()+1);
    // this.values[0] = start;
    // let end = new Date(start);
    // end.setDate(start.getDate() + 6); 
    // this.values[1] = end;
    this.values[0]= new Date(moment(evt).startOf("isoWeek").toDate()) 
    
    this.values[1]= new Date(moment(evt).endOf("isoWeek").toDate())
    console.log(this.values)
    console.log(new Date())
  }
  getBonusPayouts(){
    var fromDate = this.dateFormatter.getFormattedDate(this.values[0]);
    var toDate = this.dateFormatter.getFormattedDate(this.values[1]);
    this.bonusList = [];
    this.totalBonus=0;
    this.spinnerService.showLoader();
    this.bonusService.getBonusPayoutsForCustomerReps(fromDate,toDate).subscribe(
      res => {
        console.log(res);
        res.forEach((element,index) => {
          this.totalBonus = this.totalBonus + element.bonuseAmount
          this.bonusList.push(element);
          this.bonusList[index].firstName=(element.firstName?element.firstName:" ")+" "+(element.lastName?element.lastName:" ");
        });
      },
      err => { },
      () => {
        this.dt.reset();
        this.spinnerService.hideLoader();
      }
    );
  }

}
