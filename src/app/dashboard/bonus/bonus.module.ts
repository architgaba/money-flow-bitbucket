import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BonusRoutingModule } from './bonus-routing.module';
import { BonusViewAdminComponent } from './bonus-view-admin/bonus-view-admin.component';
import { BonusViewCustomerRepComponent } from './bonus-view-customer-rep/bonus-view-customer-rep.component';
import { PipeModule } from 'src/app/core/pipes/pipe.module';
import { IconsModule, ModalModule, WavesModule, InputsModule, ButtonsModule, TooltipModule, BreadcrumbModule } from 'ng-uikit-pro-standard';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MultiSelectModule } from 'primeng/primeng';
import { NgxPermissionsModule } from 'ngx-permissions';
import { TableModule } from 'primeng/table';
import {CalendarModule} from 'primeng/calendar';
@NgModule({
  declarations: [BonusViewAdminComponent, BonusViewCustomerRepComponent],
  imports: [
    CommonModule,
    PipeModule, 
    IconsModule,
    TableModule,
    ModalModule,
    WavesModule,
    InputsModule,
    ButtonsModule,
    TooltipModule,
    FormsModule,
    ReactiveFormsModule,
    CalendarModule,
    BreadcrumbModule,
    MultiSelectModule,
    NgxPermissionsModule.forChild(),
    BonusRoutingModule
  ]
})
export class BonusModule { }
