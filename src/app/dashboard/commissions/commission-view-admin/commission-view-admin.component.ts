import { Component, OnInit, ViewChild } from '@angular/core';
import { CommissionViewService } from './../commission-view.service';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { AlertService } from 'src/app/core/services/alert.service';
import { SelectItem } from 'primeng/api';
import { DashboardService } from '../../dashboard.service';
import { DataTable } from 'primeng/primeng';
import { Router } from '@angular/router';
@Component({
  selector: 'app-commission-view-admin',
  templateUrl: './commission-view-admin.component.html',
  styleUrls: ['./commission-view-admin.component.scss']
})
export class CommissionViewAdminComponent implements OnInit {
  monthName: string;
  selectedYear: string;
  showCustomers: boolean = false;
  yearList: SelectItem[];
  monthList: any[];
  @ViewChild("dt6") dt1: DataTable;
  @ViewChild("dt2") dt2: DataTable;
  constructor(private commissionViewService: CommissionViewService,
    private spinnerService: SpinnerService,
    private alertService: AlertService,
    private dashboardService: DashboardService, private router: Router) { }
  monthNameList = [
    { field: 'january', header: 'January' },
    { field: 'february', header: 'February' },
    { field: 'march', header: 'March' },
    { field: 'april', header: 'April' },
    { field: 'may', header: 'May' },
    { field: 'june', header: 'June' },
    { field: 'july', header: 'July' },
    { field: 'august', header: 'August' },
    { field: 'september', header: 'September' },
    { field: 'october', header: 'October' },
    { field: 'november', header: 'November' },
    { field: 'december', header: 'December' },
  ]
  commissionReportList = [];
  customerList = [];
  tableColumns = [
    { field: "referrerName", header: "Name" },
    { field: "userType", header: "User Type" },
    { field: "monthlyCustomerCount", header: "Total Customers with Monthly Subscription" },
    { field: "yearlyCustomerCount", header: "Total Customers with Yearly Subscription" },
    { field: "startPeriod", header: "Start Period" },
    { field: "endPeriod", header: "End Period" },
    { field: "commissionFromLevel1", header: "Commission Earned From Level I" },
    { field: "commissionFromLevel2", header: "Commission Earned From Level II" },
    { field: "totalCommissions", header: "Total Commission Earned" },
    { field: "action", header: "View Commission Details" }
  ];
  customerTableColumns = [
    { field: "name", header: "Customer Name" },
    { field: "userType", header: "User Type" },
    { field: "startPeriod", header: "Start Period" },
    { field: "finishPeriod", header: "Finish Period" },
    { field: "planType", header: "Subscription Plan Type" },
    { field: "commissionFromLevel1", header: "Commission Earned From Level I" },
    { field: "commissionFromLevel2", header: "Commission Earned From Level II" },
    { field: "amount", header: "Total Commission Earned" }
  ];
  ngOnInit() {
    this.dashboardService.clearMessage();
    this.dashboardService.sendMessage(6);
    this.getCommissionRecordList();
    this.yearList = [];
    this.monthList = [];
    this.getYearList();
    this.monthNameList.forEach(element => {
      this.monthList.push({ label: element.header, value: element.field });
    });
  }
  getYearList() {
    const monthNames = ["january", "february", "march", "april", "may", "june",
      "july", "august", "september", "october", "november", "december"
    ];
    let date = new Date,
      year = date.getFullYear();
    this.yearList.push({ label: (year - 2).toString(), value: (year - 2).toString() });
    this.yearList.push({ label: (year - 1).toString(), value: (year - 1).toString() });
    for (let i = year; i < year + 5; i++) {
      this.yearList.push({ label: i.toString(), value: i.toString() });
    }
    this.selectedYear = year.toString();
    this.monthName = monthNames[date.getMonth()];
  }
  checkMonth(monthName: any) {
    if (monthName == 'january') {
      return 1;
    } else if (monthName == 'february') {
      return 2;
    } else if (monthName == 'march') {
      return 3;
    } else if (monthName == 'april') {
      return 4;
    } else if (monthName == 'may') {
      return 5;
    } else if (monthName == 'june') {
      return 6;
    } else if (monthName == 'july') {
      return 7;
    } else if (monthName == 'august') {
      return 8;
    } else if (monthName == 'september') {
      return 9;
    } else if (monthName == 'october') {
      return 10;
    } else if (monthName == 'november') {
      return 11;
    } else if (monthName == 'december') {
      return 12;
    }
  }
  level1CommissionTotal = 0
  level2CommissionTotal = 0
  level1CommissionTotalCustomerReport = 0
  level2CommissionTotalCustomerReport = 0
  totalCommission = 0
  totalCommissionCustomerRecord = 0
  getCommissionReport() {
    if (this.monthName != undefined && this.selectedYear != undefined) {
      this.level1CommissionTotal = 0
      this.level2CommissionTotal = 0
      this.totalCommission = 0
      this.spinnerService.showLoader();
      let monthValue = this.checkMonth(this.monthName);
      var selectedYear = this.selectedYear;
      if (monthValue == 1) {
        monthValue = 13;
        selectedYear = (+selectedYear - 1).toString();
      }
      this.commissionViewService.getCommissionReport(monthValue, selectedYear).subscribe(res => {
        this.commissionReportList = res;
        this.commissionReportList.forEach((element,index) => {
          this.level1CommissionTotal = this.level1CommissionTotal + element.commissionFromLevel1
          this.level2CommissionTotal = this.level2CommissionTotal + element.commissionFromLevel2
          this.totalCommission = this.totalCommission + element.totalCommissions
          if(element.userType==='Customer Rep'&&!element.p2PYEA){
            this.commissionReportList[index].userType='App Ambassador'
          }
          if(element.userType==='Customer Rep'&&element.p2PYEA){
            this.commissionReportList[index].userType='P2P YEA'
          }
          if(element.userType==='P2P Distributor'){
            this.commissionReportList[index].userType='Legacy Member'
          }
        })
        console.log(res);
        console.log(this.level1CommissionTotal);
        console.log(this.level2CommissionTotal);
        console.log(this.totalCommission)
        console.log(res);
      }, err => {
      }, () => {
        this.spinnerService.hideLoader();
        this.dt1.reset();
      })
    }
    else {
      this.alertService.clearMessage();
      this.alertService.sendMessage("Please select a month and an year for viewing commission report.", "error");
    }
  }
  getCommissionRecordList() {
  }
  getCustomerCommissionReport(data: any) {
    this.customerList = [];
    if (this.monthName != undefined && this.selectedYear != undefined) {
      this.spinnerService.showLoader();
      this.totalCommissionCustomerRecord = 0;
      this.level1CommissionTotalCustomerReport = 0;
      this.level2CommissionTotalCustomerReport = 0;
      let monthValue = this.checkMonth(this.monthName);
      var selectedYear = this.selectedYear;
      if (monthValue == 1) {
        monthValue = 13;
        selectedYear = (+selectedYear - 1).toString();
      }
      this.commissionViewService.getCustomerCommissionReportByReferrer(data.commissionRecordId, monthValue, selectedYear).subscribe(
        res => {
          res.forEach((element,index) => {
            this.level1CommissionTotalCustomerReport = this.level1CommissionTotalCustomerReport + element.commissionFromLevel1
            this.level2CommissionTotalCustomerReport = this.level2CommissionTotalCustomerReport + element.commissionFromLevel2
            this.totalCommissionCustomerRecord = this.totalCommissionCustomerRecord + element.amount
            this.customerList.push(element);
            if(element.userType==='Customer Rep'&&!element.p2PYEA){
              this.customerList[index].userType='App Ambassador'
            }
            if(element.userType==='Customer Rep'&&element.p2PYEA){
              this.customerList[index].userType='P2P YEA'
            }
            if(element.userType==='P2P Distributor'){
              this.customerList[index].userType='Legacy Member'
            }
            if(element.planType==='Yearly'){
              this.customerList[index].userType='AAP'
            }
          });
          this.spinnerService.hideLoader();
        }, err => {
        }, () => {
          this.spinnerService.hideLoader();
          this.dt2.reset();
        });
    }
    else {
      this.alertService.clearMessage();
      this.alertService.sendMessage("Please select a month and an year for viewing commission report.", "error");
    }
  }
}

