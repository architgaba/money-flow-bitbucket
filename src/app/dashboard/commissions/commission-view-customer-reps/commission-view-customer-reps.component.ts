import { Component, OnInit } from '@angular/core';
import { CommissionViewService } from './../commission-view.service';
import { AlertService } from 'src/app/core/services/alert.service';
import { DashboardService } from '../../dashboard.service';
import { SelectItem } from 'primeng/api';
import { months } from 'moment';
@Component({
  selector: 'app-commission-view-customer-reps',
  templateUrl: './commission-view-customer-reps.component.html',
  styleUrls: ['./commission-view-customer-reps.component.scss']
})
export class CommissionViewCustomerRepsComponent implements OnInit {
  monthName: string;
  selectedYear: string;
  monthList: any[];
  yearList: SelectItem[];
  level1CommissionTotalCustomerReport = 0
  level2CommissionTotalCustomerReport = 0
  monthNameList = [
    { field: 'january', header: 'January' },
    { field: 'february', header: 'February' },
    { field: 'march', header: 'March' },
    { field: 'april', header: 'April' },
    { field: 'may', header: 'May' },
    { field: 'june', header: 'June' },
    { field: 'july', header: 'July' },
    { field: 'august', header: 'August' },
    { field: 'september', header: 'September' },
    { field: 'october', header: 'October' },
    { field: 'november', header: 'November' },
    { field: 'december', header: 'December' },
  ]
  constructor(private commissionViewService: CommissionViewService,
    private alertService: AlertService,
    private dashboardService: DashboardService) { }
  commissionReportList = [];
  tableColumns = [
    { field: "name", header: "Customer Name" },
    { field: "userType", header: "User Type" },
    { field: "startPeriod", header: "Start Period" },
    { field: "finishPeriod", header: "Finish Period" },
    { field: "planType", header: "Subscription Plan Type" },
    { field: "commissionFromLevel1", header: "Commission Earned From Level I" },
    { field: "commissionFromLevel2", header: "Commission Earned From Level II" },
    { field: "amount", header: "Total Commission Earned" }
  ];
  ngOnInit() {
    this.yearList = [];
    this.monthList = [];
    this.getYearList();
    this.dashboardService.clearMessage();
    this.dashboardService.sendMessage(6);
    this.monthNameList.forEach(element => {
      this.monthList.push({ label: element.header, value: element.field });
    });
  }
  checkMonth(monthName: any) {
    if (monthName == 'january') {
      return 1;
    } else if (monthName == 'february') {
      return 2;
    } else if (monthName == 'march') {
      return 3;
    } else if (monthName == 'april') {
      return 4;
    } else if (monthName == 'may') {
      return 5;
    } else if (monthName == 'june') {
      return 6;
    } else if (monthName == 'july') {
      return 7;
    } else if (monthName == 'august') {
      return 8;
    } else if (monthName == 'september') {
      return 9;
    } else if (monthName == 'october') {
      return 10;
    } else if (monthName == 'november') {
      return 11;
    } else if (monthName == 'december') {
      return 12;
    }
  }
  totalCommissionCustomerRecord = 0
  getCommissionReport() {
    if (this.monthName != undefined && this.selectedYear != undefined) {
      this.level1CommissionTotalCustomerReport=0;
      this.level2CommissionTotalCustomerReport=0;
      this.totalCommissionCustomerRecord = 0;
      let monthValue = this.checkMonth(this.monthName);
      this.commissionViewService.getCommissionReport(monthValue, this.selectedYear).subscribe(res => {
        this.commissionReportList = res;
        this.commissionReportList.forEach((element,index) => {
          if(element.userType==='Customer Rep'&&!element.p2PYEA){
            this.commissionReportList[index].userType="App Ambassador"
          }
          if(element.userType==='P2P Distributor'){
            this.commissionReportList[index].userType="Legacy Member"
          }
          if(element.userType==='Customer Rep'&&element.p2PYEA){
            this.commissionReportList[index].userType="P2P YEA"
          }
          this.level1CommissionTotalCustomerReport = this.level1CommissionTotalCustomerReport + element.commissionFromLevel1
          this.level2CommissionTotalCustomerReport = this.level2CommissionTotalCustomerReport + element.commissionFromLevel2
          this.totalCommissionCustomerRecord = this.totalCommissionCustomerRecord + element.amount
        })
      }, err => {
      }, () => {
      })
    }
    else {
      this.alertService.clearMessage();
      this.alertService.sendMessage("Please select a month and an year for viewing commission report.", "error");
    }
  }
  getYearList() {
    const monthNames = ["january", "february", "march", "april", "may", "june",
  "july", "august", "september", "october", "november", "december"
  ];
    let date = new Date,
    year = date.getFullYear();
    this.yearList.push({ label: (year - 2).toString(), value: (year - 2).toString() });
    this.yearList.push({ label: (year - 1).toString(), value: (year - 1).toString() });
    for (let i = year; i < year + 5; i++) {
      this.yearList.push({ label: i.toString(), value: i.toString() });
    }
    this.selectedYear = year.toString();
    this.monthName=monthNames[date.getMonth()];
  }
}
