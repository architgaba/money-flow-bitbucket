import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiCommonService } from 'src/app/core/services/api-common.service';
@Injectable({
  providedIn: 'root'
})
export class CommissionViewService {
  constructor(private apiCommonService: ApiCommonService) { }
  getCommissionReport(month: any, year: any): Observable<any> {
    console.log('/commission/fetch/report/?month=' + month + '&year=' + year);
    return this.apiCommonService.get('/commission/fetch/report/?month=' + month + '&year=' + year);
  }
  getCustomerCommissionReportByReferrer(data: any, month: any, year: any): Observable<any> {
    return this.apiCommonService.get('/commission/report/customer-reps/' + +data + '?month=' + month + '&year=' + year);
  }
}
