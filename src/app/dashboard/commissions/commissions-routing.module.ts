import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommissionViewAdminComponent } from './commission-view-admin/commission-view-admin.component';
import { CommissionViewCustomerRepsComponent } from './commission-view-customer-reps/commission-view-customer-reps.component';
import { NgxPermissionsGuard } from 'ngx-permissions';
const routes: Routes = [
  {
    path: 'admin',
    component: CommissionViewAdminComponent,
    canActivate: [NgxPermissionsGuard]
  }, {
    path: 'app-ambassadors',
    component: CommissionViewCustomerRepsComponent,
    canActivate: [NgxPermissionsGuard],
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommissionsRoutingModule { }
