import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableModule } from 'primeng/table';
import { MultiSelectModule } from 'primeng/primeng';
import { CommissionsRoutingModule } from './commissions-routing.module';
import { CommissionViewAdminComponent } from './commission-view-admin/commission-view-admin.component';
import { CommissionViewCustomerRepsComponent } from './commission-view-customer-reps/commission-view-customer-reps.component';
import { IconsModule, ButtonsModule, InputsModule, TooltipModule } from 'ng-uikit-pro-standard';
import { Ng2ImgMaxModule } from 'ng2-img-max';
import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';
import { PipeModule } from '../../core/pipes/pipe.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    CommissionViewAdminComponent,
    CommissionViewCustomerRepsComponent
  ],
  imports: [
    CommonModule,
    TableModule,
    MultiSelectModule,
    CommissionsRoutingModule,
    IconsModule,
    ButtonsModule,
    InputsModule,
    TooltipModule,
    Ng2ImgMaxModule,
    CalendarModule,
    PipeModule,
    DropdownModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class CommissionsModule { }
