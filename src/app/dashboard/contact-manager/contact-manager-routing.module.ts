import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactManagerComponent } from './contact-manager/contact-manager.component';
import { NgxPermissionsGuard } from 'ngx-permissions';
const routes: Routes = [{
  path: '',
  canActivate: [NgxPermissionsGuard],
  component: ContactManagerComponent
}];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactManagerRoutingModule { }
