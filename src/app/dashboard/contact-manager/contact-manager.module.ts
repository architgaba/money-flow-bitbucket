import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactManagerRoutingModule } from './contact-manager-routing.module';
import { ContactManagerComponent } from './contact-manager/contact-manager.component';
import { TableModule } from 'primeng/table';
import { IconsModule, ButtonsModule, InputsModule, WavesModule, ModalModule, TooltipModule } from 'ng-uikit-pro-standard';
import { NgxPermissionsModule } from 'ngx-permissions';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MultiSelectModule } from 'primeng/primeng';
import { PipeModule } from 'src/app/core/pipes/pipe.module';
@NgModule({
  declarations: [ContactManagerComponent],
  imports: [
    CommonModule,
    ContactManagerRoutingModule,
    CommonModule,
    IconsModule,
    TableModule,
    ModalModule,
    WavesModule,
    InputsModule,
    ButtonsModule,
    FormsModule,
    ReactiveFormsModule,
    TooltipModule,
    MultiSelectModule,
    PipeModule,
    NgxPermissionsModule.forChild()
  ]
})
export class ContactManagerModule { }
