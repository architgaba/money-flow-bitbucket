
import { Component, OnInit, ViewChild } from "@angular/core";
import { SpinnerService } from "src/app/core/services/spinner.service";
import { AlertService } from "src/app/core/services/alert.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ModalDirective } from "ng-uikit-pro-standard";
import { DataTable } from "primeng/primeng";
import { ContactManagerService } from "./contact-manager.service";
import { RolesService } from "src/app/core/services/roles.service";
import { DashboardService } from "../../dashboard.service";
@Component({
  selector: 'app-contact-manager',
  templateUrl: './contact-manager.component.html',
  styleUrls: ['./contact-manager.component.scss']
})
export class ContactManagerComponent implements OnInit {
  @ViewChild("frame") frame: ModalDirective;
  @ViewChild("profileFrame") profileframe: ModalDirective;
  @ViewChild("dt") dt: DataTable;
  @ViewChild('actionFrame') actionFrame: ModalDirective;
  isAdmin: any;
  contactId: any;
  contactList = [];
  contactManagerForm: FormGroup;
  tableColumns = [
    { field: "contactType", header: "Contact Type" },
    { field: "name", header: "Contact Name" },
    { field: "contactNumber", header: "Phone Number" },
    { field: "emailAddress", header: "Email ID" },
    { field: "action", header: "Action" }
  ];
  selectedUser: any;
  toggleAction: any;
  selectedCustomerId: any;
  constructor(
    private _fb: FormBuilder,
    private alertService: AlertService,
    private spinnerService: SpinnerService,
    private contactManagerService: ContactManagerService,
    private rolesService: RolesService,
    private dashboardService: DashboardService
  ) {
    var role = this.rolesService.getRole();
    var index = role.indexOf('ROLE_SUPER_ADMIN');
    (index < 0) ? this.isAdmin = false : this.isAdmin = true;
  }
  ngOnInit() {
    this.dashboardService.clearMessage();
    this.dashboardService.sendMessage(10);
    this.contactManagerForm = this._fb.group({
      contactId: [""],
      contactNumber: ["", [Validators.pattern(/^\(\d{3}\)\s\d{3}-\d{4}$/)]],
      contactType: [""],
      emailAddress: ["", [Validators.required, Validators.email]],
      name: ["", [Validators.required]],
      status: [""]
    })
    this.getContactList();
  }
  getContactList() {
    this.contactList = [];
    this.spinnerService.showLoader();
    this.contactManagerService.getContectList(this.isAdmin).subscribe(
      res => {
        console.log(res);
        res.forEach(element => {
          this.contactList.push(element);
        });
        this.spinnerService.hideLoader();
      },
      err => { },
      () => {
        this.dt.reset();
        this.spinnerService.hideLoader();
      });
  }
  createNewContact() {
    if (this.contactManagerForm.valid) {
      const body = this.contactManagerForm.value;
      this.spinnerService.showLoader();
      this.contactManagerService.createNewContact(body, this.isAdmin).subscribe(
        res => {
          console.log(res);
          this.alertService.clearMessage();
          this.alertService.sendMessage(res.message, 'success');
        }, err => {

        }, () => {
          this.spinnerService.hideLoader();
          this.getContactList();
          this.frame.hide();
        })
    } else {
      Object.keys(this.contactManagerForm.controls).forEach(key => {
        this.contactManagerForm.get(key).markAsDirty();
      });
    }
  }
  updateExistsContact() {
    if (this.contactManagerForm.valid) {
      this.spinnerService.showLoader();
      const body = this.contactManagerForm.value;
      const contectId = this.contactManagerForm.controls.contactId.value;
      this.contactManagerService.updateExistsContactDetails(contectId, this.isAdmin, body).subscribe(res => {
        console.log(res);
        this.alertService.clearMessage();
        this.alertService.sendMessage(res.message, 'success');
      }, err => {
      }, () => {
        this.spinnerService.hideLoader();
        this.getContactList();
        this.frame.hide();
      });
    } else {
      Object.keys(this.contactManagerForm.controls).forEach(key => {
        this.contactManagerForm.get(key).markAsDirty();
      });
    }
  }
  updateContactByAction() {
    this.spinnerService.showLoader();
    if (this.toggleAction == 'delete') {
      this.contactManagerService.deleteContact(this.contactId, this.isAdmin).subscribe(
        res => {
          console.log(res);
          this.alertService.clearMessage();
          this.alertService.sendMessage(res.message, 'success');
        }, err => {
        }, () => {
          this.spinnerService.hideLoader();
          this.getContactList();
          this.actionFrame.hide();
        })
    } else if (this.toggleAction !== 'delete') {
    }
  }
  deleteContactDetails() {
    this.spinnerService.showLoader();
    const contactId = 5;
    this.contactManagerService.deleteContact(contactId, this.isAdmin).subscribe(
      res => {
        console.log(res);
        this.alertService.clearMessage();
        this.alertService.sendMessage(res.message, 'success');
      }, err => {
      }, () => {
        this.spinnerService.hideLoader();
        this.getContactList();
        this.frame.hide();
      })
  }
  addContactDetails() {
    this.contactManagerForm.reset();
  }
  setContatactDetails(data: any) {
    this.contactManagerForm.controls.contactId.setValue(data.contactId);
    this.contactManagerForm.controls.contactType.setValue(data.contactType);
    this.contactManagerForm.controls.contactNumber.setValue(data.contactNumber, [Validators.required, Validators.pattern(/^\(\d{3}\)\s\d{3}-\d{4}$/)]);
    this.contactManagerForm.controls.name.setValue(data.name, Validators.required);
    this.contactManagerForm.controls.emailAddress.setValue(data.emailAddress, [Validators.required, Validators.email]);
  }
  showContactDetails(data: any, action: any) {
    console.log(data);
    console.log(action);
    this.selectedUser = data;
    this.toggleAction = action;
    this.contactId = data.contactId;
  }
}
