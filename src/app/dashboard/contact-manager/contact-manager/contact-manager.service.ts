import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { ApiCommonService } from "src/app/core/services/api-common.service";
@Injectable({
  providedIn: 'root'
})
export class ContactManagerService {
  constructor(private apiCommonService: ApiCommonService) { }
  getContectList(isAdmin: any): Observable<any> {
    if (!isAdmin) {
      return this.apiCommonService.get("/money-flow/contact-manager/details");
    }
  }
  createNewContact(body: any, isAdmin: any): Observable<any> {
    if (!isAdmin) {
      return this.apiCommonService.post("/money-flow/contact-manager/create", body);
    }
  }
  deleteContact(id: any, isAdmin: any): Observable<any> {
    if (!isAdmin) {
      return this.apiCommonService.delete("/money-flow/contact-manager/delete/" + id);
    }
  }
  updateExistsContactDetails(id: any, isAdmin: any, body: any): Observable<any> {
    if (!isAdmin) {
      return this.apiCommonService.put('/money-flow/contact-manager/update/' + +id +'/', body);
    }
  }
  updateExistsContactDetailsByAction(action: any, id: any, isAdmin: any): Observable<any> {
    if (!isAdmin) {
      return this.apiCommonService.put('/money-flow/contact-manager/update/' + +id + '/?status=/' + action, {});
    }
  }
}
