import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewCustomersComponent } from './view-customers/view-customers.component';
import { NgxPermissionsGuard } from 'ngx-permissions';
const routes: Routes = [{
  path: '',
  canActivate: [NgxPermissionsGuard],
  component: ViewCustomersComponent,
}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule { }
