import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerRoutingModule } from './customer-routing.module';
import { ViewCustomersComponent } from './view-customers/view-customers.component';
import { TableModule } from 'primeng/table';
import { IconsModule, ButtonsModule, InputsModule, WavesModule, ModalModule, TooltipModule } from 'ng-uikit-pro-standard';
import { NgxPermissionsModule } from 'ngx-permissions';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MultiSelectModule } from 'primeng/primeng';
import { PipeModule } from 'src/app/core/pipes/pipe.module';
@NgModule({
  declarations: [
    ViewCustomersComponent],
  imports: [
    CommonModule,
    PipeModule,
    CustomerRoutingModule,
    IconsModule,
    TableModule,
    ModalModule,
    WavesModule,
    InputsModule,
    ButtonsModule,
    FormsModule,
    ReactiveFormsModule,
    TooltipModule,
    MultiSelectModule,
    NgxPermissionsModule.forChild()
  ]
})
export class CustomerModule { }
