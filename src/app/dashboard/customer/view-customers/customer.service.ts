import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ApiCommonService } from "src/app/core/services/api-common.service";
@Injectable({
  providedIn: "root"
})
export class CustomerService {
  getLevelReport(isAdmin: any, userId: any) {
    if (isAdmin) {
      return this.apiCommonService.get("/admin/direct-indirect-users/" + userId);
    } else {
      return this.apiCommonService.get("/referrer/direct-indirect-users/" + userId);
    }
  }
  getDrillDownlist(isAdmin: any, userId: any) {
    if (isAdmin) {
      return this.apiCommonService.get("/admin/team-members/" + userId);
    } else {
      return this.apiCommonService.get("/referrer/team-members/" + userId);
    }
  }
  constructor(private apiCommonService: ApiCommonService) { }
  getCustomerList(isAdmin: any): Observable<any> {
    if (isAdmin) {
      return this.apiCommonService.get("/admin/my-customers");
    } else {
      return this.apiCommonService.get("/referrer/my-customers");
    }
  }
  createNewCustomer(body: any, isAdmin: any): Observable<any> {
    if (isAdmin) {
      return this.apiCommonService.post("/admin/add-customer", body);
    } else {
      return this.apiCommonService.post("/referrer/register/customer", body);
    }
  }
  resendMail(id: any, isAdmin: any): Observable<any> {
    if (!isAdmin) {
      return this.apiCommonService.post("/referrer/resend-mail/customer/" + id, {});
    }
    else {
      return this.apiCommonService.post("/admin/resend-activation-mail/customer/" + id, {});
    }
  }
  updateReferrerAccount(action: any, id: any, isAdmin: any): Observable<any> {
    if (isAdmin) {
      return this.apiCommonService.put('/admin/my-customer/update-account?id=' + id + '&action=' + action, {});
    }
    else {
      return this.apiCommonService.put('/referrer/my-customer/update-account?id=' + id + '&action=' + action, {});

    }
  }
  updateCustomerEmail(body: any, isAdmin: any): Observable<any> {
    if (isAdmin) {
      return this.apiCommonService.put('/admin/update-email/', body);
    }
    else {
      return this.apiCommonService.put('/referrer/update-email/', body);
    }
  }
  deleteCustomer(userId: any): Observable<any> {
    return this.apiCommonService.delete('/admin/delete-user/' + userId);
  }

  changeReferralCode(body: any): Observable<any> {
    return this.apiCommonService.post('/admin/change-referral-code', body);
  }
}
