import { Component, OnInit, ViewChild } from "@angular/core";
import { SpinnerService } from "src/app/core/services/spinner.service";
import { CustomerService } from "./customer.service";
import { AlertService } from "src/app/core/services/alert.service";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ModalDirective } from "ng-uikit-pro-standard";
import { DataTable, SelectItem } from "primeng/primeng";
import { RolesService } from "src/app/core/services/roles.service";
import { DashboardService } from "../../dashboard.service";
import { element } from "@angular/core/src/render3";
import { Principal } from "src/app/core/services/principal.service";
@Component({
  selector: "app-view-customers",
  templateUrl: "./view-customers.component.html",
  styleUrls: ["./view-customers.component.scss"]
})
export class ViewCustomersComponent implements OnInit {
  isEligibleForPromotion: Boolean = false;
  @ViewChild("frame") frame: ModalDirective;
  @ViewChild("profileFrame") profileframe: ModalDirective;
  @ViewChild("changeReferralCodeFrame") changeReferralCodeFrame: ModalDirective;
  @ViewChild("dt2") dt: DataTable;
  @ViewChild('actionFrame') actionFrame: ModalDirective;
  @ViewChild('updateEmail') updateEmail: ModalDirective;
  @ViewChild('deletionFrame') deletionFrame: ModalDirective;
  isAdmin: any;
  statusList: any = [];
  accountType: any[];
  customerList = [];
  addCustomerForm: FormGroup;
  changeReferralCodeForm: FormGroup;
  frozenCols: any[];
  tableColumns: any[];
  drilldownTableColumns: any[];
  levelReportColumns: any[];
  selectedUser: any;
  toggleAction: any;
  selectedCustomerId: any;
  selectedCustomerName: any;
  customerId: any;
  userIdToDelete = null;
  bool: SelectItem[];
  rankType: SelectItem[];
  email_id = new FormControl('');
  userType: SelectItem[];
  levelType: SelectItem[] = [];
  sponsorNames: SelectItem[] = [];
  teamSize: String
  showCustomers: any = false;
  showLevelsReport: any = false;
  drilldownList = [];
  levelReport = [];
  constructor(
    private _fb: FormBuilder,
    private alertService: AlertService,
    private spinnerService: SpinnerService,
    private customerService: CustomerService,
    private rolesService: RolesService,
    private dashboardService: DashboardService,
    private principal: Principal
  ) {
  }
  ngOnInit() {
    this.tableColumns = [
      { field: "name", header: "Name", width: "110px" },
      { field: "email", header: "Email", width: "100px" },
      { field: "contactNumber", header: "Contact", width: "90px" },
      { field: "userType", header: "Acc Type", width: "110px" },
      { field: "isAppAmbassador", header: "AAP", width: "80px" },
      { field: "status", header: "Status", width: "80px" },
      // { field: "accountType", header: "Account Type",width:"150px" },
      { field: "referrerCode", header: "Sponsor Code", width: "90px" },
      { field: "customerCount", header: "App Cust", width: "80px" },
      { field: "customerRepCount", header: "AA", width: "70px" },
      { field: "p2PYEACount", header: "YEA", width: "70px" },
      { field: "p2pDistributorCount", header: "LM", width: "70px" },
      { field: "directAAPCount", header: "AAP", width: "70px" },
      { field: "teamSizeCount", header: "Team", width: "80px" },
      { field: "recognitionCategory", header: "Title", width: "90px" },
      { field: "action", header: "Actions", width: "300px" }
    ];
    this.drilldownTableColumns = [
      { field: "level", header: "Level", width: "90px" },
      { field: "sponsorName", header: "Sponsor Name", width: "110px" },
      { field: "sponsorCode", header: "Sponsor Code", width: "110px" },
      { field: "firstName", header: "First Name", width: "110px" },
      { field: "lastName", header: "Last Name", width: "100px" },
      { field: "emailAddress", header: "Email ID", width: "110px" },
      { field: "contactNumber", header: "Contact Number", width: "110px" }
    ]
    this.levelReportColumns = [
      { field: "level", header: "Level", width: "90px" },
      { field: "referrerName", header: "Sponsor Name", width: "110px" },
      { field: "name", header: "Name", width: "110px" },
      { field: "email", header: "Email ID", width: "110px" },
      { field: "contactNumber", header: "Contact Number", width: "110px" },
      { field: "userType", header: "Acc Type", width: "110px" },
      { field: "isAppAmbassador", header: "AAP", width: "80px" },
      { field: "status", header: "Status", width: "110px" },
      { field: "referrerCode", header: "Sponsor Code", width: "110px" },
      { field: "customerCount", header: "App Cust", width: "80px" },
      { field: "customerRepCount", header: "AA", width: "80px" },
      { field: "p2PYEACount", header: "YEA", width: "80px" },
      { field: "p2pDistributorCount", header: "LM", width: "80px" },
      { field: "directAAPCount", header: "AAP", width: "80px" },
      { field: "teamSizeCount", header: "Team", width: "80px" },
      { field: "recognitionCategory", header: "Title", width: "90px" },


    ]
    this.frozenCols = [
      { field: "name", header: "Customer Name" }
    ];
    this.principal.p2pLink.subscribe(message => { this.teamSize = message });
    this.principal.isEligibleForPromotion.subscribe(message => { console.log(message); this.isEligibleForPromotion = message });
    this.dashboardService.clearMessage();
    this.dashboardService.sendMessage(3);
    this.addCustomerForm = this._fb.group({
      customerId: [""],
      contactNumber: ['', [Validators.pattern(/^\(\d{3}\)\s\d{3}-\d{4}$/)]],
      email: ["", [Validators.required, Validators.email]],
      firstName: ["", [Validators.required]],
      lastName: [""],
      test: ["", [Validators.required]],
    });
    this.changeReferralCodeForm = this._fb.group({
      currentReferralCode: ["", Validators.required],
      newReferralCode: ["", Validators.required],
      userRegistrationId: ["", Validators.required],
    });
    this.email_id.setValidators([Validators.required, Validators.email]);
    this.statusList = [
      { label: 'Active', value: 'ACTIVE' },
      { label: 'Inactive', value: 'INACTIVE' },
      { label: 'Archived', value: 'ARCHIVED' },
      { label: 'Activation Pending', value: 'ACTIVATION PENDING' },
      { label: 'Subscription Pending', value: 'SUBSCRIPTION PENDING' },
      { label: 'Subscription Expired', value: 'SUBSCRIPTION EXPIRED' },
      { label: 'Subscription Canceled', value: 'SUBSCRIPTION CANCELLED' }
    ];
    this.accountType = [
      { label: 'Free', value: 'FREE' },
      { label: 'Paid', value: 'PAID' }
    ];
    this.userType = [
      { label: 'Customer', value: 'Customer' },
      { label: 'App Ambassadors', value: 'App Ambassador' },
      { label: 'P2P YEA', value: 'P2P YEA' },
      { label: 'Legacy Members', value: 'Legacy Member' }
    ];
    this.rankType = [
      { label: 'N.A.', value: 'N.A' },
      { label: 'Bronze', value: 'Bronze' },
      { label: 'Diamond', value: 'Diamond' },
      { label: 'Platinum', value: 'Platinum' },

    ];
    this.bool = [
      { label: 'Yes', value: 'Yes' },
      { label: 'No', value: 'No' },
    ];
    var role = [];
    role = this.rolesService.getRole();
    var index = role.indexOf("ROLE_SUPER_ADMIN");
    (index < 0) ? this.isAdmin = false : this.isAdmin = true;
    var subAdmin;
    var role = this.rolesService.getRole();
    var index = role.indexOf("ROLE_ADMIN");
    (index < 0) ? subAdmin = false : subAdmin = true;
    // if(this.isAdmin){
    //   this.tableColumns.splice(9, 3);
    // }
    this.getCustomerList();

  }
  showUserDetails(data: any, action: any) {
    console.log(data);
    console.log(action);
    this.selectedUser = data;
    this.toggleAction = action;
    this.selectedCustomerId = data.userId;
  }

  selectUser(rowData: any) {
    console.log(rowData);
    this.changeReferralCodeForm.reset();
    this.changeReferralCodeForm.controls.userRegistrationId.setValue(rowData.userId);
    this.changeReferralCodeForm.controls.currentReferralCode.setValue(rowData.sponsorReferrerCode);
  }



  changeReferralCode() {
    if (this.changeReferralCodeForm.valid) {
      this.spinnerService.showLoader();
      const body = {
        "currentReferralCode": this.changeReferralCodeForm.controls.currentReferralCode.value,
        "newReferralCode": this.changeReferralCodeForm.controls.newReferralCode.value,
        "userRegistrationId": this.changeReferralCodeForm.controls.userRegistrationId.value,
      }
      this.customerService.changeReferralCode(this.changeReferralCodeForm.value).subscribe(res => {
        this.spinnerService.hideLoader();
        this.alertService.clearMessage();
        this.alertService.sendMessage(res.message, 'success');
        this.changeReferralCodeForm.reset();
        this.getCustomerList();
        this.changeReferralCodeFrame.hide();
      }, (err) => {

      }, () => {

      });
    } else {
      Object.keys(this.changeReferralCodeForm.controls).forEach(key => {
        this.changeReferralCodeForm.get(key).markAsDirty();
      });
    }

  }
  getCustomerList() {
    this.customerList = [];
    this.spinnerService.showLoader();
    this.customerService.getCustomerList(this.isAdmin).subscribe(
      res => {
        console.log(res);
        res.forEach(element => {
          this.customerList.push(element);
        });
        this.customerList.forEach((element, index) => {
          if (element.userType === 'Customer-Rep' && !element.p2PYEA) {
            this.customerList[index].userType = "App Ambassador"
          }
          if (element.userType === 'Customer-Rep' && element.p2PYEA) {
            this.customerList[index].userType = "P2P YEA"
          }
          if (element.userType === 'P2P Distributor') {
            this.customerList[index].userType = "Legacy Member"
          }
          if (element.isAppAmbassador) {
            this.customerList[index].isAppAmbassador = 'Yes'
          } else {
            this.customerList[index].isAppAmbassador = 'No'
          }
        });
        this.spinnerService.hideLoader();
      },
      err => { },
      () => {
        this.dt.reset();
        this.spinnerService.hideLoader();
      }
    );
  }
  selectCustomer(rowData: any) {
    console.log(rowData);
    this.customerId = rowData.userId;
    this.email_id.markAsUntouched();
  }
  setUserToDelete(rowData: any) {
    this.userIdToDelete = rowData.userId;
    this.selectedUser = rowData
  }
  deleteUser() {
    this.spinnerService.showLoader();
    this.customerService.deleteCustomer(this.userIdToDelete).subscribe(res => {
      this.spinnerService.hideLoader();
      this.alertService.clearMessage();
      this.alertService.sendMessage(res.message, 'success');
    }, (err) => {
      this.alertService.clearMessage();
      this.alertService.sendMessage(err.message, 'error');
    }, () => {
      this.deletionFrame.hide();
      this.userIdToDelete = null;
      this.getCustomerList();
      this.spinnerService.hideLoader();

    });
  }
  updateCustomerEmail() {
    if (this.email_id.valid) {
      this.spinnerService.showLoader();
      const body = {
        "emailId": this.email_id.value,
        "userId": this.customerId
      };
      this.customerService.updateCustomerEmail(body, this.isAdmin).subscribe(res => {
        this.alertService.clearMessage();
        this.alertService.sendMessage(res.message, "success");
      }, () => {
        this.email_id = new FormControl('');
        this.customerId = null;
        this.updateEmail.hide();
        this.getCustomerList();
        this.spinnerService.hideLoader();
      });
    }
    else {
      this.email_id.markAsDirty();
    }
  }
  updateCustomerAccount() {
    this.spinnerService.showLoader();
    this.customerService.updateReferrerAccount(this.toggleAction, this.selectedCustomerId, this.isAdmin).subscribe(res => {
      console.log(res);
    }, (err) => {
    }, () => {
      this.actionFrame.hide();
      this.spinnerService.hideLoader();
      this.getCustomerList();
    });
  }
  resendMail(rowData: any) {
    this.spinnerService.showLoader();
    this.customerService.resendMail(rowData.userId, this.isAdmin).subscribe(
      res => {
        console.log(res);
        this.alertService.clearMessage();
        this.alertService.sendMessage(res.message, "success");
      },
      err => { },
      () => {
        this.spinnerService.hideLoader();
      }
    );
  }
  selectedPlan(value: any) {
    this.addCustomerForm.controls.test.setValue(value);
  }
  createNewUser() {
    if (this.addCustomerForm.valid) {
      const body = {
        contactNumber: this.addCustomerForm.controls.contactNumber.value,
        email: this.addCustomerForm.controls.email.value,
        firstName: this.addCustomerForm.controls.firstName.value,
        lastName: this.addCustomerForm.controls.lastName.value,
        accountType: this.addCustomerForm.controls.test.value
      };
      this.spinnerService.showLoader();
      this.customerService.createNewCustomer(body, this.isAdmin).subscribe(
        res => {
          console.log(res);
          this.alertService.clearMessage();
          this.alertService.sendMessage(res.message, 'success');
        },
        err => {
        },
        () => {
          this.spinnerService.hideLoader();
          this.getCustomerList();
          this.frame.hide();
        }
      );
    } else {
      Object.keys(this.addCustomerForm.controls).forEach(key => {
        this.addCustomerForm.get(key).markAsDirty();
      });
    }
  }
  getDrillDownList(row) {
    this.selectedCustomerName = row.name
    this.drilldownList = [];
    this.sponsorNames = [];
    this.levelType = [];
    this.spinnerService.showLoader();
    this.customerService.getDrillDownlist(this.isAdmin, row.userId).subscribe(
      res => {
        console.log(res);
        res.forEach((element, index) => {
          this.drilldownList.push(element);
          if (!this.levelType.some(x => x.value === 'Level' + ' ' + this.drilldownList[index].level)) {
            this.levelType.push({ label: 'Level' + ' ' + this.drilldownList[index].level, value: 'Level' + ' ' + this.drilldownList[index].level })
          }
          if (!this.sponsorNames.some(x => x.value === this.drilldownList[index].sponsorName)) {
            this.sponsorNames.push({ label: this.drilldownList[index].sponsorName, value: this.drilldownList[index].sponsorName })
          }
        });
        this.drilldownList.forEach((element, index) => {
          this.drilldownList[index].level = 'Level' + ' ' + this.drilldownList[index].level
        })
        this.showCustomers = true;
        this.spinnerService.hideLoader();
      },
      err => { },
      () => {
        // this.dt.reset();
        this.spinnerService.hideLoader();
      }
    );
  }

  getLevelReport(row) {
    this.selectedCustomerName = row.name
    this.levelReport = [];
    this.sponsorNames = [];
    this.levelType = [];
    this.spinnerService.showLoader();
    this.customerService.getLevelReport(this.isAdmin, row.userId).subscribe(
      res => {
        console.log(res);
        res.forEach((element, index) => {
          this.levelReport.push(element);
          if (!this.levelType.some(x => x.value === 'Level' + ' ' + this.levelReport[index].level)) {
            this.levelType.push({ label: 'Level' + ' ' + this.levelReport[index].level, value: 'Level' + ' ' + this.levelReport[index].level })
          }
          if (!this.sponsorNames.some(x => x.value === this.levelReport[index].referrerName)) {
            this.sponsorNames.push({ label: this.levelReport[index].referrerName, value: this.levelReport[index].referrerName })
          }

          if (element.userType === 'Customer-Rep' && !element.p2PYEA) {
            this.levelReport[index].userType = "App Ambassador"
          }
          if (element.userType === 'Customer-Rep' && element.p2PYEA) {
            this.levelReport[index].userType = "P2P YEA"
          }
          if (element.userType === 'P2P Distributor') {
            this.levelReport[index].userType = "Legacy Member"
          }
          if (element.isAppAmbassador) {
            this.levelReport[index].isAppAmbassador = 'Yes'
          } else {
            this.levelReport[index].isAppAmbassador = 'No'
          }
        });
        this.levelReport.forEach((element, index) => {
          this.levelReport[index].level = 'Level' + ' ' + this.levelReport[index].level
        })
        this.showLevelsReport = true;
        this.spinnerService.hideLoader();
      },
      err => { },
      () => {
        // this.dt.reset();
        this.spinnerService.hideLoader();
      }
    );
  }
}
