import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { NgxPermissionsGuard } from "ngx-permissions";
import { ManagePlansComponent } from "./manage-plans/manage-plans.component";
const routes: Routes = [
  {
    path: "",
    canActivateChild: [NgxPermissionsGuard],
    component: DashboardComponent,
    children: [
      {
        path: "money-flow-reports",
        canActivate: [NgxPermissionsGuard],
        loadChildren: "src/app/reports/reports.module#ReportsModule",
        data: {
          permissions: {
            only: [
              "ROLE_ADMIN",
              "ROLE_SUPER_ADMIN",
              "ROLE_CUSTOMER_REP",
              "ROLE_CUSTOMER"
            ],
            redirectTo: "/home"
          }
        }
      },
      {
        path: "profile",
        canActivate: [NgxPermissionsGuard],
        loadChildren: "src/app/dashboard/profile/profile.module#ProfileModule",
        data: {
          permissions: {
            only: [
              "ROLE_ADMIN",
              "ROLE_CUSTOMER_REP",
              "ROLE_CUSTOMER",
              "ROLE_SUPER_ADMIN",
              "ROLE_PROSPECT"
            ],
            redirectTo: "/home"
          }
        }
      },
      {
        path: "transactions",
        canActivate: [NgxPermissionsGuard],
        loadChildren:
          "src/app/dashboard/transaction/transaction.module#TransactionModule",
        data: {
          permissions: {
            only: [
              "ROLE_ADMIN",
              "ROLE_SUPER_ADMIN",
              "ROLE_CUSTOMER_REP",
              "ROLE_CUSTOMER"
            ],
            redirectTo: "/home"
          }
        }
      },
      {
        path: "commission",
        canActivate: [NgxPermissionsGuard],
        loadChildren:
          "src/app/dashboard/commissions/commissions.module#CommissionsModule",
        data: {
          permissions: {
            only: ["ROLE_SUPER_ADMIN","ROLE_ADMIN","ROLE_CUSTOMER_REP"],
            redirectTo: "/home"
          }
        }
      },
      {
        path: "bonus",
        canActivate: [NgxPermissionsGuard],
        loadChildren:
          "src/app/dashboard/bonus/bonus.module#BonusModule",
        data: {
          permissions: {
            only: ["ROLE_SUPER_ADMIN","ROLE_ADMIN","ROLE_CUSTOMER_REP"],
            redirectTo: "/home"
          }
        }
      }, 
      {
        path: "money-flow-manager",
        canActivate: [NgxPermissionsGuard],
        loadChildren:
          "src/app/money-flow-manager/money-flow-manager.module#MoneyFlowManagerModule",
        data: {
          permissions: {
            only: ["ROLE_CUSTOMER_REP", "ROLE_CUSTOMER"],
            redirectTo: "/home"
          }
        }
      },
      {
        path: "customers",
        canActivate: [NgxPermissionsGuard],
        loadChildren:
          "src/app/dashboard/customer/customer.module#CustomerModule",
        data: {
          permissions: {
            only: ["ROLE_ADMIN", "ROLE_SUPER_ADMIN", "ROLE_CUSTOMER_REP"],
            redirectTo: "/home"
          }
        }
      },
      {
        path: "app-ambassadors",
        canActivate: [NgxPermissionsGuard],
        loadChildren:
          "src/app/dashboard/referrers/referrers.module#ReferrersModule",
        data: {
          permissions: {
            only: ["ROLE_ADMIN", "ROLE_SUPER_ADMIN"],
            redirectTo: "/home"
          }
        }
      },
      {
        path: "all-customers",
        canActivate: [NgxPermissionsGuard],
        loadChildren:
          "src/app/dashboard/all-customers/all-customers.module#AllCustomersModule",
        data: {
          permissions: {
            only: ["ROLE_ADMIN", "ROLE_SUPER_ADMIN"],
            redirectTo: "/home"
          }
        }
      },
      {
        path: "manage-app-customers",
        canActivate: [NgxPermissionsGuard],
        loadChildren:
          "src/app/dashboard/manage-aap/manage-aap.module#ManageAapModule",
        data: {
          permissions: {
            only: ["ROLE_ADMIN", "ROLE_SUPER_ADMIN"],
            redirectTo: "/home"
          }
        }
      },
      {
        path: "leaders-team",
        canActivate: [NgxPermissionsGuard],
        loadChildren:
          "src/app/dashboard/leaders-team/leaders-team.module#LeadersTeamModule",
        data: {
          permissions: {
            only: ["ROLE_ADMIN", "ROLE_SUPER_ADMIN"],
            redirectTo: "/home"
          }
        }
      },
      {
        path: "marketing",
        canActivate: [NgxPermissionsGuard],
        loadChildren:
          "src/app/dashboard/marketing/marketing.module#MarketingModule",
        data: {
          permissions: {
            only: ["ROLE_ADMIN", "ROLE_SUPER_ADMIN", "ROLE_CUSTOMER_REP"],
            redirectTo: "/home"
          }
        }
      },
      {
        path: "manage-admins",
        canActivate: [NgxPermissionsGuard],
        loadChildren:
          "src/app/dashboard/manage-admin/manage-admin.module#ManageAdminModule",
        data: {
          permissions: {
            only: ["ROLE_SUPER_ADMIN"],
            redirectTo: "/home"
          }
        }
      },
      // {
      //   path: "manage-prospects",
      //   canActivate: [NgxPermissionsGuard],
      //   loadChildren:
      //     "src/app/dashboard/manage-prospects/manage-prospects.module#ManageProspectsModule",
      //   data: {
      //     permissions: {
      //       only: ["ROLE_SUPER_ADMIN", "ROLE_ADMIN"],
      //       redirectTo: "/home"
      //     }
      //   }
      // },
      {
        path: "manage-plans",
        canActivate: [NgxPermissionsGuard],
        component: ManagePlansComponent,
        data: {
          permissions: {
            only: ["ROLE_SUPER_ADMIN"],
            redirectTo: "/home"
          }
        }
      },
      {
        path: "contact-manager",
        canActivate: [NgxPermissionsGuard],
        loadChildren:
          "src/app/dashboard/contact-manager/contact-manager.module#ContactManagerModule",
        data: {
          permissions: {
            only: ["ROLE_CUSTOMER_REP", "ROLE_CUSTOMER"],
            redirectTo: "/home"
          }
        }
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
