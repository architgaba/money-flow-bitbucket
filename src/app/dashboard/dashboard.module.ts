import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DashboardRoutingModule } from "./dashboard-routing.module";
import {
  AccordionModule,
  ModalModule,
  TooltipModule,
  PopoverModule,
  WavesModule,
  NavbarModule,
  ButtonsModule,
  CardsFreeModule,
  TabsModule,
  InputsModule,
  IconsModule,
  CheckboxModule,
} from "ng-uikit-pro-standard";
import { NgxPermissionsModule } from "ngx-permissions";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TableModule } from "primeng/table";
import { ProfileModule } from "./profile/profile.module";
import { ManagePlansComponent } from './manage-plans/manage-plans.component';
import { DropdownModule, MultiSelectModule } from "primeng/primeng";
@NgModule({
  declarations: [DashboardComponent, ManagePlansComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    NgxPermissionsModule.forChild(),
    AccordionModule,
    WavesModule,
    NavbarModule,
    ProfileModule,
    ButtonsModule,
    CardsFreeModule,
    TableModule,
    FormsModule,
    InputsModule,
    IconsModule,
    TabsModule,
    ReactiveFormsModule,
    CheckboxModule,
    ModalModule,
    TooltipModule,
    PopoverModule,
    DropdownModule,
    MultiSelectModule
  ],
})
export class DashboardModule { }
