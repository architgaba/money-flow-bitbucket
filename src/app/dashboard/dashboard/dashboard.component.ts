import { Component, OnInit } from '@angular/core';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { DashboardService } from '../dashboard.service';
import { Subscription } from 'rxjs';
import { Principal } from 'src/app/core/services/principal.service';
@Component({
  selector: 'app-dashboard-component',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  userPermissions: any;
  selectedIndex: any = 4;
  messageSubscription: Subscription;
  constructor(private spinnerService: SpinnerService,
    private dashboardService: DashboardService,private principal:Principal) {
      this.principal.changeMessage(false);
  }
  setSelectedIndex(index: any) {
    console.log(index);
    this.selectedIndex = index;
  }
  ngOnInit() {
    this.spinnerService.hideLoader();
    if (this.selectedIndex != null || this.selectedIndex != undefined) {
      this.messageSubscription = this.dashboardService.getMessage().subscribe(message => {
        this.selectedIndex = message;
      });
    }
    console.log(this.selectedIndex);
  }
  authorizedFunction() {
  }
}
