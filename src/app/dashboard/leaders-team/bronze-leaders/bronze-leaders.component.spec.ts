import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BronzeLeadersComponent } from './bronze-leaders.component';

describe('BronzeLeadersComponent', () => {
  let component: BronzeLeadersComponent;
  let fixture: ComponentFixture<BronzeLeadersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BronzeLeadersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BronzeLeadersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
