import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { LeaderTeamService } from '../leaders-team.service';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { AlertService } from 'src/app/core/services/alert.service';

@Component({
  selector: 'app-bronze-leaders',
  templateUrl: './bronze-leaders.component.html',
  styleUrls: ['./bronze-leaders.component.scss']
})
export class BronzeLeadersComponent implements OnInit {
  bronzeLeaders = []
  constructor(private service: LeaderTeamService,private spinnerService:SpinnerService,private alertService:AlertService) { }
  ngOnInit() {
    this.service.getBronzeLeaders().subscribe((data) => {
      if (data != null) {
        this.bronzeLeaders = [];
        data.forEach(element => {
          this.bronzeLeaders.push(element);
        });
        console.log(this.bronzeLeaders)
      }
    });
  }
  deleteLeader(id){
    this.spinnerService.showLoader()
    this.service.deleteLeader(id).subscribe(res=>{
      this.alertService.clearMessage();
      this.alertService.sendMessage(res.message, "success");
      this.service.emitChange();
    },
    (error)=>{
      this.alertService.clearMessage();
      this.alertService.sendMessage(error.message, "error");
    },
    ()=>{
      this.spinnerService.hideLoader();
    })
  }
}

