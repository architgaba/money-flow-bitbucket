import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiamondLeadersComponent } from './diamond-leaders.component';

describe('DiamondLeadersComponent', () => {
  let component: DiamondLeadersComponent;
  let fixture: ComponentFixture<DiamondLeadersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiamondLeadersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiamondLeadersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
