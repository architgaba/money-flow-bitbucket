import { Component, OnInit } from '@angular/core';
import { LeaderTeamService } from '../leaders-team.service';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { AlertService } from 'src/app/core/services/alert.service';

@Component({
  selector: 'app-diamond-leaders',
  templateUrl: './diamond-leaders.component.html',
  styleUrls: ['./diamond-leaders.component.scss']
})
export class DiamondLeadersComponent implements OnInit {

  diamondLeaders = []
  constructor(private service: LeaderTeamService, private spinnerService:SpinnerService,private alertService:AlertService) { }
  ngOnInit() {
    this.service.getDiamondLeaders().subscribe((data) => {
      if (data != null) {
        this.diamondLeaders = [];
        data.forEach(element => {
          this.diamondLeaders.push(element);
        });
        console.log(this.diamondLeaders)
      }
    });
  }

  deleteLeader(id){
    this.spinnerService.showLoader()
    this.service.deleteLeader(id).subscribe(res=>{
      this.alertService.clearMessage();
      this.alertService.sendMessage(res.message, "success");
      this.service.emitChange();
    },
    (error)=>{
      this.alertService.clearMessage();
      this.alertService.sendMessage(error.message, "error");
    },
    ()=>{
      this.spinnerService.hideLoader();
    })
  }

}
