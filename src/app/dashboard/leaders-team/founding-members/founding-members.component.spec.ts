import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoundingMembersComponent } from './founding-members.component';

describe('FoundingMembersComponent', () => {
  let component: FoundingMembersComponent;
  let fixture: ComponentFixture<FoundingMembersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoundingMembersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoundingMembersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
