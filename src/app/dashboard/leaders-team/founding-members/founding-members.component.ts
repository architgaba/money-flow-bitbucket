import { Component, OnInit } from '@angular/core';
import { LeaderTeamService } from '../leaders-team.service';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { AlertService } from 'src/app/core/services/alert.service';

@Component({
  selector: 'app-founding-members',
  templateUrl: './founding-members.component.html',
  styleUrls: ['./founding-members.component.scss']
})
export class FoundingMembersComponent implements OnInit {

  foundingMembers = []
  constructor(private service: LeaderTeamService,private spinnerService:SpinnerService, private alertService:AlertService) { }
  ngOnInit() {
    this.service.getFoundingMembers().subscribe((data) => {
      if (data != null) {
        this.foundingMembers = [];
        data.forEach(element => {
          this.foundingMembers.push(element);
        });
        console.log(this.foundingMembers)
      }
    });
  }
  deleteLeader(id){
    this.spinnerService.showLoader()
    this.service.deleteLeader(id).subscribe(res=>{
      this.alertService.clearMessage();
      this.alertService.sendMessage(res.message, "success");
      this.service.emitChange();
    },
    (error)=>{
      this.alertService.clearMessage();
      this.alertService.sendMessage(error.message, "error");
    },
    ()=>{
      this.spinnerService.hideLoader();
    })
  }
}
