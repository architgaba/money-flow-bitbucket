import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { LeadersTeamComponent } from './leaders-team/leaders-team.component';
import { BronzeLeadersComponent } from './bronze-leaders/bronze-leaders.component';
import { DiamondLeadersComponent } from './diamond-leaders/diamond-leaders.component';
import { PlatinumLeadersComponent } from './platinum-leaders/platinum-leaders.component';
import { FoundingMembersComponent } from './founding-members/founding-members.component';
const routes: Routes = [
  {
    path: '',
    canActivate: [NgxPermissionsGuard],
    component: LeadersTeamComponent,
    children: [
      {
        path: '',
        canActivate: [NgxPermissionsGuard],
        redirectTo: 'bronze'
      },
      {
        path: 'bronze',
        canActivate: [NgxPermissionsGuard],
        component: BronzeLeadersComponent
      },
      {
        path: 'diamond',
        canActivate: [NgxPermissionsGuard],
        component: DiamondLeadersComponent
      },
      {
        path: 'platinum',
        canActivate: [NgxPermissionsGuard],
        component: PlatinumLeadersComponent
      },
      {
        path: 'founding-members',
        canActivate: [NgxPermissionsGuard],
        component: FoundingMembersComponent
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LeadersTeamRoutingModule { }
