import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeadersTeamComponent } from './leaders-team/leaders-team.component';
import { BronzeLeadersComponent } from './bronze-leaders/bronze-leaders.component';
import { PlatinumLeadersComponent } from './platinum-leaders/platinum-leaders.component';
import { DiamondLeadersComponent } from './diamond-leaders/diamond-leaders.component';
import { FoundingMembersComponent } from './founding-members/founding-members.component';
import { LeadersTeamRoutingModule } from './leaders-team-routing.module';
import { TabMenuModule } from 'primeng/tabmenu';
import {
  IconsModule, PopoverModule, TooltipModule,
  ModalModule, CheckboxModule, TabsModule, InputsModule, CardsFreeModule, ButtonsModule, NavbarModule,
  WavesModule, AccordionModule, CarouselModule,SelectModule
} from 'ng-uikit-pro-standard';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { PipeModule } from 'src/app/core/pipes/pipe.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { Ng2ImgMaxModule } from 'ng2-img-max';
import { DataViewModule } from 'primeng/dataview';
@NgModule({
  declarations: [LeadersTeamComponent, BronzeLeadersComponent, PlatinumLeadersComponent, DiamondLeadersComponent, FoundingMembersComponent],
  imports: [
    CommonModule,
    LeadersTeamRoutingModule,
    PipeModule,
    Ng2ImgMaxModule,
    NgxPermissionsModule.forChild(),
    AccordionModule,
    WavesModule,
    NavbarModule,
    TabMenuModule,
    ButtonsModule,
    CardsFreeModule,
    TableModule,
    FormsModule,
    InputsModule,
    IconsModule,
    TabsModule,
    ReactiveFormsModule,
    CheckboxModule,
    ModalModule,
    TooltipModule,
    PopoverModule,
    CarouselModule,
    SelectModule,
    DataViewModule,
    TooltipModule
  ]
})
export class LeadersTeamModule { }
