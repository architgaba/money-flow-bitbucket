import { Injectable } from '@angular/core';
import { ApiCommonService } from 'src/app/core/services/api-common.service';
import { Observable, BehaviorSubject, Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class LeaderTeamService {
  deleteLeader(id) :Observable<any>{
    return this.apiCommonService.delete('/leaders-wall/delete/'+id)
  }
  private emitChangeSource = new Subject<any>();

  changeEmitted$ = this.emitChangeSource.asObservable();

  emitChange() {
      this.emitChangeSource.next();
  }
  bronze = new BehaviorSubject<any>(null);
  diamond = new BehaviorSubject<any>(null);
  platinum = new BehaviorSubject<any>(null);
  foundingMembers = new BehaviorSubject<any>(null);

  sendLeadersData(bronze, diamond, platinum, foundingMembers) {
    this.bronze.next(bronze);
    this.diamond.next(diamond);
    this.platinum.next(platinum);
    this.foundingMembers.next(foundingMembers);
  }
  clearData() {
    this.bronze.next([]);
    this.diamond.next([]);
    this.platinum.next([]);
    this.foundingMembers.next([]);
  }
  getBronzeLeaders(): Observable<any> {
    return this.bronze.asObservable();
  }
  getDiamondLeaders(): Observable<any> {
    return this.diamond.asObservable();
  }
  getPlatinumLeaders(): Observable<any> {
    return this.platinum.asObservable();
  }
  getFoundingMembers(): Observable<any> {
    return this.foundingMembers.asObservable();
  }
  getLeaders() {
    return this.apiCommonService.get('/leaders-wall/list');
  }
  constructor(private apiCommonService: ApiCommonService) { }
  createLeader(id, body): Observable<any> {
    return this.apiCommonService.postWithFormData('/leaders-wall/add/' + id, body);
  }
}
