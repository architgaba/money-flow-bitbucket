import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeadersTeamComponent } from './leaders-team.component';

describe('LeadersTeamComponent', () => {
  let component: LeadersTeamComponent;
  let fixture: ComponentFixture<LeadersTeamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeadersTeamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeadersTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
