import { Component, OnInit, ViewChild } from '@angular/core';
import { DashboardService } from '../../dashboard.service';
import { MenuItem } from "primeng/api";
import { ModalDirective } from 'ng-uikit-pro-standard';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Ng2ImgMaxService } from 'ng2-img-max';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { DomSanitizer } from '@angular/platform-browser';
import { LeaderTeamService } from '../leaders-team.service';
import { AlertService } from 'src/app/core/services/alert.service';
@Component({
  selector: 'app-leaders-team',
  templateUrl: './leaders-team.component.html',
  styleUrls: ['./leaders-team.component.scss']
})
export class LeadersTeamComponent implements OnInit {
  items: MenuItem[];
  @ViewChild("frame") frame: ModalDirective;
  uploadForm: FormGroup;
  url = "";
  selectedFile: File = null;
  selectedFileName = "";
  constructor(private alertService: AlertService, private leaderService: LeaderTeamService, private sanitizer: DomSanitizer,
    private ng2ImgMax: Ng2ImgMaxService,
    private spinnerService: SpinnerService, private _fb: FormBuilder, private dashboardService: DashboardService) { }

  resetForm() {
    this.uploadForm.reset();
    this.uploadForm.markAsUntouched();
    this.selectedFile = null;
    this.selectedFileName = "";
    this.url = ""
  }
  resetFileInput() {
    this.url = "";
    this.selectedFile = null;
    this.selectedFileName = "";
  }
  getImagePreview(file: File) {
    const reader: FileReader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (event: any) => {
      this.url = event.target.result;
    };
  }
  selectFile(imageInput) {
    console.log(imageInput);
    console.log("File Upload function");
    this.spinnerService.showLoader();
    this.selectedFile = null;
    this.selectedFileName = "";
    this.url = "";
    this.selectedFileName = imageInput.files[0].name;
    this.ng2ImgMax.compressImage(imageInput.files[0], 2).subscribe(
      result => {
        this.selectedFile = new File([result], result.name);
        this.getImagePreview(this.selectedFile);
        this.spinnerService.hideLoader();
      },
      error => {
        this.spinnerService.hideLoader();
      }
    );
  }
  showFile() {
    return this.selectedFileName;
  }
  selectCategory(value: any) {
    this.uploadForm.controls.category.setValue(value);
  }
  ngOnInit() {
    this.leaderService.changeEmitted$.subscribe(data => {
      this.getLeaders();
    })
    this.dashboardService.clearMessage();
    this.dashboardService.sendMessage(14);
    this.uploadForm = this._fb.group({
      file: [null],
      category: ["", [Validators.required]]
    });
    this.items = [
      {
        label: "Bronze",
        icon: "fa fa-fw fa-graduation-cap",
        routerLink: "./bronze"
      },
      {
        label: "Diamond",
        icon: "fa  fa-fw fa-diamond",
        routerLink: "./diamond"
      },
      {
        label: "Platinum",
        icon: "fa fa-fw fa-star",
        routerLink: "./platinum"
      },
      {
        label: "Founders Club",
        icon: "fa fa-fw fa-user-circle",
        routerLink: "./founding-members"
      }
    ];
    this.getLeaders();
  }
  addLeader() {
    if (this.uploadForm.valid && this.selectedFile != null) {
      const body = new FormData();
      body.append("file", this.selectedFile);
      console.log(body);
      this.spinnerService.showLoader();
      this.leaderService.createLeader(this.uploadForm.controls.category.value, body).subscribe(
        res => {
          this.frame.hide();
          this.alertService.clearMessage();
          this.alertService.sendMessage(res.message, "success");
        },
        err => {
          this.frame.hide();
          this.alertService.clearMessage();
          this.alertService.sendMessage(err.message, "error");
        },
        () => {
          this.spinnerService.hideLoader();
          this.frame.hide();
          this.getLeaders();
        }
      );
    }
    else {
      Object.keys(this.uploadForm.controls).forEach(key => {
        this.uploadForm.get(key).markAsDirty();
      });
    }
  }
  getLeaders() {
    this.spinnerService.showLoader();
    this.leaderService.getLeaders().subscribe(res => {
      this.leaderService.sendLeadersData(res["Bronze"], res["Diamond"], res["Platinum"], res["Founding Members"])
      this.spinnerService.hideLoader();
    },
      (err) => {
        console.log(err)
        this.alertService.clearMessage();
        this.alertService.sendMessage(err.message, "error");
      },
      () => {
        this.spinnerService.hideLoader();
      })
  }
}
