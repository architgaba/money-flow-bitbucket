import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlatinumLeadersComponent } from './platinum-leaders.component';

describe('PlatinumLeadersComponent', () => {
  let component: PlatinumLeadersComponent;
  let fixture: ComponentFixture<PlatinumLeadersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlatinumLeadersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatinumLeadersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
