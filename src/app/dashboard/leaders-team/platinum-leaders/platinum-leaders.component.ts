import { Component, OnInit } from '@angular/core';
import { LeaderTeamService } from '../leaders-team.service';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { AlertService } from 'src/app/core/services/alert.service';

@Component({
  selector: 'app-platinum-leaders',
  templateUrl: './platinum-leaders.component.html',
  styleUrls: ['./platinum-leaders.component.scss']
})
export class PlatinumLeadersComponent implements OnInit {
  platinumLeaders = []
  constructor(private service: LeaderTeamService,private spinnerService:SpinnerService, private alertService:AlertService) { }
  ngOnInit() {
    this.service.getPlatinumLeaders().subscribe((data) => {
      if (data != null) {
        this.platinumLeaders = [];
        data.forEach(element => {
          this.platinumLeaders.push(element);
        });
        console.log(this.platinumLeaders)
      }
    });
  }
  deleteLeader(id){
    this.spinnerService.showLoader()
    this.service.deleteLeader(id).subscribe(res=>{
      this.alertService.clearMessage();
      this.alertService.sendMessage(res.message, "success");
      this.service.emitChange();
    },
    (error)=>{
      this.alertService.clearMessage();
      this.alertService.sendMessage(error.message, "error");
    },
    ()=>{
      this.spinnerService.hideLoader();
    })
  }
}
