import { TestBed } from '@angular/core/testing';

import { AapService } from './aap.service';

describe('AapService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AapService = TestBed.get(AapService);
    expect(service).toBeTruthy();
  });
});
