import { Injectable } from '@angular/core';
import { ApiCommonService } from 'src/app/core/services/api-common.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AapService {
  createNewEligibleP2PDist(userRegistrationId):Observable<any> {
    return this.apiCommonService.post('/admin/app-ambassador/eligible-for-bonuses/'+userRegistrationId,{});
  }
  getP2PDistList() {
    return this.apiCommonService.get('/admin/p2p-distributor');
  }
  deleteCustomer(userIdToDelete: any): Observable<any> {
    return this.apiCommonService.delete('/admin/app-ambassador/eligible-for-bonuses/'+userIdToDelete);
  }
  getAllCustomers() {
    return this.apiCommonService.get('/admin/app-ambassador/eligible-for-bonuses');
  }

 
  constructor(private apiCommonService: ApiCommonService) { }
}
