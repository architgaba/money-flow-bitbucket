import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService } from 'src/app/core/services/alert.service';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { DashboardService } from '../../dashboard.service';
import { AapService } from '../aap.service';
import { DataTable, SelectItem } from 'primeng/primeng';
import { ModalDirective } from 'ng-uikit-pro-standard';
import { CustomerService } from '../../customer/view-customers/customer.service';

@Component({
  selector: 'app-aap',
  templateUrl: './aap.component.html',
  styleUrls: ['./aap.component.scss']
})
export class AapComponent implements OnInit {
  customerList = [];
  userIdToDelete = null;
  selectedUser: any;
  selectedCustomerName:any
  drilldownList:any=[]
  drilldownTableColumns:any=[];
  levelType: SelectItem[]=[];
  showCustomers: any = false;
  @ViewChild("dt1") dt: DataTable;
  @ViewChild('deletionFrame') deletionFrame: ModalDirective;
  @ViewChild("frame") frame: ModalDirective;
  addP2PDistForm: FormGroup;
  constructor(
    private _fb: FormBuilder,
    private alertService: AlertService,
    private spinnerService: SpinnerService,
    private aapService: AapService,
    private customerService:CustomerService,
    private dashboardService: DashboardService
  ) {
  }
  tableColumns = [
    { field: "firstName", header: "First Name", width: "100px" },
    { field: "lastName", header: "Last Name", width: "100px" },
    { field: "contactNumber", header: "Contact", width: "100px" },
    { field: "email", header: "Email", width: "100px" },
    { field: "referrerName", header: "Sponsor", width: "120px" },
    { field: "appAmbassadorCount", header: "AAP", width: "70px" },
    { field: "p2PYEACount", header: "YEA", width: "70px" },
    { field: "teamSizeCount", header: "Team", width: "70px" },
    { field: "recognitionCategory", header: "Title", width: "90px" },
    { field: "creationDate", header: "Enrollment Date", width: "110px" },  
    { field: "action", header: "Actions", width: "100px" }
  ];
  sponsorName:SelectItem[]=[];
  rankType:SelectItem[]=[];
  p2pDistList: SelectItem[];
  ngOnInit() {
    this.dashboardService.clearMessage();
    this.dashboardService.sendMessage(13);
    this.getAllCustomers();
    this.getP2PDistList();
    this.addP2PDistForm = this._fb.group({
      selectedP2PDistributor: ["", [Validators.required]],
    });
    this.rankType = [
      { label: 'N.A.', value: 'N.A' },
      { label: 'Bronze', value: 'Bronze' },
      { label: 'Diamond', value: 'Diamond' },
      { label: 'Platinum', value: 'Platinum' },
    
    ];
    this.drilldownTableColumns = [
      { field: "userRegistrationId", header: "User Id", width: "110px" },
      { field: "level", header: "Level", width: "90px" },
      { field: "firstName", header: "First Name", width: "110px" },
      { field: "lastName", header: "Last Name", width: "100px" }
    ]
  }
  getP2PDistList() {
    this.p2pDistList = [];
    this.spinnerService.showLoader();
    this.aapService.getP2PDistList().subscribe(
      res => {
        console.log(res);
        res.forEach(element => {
          this.p2pDistList.push({ label: element.name, value: element.userRegistrationId });
        });
        this.spinnerService.hideLoader();
      },
      err => { },
      () => {
        this.dt.reset();
      }
    );
  }
  setUserToDelete(rowData: any) {
    this.userIdToDelete = rowData.userRegistrationId;
    this.selectedUser = rowData
  }
  getAllCustomers() {
    this.customerList = [];
    this.spinnerService.showLoader();
    this.aapService.getAllCustomers().subscribe(
      res => {
        console.log(res);
        res.forEach((element,index) => {
          this.customerList.push(element);
          if(!this.sponsorName.some(x => x.value ===  this.customerList[index].referrerName)) {
            this.sponsorName.push({label:this.customerList[index].referrerName,value:this.customerList[index].referrerName})
          }
        });
        this.spinnerService.hideLoader();
      },
      err => { 
        this.spinnerService.hideLoader();
      },
      () => {
        this.dt.reset();
      }
    );
  }
  deleteUser() {
    this.spinnerService.showLoader();
    this.aapService.deleteCustomer(this.userIdToDelete).subscribe((res) => {
      this.spinnerService.hideLoader();
      this.alertService.clearMessage();
      this.alertService.sendMessage(res.message, 'success');
    }, (err) => {
      this.alertService.clearMessage();
      this.alertService.sendMessage(err.message, 'error');
    }, () => {
      this.deletionFrame.hide();
      this.userIdToDelete = null;
      this.getAllCustomers();
      this.spinnerService.hideLoader();
    });
  }
  addAap() {
    if (this.addP2PDistForm.valid) {
      this.spinnerService.showLoader();
      this.aapService.createNewEligibleP2PDist(this.addP2PDistForm.controls.selectedP2PDistributor.value).subscribe(
        res => {
          console.log(res);
          this.alertService.clearMessage();
          this.alertService.sendMessage(res.message, 'success');
        },
        err => {
        },
        () => {
          this.spinnerService.hideLoader();
          this.getAllCustomers();
          this.frame.hide();
        }
      );
    } else {
      Object.keys(this.addP2PDistForm.controls).forEach(key => {
        this.addP2PDistForm.get(key).markAsDirty();
      });
    }
  }

  getDrillDownList(row) {
    this.selectedCustomerName=row.firstName+' '+row.lastName
    this.drilldownList = [];
    this.spinnerService.showLoader();
    this.customerService.getDrillDownlist(true, row.userRegistrationId).subscribe(
      res => {
        console.log(res);
        res.forEach((element,index)  => {
          this.drilldownList.push(element);
          if(!this.levelType.some(x => x.value ===  'Level'+' '+this.drilldownList[index].level)) {
            this.levelType.push({label:'Level'+' '+this.drilldownList[index].level,value:'Level'+' '+this.drilldownList[index].level})
        }
        });
        this.drilldownList.forEach((element,index)=>{
          this.drilldownList[index].level='Level'+' '+this.drilldownList[index].level
        })
        this.showCustomers=true;
        this.spinnerService.hideLoader();
      },
      err => { },
      () => {
        // this.dt.reset();
        this.spinnerService.hideLoader();
      }
    );
  }
}

