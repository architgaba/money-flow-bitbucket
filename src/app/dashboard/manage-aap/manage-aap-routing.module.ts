import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { AapComponent } from './aap/aap.component';

const routes: Routes = [{
  path: '',
  canActivate: [NgxPermissionsGuard],
  component: AapComponent,
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageAapRoutingModule { }
