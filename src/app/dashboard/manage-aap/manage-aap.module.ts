import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageAapRoutingModule } from './manage-aap-routing.module';
import { AapComponent } from './aap/aap.component';
import { PipeModule } from 'src/app/core/pipes/pipe.module';
import { IconsModule, ModalModule, WavesModule, InputsModule, ButtonsModule, TooltipModule, BreadcrumbModule } from 'ng-uikit-pro-standard';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CalendarModule, MultiSelectModule } from 'primeng/primeng';
import { NgxPermissionsModule } from 'ngx-permissions';
import { TableModule } from 'primeng/table';
import {DropdownModule} from 'primeng/dropdown';
@NgModule({
  declarations: [AapComponent],
  imports: [
    CommonModule,
    PipeModule, 
    IconsModule,
    TableModule,
    ModalModule,
    WavesModule,
    InputsModule,
    ButtonsModule,
    TooltipModule,
    FormsModule,
    DropdownModule,
    ReactiveFormsModule,
    CalendarModule,
    BreadcrumbModule,
    MultiSelectModule,
    NgxPermissionsModule.forChild(),
    ManageAapRoutingModule
  ]
})
export class ManageAapModule { }
