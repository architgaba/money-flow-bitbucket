import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManageAdminComponent } from './manage-admin/manage-admin.component';
import { NgxPermissionsGuard } from 'ngx-permissions';
const routes: Routes = [{
  path: '',
  canActivate: [NgxPermissionsGuard],
  component: ManageAdminComponent,
}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageAdminRoutingModule { }
