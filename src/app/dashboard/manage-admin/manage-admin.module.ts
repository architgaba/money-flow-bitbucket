import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableModule } from 'primeng/table';
import { ManageAdminRoutingModule } from './manage-admin-routing.module';
import { ManageAdminComponent } from './manage-admin/manage-admin.component';
import { MultiSelectModule } from 'primeng/primeng';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IconsModule, ButtonsModule, InputsModule, WavesModule, ModalModule, TooltipModule } from 'ng-uikit-pro-standard';
import { PipeModule } from 'src/app/core/pipes/pipe.module';
@NgModule({
  declarations: [ManageAdminComponent],
  imports: [
    CommonModule,
    PipeModule,
    ManageAdminRoutingModule,
    TableModule,
    IconsModule,
    ButtonsModule, InputsModule, WavesModule, ModalModule,
    TooltipModule,
    MultiSelectModule,
    FormsModule, ReactiveFormsModule
  ]
})
export class ManageAdminModule { }
