import { Component, OnInit, ViewChild } from '@angular/core';
import { ManageAdminService } from './manage-admin.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { ModalDirective } from 'ng-uikit-pro-standard';
import { DashboardService } from '../../dashboard.service';
import { AlertService } from 'src/app/core/services/alert.service';
@Component({
  selector: 'app-manage-admin',
  templateUrl: './manage-admin.component.html',
  styleUrls: ['./manage-admin.component.scss']
})
export class ManageAdminComponent implements OnInit {
  adminForm: FormGroup;
  @ViewChild('addframe') frame: ModalDirective;
  @ViewChild('actionFrame') actionFrame: ModalDirective;
  @ViewChild('updateEmail') updateEmail: ModalDirective;
  @ViewChild('deletionFrame') deletionFrame: ModalDirective;
  toggleAction: any;
  adminId: any;
  tableColumns = [
    { field: "name", header: "Admin Name" },
    { field: "email", header: "Email ID" },
    { field: "contactNumber", header: "Phone Number" },
    { field: "status", header: "Account Status" },
    { field: "action", header: "Actions" }
  ];
  statusList = [
    { label: 'Active', value: 'ACTIVE' },
    { label: 'Inactive', value: 'INACTIVE' },
    { label: 'Archived', value: 'ARCHIVED' }
  ];
  adminList = [];
  email_id = new FormControl('');
  selectedCustomerId: any;
  selectedUser: any;
  userIdToDelete = null;
  constructor(private spinnerService: SpinnerService,
    private manageAdminService: ManageAdminService, private _fb: FormBuilder,
    private alertService: AlertService,
    private dashboardService: DashboardService) { }
  ngOnInit() {
    this.email_id.setValidators([Validators.required, Validators.email]);
    this.dashboardService.clearMessage();
    this.dashboardService.sendMessage(8);
    this.adminForm = this._fb.group({
      referrerId: [""],
      contactNumber: ['', [Validators.pattern(/^\(\d{3}\)\s\d{3}-\d{4}$/)]],
      email: ["", [Validators.required, Validators.email]],
      firstName: ["", [Validators.required]],
      lastName: [""]
    });
    this.getAllAdmins();
  }
  selectedAdmin(rowData: any) {
    console.log(rowData);
    this.adminId = rowData.userId;
    this.email_id.markAsUntouched();
  }
  setUserToDelete(rowData: any) {
    this.userIdToDelete = rowData.userId;
    this.selectedUser = rowData
  }
  deleteAdmin() {
    this.spinnerService.showLoader();
    this.manageAdminService.deleteAdmin(this.userIdToDelete).subscribe(res => {
      this.spinnerService.hideLoader();
      this.alertService.clearMessage();
      this.alertService.sendMessage(res.message, 'success');
    }, (err) => {
      this.alertService.clearMessage();
      this.alertService.sendMessage(err.message, 'error');
    }, () => {
      this.deletionFrame.hide();
      this.getAllAdmins();
      this.spinnerService.hideLoader();
    });
  }
  showUserDetails(rowData: any, action: any) {
    this.selectedUser = rowData;
    this.toggleAction = action;
    this.selectedCustomerId = rowData.userId;
    console.log(this.selectedUser);
    console.log(this.toggleAction);
    console.log(this.selectedCustomerId);
  }
  resendMail(rowData: any) {
    this.spinnerService.showLoader();
    this.manageAdminService.resendMail(rowData.userId).subscribe(
      res => {
        console.log(res);
        this.alertService.clearMessage();
        this.alertService.sendMessage(res.message, "success");
      },
      err => { },
      () => {
        this.spinnerService.hideLoader();
      }
    );
  }
  updateAdminAccount() {
    this.spinnerService.showLoader();
    this.manageAdminService.updateAdminAccount(this.toggleAction, this.selectedCustomerId).subscribe(res => {
      console.log(res);
    }, (err) => {
    }, () => {
      this.actionFrame.hide();
      this.spinnerService.hideLoader();
      this.getAllAdmins();
    });
  }
  getAllAdmins(): any {
    this.spinnerService.showLoader();
    this.manageAdminService.getAllAdmins().subscribe(res => {
      console.log(res);
      this.adminList = res;
    }, (err) => {
    }, () => {
      this.spinnerService.hideLoader();
    });
  }
  createNewAdmin() {
    if (this.adminForm.valid) {
      this.spinnerService.showLoader();
      const body = {
        "contactNumber": this.adminForm.controls.contactNumber.value,
        "email": this.adminForm.controls.email.value,
        "firstName": this.adminForm.controls.firstName.value,
        "lastName": this.adminForm.controls.lastName.value,
        "test": ""
      };
      this.manageAdminService.createNewAdmin(body).subscribe(res => {
        console.log(res);
      }, (err) => {
      }, () => {
        this.spinnerService.hideLoader();
        this.frame.hide();
        this.getAllAdmins();
      });
    }
    else {
      Object.keys(this.adminForm.controls).forEach(key => {
        this.adminForm.get(key).markAsDirty();
      });
    }
  }
  updateAdminEmail() {
    if (this.email_id.valid) {
      this.spinnerService.showLoader();
      const body = {
        "emailId": this.email_id.value,
        "userId": this.adminId
      };
      this.manageAdminService.updateAdminEmail(body).subscribe(res => {
        this.alertService.clearMessage();
        this.alertService.sendMessage(res.message, "success");
      }, () => {
        this.email_id = new FormControl('');
        this.adminId = null;
        this.updateEmail.hide();
        this.getAllAdmins();
        this.spinnerService.hideLoader();
      });
    }
    else {
      this.email_id.markAsDirty();
    }
  }
}
