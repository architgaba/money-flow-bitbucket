import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ApiCommonService } from "src/app/core/services/api-common.service";
@Injectable({
    providedIn: "root"
})
export class ManageAdminService {
    constructor(private apiCommonService: ApiCommonService) { }
    updateAdminEmail(body: any) {
        return this.apiCommonService.put('/admin/update-email/', body);
    }
    getAllAdmins(): Observable<any> {
        return this.apiCommonService.get('/admin/get-admin');
    }
    createNewAdmin(body: any): Observable<any> {
        return this.apiCommonService.post('/admin/add-admin', body);
    }
    updateAdminAccount(action: any, id: any): Observable<any> {
        return this.apiCommonService.put('/admin/update-admin?id=' + id + '&action=' + action, {});
    }
    resendMail(id: any): Observable<any> {
        return this.apiCommonService.post("/admin/resend-activation-mail/admin/" + id, {});
    }
    deleteAdmin(userId: any): Observable<any> {
        return this.apiCommonService.delete('/admin/delete-user/' + userId);
    }
}