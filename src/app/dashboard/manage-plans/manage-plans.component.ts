import { Component, OnInit, ViewChild } from "@angular/core";
import { ManagePlansService } from "./manage-plans.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AlertService } from "src/app/core/services/alert.service";
import { SpinnerService } from "src/app/core/services/spinner.service";
import { ModalDirective } from "ng-uikit-pro-standard";
import { DashboardService } from "../dashboard.service";
@Component({
  selector: "app-manage-plans",
  templateUrl: "./manage-plans.component.html",
  styleUrls: ["./manage-plans.component.scss"]
})
export class ManagePlansComponent implements OnInit {
  @ViewChild("frame") frame: ModalDirective;
  @ViewChild("deleteFrame") deleteFrame: ModalDirective;
  tableColumns = [
    { field: "planName", header: "Plan Name" },
    { field: "planType", header: "Plan Type" },
    { field: "planAmount", header: "Amount" },
    { field: "planInterval", header: "Interval" },
    { field: "activeStatus", header: "Plan Status" },
    { field: "action", header: "Action" }
  ];
  plansList: any[] = [];
  managePlansGroup: FormGroup;
  planTypes = [
    { label: "Customer", value: "Customer" },
    { label: "Customer Rep", value: "Customer-Rep" }
  ];
  planInterval = [
    { label: "Monthly", value: "month" },
    { label: "Yearly", value: "year" },
    { label: "One Time", value: "One Time" },
  ];
  planStatus = [
    { label: "Activate", value: "Active" },
    { label: "Deactivate", value: "Inactive" }
  ];
  constructor(
    private managePlansService: ManagePlansService,
    private fb: FormBuilder,
    private alertService: AlertService,
    private loaderService: SpinnerService,
    private dashboardService: DashboardService) { }
  getPlans() {
    this.loaderService.showLoader();
    this.managePlansService.getPlans().subscribe(res => {
      console.log(res);
      this.plansList = res;
      console.log(this.plansList);
    }, err => {
    }, () => {
      this.loaderService.hideLoader();
    });
  }
  setPlanDetails(data) {
    this.managePlansGroup.reset();
    this.managePlansGroup.setValue({
      planId: data.planId,
      planName: data.planName,
      planAmount: data.planAmount,
      planDescription: data.planDescription,
      planType: data.planType,
      planInterval: data.planInterval,
      activeStatus: data.activeStatus
    });
  }
  addPlan() {
    if (this.managePlansGroup.valid) {
      this.loaderService.showLoader();
      const body = this.managePlansGroup.value;
      this.managePlansService.addPlan(body).subscribe(
        res => {
          this.alertService.clearMessage();
          this.alertService.sendMessage(res.message, "success");
        },
        err => { },
        () => {
          this.managePlansGroup.reset();
          this.getPlans();
          this.loaderService.hideLoader();
          this.frame.hide();
        }
      );
    } else {
      Object.keys(this.managePlansGroup.controls).forEach(key => {
        this.managePlansGroup.get(key).markAsDirty();
      });
    }
  }
  updatePlan() {
    console.log("hibgu vyhi " + this.managePlansGroup.controls.activeStatus.value)
    if (this.managePlansGroup.controls.activeStatus.value === 'Active' || this.managePlansGroup.controls.activeStatus.value === 'Inactive') {
      this.loaderService.showLoader();
      this.managePlansService
        .updatePlan(
          this.managePlansGroup.controls.planId.value,
          this.managePlansGroup.controls.activeStatus.value
        )
        .subscribe(
          res => {
            this.alertService.clearMessage();
            this.alertService.sendMessage(res.message, "success");
          },
          err => { },
          () => {
            this.getPlans();
            this.loaderService.hideLoader();
            this.frame.hide();
          }
        );
    } else {
      this.alertService.clearMessage();
      this.alertService.sendMessage("Select a status first", "error");
    }
  }
  deletePlan() {
    this.loaderService.showLoader();
    this.managePlansService
      .deletePlan(this.managePlansGroup.controls.planId.value)
      .subscribe(
        res => {
          this.alertService.clearMessage();
          this.alertService.sendMessage(res.message, "success");
        },
        err => { },
        () => {
          this.getPlans();
          this.loaderService.hideLoader();
          this.deleteFrame.hide();
        }
      );
  }
  ngOnInit() {
    this.dashboardService.clearMessage();
    this.dashboardService.sendMessage(10);
    this.getPlans();
    this.managePlansGroup = this.fb.group({
      planId: [null],
      planName: [null, [Validators.required]],
      planAmount: [null, [Validators.required]],
      planDescription: [null],
      planType: [null, [Validators.required]],
      planInterval: [null, [Validators.required]],
      activeStatus: [null]
    });
  }
}
