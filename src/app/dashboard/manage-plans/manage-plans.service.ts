import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ApiCommonService } from "src/app/core/services/api-common.service";
@Injectable({
  providedIn: "root"
})
export class ManagePlansService {
  constructor(private apiCommonService: ApiCommonService) {}
  getPlans(): Observable<any> {
    return this.apiCommonService.get("/plan/fetch");
  }
  addPlan(data): Observable<any> {
    return this.apiCommonService.post('/plan/create', data);
  }
  updatePlan(id , status): Observable<any> {
    return this.apiCommonService.put(`/plan/update?planId=${id}&status=${status}`, {});
  }
  deletePlan(id): Observable<any> {
    return this.apiCommonService.delete('/plan/delete/' + id);
  }
}
