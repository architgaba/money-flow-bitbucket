import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManageProspectsComponent } from './manage-prospects/manage-prospects.component';
import { NgxPermissionsGuard } from 'ngx-permissions';
const routes: Routes = [{
  path: '',
  canActivate: [NgxPermissionsGuard],
  component: ManageProspectsComponent,
}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageProspectsRoutingModule { }
