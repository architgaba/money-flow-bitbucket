import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageProspectsRoutingModule } from './manage-prospects-routing.module';
import { ManageProspectsComponent } from './manage-prospects/manage-prospects.component';
import { TableModule } from 'primeng/table';
import { MultiSelectModule } from 'primeng/primeng';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IconsModule, ButtonsModule, InputsModule, WavesModule, ModalModule, TooltipModule } from 'ng-uikit-pro-standard';
@NgModule({
  declarations: [ManageProspectsComponent],
  imports: [
    CommonModule,
    ManageProspectsRoutingModule,
    CommonModule,
    TableModule,
    IconsModule,
    ButtonsModule, InputsModule, WavesModule, ModalModule,
    TooltipModule,
    MultiSelectModule,
    FormsModule, ReactiveFormsModule
  ]
})
export class ManageProspectsModule { }
