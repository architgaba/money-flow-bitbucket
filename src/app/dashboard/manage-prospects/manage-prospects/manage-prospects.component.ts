import { Component, OnInit } from '@angular/core';
import { ManageProspectsService } from './manage-prospects.service';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { DashboardService } from '../../dashboard.service';
@Component({
  selector: 'app-manage-prospects',
  templateUrl: './manage-prospects.component.html',
  styleUrls: ['./manage-prospects.component.scss']
})
export class ManageProspectsComponent implements OnInit {
  prospectsList = [];
  tableColumns = [
    { field: "name", header: "Prospect Name" },
    { field: "email", header: "Email ID" },
    { field: "joiningDate", header: "Enquiry Date" }
  ];
  constructor(private spinnerService: SpinnerService,
    private manageProspectsService: ManageProspectsService,
    private dashboardService: DashboardService) { }
  ngOnInit() {
    this.dashboardService.clearMessage();
    this.dashboardService.sendMessage(9);
    this.getAllGuestUsers();
  }
  getAllGuestUsers() {
    this.spinnerService.showLoader();
    this.manageProspectsService.getAllGuestUsers().subscribe(res => {
      console.log(res);
      this.prospectsList = res;
    }, (err) => {
    }, () => {
      this.spinnerService.hideLoader();
    });
  }
}
