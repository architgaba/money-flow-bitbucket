import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ApiCommonService } from "src/app/core/services/api-common.service";
@Injectable({
    providedIn: "root"
})
export class ManageProspectsService {
    constructor(private apiCommonService: ApiCommonService) { }
    getAllGuestUsers(): Observable<any> {
        return this.apiCommonService.get('/admin/guest-users');
    }
}