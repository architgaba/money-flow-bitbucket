import { Component, OnInit } from "@angular/core";
import { NgxRolesService } from "ngx-permissions";
import { MarketingService } from "./marketing.service";
import { SpinnerService } from "src/app/core/services/spinner.service";
import { DateFormatterService } from "src/app/core/services/date-formatter.service";
import { Principal } from "src/app/core/services/principal.service";
import { AlertService } from "src/app/core/services/alert.service";
import { FormControl, FormBuilder, Validators } from "@angular/forms";
import { RolesService } from "src/app/core/services/roles.service";
import { DashboardService } from "../../dashboard.service";
import { Router } from "@angular/router";
import { AapService } from "../../manage-aap/aap.service";
declare var $: any;
@Component({
  selector: "app-create-template",
  templateUrl: "./create-template.component.html",
  styleUrls: ["./create-template.component.scss"]
})
export class CreateTemplateComponent implements OnInit {
  ckeditorContent = "";
  subject = new FormControl('', [Validators.required]);
  allUsers: {
    email: string; name: string; status: string;
    customers: string
  }[];
  selectedUsers: any[];
  isAdmin: boolean;
  currentDate: string;
  referrersArray: any;
  name: string;
  role: any;
  status: any;
  statusList: { id: number; name: string }[];
  allUsersCopy: any[];
  showReferrerList: boolean;
  selectedReferrer: any;
  mailAttachments: any[] = [];
  selectedFileName = "";
  url = "";
  constructor(
    private spinnerService: SpinnerService,
    private dateService: DateFormatterService,
    private principal: Principal,
    private roleService: NgxRolesService,
    private marketingService: MarketingService,
    private alertService: AlertService,
    private _fb: FormBuilder,
    private rolesService: RolesService,
    private router: Router,
    private dashboardService: DashboardService,
    private aapService:AapService
  ) {
    this.showReferrerList = false;
    this.currentDate = this.dateService.getFormattedDate(new Date());
    this.principal.currentUser.subscribe(name => (this.name = name));
    var role = this.rolesService.getRole();
    var index = role.indexOf('ROLE_SUPER_ADMIN');
    (index < 0) ? this.isAdmin = false : this.isAdmin = true;
    this.role = 'CUSTOMERS';
    this.status = 'ALL';
  }
  onUpload(event) {
    for (let file of event.files) {
      this.mailAttachments.push(file);
    }
    console.log(this.mailAttachments)
  }
  getActiveTab(event) {
    console.log(event)
    if (event.activeTabIndex == 0) {
      this.router.navigate(["./dashboard/marketing/sample1"])
    }
    if (event.activeTabIndex == 1) {
      this.router.navigate(["./dashboard/marketing/sample2"])
    }
  }
  setRole(role: any) {
    this.role = role;
    this.allUsers = [];
    this.allUsersCopy = [];
    console.log(role);
    if (role === 'CUSTOMER_REPS') {
      this.showReferrerList = false;
      this.getAllReferrer("Customer_Reps");
    }
    if (role === 'P2P_DISTRIBUTORS') {
      this.showReferrerList = false;
      this.getP2PDistributors();
      console.log(this.allUsers);
    }

    if (role === 'CUSTOMERS') {
      this.showReferrerList = false;
      this.getCustomerList();
    }
    if (role === 'INDIRECT_CUSTOMER') {
      this.getAllReferrer("All");
      this.showReferrerList = true;
    }
    if (role === 'PROSPECT_USERS') {
      this.getAllGuestUsers();
      this.showReferrerList = false;
    }
    if (role === 'AAP') {
      this.getAllAapCustomers();
      this.showReferrerList = false;
    }
    if (role === 'YEA') {
      this.getAllYEA();
      this.showReferrerList = false;
    }
  }
  getAllGuestUsers() {
    this.allUsers = [];
    this.allUsersCopy = [];
    this.spinnerService.showLoader();
    this.marketingService.getAllGuestUsers().subscribe(res => {
      console.log(res);
      res.forEach(element => {
        this.allUsers.push({
          "email": element.email,
          "name": element.name,
          "status": element.status,
          "customers": element.customers
        });
        this.allUsersCopy.push({
          "email": element.email,
          "name": element.name,
          "status": element.status,
          "customers": element.customers
        });
      });
    }, (err) => {
    }, () => {
      this.spinnerService.hideLoader();
    });
  }
  selectFile(attachments) {
    console.log(attachments.files[0]);
    this.spinnerService.showLoader();
    this.selectedFileName = "";
    this.url = "";
    this.selectedFileName = attachments.files[0].name;
    this.mailAttachments.push(
      new File([attachments.files[0]], attachments.files[0].name)
    );
    console.log(this.mailAttachments);
    this.spinnerService.hideLoader();
  }
  onChangeStatus(event: any) {
    console.log(this.allUsersCopy);
    this.status = event;
    var temp = this.allUsers;
    console.log(this.allUsers);
    if (event === 'ALL') {
      this.allUsers = this.allUsersCopy;
    }
    else {
      if (this.allUsersCopy)
        this.allUsers = this.allUsersCopy.filter(user => user.status === event);
      else
        this.allUsers = []
    }
    console.log(this.allUsers);
  }
  ngOnInit() {
    this.dashboardService.clearMessage();
    this.dashboardService.sendMessage(7);
    this.allUsers = [];
    this.selectedUsers = [];
    this.statusList = [
      { id: 1, name: "ALL" },
      { id: 2, name: "ACTIVE" },
      { id: 3, name: "INACTIVE" },
      { id: 4, name: "SUBSCRIPTION EXPIRED" },
      { id: 5, name: "ACTIVATION PENDING" },
      { id: 5, name: "SUBSCRIPTION PENDING" },
      { id: 6, name: "ARCHIVED" },
      { id: 7, name: 'SUBSCRIPTION CANCELLED' }
    ];
    this.getCustomerList();
    this.allUsersCopy = [];
  }
  onReferrerSelection(event: any) {
    console.log(event);
    this.allUsers = [];
    this.allUsersCopy = [];
    this.allUsers = event;
    this.allUsersCopy = event;
  }
  getAllReferrer(type) {
    this.allUsers = [];
    this.allUsersCopy = [];
    this.spinnerService.showLoader();
    this.marketingService.getAllReferrer().subscribe(
      res => {
      
        res.forEach(element => {
          if(type==="Customer_Reps"){
            if(element.accountType==="PAID"&&!element.p2PYEA){
              this.allUsers.push({
                "email": element.email,
                "name": element.name,
                "status": element.status,
                "customers": element.customers,
              });
              this.allUsersCopy.push({
                "email": element.email,
                "name": element.name,
                "status": element.status,
                "customers": element.customers
              });
            }
          }else{
            this.allUsers.push({
              "email": element.email,
              "name": element.name,
              "status": element.status,
              "customers": element.customers,
            });
            this.allUsersCopy.push({
              "email": element.email,
              "name": element.name,
              "status": element.status,
              "customers": element.customers
            });
          }

        });
      
      },
      err => { },
      () => {
        if (this.showReferrerList) {
          this.referrersArray = this.allUsers;
          this.allUsersCopy = this.allUsers;
          this.allUsers = [];
          console.log(this.referrersArray);
        }
        this.spinnerService.hideLoader();
      }
    );
  }

  getAllYEA() {
    this.allUsers = [];
    this.allUsersCopy = [];
    this.spinnerService.showLoader();
    this.marketingService.getAllReferrer().subscribe(
      res => {
      
        res.forEach(element => {
          
            if(element.accountType==="PAID"&&element.p2PYEA){
              this.allUsers.push({
                "email": element.email,
                "name": element.name,
                "status": element.status,
                "customers": element.customers,
              });
              this.allUsersCopy.push({
                "email": element.email,
                "name": element.name,
                "status": element.status,
                "customers": element.customers
              });
            }
      

        });
      
      },
      err => { },
      () => {
        this.spinnerService.hideLoader();
      }
    );
  }
  getP2PDistributors() {
    this.allUsers = [];
    this.allUsersCopy = [];
    this.spinnerService.showLoader();
    this.marketingService.getAllReferrer().subscribe(
      res => {
        res.forEach(element => {
          if(element.accountType==="P2P Distributor"){
            this.allUsers.push({
              "email": element.email,
              "name": element.name,
              "status": element.status,
              "customers": element.customers
            });
            this.allUsersCopy.push({
              "email": element.email,
              "name": element.name,
              "status": element.status,
              "customers": element.customers
            });
          }
        });
      },
      err => { },
      () => {
        this.spinnerService.hideLoader();
      }
    );
  }
  getAllAapCustomers() {
    this.allUsers = [];
    this.allUsersCopy = [];
    this.spinnerService.showLoader();
    this.aapService.getAllCustomers().subscribe(
      res => {
        res.forEach(element => {
          this.allUsers.push({
            "email": element.email,
            "name": element.firstName+''+element.lastName,
            "status": element.status,
            "customers": element.customers
          });
          this.allUsersCopy.push({
            "email": element.email,
            "name": element.firstName+''+element.lastName,
            "status": element.status,
            "customers": element.customers
          });
        });
      },
      err => { },
      () => {
        this.spinnerService.hideLoader();
      }
    );
  }
  getCustomerList() {
    this.spinnerService.showLoader();
    this.marketingService.getCustomerList(this.isAdmin).subscribe(res => {
      res.forEach(element => {
        this.allUsers.push({
          "email": element.email,
          "name": element.name,
          "status": element.status,
          "customers": element.customers
        });
        this.allUsersCopy.push({
          "email": element.email,
          "name": element.name,
          "status": element.status,
          "customers": element.customers
        });
      });
    },
      err => {
      },
      () => {
        this.spinnerService.hideLoader();
      }
    );
  }
  onFileUploadRequest(event: any) {
    console.log(event);
  }
  sendMail() {
    console.log(this.mailAttachments);
    var emailId = [];
    this.selectedUsers.forEach(user => {
      emailId.push(user.email);
    });
    if (this.subject.valid && emailId.length > 0) {
      this.ckeditorContent = "<html><body>" + $('iframe').contents().find("body").html() + "</html></body>";
      const body = new FormData();
      body.append("message", this.ckeditorContent);
      body.append("subject", this.subject.value);
      for (var i = 0; i < this.mailAttachments.length; i++) {
        body.append("attachments", this.mailAttachments[i]);
      }
      for (var i = 0; i < emailId.length; i++) {
        body.append("email", emailId[i]);
      }
      console.log(this.ckeditorContent);
      this.spinnerService.showLoader();
      this.marketingService.sendMail(body).subscribe(res => {
        this.alertService.clearMessage();
        this.alertService.sendMessage(res.message, "success");
      }, (err) => {
      }, () => {
        this.spinnerService.hideLoader();
      });
      console.log(body);
    }
    else {
      this.subject.markAsDirty();
      if (emailId.length === 0) {
        this.alertService.clearMessage();
        this.alertService.sendMessage("Mail Recipient empty", "error");
      }
    }
    this.mailAttachments = [];
  }
}
