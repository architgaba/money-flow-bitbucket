import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ApiCommonService } from "src/app/core/services/api-common.service";
@Injectable({
    providedIn: "root"
})
export class MarketingService {
    constructor(private apiCommonService: ApiCommonService) { }
    getAllReferrer(): Observable<any> {
        return this.apiCommonService.get('/admin/customer-reps');
    }
    getCustomerList(isAdmin: any): Observable<any> {
        if (isAdmin) {
            return this.apiCommonService.get("/admin/my-customers");
        } else {
            return this.apiCommonService.get("/referrer/my-customers");
        }
    }
    sendMail(body: any): Observable<any> {
        return this.apiCommonService.postWithFormData("/marketing/send-mail", body);
    }
    getAllGuestUsers(): Observable<any> {
        return this.apiCommonService.get('/admin/guest-users');
    }
}