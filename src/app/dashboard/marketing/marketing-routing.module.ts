import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateTemplateComponent } from './create-template/create-template.component';
import { NgxPermissionsGuard } from 'ngx-permissions';
const routes: Routes = [{
  path: '',
  canActivate: [NgxPermissionsGuard],
  component: CreateTemplateComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MarketingRoutingModule { }
