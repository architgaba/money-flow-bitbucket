import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MarketingRoutingModule } from './marketing-routing.module';
import { CreateTemplateComponent } from './create-template/create-template.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PickListModule } from 'primeng/picklist';
import { ButtonsModule, SelectModule } from 'ng-uikit-pro-standard';
import { NgxPermissionsModule } from 'ngx-permissions';
import { CkeditorModule } from 'src/app/ckeditor/ckeditor.module';
import { InputsModule, WavesModule } from 'ng-uikit-pro-standard'
import { FileUploadModule } from 'primeng/primeng';
@NgModule({
  declarations: [CreateTemplateComponent],
  imports: [
    CommonModule,
    MarketingRoutingModule,
    FormsModule,
    PickListModule,
    ButtonsModule,
    SelectModule,
    CkeditorModule,
    InputsModule, WavesModule,
    ReactiveFormsModule,
    FileUploadModule,
    NgxPermissionsModule.forChild()
  ]
})
export class MarketingModule {
}
