import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { BillingAddressService } from './billing-address.service';
import { ModalDirective } from 'ng-uikit-pro-standard';
import { AlertService } from 'src/app/core/services/alert.service';
import { ProfileService } from '../profile.service';
import { TabService } from '../tab.service';
import { RolesService } from 'src/app/core/services/roles.service';
@Component({
  selector: 'app-billing-address',
  templateUrl: './billing-address.component.html',
  styleUrls: ['./billing-address.component.scss']
})
export class BillingAddressComponent implements OnInit {
  @ViewChild('frame') frame: ModalDirective;
  @ViewChild('deleteFrame') deleteFrame: ModalDirective;
  billingAddressForm: FormGroup;
  countries: Countries[];
  states = [];
  table: any = [];
  id: any;
  selectedAddressId: any;
  tableColumns: any = [
    { field: 'addressName', header: 'Address Name' },
    { field: 'firstName', header: 'Name' },
    { field: 'contactNumber', header: 'Contact Number' },
    { field: 'billingAddressCity', header: 'City' },
    { field: 'billingAddressPinCode', header: 'Zipcode' },
    { field: 'action', header: 'Action' },
  ];
  isAdmin:any;
  constructor(private tabService: TabService,private rolesService: RolesService,
    private billingAddressService: BillingAddressService,
    private profileService: ProfileService,
    private _fb: FormBuilder, private spinnerService: SpinnerService, private alert: AlertService) {
      console.log(this.rolesService.getRole());
      var index = this.rolesService.getRole().indexOf('ROLE_SUPER_ADMIN');
      console.log(index);
      (index < 0) ? this.isAdmin = false : this.isAdmin = true;
      if (!this.isAdmin) {
        var index = this.rolesService.getRole().indexOf('ROLE_ADMIN');
        (index < 0) ? this.isAdmin = false : this.isAdmin = true;
      }
     }
  ngOnInit() {
    this.billingAddressForm = this._fb.group({
      "userBillingAddressId": [],
      "addressName": [null, [Validators.required]],
      "companyName": [null],
      "firstName": [null, [Validators.required]],
      "lastName": [null, [Validators.required]],
      "billingAddress1": [null, [Validators.required]],
      "billingAddress2": [null],
      "contactNumber": ['', [Validators.pattern(/^\(\d{3}\)\s\d{3}-\d{4}$/)]],
      "faxNumber": [null],
      "billingAddressCity": [null, [Validators.required]],
      "billingAddressPinCode": [null, [Validators.required]],
      "billingAddressCountry": [null, [Validators.required]],
      "billingAddressState": [null, [Validators.required]],
      "primaryAddress": []
    });
    this.billingAddressForm.controls.primaryAddress.setValue(false);
    this.getBillingAddress();
    console.log(this.table)
    if(this.isAdmin){
      this.tabService.clearMessage();
      this.tabService.sendMessage(2);
    }else{
      this.tabService.clearMessage();
      this.tabService.sendMessage(3);
    }
  }
  selectCountry() {
    this.states = [];
    this.countries.forEach(country => {
      if (this.billingAddressForm.controls.billingAddressCountry.value === country.countryName) {
        this.states = country['statesList'];
      }
    });
  }
  resetForm() {
    this.billingAddressForm.reset();
  }
  setStateAndCountry() {
    this.states = [];
    this.billingAddressForm.controls.billingAddressCountry.setValue(this.countries[0].countryName)
    this.countries.forEach(country => {
      if (this.billingAddressForm.controls.billingAddressCountry.value === country.countryName) {
        this.states = country['statesList'];
      }
    });
    this.billingAddressForm.controls.billingAddressState.setValue(this.states[0].stateName)
  }
  getBillingAddress() {
    this.spinnerService.showLoader();
    this.countries = [];
    this.states = [];
    this.profileService.getListOfCountriesAndState().subscribe(res => {
      res.forEach(element => {
        this.countries.push(element);
      });
    }, (err) => {
    }, () => {
    });
    this.countries.forEach(country => {
      if (this.billingAddressForm.controls.billingAddressCountry.value === country.countryName) {
        this.states = country['statesList'];
      }
    });
    this.billingAddressService.getBillingAddress().subscribe(res => {
      this.spinnerService.hideLoader();
      console.log(res);
      this.table = res;
    }, (err) => {
      this.spinnerService.hideLoader();
    }, () => {
    });
  }
  saveBillingAddress() {
    if (this.billingAddressForm.valid) {
      this.spinnerService.showLoader();
      const body = {
        "userBillingAddressId": 0,
        "addressName": this.billingAddressForm.controls.addressName.value,
        "billingAddress1": this.billingAddressForm.controls.billingAddress1.value,
        "billingAddress2": this.billingAddressForm.controls.billingAddress2.value,
        "billingAddressCity": this.billingAddressForm.controls.billingAddressCity.value,
        "billingAddressCountry": this.billingAddressForm.controls.billingAddressCountry.value,
        "billingAddressPinCode": this.billingAddressForm.controls.billingAddressPinCode.value,
        "billingAddressState": this.billingAddressForm.controls.billingAddressState.value,
        "companyName": this.billingAddressForm.controls.companyName.value,
        "contactNumber": this.billingAddressForm.controls.contactNumber.value,
        "faxNumber": this.billingAddressForm.controls.faxNumber.value,
        "firstName": this.billingAddressForm.controls.firstName.value,
        "lastName": this.billingAddressForm.controls.lastName.value,
        "primaryAddress": this.billingAddressForm.controls.primaryAddress.value ? this.billingAddressForm.controls.primaryAddress.value : false,
      }
      console.log(body);
      this.billingAddressService.saveBillingAddress(body).subscribe(res => {
        this.spinnerService.hideLoader();
        this.frame.hide();
        this.alert.clearMessage();
        this.alert.sendMessage(res.message, 'success');
        console.log(res);
      }, (err) => {
        this.spinnerService.hideLoader();
        this.frame.show();
        this.alert.clearMessage();
        this.alert.sendMessage(err.message, 'error');
      }, () => {
        this.getBillingAddress();
        this.frame.hide();
      }
      );
    } else {
      Object.keys(this.billingAddressForm.controls).forEach(key => {
        this.billingAddressForm.get(key).markAsDirty();
      });
    }
  }
  chooseBillingAddress(row: any) {
    this.selectedAddressId = row.userBillingAddressId;
    this.billingAddressForm.controls.userBillingAddressId.setValue(row.userBillingAddressId);
    this.billingAddressForm.controls.addressName.setValue(row.addressName);
    this.billingAddressForm.controls.billingAddress1.setValue(row.billingAddress1);
    this.billingAddressForm.controls.billingAddress2.setValue(row.billingAddress2);
    this.billingAddressForm.controls.billingAddressCity.setValue(row.billingAddressCity);
    this.billingAddressForm.controls.billingAddressCountry.setValue(row.billingAddressCountry);
    this.billingAddressForm.controls.billingAddressPinCode.setValue(row.billingAddressPinCode);
    this.countries.forEach(country => {
      if (this.billingAddressForm.controls.billingAddressCountry.value === country.countryName) {
        this.states = country['statesList'];
      }
    });
    this.billingAddressForm.controls.billingAddressState.setValue(row.billingAddressState);
    this.billingAddressForm.controls.companyName.setValue(row.companyName);
    this.billingAddressForm.controls.contactNumber.setValue(row.contactNumber);
    this.billingAddressForm.controls.faxNumber.setValue(row.faxNumber);
    this.billingAddressForm.controls.firstName.setValue(row.firstName);
    this.billingAddressForm.controls.lastName.setValue(row.lastName);
    this.billingAddressForm.controls.primaryAddress.setValue(row.primaryAddress);
    console.log(row);
  }
  updateBillingAddress() {
    console.log(this.billingAddressForm.controls.primaryAddress.value);
    if (this.billingAddressForm.valid) {
      this.spinnerService.showLoader();
      const body = {
        "addressName": this.billingAddressForm.controls.addressName.value,
        "billingAddress1": this.billingAddressForm.controls.billingAddress1.value,
        "billingAddress2": this.billingAddressForm.controls.billingAddress2.value,
        "billingAddressCity": this.billingAddressForm.controls.billingAddressCity.value,
        "billingAddressCountry": this.billingAddressForm.controls.billingAddressCountry.value,
        "billingAddressPinCode": this.billingAddressForm.controls.billingAddressPinCode.value,
        "billingAddressState": this.billingAddressForm.controls.billingAddressState.value,
        "companyName": this.billingAddressForm.controls.companyName.value,
        "contactNumber": this.billingAddressForm.controls.contactNumber.value,
        "faxNumber": this.billingAddressForm.controls.faxNumber.value,
        "firstName": this.billingAddressForm.controls.firstName.value,
        "lastName": this.billingAddressForm.controls.lastName.value,
        "primaryAddress": this.billingAddressForm.controls.primaryAddress.value ? this.billingAddressForm.controls.primaryAddress.value : false,
      }
      console.log(body);
      this.billingAddressService.updateBillingAddress(this.selectedAddressId, body).subscribe(res => {
        this.spinnerService.hideLoader();
        this.frame.hide();
        this.alert.clearMessage();
        this.alert.sendMessage(res.message, 'success');
      }, (err) => {
        this.spinnerService.hideLoader();
        this.frame.hide();
        this.alert.clearMessage();
        this.alert.sendMessage(err.message, 'error');
      }, () => {
        this.spinnerService.hideLoader();
        this.getBillingAddress();
        this.frame.hide();
      }
      );
    }
  }
  setBillingAddressId(id: any) {
    this.selectedAddressId = id;
  }
  deleteBillingAddress() {
    console.log(this.selectedAddressId);
    this.spinnerService.showLoader();
    this.billingAddressService.deleteBillingAddress(this.selectedAddressId).subscribe(res => {
      this.deleteFrame.hide();
      this.spinnerService.hideLoader();
      this.alert.clearMessage();
      this.alert.sendMessage(res.message, 'success');
    }, (err) => {
      this.alert.clearMessage();
      this.alert.sendMessage(err.message, 'error');
    }, () => {
      this.selectedAddressId = null;
      this.getBillingAddress();
      this.spinnerService.hideLoader();
      this.deleteFrame.hide();
    });
  }
}
export interface Countries {
  countryId: any;
  countryCode: any;
  countryName: any;
  states: [];
}
