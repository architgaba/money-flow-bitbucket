import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiCommonService } from 'src/app/core/services/api-common.service';
@Injectable({
  providedIn: 'root'
})
export class BillingAddressService {
  constructor(private apiCommonService: ApiCommonService) { }
  getBillingAddress(): Observable<any> {
    return this.apiCommonService.get('/user/information/address/billing');
  }
  saveBillingAddress(body): Observable<any> {
    return this.apiCommonService.post('/user/information/address/billing', body);
  }
  updateBillingAddress(id,body):Observable<any>{
    return this.apiCommonService.put('/user/information/address/billing/'+id,body)
  }
  deleteBillingAddress(id):Observable<any>{
    return this.apiCommonService.delete('/user/information/address/billing/' +id )
  }
}
