import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PasswordManagementService } from './password-management.service';
import { AlertService } from 'src/app/core/services/alert.service';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { RolesService } from 'src/app/core/services/roles.service';
import { TabService } from '../tab.service';
@Component({
  selector: 'app-password-management',
  templateUrl: './password-management.component.html',
  styleUrls: ['./password-management.component.scss']
})
export class PasswordManagementComponent implements OnInit {
  show_button1: Boolean = false;
  show_button2: Boolean = false;
  show_button3: Boolean = false;
  show_eye1: Boolean = false;
  show_eye2: Boolean = false;
  show_eye3: Boolean = false;
  isAdmin: any;
  passwordManagementForm: FormGroup;
  constructor(    private tabService: TabService,private spinnerService: SpinnerService, private rolesService: RolesService, private alertService: AlertService, private passwordManagementService: PasswordManagementService,
    private _fb: FormBuilder) {
      console.log(this.rolesService.getRole());
      var index = this.rolesService.getRole().indexOf('ROLE_SUPER_ADMIN');
      console.log(index);
      (index < 0) ? this.isAdmin = false : this.isAdmin = true;
      if (!this.isAdmin) {
        var index = this.rolesService.getRole().indexOf('ROLE_ADMIN');
        (index < 0) ? this.isAdmin = false : this.isAdmin = true;
      }
     }
  ngOnInit() {
    this.passwordManagementForm = this._fb.group({
      "confirmNewPassword": ['', [Validators.required]],
      "newPassword": ['', [Validators.required]],
      "oldPassword": ['', [Validators.required]]
    });

    if(this.isAdmin){
      this.tabService.clearMessage();
      this.tabService.sendMessage(1);
    }else{
      this.tabService.clearMessage();
      this.tabService.sendMessage(2);
    }

  }
  updatePassword() {
    if (this.passwordManagementForm.valid) {
      if (this.passwordManagementForm.controls.newPassword.value != this.passwordManagementForm.controls.confirmNewPassword.value) {
        this.alertService.clearMessage();
        this.alertService.sendMessage("Password did not Matched", 'error');
        return;
      }
      this.spinnerService.showLoader();
      const body = {
        "confirmNewPassword": this.passwordManagementForm.controls.confirmNewPassword.value,
        "newPassword": this.passwordManagementForm.controls.newPassword.value,
        "oldPassword": this.passwordManagementForm.controls.oldPassword.value
      }
      this.passwordManagementService.updatePassword(body).subscribe(res => {
        console.log(res);
        this.alertService.clearMessage();
        this.alertService.sendMessage(res.message, 'success');
      }, (err) => {
      }, () => {
        this.spinnerService.hideLoader();
        this.passwordManagementForm.reset();
      });
    }
    else {
      Object.keys(this.passwordManagementForm.controls).forEach(key => {
        this.passwordManagementForm.get(key).markAsDirty();
      });
    }
  }
  showPassword(val: any) {
    if (val === 'first') {
      this.show_button1 = !this.show_button1;
      this.show_eye1 = !this.show_eye1;
    }
    else if (val === 'second') {
      this.show_button2 = !this.show_button2;
      this.show_eye2 = !this.show_eye2;
    } else {
      this.show_button3 = !this.show_button3;
      this.show_eye3 = !this.show_eye3;
    }
  }
}
