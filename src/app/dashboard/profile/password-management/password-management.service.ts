import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiCommonService } from 'src/app/core/services/api-common.service';
@Injectable({
    providedIn: 'root'
})
export class PasswordManagementService {
    constructor(private apiCommonService: ApiCommonService) { }
    updatePassword(body: any): Observable<any> {
        return this.apiCommonService.post('/user/change-password', body);
    }
}