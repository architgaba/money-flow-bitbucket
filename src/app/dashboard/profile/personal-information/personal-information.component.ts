import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProfileService } from '../profile.service';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { AlertService } from 'src/app/core/services/alert.service';
import { TabService } from '../tab.service';
import { RolesService } from 'src/app/core/services/roles.service';
@Component({
  selector: 'app-personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['./personal-information.component.scss']
})
export class PersonalInformationComponent implements OnInit {
  profileForm: FormGroup;
  loginInfoForm: FormGroup;
  addressInfoForm: FormGroup;
  countries: Countries[];
  states = [];
  isAdmin: any;
  constructor(private profileService: ProfileService,
    private _fb: FormBuilder,
    private rolesService: RolesService,
    private loaderService: SpinnerService,
    private alertService: AlertService,
    private tabService: TabService
    
  ) {
    console.log(this.rolesService.getRole());
    var index = this.rolesService.getRole().indexOf('ROLE_SUPER_ADMIN');
    console.log(index);
    (index < 0) ? this.isAdmin = false : this.isAdmin = true;
    if (!this.isAdmin) {
      var index = this.rolesService.getRole().indexOf('ROLE_ADMIN');
      (index < 0) ? this.isAdmin = false : this.isAdmin = true;
    }
  }
  getProfile() {
    this.countries = [];
    this.states = [];
    this.loaderService.showLoader();
    this.profileService.getListOfCountriesAndState().subscribe(res => {
      console.log('<-------List of Countries and States--------->');
      console.log(res);
      res.forEach(element => {
        this.countries.push(element);
      });
    }, (err) => {
    }, () => {
    });
    this.profileService.getBasicProfileInfo().subscribe(res => {
      console.log('<-------Basic Profile Info--------->');
      console.log(res);
      this.profileForm.controls.firstName.setValue(res.firstName);
      this.profileForm.controls.lastName.setValue(res.lastName);
      this.profileForm.controls.emailAddress.setValue(res.emailAddress);
      this.profileForm.controls.userName.setValue(res.userName);
      if (res.contactNumber) { this.profileForm.controls.contactNumber.setValue(res.contactNumber); }
      this.profileForm.controls.userRegistrationId.setValue(res.userRegistrationId);
      this.loginInfoForm.controls.creationDate.setValue(res.creationDate);
      this.loginInfoForm.controls.lastPasswordChangeDate.setValue(res.lastPasswordChangeDate);
      this.loginInfoForm.controls.lastModifiedDate.setValue(res.lastModifiedDate);
      this.loginInfoForm.controls.lastSignedInDate.setValue(res.lastSignedInDate);
      this.loginInfoForm.controls.userStatus.setValue(res.userStatus);
    }, (err) => {
    }, () => {
      this.getAddress();
    });
  }
  getAddress() {
    this.profileService.getPhysicalAddress().subscribe(res => {
      console.log('<-------Physical Address Info--------->');
      console.log(res);
      this.addressInfoForm.controls.userPhysicalAddressId.setValue(res.userPhysicalAddressId);
      this.addressInfoForm.controls.physicalAddress1.setValue(res.physicalAddress1);
      this.addressInfoForm.controls.physicalAddress2.setValue(res.physicalAddress2);
      this.addressInfoForm.controls.physicalAddressCity.setValue(res.physicalAddressCity);
      (res.physicalAddressCountry) ? (this.addressInfoForm.controls.physicalAddressCountry.setValue(res.physicalAddressCountry)) : (this.addressInfoForm.controls.physicalAddressCountry.setValue(this.countries[0].countryName));
      this.countries.forEach(country => {
        if (this.addressInfoForm.controls.physicalAddressCountry.value === country.countryName) {
          this.states = country['statesList'];
          console.log('<----setting states----->');
          console.log(this.states);
        }
      });
      (res.physicalAddressState) ? (this.addressInfoForm.controls.physicalAddressState.setValue(res.physicalAddressState)) : (this.addressInfoForm.controls.physicalAddressState.setValue(this.states[0].stateName));
      this.addressInfoForm.controls.physicalAddressPinCode.setValue(res.physicalAddressPinCode);
    }, (err) => {
    }, () => {
      this.loaderService.hideLoader();
    });
  }
  selectCountry() {
    this.states = [];
    this.countries.forEach(country => {
      if (this.addressInfoForm.controls.physicalAddressCountry.value === country.countryName) {
        this.states = country['statesList'];
      }
    });
  }
  savePersonalInformation() {
    if (this.profileForm.valid) {
      this.loaderService.showLoader();
      const body = {
        "contactNumber": this.profileForm.controls.contactNumber.value,
        "emailAddress": this.profileForm.controls.emailAddress.value,
        "firstName": this.profileForm.controls.firstName.value,
        "lastName": this.profileForm.controls.lastName.value,
        "userName": this.profileForm.controls.userName.value
      };
      this.profileService.saveBasicProfileInfo(body).subscribe(res => {
        console.log('<---------Saved Personal Info---------->');
        console.log(res);
        this.alertService.clearMessage();
        this.alertService.sendMessage(res.message, 'success');
      }, (err) => {
        this.alertService.clearMessage();
        this.alertService.sendMessage(err.message, 'error');
      }, () => {
        this.getProfile();
        this.loaderService.hideLoader();
      });
    } else {
      Object.keys(this.profileForm.controls).forEach(key => {
        this.profileForm.get(key).markAsDirty();
      });
    }
  }
  savePhysicalAddress() {
    if (this.addressInfoForm.valid) {
      this.loaderService.showLoader();
      const body = {
        "physicalAddress1": this.addressInfoForm.controls.physicalAddress1.value,
        "physicalAddress2": this.addressInfoForm.controls.physicalAddress2.value,
        "physicalAddressCity": this.addressInfoForm.controls.physicalAddressCity.value,
        "physicalAddressCountry": this.addressInfoForm.controls.physicalAddressCountry.value,
        "physicalAddressPinCode": this.addressInfoForm.controls.physicalAddressPinCode.value,
        "physicalAddressState": this.addressInfoForm.controls.physicalAddressState.value,
        "userPhysicalAddressId": this.addressInfoForm.controls.userPhysicalAddressId.value,
      };
      this.profileService.savePhysicalAddress(body).subscribe(res => {
        console.log('<---------Saved Physical Address Info---------->');
        console.log(res);
        this.alertService.clearMessage();
        this.alertService.sendMessage(res.message, 'success');
      },
        (err) => {
          this.alertService.clearMessage();
          this.alertService.sendMessage(err.message, 'error');
        },
        () => {
          this.getProfile();
          this.loaderService.hideLoader();
        });
    } else {
      Object.keys(this.addressInfoForm.controls).forEach(key => {
        this.addressInfoForm.get(key).markAsDirty();
      });
    }
  }
  ngOnInit() {
    this.profileForm = this._fb.group({
      "firstName": [null, [Validators.required]],
      "lastName": [null],
      "emailAddress": [null, [Validators.required, Validators.email]],
      "userName": [null, [Validators.required]],
      "contactNumber": ['', [Validators.pattern(/^\(\d{3}\)\s\d{3}-\d{4}$/)]],
      "userRegistrationId": [null]
    });
    this.loginInfoForm = this._fb.group({
      "creationDate": [{ value: null, disabled: true }],
      "lastPasswordChangeDate": [{ value: null, disabled: true }],
      "lastModifiedDate": [{ value: null, disabled: true }],
      "lastSignedInDate": [{ value: null, disabled: true }],
      "userStatus": [{ value: null, disabled: true }],
    });
    this.addressInfoForm = this._fb.group({
      "userPhysicalAddressId": [null],
      "physicalAddress1": [null, [Validators.required]],
      "physicalAddress2": [null],
      "physicalAddressCity": [null, [Validators.required]],
      "physicalAddressCountry": [null, [Validators.required]],
      "physicalAddressState": [null, [Validators.required]],
      "physicalAddressPinCode": [null, [Validators.required]]
    });
    this.getProfile();
    if(this.isAdmin){
      this.tabService.clearMessage();
      this.tabService.sendMessage(0);
    }else{
      this.tabService.clearMessage();
      this.tabService.sendMessage(1);
    }

  }
}
export interface Countries {
  countryId: any;
  countryCode: any;
  countryName: any;
  states: [];
}
