import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './profile.component';
import { PersonalInformationComponent } from './personal-information/personal-information.component';
import { SubscriptionPlansComponent } from './subscription-plans/subscription-plans.component';
import { PasswordManagementComponent } from './password-management/password-management.component';
import { BillingAddressComponent } from './billing-address/billing-address.component';
import { NgxPermissionsGuard } from 'ngx-permissions';
const routes: Routes = [
  {
    path: '',
    canActivate: [NgxPermissionsGuard],
    component: ProfileComponent,
    children: [
      {
        path: '',
        canActivate: [NgxPermissionsGuard],
        redirectTo: 'subscription-plans'
      },
      {
        path: 'personal-information',
        canActivate: [NgxPermissionsGuard],
        component: PersonalInformationComponent
      },
      {
        path: 'subscription-plans',
        canActivate: [NgxPermissionsGuard],
        component: SubscriptionPlansComponent,
        // data: {
        //   permissions: {
        //     only: [
        //       "ROLE_CUSTOMER_REP",
        //       "ROLE_CUSTOMER",
        //       "ROLE_PROSPECT"],
        //     redirectTo: "/home"
        //   }
        // }
      },
      {
        path: 'password-management',
        canActivate: [NgxPermissionsGuard],
        component: PasswordManagementComponent
      },
      {
        path: 'billing-address',
        canActivate: [NgxPermissionsGuard],
        component: BillingAddressComponent
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
