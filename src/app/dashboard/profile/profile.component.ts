import { Component, OnInit, ViewChild } from "@angular/core";
import { MenuItem } from "primeng/api";
import { DashboardComponent } from "../dashboard/dashboard.component";
import { DashboardService } from "../dashboard.service";
import { RolesService } from "src/app/core/services/roles.service";
import { Subscription } from "rxjs";
import { TabService } from "./tab.service";
@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.scss"]
})
export class ProfileComponent implements OnInit {
  @ViewChild(DashboardComponent)
  dashboardComponentChild: DashboardComponent;
  items: MenuItem[];
  isAdmin: any;
  currentTab = {};
  messageSubscription: Subscription;
  constructor(
    private rolesService: RolesService,
    private dashboardService: DashboardService,
    private tabService: TabService
  ) {
    console.log(this.rolesService.getRole());
    var index = this.rolesService.getRole().indexOf('ROLE_SUPER_ADMIN');
    console.log(index);
    (index < 0) ? this.isAdmin = false : this.isAdmin = true;
    if (!this.isAdmin) {
      var index = this.rolesService.getRole().indexOf('ROLE_ADMIN');
      (index < 0) ? this.isAdmin = false : this.isAdmin = true;
    }
  }
  ngOnInit() {
    this.dashboardService.clearMessage();
    this.dashboardService.sendMessage(0);
    if (this.isAdmin) {
      this.items = [
        {
          label: "Personal Information",
          icon: "fa fa-fw fa-user",
          routerLink: "./personal-information"
        },
        {
          label: "Password Management",
          icon: "fa fa-fw fa-unlock-alt",
          routerLink: "./password-management"
        },
        {
          label: "Billing Address",
          icon: "fa fa-fw fa-vcard",
          routerLink: "./billing-address"
        }
      ];
    } else {
      this.items = [
        {
          label: "Subscription Plan",
          icon: "fa fa-fw fa-rss",
          routerLink: "./subscription-plans"
        },
        {
          label: "Personal Information",
          icon: "fa fa-fw fa-user",
          routerLink: "./personal-information"
        },
        {
          label: "Password Management",
          icon: "fa fa-fw fa-unlock-alt",
          routerLink: "./password-management"
        },
        {
          label: "Billing Address",
          icon: "fa fa-fw fa-vcard",
          routerLink: "./billing-address"
        }
      ];
    }
    if (this.currentTab != {}) {
      this.messageSubscription = this.tabService.getMessage().subscribe(message => {
        this.currentTab = this.items[message];
      });
    }
  }
}
