import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileRoutingModule } from './profile-routing.module';
import { BillingAddressComponent } from './billing-address/billing-address.component';
import { PersonalInformationComponent } from './personal-information/personal-information.component';
import { SubscriptionPlansComponent } from './subscription-plans/subscription-plans.component';
import { PasswordManagementComponent } from './password-management/password-management.component';
import { NgxPermissionsModule } from 'ngx-permissions';
import { ProfileComponent } from './profile.component';
import { TabMenuModule } from 'primeng/tabmenu';
import {
  IconsModule, PopoverModule, TooltipModule,
  ModalModule, CheckboxModule, TabsModule, InputsModule, CardsFreeModule, ButtonsModule, NavbarModule,
  WavesModule, AccordionModule, CarouselModule,SelectModule
} from 'ng-uikit-pro-standard';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { PipeModule } from 'src/app/core/pipes/pipe.module';
@NgModule({
  declarations: [
    ProfileComponent,
    BillingAddressComponent,
    PersonalInformationComponent,
    SubscriptionPlansComponent,
    PasswordManagementComponent,
  ],
  imports: [
    CommonModule,
    PipeModule,
    NgxPermissionsModule.forChild(),
    ProfileRoutingModule,
    AccordionModule,
    WavesModule,
    NavbarModule,
    TabMenuModule,
    ButtonsModule,
    CardsFreeModule,
    TableModule,
    FormsModule,
    InputsModule,
    IconsModule,
    TabsModule,
    ReactiveFormsModule,
    CheckboxModule,
    ModalModule,
    TooltipModule,
    PopoverModule,
    CarouselModule,
    SelectModule
  ]
})
export class ProfileModule { }
