import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiCommonService } from 'src/app/core/services/api-common.service';
@Injectable({ providedIn: 'root' })
export class ProfileService {
    constructor(private apiCommonService: ApiCommonService) { }
    getBasicProfileInfo(): Observable<any> {
        return this.apiCommonService.get('/user/information/personal');
    }
    getPhysicalAddress(): Observable<any> {
        return this.apiCommonService.get('/user/information/address/physical');
    }
    getListOfCountriesAndState(): Observable<any> {
        return this.apiCommonService.get('/user/countries');
    }
    saveBasicProfileInfo(body): Observable<any> {
        return this.apiCommonService.put('/user/information/update', body);
    }
    savePhysicalAddress(body): Observable<any> {
        return this.apiCommonService.post('/user/information/address/physical', body);
    }
}
