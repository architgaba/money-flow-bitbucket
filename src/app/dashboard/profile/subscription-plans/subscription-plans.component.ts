import { Component, OnInit, ViewChild } from "@angular/core";
import { SubscriptionPlansService } from "./subscription-plans.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { SpinnerService } from "src/app/core/services/spinner.service";
import { AlertService } from "src/app/core/services/alert.service";
import { Router } from "@angular/router";
import { ModalDirective } from "ng-uikit-pro-standard";
import {
  StripeService,
  Elements,
  Element as StripeElement,
  ElementsOptions
} from "ngx-stripe";
import { NgxRolesService } from "ngx-permissions";
import { RolesService } from "src/app/core/services/roles.service";
import { ProfileComponent } from "../profile.component";
import { TabService } from "../tab.service";
import { AccountService } from "src/app/core/services/account.service";
import { SessionStorageService } from "ngx-webstorage";
@Component({
  selector: "app-subscription-plans",
  templateUrl: "./subscription-plans.component.html",
  styleUrls: ["./subscription-plans.component.scss"]
})
export class SubscriptionPlansComponent implements OnInit {
  private userIdentity: any;
  showActivePlan: boolean;
  @ViewChild("cardFrame") cardFrame: ModalDirective;
  @ViewChild("confirmationFrame") confirmationFrame: ModalDirective;
  @ViewChild("cancellationframe") cancellationframe: ModalDirective;
  @ViewChild("profileComponentChild") profileComponentChild: ProfileComponent;
  showHideoptionalPlans: Boolean = false;
  message: any = "";
  test = [];
  elements: Elements;
  card: StripeElement;
  showDescription: Boolean;
  elementsOptions: ElementsOptions = {
    locale: "auto"
  };
  stripeForm: FormGroup;
  selectedPlan: any;
  customer = true;
  referrer = false;
  prospect = false;
  visiblePlan = "CUSTOMER";
  currentPlan: CurrentPlan;
  userReferenceType: number;
  planDescription = [];
  testAccountMessage = '';
  customerPlanDescription =
    "It is designed to assist you in keeping more of your hard earned income. Get Your Paycheck Checkup today and use the Money Flow Manager App to assist you in minimizing your taxes by tracking and recording your business related expenses and reduce your taxable income, therefore; reduce the amount of tax you owe.";
  referrerPlanDescription =
    "You are automatically qualified to refer other customers and earn a referral fee.  You will earn $5 for every customer each month for as long as they continue to pay their customer fee of $13.75 each month. Refer 100 customers and earn $500 per month, it's just that simple. Once you set up your Customer Rep account, click the commission link , and select order your Pay Card which will be used to pay your referral fees.";
  subscriptionForm: FormGroup;
  fetchedPlansList: any;
  isAdmin: boolean;
  activePlanList: any;
  subscriptionDate: any;
  subscriptionExpiryDate: any;
  form: FormGroup;
  plans = [];
  termscheckBox: Boolean;
  selectedCard: any;
  public radioModel: any = '';
  public optionalPlanName: string = null;
  public optionalPlanAmount: string = null;
  showMessage: Boolean;
  cols = [
    { field: "creditCard", header: "Credit Card" },
    { field: "name", header: "Name on Card" },
    { field: "expiryDate", header: "Expires On" },
    { field: "action", header: "Action" }
  ];
  listCard = [
    { field: "creditCard", header: "Credit Card" },
    { field: "name", header: "Name on Card" },
    { field: "expiryDate", header: "Expires On" }
  ];
  customers = [
  ];
  selectedCardId: any;
  token: any;
  isTest: any;
  constructor(
    private fb: FormBuilder,
    private stripeService: StripeService,
    private router: Router,
    private alertService: AlertService,
    private subscriptionPlansService: SubscriptionPlansService,
    private _fb: FormBuilder,
    private spinnerService: SpinnerService,
    private roleService: NgxRolesService,
    private rolesService: RolesService,
    private tabService: TabService,
    private account: AccountService,
    private sessionStorage: SessionStorageService,
  ) {
    this.showMessage = false;
    this.token = null;
    this.selectedCardId = null;
    this.termscheckBox = false;
    this.showDescription = false;
    this.showActivePlan = true;
    this.userReferenceType = 1;
    var role = this.rolesService.getRole();
    var index = role.indexOf('ROLE_SUPER_ADMIN');
    (index < 0) ? this.isAdmin = false : this.isAdmin = true;
    this.getCards();
    this.tabService.clearMessage();
    this.tabService.sendMessage(0);
  }
  setValidation() {
    this.stripeForm.get('name').setValidators([Validators.required]);
    this.stripeForm.get('name').updateValueAndValidity();
    this.selectedCard = [];
    this.selectedCardId = null;
  }
  onChange(rowData: any) {
    this.stripeForm.reset();
    console.log(rowData);
    this.selectedCardId = null;
    this.stripeForm.controls.name.reset();
    this.card.clear();
    this.selectedCardId = rowData.cardId;
    this.stripeForm.get('name').clearValidators();
    this.stripeForm.get('name').updateValueAndValidity();
  }
  showCardDetailsModal() {
    this.cardFrame.show();
  }
  showConfirmationDilaog(plan: any) {
    this.card.clear();
    if (this.plans.length > 0) {
      this.radioModel = '';
    }
    this.optionalPlanAmount = null;
    this.optionalPlanName = null;
    console.log(plan);
    if (plan.planType === 'Customer-Rep' && this.plans.length > 0 && plan.planName != 'P2P YEA') {
      this.showHideoptionalPlans = true;
    }
    else if (plan.planType == 'Customer-Rep' && plan.planName == 'P2P YEA') {
      this.showHideoptionalPlans = false;
    }
    else {
      this.showHideoptionalPlans = false;
    }
    this.setSelectedPlan(plan);
    var confirmationFrame = true;
    if (this.activePlanList.length == 0) {
      this.cardFrame.show();
      return;
    }
    for (var i = 0; i < this.activePlanList.length; i++) {

      if (this.activePlanList[i].planInterval === 'One Time Pay') {
        confirmationFrame = false;
      }
      if (plan.planType === 'Customer' && this.activePlanList[i].planType === 'Customer') {
        this.message = 'Are you sure you want to change the current plan?';
        confirmationFrame = true;
        break
      }
      if (plan.planType === 'Customer-Rep') {
        this.message = 'Are you sure you want to upgrade plan?';
      }
    }
    console.log(this.message);
    if (confirmationFrame) {
      this.confirmationFrame.show();
    }
    if (!confirmationFrame) {
      this.cardFrame.show();
    }
  }
  myPlan(plan: any) {
    console.log(plan);
  }
  acceptTerms(termscheckBox: { checked: Boolean; }) {
    console.log(termscheckBox.checked);
    this.termscheckBox = termscheckBox.checked;
  }
  checkValidation() {
    var formValid = false;
    if ((this.selectedPlan.planType === "Customer-Rep" && this.radioModel !== '') || (this.selectedPlan.planType === "Customer-Rep" && !this.showHideoptionalPlans)) {
      formValid = true;
    }
    if (this.selectedPlan.planType === "Customer") {
      formValid = true;
    }
    return formValid;
  }
  buySubscription() {
    this.token = null;
    console.log(this.selectedPlan);
    let isValid = this.checkValidation();
    if (isValid && this.stripeForm.valid && this.termscheckBox) {
      this.spinnerService.showLoader();
      const name = this.stripeForm.get("name").value;
      if (this.selectedCardId === null) {
        this.stripeService.createToken(this.card, { name }).subscribe(result => {
          if (result.token) {
            const body = {
              planId: this.selectedPlan.planId,
              token: result.token.id,
              optionalPlanId: this.radioModel
            };
            this.token = result.token.id;
            this.subscriptionPlansService.buySubscription(body).subscribe(
              res => {
                console.log(res);
                this.alertService.clearMessage();
                this.alertService.sendMessage(res.message, "success");
                this.account
                  .get()
                  .toPromise()
                  .then(response => {
                    const account = response.body;
                    if (account) {
                      this.roleService.flushRoles();
                      this.rolesService.clearRole();
                      this.sessionStorage.clear('permissions')
                      console.log("geting roles");
                      console.log(account);
                      this.userIdentity = account['role'];
                      this.userIdentity.forEach(element => {
                        this.roleService
                          .addRole(element, []);
                        this.rolesService.setRole(element);
                      });
                      this.roleService
                        .addRole(account['permission'], []);
                      this.rolesService.setRole(account['permission']);
                      this.roleService.roles$.subscribe((data) => {
                        this.sessionStorage.store('permissions', JSON.stringify(data));
                      })
                    }
                    window.location.reload();
                    this.router.navigate(["./dashboard/profile/subscription-plans"]);
                  })
              },
              err => { },
              () => {
                this.cardFrame.hide();
                this.spinnerService.hideLoader();
              }
            );
            console.log(result.token);
          } else if (result.error) {
            this.spinnerService.hideLoader();
            this.alertService.clearMessage();
            this.alertService.sendMessage(result.error.message, "error");
            console.log(result.error.message);
          }
        });
      }
      else {
        const body = {
          planId: this.selectedPlan.planId,
          token: this.selectedCardId,
          optionalPlanId: this.radioModel
        };
        this.token = this.selectedCardId;
        this.subscriptionPlansService.buySubscription(body).subscribe(
          res => {
            console.log(res);
            this.alertService.clearMessage();
            this.alertService.sendMessage(res.message, "success");
            this.account
              .get()
              .toPromise()
              .then(response => {
                const account = response.body;
                if (account) {
                  this.roleService.flushRoles();
                  this.rolesService.clearRole();
                  this.sessionStorage.clear('permissions')
                  console.log("geting roles");
                  console.log(account);
                  this.userIdentity = account['role'];
                  this.userIdentity.forEach(element => {
                    this.roleService
                      .addRole(element, []);
                    this.rolesService.setRole(element);
                  });
                  this.roleService
                    .addRole(account['permission'], []);
                  this.rolesService.setRole(account['permission']);
                  this.roleService.roles$.subscribe((data) => {
                    this.sessionStorage.store('permissions', JSON.stringify(data));
                  })
                }
                window.location.reload();
                this.router.navigate(["./dashboard/profile/subscription-plans"]);
              }).catch(err => {
                console.log("CATCH OF")
              });
          },
          err => { },
          () => {
            this.cardFrame.hide();
            this.spinnerService.hideLoader();
          }
        );
      }
    }
    else {
      this.showValidationMessage();
    }
  }
  showValidationMessage() {
    if (!this.stripeForm.valid) {
      this.stripeForm.controls.name.markAsDirty();
      return;
    }
    if (this.showHideoptionalPlans && this.radioModel === "") {
      this.alertService.clearMessage();
      this.alertService.sendMessage("Please select a customer plan as well before proceeding with the payment.", "error");
      return;
    }
    if (this.token === null && this.selectedCardId === null && this.stripeForm.get("name").value === "") {
      this.alertService.clearMessage();
      this.alertService.sendMessage("Please select an existing card or add a new card before payment.", "error");
      return;
    }
    if (!this.termscheckBox) {
      this.alertService.clearMessage();
      this.alertService.sendMessage("Please accept the terms and conditions.", "error");
    }
  }
  createToken() {
    this.token = null;
    const name = this.stripeForm.get("name").value;
    this.stripeService.createToken(this.card, { name }).subscribe(result => {
      if (result.token) {
        this.token = result.token.id;
        console.log(" Token :: " + this.token + " ::  Result ::: " + result);
        return result.token.id;
      }
      else if (result.error) {
        this.spinnerService.hideLoader();
        this.alertService.clearMessage();
        this.alertService.sendMessage(result.error.message, "error");
        console.log(result.error.message);
      }
    });
  }
  chooseOptionalPlan(optionalPlan: any) {
    console.log(optionalPlan);
    this.optionalPlanAmount = optionalPlan.amount;
    this.optionalPlanName = optionalPlan.name;
  }
  getPlans() {
    this.spinnerService.showLoader();
    this.activePlanList = [];
    this.fetchedPlansList = [];
    this.subscriptionPlansService.getPlans().subscribe(res => {
      this.isTest = res.test;
      if (this.isTest) {
        this.testAccountMessage = res.message;
      }
      console.log(res);
      if (this.isAdmin) {
        this.fetchedPlansList = res;
      } else {
        this.fetchedPlansList = res["availablePlans"];
        this.activePlanList = res["activePlan"];
        var temp = [];
        temp = res["optionalPlans"];
        temp.forEach(plan => {
          this.plans.push({ id: plan.planId, name: plan.planName, amount: plan.planAmount, planInterval: plan.planInterval === 'Monthly' ? 'month' : 'year' });
        });
        if (this.activePlanList.length === 0) {
          this.showActivePlan = false;
        }
        this.fetchedPlansList = this.fetchedPlansList.filter(function (el: { newDescription: any; planDescription: { slice: (arg0: number, arg1: number) => void; }; }) {
          return (el.newDescription = el.planDescription.slice(0, 120));
        });
        this.activePlanList = this.activePlanList.filter(function (el: { newDescription: any; planDescription: { slice: (arg0: number, arg1: number) => void; }; }) {
          return (el.newDescription = el.planDescription.slice(0, 120));
        });

        this.activePlanList.forEach((element, index) => {
          if (element.planName === "Customer Rep") {
            this.activePlanList[index].planName = "App Ambassador"
          }
          if (element.planName === "Customer" && element.planInterval === 'Yearly') {
            this.activePlanList[index].planName = "App Ambassador Plus"
          }
        });


        this.fetchedPlansList.forEach((element, index) => {
          if (element.planName === "Customer Rep") {
            this.fetchedPlansList[index].planName = "App Ambassador"
          }
          if (element.planName === "Customer" && element.planInterval === 'Yearly') {
            this.fetchedPlansList[index].planName = "App Ambassador Plus"
          }
        });

        var first = "P2P YEA";
        this.fetchedPlansList.sort(function (x, y) { return x.planName == first ? -1 : y.planName == first ? 1 : 0; });
        this.activePlanList.sort(function (x, y) { return x.planName == first ? -1 : y.planName == first ? 1 : 0; });
      }
      console.log(this.activePlanList);
      this.spinnerService.hideLoader();
    },
      err => {
        this.spinnerService.hideLoader();
      },
      () => {
      }
    );
  }
  setSelectedPlan(data: any) {
    console.log(data);
    this.stripeForm.reset();
    this.selectedPlan = data;
    console.log(this.selectedPlan);
  }
  viewCurrentPlan(plan: any) {
    this.currentPlan = new CurrentPlan();
    console.log(plan);
    this.currentPlan.activeStatus = plan.activeStatus;
    this.currentPlan.newDescription = plan.newDescription;
    this.currentPlan.planAmount = plan.planAmount;
    this.currentPlan.planDescription = plan.planDescription;
    this.currentPlan.planId = plan.planId;
    this.currentPlan.planInterval = plan.planInterval;
    this.currentPlan.planName = plan.planName;
    this.currentPlan.planType = plan.planType;
    this.currentPlan.purchaseDate = plan.purchaseDate;
    this.currentPlan.subscriptionId = plan.subscriptionId;
    this.currentPlan.transactionId = plan.transactionId;
    this.currentPlan.transactionRepository = plan.transactionRepository;
    this.currentPlan.validity = plan.validity;
  }
  cancelCurrentPlan() {
    console.log(this.selectedPlan);
    this.spinnerService.showLoader();
    this.subscriptionPlansService.cancelPlan(this.selectedPlan.subscriptionId).subscribe(res => {
      this.account
        .get()
        .toPromise()
        .then(response => {
          const account = response.body;
          if (account) {
            this.roleService.flushRoles();
            this.rolesService.clearRole();
            this.sessionStorage.clear('permissions')
            console.log("geting roles");
            console.log(account);
            this.userIdentity = account['role'];
            this.userIdentity.forEach(element => {
              this.roleService
                .addRole(element, []);
              this.rolesService.setRole(element);
            });
            this.roleService
              .addRole(account['permission'], []);
            this.rolesService.setRole(account['permission']);
            this.roleService.roles$.subscribe((data) => {
              this.sessionStorage.store('permissions', JSON.stringify(data));
            })
          }
          window.location.reload();
          this.router.navigate(["./dashboard/profile/subscription-plans"]);
        }).catch(err => {
          console.log("CATCH OF")
        });
    }, (err) => {
    }, () => {
      this.cancellationframe.hide();
      this.spinnerService.hideLoader();
      this.getPlans();
    });
  }
  getCards() {
    this.customers = [];
    this.spinnerService.showLoader();
    this.subscriptionPlansService.getCards().subscribe(res => {
      console.log(res);
      this.customers = res;
    }, (err) => {
    }, () => {
      this.spinnerService.hideLoader();
    });
  }
  deleteCard(rowData: any) {
    console.log(rowData);
    this.spinnerService.showLoader();
    this.subscriptionPlansService.deleteCard(rowData.cardId).subscribe(res => {
      console.log(res);
    }, (err) => {
    }, () => {
      this.stripeForm.get('name').setValidators([Validators.required]);
      this.stripeForm.get('name').updateValueAndValidity();
      this.selectedCard = [];
      this.selectedCardId = '';
      this.spinnerService.hideLoader();
      this.getCards();
    });
  }
  addCard(token: any) {
    this.spinnerService.showLoader();
    this.subscriptionPlansService.addCard(token).subscribe(res => {
      console.log(res);
    }, (err) => {
    }, () => {
      this.spinnerService.hideLoader();
    });
  }
  ngOnInit() {
    this.getPlans();
    this.referrer = null;
    this.subscriptionForm = this._fb.group({
      userType: ["", [Validators.required]],
      referrerCode: [""]
    });
    this.stripeForm = this.fb.group({
      name: ["", [Validators.required]]
    });
    this.stripeService.elements(this.elementsOptions).subscribe(elements => {
      this.elements = elements;
      if (!this.card) {
        this.card = this.elements.create("card", {
          style: {
            base: {
              iconColor: "#666EE8",
              color: "#FFFFF",
              lineHeight: "40px",
              fontWeight: 600,
              fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
              fontSize: "16px",
              "::placeholder": {
                color: "#FFFFF"
              }
            }
          }
        });
        this.card.mount("#card-element");
      }
    });
    if (this.activePlanList.length < 0 || this.fetchedPlansList.length < 0) {
      this.showDescription = true;
    }
  }
}
class CurrentPlan {
  activeStatus: any
  newDescription: any
  planAmount: any
  planDescription: any
  planId: any
  planInterval: any
  planName: any
  planType: any
  purchaseDate: any
  subscriptionId: any
  transactionId: any
  transactionRepository: any
  validity: any
}
