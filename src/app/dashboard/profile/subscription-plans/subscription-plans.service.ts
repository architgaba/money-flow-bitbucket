import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ApiCommonService } from "src/app/core/services/api-common.service";
@Injectable({
  providedIn: "root"
})
export class SubscriptionPlansService {
  constructor(private apiCommonService: ApiCommonService) { }
  buySubscription(body: any): Observable<any> {
    return this.apiCommonService.post('/subscription/create', body);
  }
  getUserReferenceType(): Observable<any> {
    return this.apiCommonService.get("/user/information/reference-type");
  }
  getPlans(): Observable<any> {
    return this.apiCommonService.get("/plan/fetch");
  }
  cancelPlan(planId: any): Observable<any> {
    return this.apiCommonService.put("/subscription/cancel?subscriptionId=" + planId, {});
  }
  getCards(): Observable<any> {
    return this.apiCommonService.get("/subscription/cards");
  }
  deleteCard(cardId: any): Observable<any> {
    return this.apiCommonService.delete("/subscription/delete-card?cardId=" + cardId);
  }
  addCard(token): Observable<any> {
    return this.apiCommonService.post("/subscription/add-card?token=" + token, {});
  }
}
