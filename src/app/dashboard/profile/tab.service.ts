import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
@Injectable({
    providedIn: 'root'
})
export class TabService {
    private currentIndex = new Subject<any>();
    sendMessage(message: any
    ) {
        this.currentIndex.next(message);
    }
    clearMessage() {
        this.currentIndex.next();
    }
    getMessage(): Observable<any> {
        return this.currentIndex.asObservable();
    }
}