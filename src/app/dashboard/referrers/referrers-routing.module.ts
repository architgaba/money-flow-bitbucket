import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewReferrersComponent } from './view-referrers/view-referrers.component';
import { NgxPermissionsGuard } from 'ngx-permissions';
const routes: Routes = [{
  path: '',
  canActivate: [NgxPermissionsGuard],
  component: ViewReferrersComponent,
  }];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReferrersRoutingModule { }
