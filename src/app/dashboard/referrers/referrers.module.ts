import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableModule } from 'primeng/table';
import { ReferrersRoutingModule } from './referrers-routing.module';
import { ViewReferrersComponent } from './view-referrers/view-referrers.component';
import { IconsModule, InputsModule, WavesModule, ModalModule, ButtonsModule, TooltipModule } from 'ng-uikit-pro-standard';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { NgxPermissionsModule } from 'ngx-permissions';
import { MultiSelectModule } from 'primeng/primeng';
import { PipeModule } from 'src/app/core/pipes/pipe.module';
@NgModule({
  declarations: [ViewReferrersComponent],
  imports: [
    CommonModule,
    PipeModule, 
    ReferrersRoutingModule,
    IconsModule,
    TableModule,
    ModalModule,
    WavesModule,
    InputsModule,
    ButtonsModule,
    TooltipModule,
    FormsModule,
    ReactiveFormsModule,
    BreadcrumbModule,
    MultiSelectModule,
    NgxPermissionsModule.forChild()
  ]
})
export class ReferrersModule { }
