import { Injectable } from '@angular/core';
import { ApiCommonService } from 'src/app/core/services/api-common.service';
import { Observable } from 'rxjs';
@Injectable({
    providedIn: 'root'
})
export class ReferrerService {
    constructor(private apiCommonService: ApiCommonService) { }
    getAllReferrer(): Observable<any> {
        return this.apiCommonService.get('/admin/customer-reps');
    }
    getAllCustomers(): Observable<any> {
        return this.apiCommonService.get('/admin/all-customers');
    }
    createNewReferrer(body: any): Observable<any> {
        return this.apiCommonService.post('/admin/add-customer-rep', body);
    }
    updateReferrerAccount(action: any, id: any): Observable<any> {
        return this.apiCommonService.put('/admin/customer-reps/update-account?id=' + id + '&action=' + action, {});
    }
    resendMail(id: any): Observable<any> {
        return this.apiCommonService.post("/admin/resend-activation-mail/customer-rep/" + id, {});
    }
    updateReferrerEmail(body: any): Observable<any> {
        return this.apiCommonService.put('/admin/update-email', body);
    }
    deleteReferrers(userId: any): Observable<any> {
        return this.apiCommonService.delete('/admin/delete-user/' + userId);
    }

    p2pDistributorSelfRegister(body: any): Observable<any> {
        return this.apiCommonService.post('/admin/add-p2p-distributor/self-register', body);
    }
}