import { Component, OnInit, ViewChild } from "@angular/core";
import { ReferrerService } from "./referrer.service";
import { AlertService } from "src/app/core/services/alert.service";
import { SpinnerService } from "src/app/core/services/spinner.service";
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { ModalDirective } from "ng-uikit-pro-standard";
import { SelectItem } from "primeng/api";
import { DataTable } from "primeng/primeng";
import { DashboardService } from "../../dashboard.service";
import { element } from "@angular/core/src/render3";
import { CustomerService } from "../../customer/view-customers/customer.service";
@Component({
  selector: "app-view-referrers",
  templateUrl: "./view-referrers.component.html",
  styleUrls: ["./view-referrers.component.scss"]
})
export class ViewReferrersComponent implements OnInit {
  @ViewChild("addframe") frame: ModalDirective;
  @ViewChild("dt1") dt: DataTable;
  @ViewChild("dt2") dt2: DataTable;
  @ViewChild('actionFrame') actionFrame: ModalDirective;
  @ViewChild('updateEmail') updateEmail: ModalDirective;
  @ViewChild('deletionFrame') deletionFrame: ModalDirective;

  statusList: SelectItem[] = [];
  rankType: SelectItem[] = [];
  userType:SelectItem[]=[];
  showCustomers: any = false;
  userIdToDelete = null;
  selectedId: any;
  selectedReferrerName: any;
  addReferrerForm: FormGroup;
  selectedCustomerName: any;
  levelReport=[];
  showLevelsReport:any=false;
  sponsorNames: SelectItem[]=[];
  levelType: SelectItem[]=[];
  tableColumns = [
    { field: "name", header: "Name",width:"110px" },
    { field: "email", header: "Email" ,width:"110px" },
    { field: "contactNumber", header: "Contact",width:"110px"  },
    { field: "status", header: "Status",width:"110px"  },
    { field: "accountType", header: "Acc Type",width:"100px"  },
    { field: "isAppAmbassador", header: "AAP",width:"100px"  },
    { field: "referrerCode", header: "SC",width:"80px"  },
    { field: "customerCount", header: "App Cust",width:"70px"  },
    { field: "customerRepCount", header: "AA",width:"70px"  },
    { field: "p2PYEACount", header: "YEA",width:"70px"  },
    { field: "p2pDistributorCount", header: "LM",width:"70px"  },
    { field: "directAAPCount", header: "AAP",width:"70px"  },
    { field: "teamSizeCount", header: "Team",width:"70px"  },
    { field: "recognitionCategory", header: "Title",width:"90px"  },
    { field: "action", header: "Actions",width:"270px"  }
  ];
  customerTableColumns = [
    { field: "name", header: "Name" ,width:"110px" },
    { field: "email", header: "Email" ,width:"110px" },
    { field: "contactNumber", header: "Contact",width:"110px"  },
    { field: "status", header: "Status",width:"110px"  },
    { field: "userType", header: "Acc Type" ,width:"110px" },
    { field: "isAppAmbassador", header: "AAP",width:"100px"  },
    // { field: "accountType", header: "Acc Type" ,width:"100px" },
    { field: "referrerCode", header: "SC",width:"80px"  },
    { field: "customerCount", header: "App Cust",width:"70px"  },
    { field: "customerRepCount", header: "AA" ,width:"70px" },
    { field: "p2PYEACount", header: "YEA",width:"70px"  },
    { field: "p2pDistributorCount", header: "LM" ,width:"70px" },
    { field: "directAAPCount", header: "AAP",width:"70px"  },
    { field: "teamSizeCount", header: "Team",width:"70px"  },
    { field: "recognitionCategory", header: "Title",width:"90px"  },
    { field: "action", header: "Actions" ,width:"220px" }
  ];
  referrerList = [];
  customerList = [];
  selectedUser: any;
  toggleAction: any;
  levelReportColumns:any[];
  role: string;
  currentReferrer = [];
  selectedReferrerId: any;
  accountType: SelectItem[];
  bool:SelectItem[];
  email_id = new FormControl('');
  userId: any;
  showReferralCodeField: boolean;
  constructor(
    private _fb: FormBuilder,
    private alertService: AlertService,
    private spinnerService: SpinnerService,
    private referrerService: ReferrerService,
    private dashboardService: DashboardService,
    private customerService:CustomerService
  ) {
  }
  ngOnInit() {
    this.showReferralCodeField = false;
    this.dashboardService.clearMessage();
    this.dashboardService.sendMessage(2);
    this.email_id.setValidators([Validators.required, Validators.email]);
    this.role = 'referrer';
    this.addReferrerForm = this._fb.group({
      referrerId: [""],
      contactNumber: ['', [Validators.pattern(/^\(\d{3}\)\s\d{3}-\d{4}$/)]],
      email: ["", [Validators.required, Validators.email]],
      firstName: ["", [Validators.required]],
      lastName: [""],
      test: ["", [Validators.required]],
      p2pDistributer: [""],
      referralCode: [null]
    });
    this.statusList = [
      { label: 'Active', value: 'ACTIVE' },
      { label: 'Inactive', value: 'INACTIVE' },
      { label: 'Archived', value: 'ARCHIVED' },
      { label: 'Activation Pending', value: 'ACTIVATION PENDING' },
      { label: 'Subscription Pending', value: 'SUBSCRIPTION PENDING' },
      { label: 'Subscription Expired', value: 'SUBSCRIPTION EXPIRED' },
      { label: 'Subscription Canceled', value: 'SUBSCRIPTION CANCELLED' }
    ];
    this.accountType = [
      { label: 'Free', value: 'FREE' },
      { label: 'App Ambassadors', value: 'App Ambassador' },
      { label: 'Legacy Members', value: 'Legacy Member' },
      { label: 'P2P YEA', value: 'P2P YEA' }
    ];
    this.userType = [
      { label: 'App Ambassadors', value: 'App Ambassador' },
      { label: 'Legacy Members', value: 'Legacy Member' },
      { label: 'Customers', value: 'Customer' },
      { label: 'P2P YEA', value: 'P2P YEA' }
    ];
    this.rankType = [
      { label: 'N.A.', value: 'N.A' },
      { label: 'Bronze', value: 'Bronze' },
      { label: 'Diamond', value: 'Diamond' },
      { label: 'Platinum', value: 'Platinum' },
    
    ];
    this.levelReportColumns = [
      { field: "level", header: "Level", width: "90px" },
      {field: "referrerName", header: "Sponsor Name", width: "110px" },
      { field: "name", header: "Name", width: "110px" },
      {field: "email", header: "Email ID", width: "110px" },
      {field: "contactNumber", header: "Contact Number", width: "110px" },
      { field: "userType", header: "Acc Type", width: "110px" },
      { field: "isAppAmbassador", header: "AAP", width: "80px" },
      {field: "status", header: "Status", width: "110px" },
      {field: "referrerCode", header: "Sponsor Code", width: "110px" },
      { field: "customerCount", header: "App Cust", width: "80px" },
      { field: "customerRepCount", header: "AA", width: "80px" },
      { field: "p2PYEACount", header: "YEA", width: "80px" },
      { field: "p2pDistributorCount", header: "LM", width: "80px" },
      { field: "directAAPCount", header: "AAP", width: "80px" },
      { field: "teamSizeCount", header: "Team", width: "80px" },
      { field: "recognitionCategory", header: "Title", width: "90px" },


    ]
    this.bool = [
      { label: 'Yes', value: 'Yes' },
      { label: 'No', value: 'No' },
    ];
    this.getAllReferrers();
  }
  setUserToDelete(rowData: any, role) {
    this.userIdToDelete = rowData.userId;
    this.selectedUser = rowData
    if (role === 'referrer') {
      this.role = 'referrer';
    }
    else {
      this.role = 'customer';
    }
  }
  deleteUser() {
    this.spinnerService.showLoader();
    this.referrerService.deleteReferrers(this.userIdToDelete).subscribe(res => {
      this.spinnerService.hideLoader();
      this.alertService.clearMessage();
      this.alertService.sendMessage(res.message, 'success');
    }, (err) => {
      this.alertService.clearMessage();
      this.alertService.sendMessage(err.message, 'error');
    }, () => {
      this.deletionFrame.hide();
      this.userIdToDelete = null;
      this.getAllReferrers();
      this.spinnerService.hideLoader();
    });
  }
  updateReferrerEmail() {
    if (this.email_id.valid) {
      this.spinnerService.showLoader();
      const body = {
        "emailId": this.email_id.value,
        "userId": this.selectedReferrerId
      };
      this.referrerService.updateReferrerEmail(body).subscribe(res => {
        this.alertService.clearMessage();
        this.alertService.sendMessage(res.message, "success");
      }, () => {
        this.email_id = new FormControl('');
        this.updateEmail.hide();
        this.getAllReferrers();
        this.spinnerService.hideLoader();
      });
    }
    else {
      this.email_id.markAsDirty();
    }
  }
  showUserDetails(data: any, action: any, role: any) {
    this.selectedUser = data;
    this.toggleAction = action;
    if (role === 'referrer') {
      this.role = 'referrer';
      this.selectedId = data.userId;
    }
    else {
      this.role = 'customer';
      this.selectedId = data.userId;
    }
  }
  resendMail(rowData: any) {
    this.spinnerService.showLoader();
    this.referrerService.resendMail(rowData.userId).subscribe(
      res => {
        console.log(res);
        this.alertService.clearMessage();
        this.alertService.sendMessage(res.message, "success");
      },
      err => { },
      () => {
        this.spinnerService.hideLoader();
      }
    );
  }
  selectedReferrer(rowData: any) {
    this.email_id.markAsUntouched();
    this.customerList = [];
    this.currentReferrer = rowData;
    this.selectedId = rowData.userId;
    this.selectedReferrerId = rowData.userId;
    this.selectedReferrerName = rowData.name;
    this.referrerList.forEach(referrer => {
      console.log(referrer);
      console.log(referrer.userId + "" + this.selectedId);
      if (referrer.userId === this.selectedId) {
        this.customerList = referrer["customers"];
        this.customerList.forEach((element,index)=>{
          if( element.accountType==='PAID'&&!element.p2PYEA){
            this.customerList[index].accountType='App Ambassador'
          }
          if( element.userType==='Customer-Rep'&&!element.p2PYEA){
            this.customerList[index].userType='App Ambassador'
          }

          if( element.accountType==='PAID'&&element.p2PYEA){
            this.customerList[index].accountType='P2P YEA'
          }
          if( element.userType==='Customer-Rep'&&element.p2PYEA){
            this.customerList[index].userType='P2P YEA'
          }
          if( element.userType==='P2P Distributor'){
            this.customerList[index].userType='Legacy Member'
          }
            if(element.isAppAmbassador){
              this.customerList[index].isAppAmbassador='Yes'
            }else{
              this.customerList[index].isAppAmbassador='No'
            }
        })
      }
    });
    console.log(this.customerList);
  }
  selectedPlan(value: any) {
    this.showReferralCodeField = false;
    if (value === '3') {
      this.showReferralCodeField = true;
    }
    this.addReferrerForm.controls.test.setValue(value);
  }
  updateReferrerAccount() {
    this.spinnerService.showLoader();
    this.referrerService.updateReferrerAccount(this.toggleAction, this.selectedId).subscribe(res => {
      console.log(res);
    }, (err) => {
    }, () => {
      this.getAllReferrers();
      this.spinnerService.hideLoader();
      this.actionFrame.hide();
    });
  }
  setNewCustomerList(userId: any) {
    this.customerList = [];
    console.log(userId);
    console.log(this.referrerList);
    this.referrerList.forEach(referrer => {
      if (referrer.userId === userId){
        this.customerList.forEach((element,index)=>{
          this.customerList = referrer["customers"];
          if( element.accountType==='PAID'&&!element.p2PYEA){
            this.customerList[index].accountType='App Ambassador'
          }
          if( element.userType==='Customer-Rep'&&!element.p2PYEA){
            this.customerList[index].userType='App Ambassador'
          }
          if( element.accountType==='PAID'&&element.p2PYEA){
            this.customerList[index].accountType='P2P YEA'
          }
          if( element.userType==='Customer-Rep'&&element.p2PYEA){
            this.customerList[index].userType='P2P YEA'
          }

          if( element.userType==='P2P Distributor'){
            this.customerList[index].userType='Legacy Member'
          }
            if(element.isAppAmbassador){
              this.customerList[index].isAppAmbassador='Yes'
            }else{
              this.customerList[index].isAppAmbassador='No'
            }
        })
      }
       
    });
  }
  getAllReferrers() {
    this.referrerList = [];
    this.spinnerService.showLoader();
    this.referrerService.getAllReferrer().subscribe(
      res => {
        console.log(res);
        res.forEach((element,index) => {
          this.referrerList.push(element);
          if( element.accountType==='PAID'&&!element.p2PYEA){
            this.referrerList[index].accountType='App Ambassador'
          }
          if( element.accountType==='PAID'&&element.p2PYEA){
            this.referrerList[index].accountType='P2P YEA'
          }
          if( element.accountType==='P2P Distributor'){
            this.referrerList[index].accountType='Legacy Member'
          }
            
            if(element.isAppAmbassador){
              this.referrerList[index].isAppAmbassador='Yes'
            }else{
              this.referrerList[index].isAppAmbassador='No'
            }
        });
        this.referrerList.forEach(element => {
          if (element.customerCount === 0 && element.customerRepCount === null && element.p2pDistributorCount === null) {
            element.customerCount = 0;
            element.customerRepCount = 0;
            element.p2pDistributorCount = 0;
          }

        })
      },
      err => { },
      () => {
        if (this.role === 'referrer') {
          this.dt.reset();
        }
        else {
          this.dt2.reset();
        }
        this.spinnerService.hideLoader();
        if (this.role === 'customer') {
          this.setNewCustomerList(this.selectedReferrerId);
        }
      }
    );
  }
  createNewReferrer() {
    if (this.addReferrerForm.valid) {
      this.spinnerService.showLoader();
      const body = {
        contactNumber: this.addReferrerForm.controls.contactNumber.value,
        email: this.addReferrerForm.controls.email.value,
        firstName: this.addReferrerForm.controls.firstName.value,
        lastName: this.addReferrerForm.controls.lastName.value,
        accountType: this.addReferrerForm.controls.test.value,
        referralCode: this.addReferrerForm.controls.referralCode.value,
      };
      this.referrerService.createNewReferrer(body).subscribe(
        res => {
          console.log(res);
          this.alertService.clearMessage();
          this.alertService.sendMessage(res.message, "success");
        },
        err => {
        },
        () => {
          this.spinnerService.hideLoader();
          this.getAllReferrers();
          this.frame.hide();
        }
      );
    } else {
      Object.keys(this.addReferrerForm.controls).forEach(key => {
        this.addReferrerForm.get(key).markAsDirty();
      });
    }
  }


  getLevelReport(row) {
    this.selectedCustomerName=row.name
    this.levelReport = [];
    this.sponsorNames=[];
    this.levelType=[];
    this.spinnerService.showLoader();
    this.customerService.getLevelReport(true, row.userId).subscribe(
      res => {
        console.log(res);
        res.forEach((element,index)  => {
          this.levelReport.push(element);
          if(!this.levelType.some(x => x.value ===  'Level'+' '+this.levelReport[index].level)) {
            this.levelType.push({label:'Level'+' '+this.levelReport[index].level,value:'Level'+' '+this.levelReport[index].level})
         }
         if(!this.sponsorNames.some(x => x.value ===this.levelReport[index].referrerName)) {
          this.sponsorNames.push({label:this.levelReport[index].referrerName,value:this.levelReport[index].referrerName})
        }

        if (element.userType === 'Customer-Rep' && !element.p2PYEA) {
          this.levelReport[index].userType = "App Ambassador"
        }
        if (element.userType === 'Customer-Rep' && element.p2PYEA) {
          this.levelReport[index].userType = "P2P YEA"
        }
        if (element.userType === 'P2P Distributor') {
          this.levelReport[index].userType = "Legacy Member"
        }
        if (element.isAppAmbassador) {
          this.levelReport[index].isAppAmbassador = 'Yes'
        } else {
          this.levelReport[index].isAppAmbassador = 'No'
        }
        });
        this.levelReport.forEach((element,index)=>{
          this.levelReport[index].level='Level'+' '+this.levelReport[index].level
        })
        this.showLevelsReport=true;
        this.spinnerService.hideLoader();
      },
      err => { },
      () => {
        // this.dt.reset();
        this.spinnerService.hideLoader();
      }
    );
  }
}
