import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MultiSelectModule } from 'primeng/primeng';
import { TransactionRoutingModule } from './transaction-routing.module';
import { TransactionComponent } from './transaction/transaction.component';
import { TableModule } from 'primeng/table';
import { IconsModule, ButtonsModule, InputsModule, WavesModule, ModalModule, TooltipModule } from 'ng-uikit-pro-standard';
@NgModule({
  declarations: [TransactionComponent],
  imports: [
    CommonModule,
    TransactionRoutingModule,
    TableModule,
    MultiSelectModule,
    IconsModule, ButtonsModule, InputsModule, WavesModule, ModalModule, MultiSelectModule,TooltipModule
  ]
})
export class TransactionModule { }
