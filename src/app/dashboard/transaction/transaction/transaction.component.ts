import { Component, OnInit } from '@angular/core';
import { TransactionService } from './transaction.service';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { DashboardService } from '../../dashboard.service';
import { SortEvent } from 'primeng/primeng';
import * as moment from 'moment';
import { element } from '@angular/core/src/render3';
@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent implements OnInit {
  transactionList = [];
  tableColumns = [
    { field: "name", header: "Account Name" ,width:"150px"},
    { field: "referredBy", header: "Referred By",width:"150px" },
    { field: "referrerCode", header: "Referral Code",width:"150px" },
    { field: "transactionId", header: "Transaction ID",width:"150px" },
    { field: "planType", header: "Plan Type",width:"150px" },
    { field: "transactionType", header: "Transaction Status",width:"150px" },
    { field: "transactionDate", header: "Transaction Date",width:"150px" },
    { field: "transactionAmount", header: "Transaction Amount",width:"150px" }
  ];
  constructor(private spinnerService: SpinnerService,
    private transactionService: TransactionService,
    private dashboardService: DashboardService) { }
  ngOnInit() {
    this.dashboardService.clearMessage();
    this.dashboardService.sendMessage(5);
    this.getAllTransactions();
  }
  getAllTransactions() {
    this.spinnerService.showLoader();
    this.transactionService.getAllTransactions().subscribe(res => {
      console.log(res);
      this.transactionList = res;
      this.transactionList.forEach((element,index)=>{
        if(element.planType==='Customer Rep'){
          this.transactionList[index].planType="App Ambassador"
        }
      })
    }, (err) => {
    }, () => {
      this.spinnerService.hideLoader();
    });
  }
  customSort(event: SortEvent) {
    if(event.field==="transactionDate"){
      this.transactionList.sort((a, b): number =>{
        let formatedA = moment(a.transactionDate, "MM/DD/YYYY").format('YYYY-MM-DD');
        let formatedB = moment(b.transactionDate, "MM/DD/YYYY").format('YYYY-MM-DD');
        let result: number = -1;
  
        if (moment(formatedB).isBefore(formatedA, 'day')) result = 1;
        return result * event.order;
      });

    }


  }
}
