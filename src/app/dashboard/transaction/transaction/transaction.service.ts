import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ApiCommonService } from "src/app/core/services/api-common.service";
@Injectable({
    providedIn: "root"
})
export class TransactionService {
    constructor(private apiCommonService: ApiCommonService) { }
    getAllTransactions(): Observable<any> {
        return this.apiCommonService.get('/subscription/fetch-transactions');
    }
}