import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { SpinnerService } from '../core/services/spinner.service';
import { ApiCommonService } from '../core/services/api-common.service';
import { AlertService } from '../core/services/alert.service';
import { DataTable } from 'primeng/primeng';

@Component({
  selector: 'app-directory',
  templateUrl: './directory.component.html',
  styleUrls: ['./directory.component.scss']
})
export class DirectoryComponent implements OnInit {
  displayDialog: boolean;
  selectedUser: User;
  sortKey: string;
  sortField: string;
  sortOrder: number;
  users: User[];
  customerList = [];
  statusList: any = [];
  accountType: any[];
  @ViewChild("dt") dt: DataTable;
  tableColumns = [
    { field: "name", header: "Name" },
    { field: "emailId", header: "User Type" },
    { field: "contactNumber", header: "Phone Number" },
    { field: "creationDate", header: "Creation Date" },
    { field: "referrerCode", header: "Referral Code" },
    { field: "userStatus", header: "Account Status" },
    { field: "accountType", header: "Account Type" },
    { field: "customerCount", header: "Total Customers" },
    { field: "referrerCount", header: "Total Customer Reps" },
    { field: "p2pDistributerCount", header: "Total P2P Distributors" },
    { field: "userReferrerName", header: "Referred By" },
    { field: "planType", header: "Subscription Plan" },
    { field: "userId", header: "User Id" }

  ];

  constructor(private spinnerService: SpinnerService, private apiCommonService: ApiCommonService) { }
  selectUser(event: Event, user: User) {
    this.selectedUser = user;
    this.getCustomerList(this.selectedUser.userId)
    event.preventDefault();
  }
  getCustomerList(userId) {
    this.customerList = [];
    this.spinnerService.showLoader();
    this.apiCommonService.get('/admin/customers/' + userId).subscribe(
      res => {
        console.log(res);
        res.forEach(element => {
          this.customerList.push(element);
        });
        this.spinnerService.hideLoader();
      },
      err => { },
      () => {
        this.dt.reset();
        this.spinnerService.hideLoader();
        this.displayDialog = true;
      }
    );
  }
  ngOnInit() {
    this.statusList = [
      { label: 'Active', value: 'ACTIVE' },
      { label: 'Inactive', value: 'INACTIVE' },
      { label: 'Archived', value: 'ARCHIVED' },
      { label: 'Activation Pending', value: 'ACTIVATION PENDING' },
      { label: 'Subscription Pending', value: 'SUBSCRIPTION PENDING' },
      { label: 'Subscription Expired', value: 'SUBSCRIPTION EXPIRED' },
      { label: 'Subscription Canceled', value: 'SUBSCRIPTION CANCELLED' }
    ];
    this.accountType = [
      { label: 'P2P Distributor', value: 'P2P DISTRIBUTOR' },
      { label: 'Customer', value: 'CUSTOMER' },
      { label: 'Customer Rep', value: 'CUSTOMER REP' },
      { label: 'Prospect', value: 'PROSPECT' },
      { label: 'Super Admin', value: 'SUPER ADMIN' },
      { label: 'Admin', value: 'ADMIN' },
      { label: 'Test Customer', value: 'TEST CUSTOMER' },
      { label: 'Test Customer Rep', value: 'TEST REFERRER' }
    ];
    this.getUsers();
  }
  onDialogHide() {
    this.selectedUser = null;
    this.displayDialog = false;
  }
  getUsers() {
    this.users = [];
    this.spinnerService.showLoader();
    this.apiCommonService.get('/admin/all-users').subscribe(res => {
      this.users = res;
      console.log(res)
    }, (err) => {
    }, () => {
      console.log(this.users);
      this.spinnerService.hideLoader();
    });
  }
}
export interface User {
  userId: any
  name: string
  emailId: string
  contactNumber: string
  creationDate: string
  referrerCode: string
  userStatus: string
  accountType: string
  customerCount: string
  referrerCount: string
  p2pDistributerCount: string
  userReferrerName: string
  planType: string
  userName: string
  p2PYEACount: string
  directAAPCount:string
  teamSizeCount: string
}

