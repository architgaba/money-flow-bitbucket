import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DirectoryRoutingModule } from './directory-routing.module';
import { DirectoryComponent } from './directory.component';
import { PanelModule, DialogModule, MultiSelectModule } from 'primeng/primeng';
import {DropdownModule} from 'primeng/dropdown';
import { DataViewModule } from 'primeng/dataview';
import { ModalModule, InputsModule } from 'ng-uikit-pro-standard';
import { FormsModule } from '@angular/forms';
import {TooltipModule} from 'primeng/tooltip';
import { TableModule } from 'primeng/table';
@NgModule({
  declarations: [DirectoryComponent],
  imports: [
    CommonModule,
    DirectoryRoutingModule,
    PanelModule,
    DataViewModule,
    DialogModule,
    DropdownModule,
    ModalModule,
    FormsModule,
    InputsModule,
    TooltipModule,    
    TableModule,
    MultiSelectModule
  ]
})
export class DirectoryModule { }
