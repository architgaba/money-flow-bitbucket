import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthServerProvider } from '../core/services/auth-jwt.service';
@Component({
  selector: 'app-error-component',
  templateUrl: './error-component.component.html',
  styleUrls: ['./error-component.component.scss']
})
export class ErrorComponent implements OnInit {
  errorMessage: string;
  errorCode: string;
  constructor(private route: ActivatedRoute) {
  }
  ngOnInit() {
    window.scroll(0, 0);
    this.route.queryParams.subscribe(params => {
      console.log(params);
      this.errorMessage = params["message"];
      this.errorCode = params["error"];
    });
    console.log(this.errorCode);
    console.log(this.errorMessage);
  }
}
