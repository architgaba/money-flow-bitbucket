import { Component, OnInit, ViewChild } from "@angular/core";
import { AuthServerProvider } from "src/app/core/services/auth-jwt.service";
import { Router } from "@angular/router";
import { Principal } from "src/app/core/services/principal.service";
import { SubscriptionPlansService } from "src/app/dashboard/profile/subscription-plans/subscription-plans.service";
import { ApiCommonService } from "src/app/core/services/api-common.service";
import { SpinnerService } from "src/app/core/services/spinner.service";
import { AlertService } from "src/app/core/services/alert.service";
import { FormControl } from "@angular/forms";
import { ModalDirective } from "ng-uikit-pro-standard";
@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit {
  name: any;
  referralCode: any
  userReferenceType: any;
  mainComponentLoaded:boolean=false;
  p2pLinkVal: any;
  isLogin: Boolean;
  p2pLink = new FormControl('');
  onNavigate() {
  }
  @ViewChild('basicModal') p2pFrame: ModalDirective;
  constructor(
    private principal: Principal,
    private router: Router,
    private authServerProvider: AuthServerProvider, private subscriptionPlanService: SubscriptionPlansService,
    private apiCommonService: ApiCommonService,
    private spinnerService: SpinnerService,
    private alertService: AlertService
  ) {
    this.isLogin = false;
  }
  ngOnInit() {
    var token = window.sessionStorage.getItem("ngx-webstorage|authenticationtoken");
    console.log(token);
    this.principal.currentMessage.subscribe(val=>{
      this.mainComponentLoaded=val;
    })
    this.principal.currentUser.subscribe(message => (this.name = message));
    this.principal.currentUserReferrerCode.subscribe(message => (this.referralCode = message));
    this.principal.currentUserReferenceType.subscribe(message => (this.userReferenceType = message));
    this.principal.p2pLink.subscribe(message => { this.p2pLinkVal = message; this.p2pLink.setValue(message) });
    console.log(this.name);
    console.log(this.referralCode);
    console.log(this.userReferenceType)
    console.log(this.p2pLinkVal)
  }
  getRefernceType() {
    this.subscriptionPlanService.getUserReferenceType().subscribe(res => {
      console.log(res.p2pLink)
      this.principal.p2pLink.next(res.p2pLink);
      this.p2pLink.setValue(res.p2pLink);
    }, (err) => {
    }, () => {
      this.p2pFrame.hide();
      this.spinnerService.hideLoader();
    })
  }
  updatep2pLink() {
    this.spinnerService.showLoader();
    console.log(this.p2pLink.value);
    this.apiCommonService.put('/user/information/update-p2pLink', this.p2pLink.value).subscribe(res => {
      this.alertService.clearMessage();
      this.alertService.sendMessage(res.message, "success");
      this.getRefernceType();
    }, (err) => {
    }), () => {
      this.spinnerService.hideLoader();
    };
  }
  logout() {
    this.authServerProvider.logout().subscribe();
    this.router.navigate(["home"]);
  }
}
