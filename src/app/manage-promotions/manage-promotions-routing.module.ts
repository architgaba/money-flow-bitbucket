import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManagePromotionsComponent } from './manage-promotions/manage-promotions.component';

const routes: Routes = [{
  path: '',
  component: ManagePromotionsComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagePromotionsRoutingModule { }
