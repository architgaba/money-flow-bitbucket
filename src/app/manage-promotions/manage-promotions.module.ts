import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManagePromotionsRoutingModule } from './manage-promotions-routing.module';
import { ManagePromotionsComponent } from './manage-promotions/manage-promotions.component';
import { ModalModule, WavesModule, InputsModule, ButtonsModule, TooltipModule } from 'ng-uikit-pro-standard';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MultiSelectModule } from 'primeng/primeng';
import { NgxPermissionsModule } from 'ngx-permissions';
import { TableModule } from 'primeng/table';
import { NgSelectModule } from '@ng-select/ng-select';
@NgModule({
  declarations: [ManagePromotionsComponent],
  imports: [
    CommonModule,
    ManagePromotionsRoutingModule,
    TableModule,
    ModalModule,
    WavesModule,
    InputsModule,
    ButtonsModule,
    TooltipModule,
    FormsModule,
    ReactiveFormsModule,
    MultiSelectModule,
    NgxPermissionsModule.forChild(),
    NgSelectModule
  ]
})
export class ManagePromotionsModule { }
