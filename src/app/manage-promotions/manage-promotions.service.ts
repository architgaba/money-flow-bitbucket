import { Injectable } from "@angular/core";
import { Observable, of } from 'rxjs';
import { ApiCommonService } from "src/app/core/services/api-common.service";
import { HttpClient } from "@angular/common/http";
export interface Person {
    id: string;
    isActive: boolean;
    age: number;
    name: string;
    gender: string;
    company: string;
    email: string;
    phone: string;
    disabled?: boolean;
}
@Injectable({
    providedIn: "root"
})
export class ManagePromotionsService {
    constructor(private apiCommonService: ApiCommonService, private http: HttpClient) { }
    getAllUsers(): Observable<any> {
        return this.apiCommonService.get('/admin/user-with-group');
    }

    getAllUserEligibleForPromotion(): Observable<any> {
        return this.apiCommonService.get('/admin/users-eligible-for-Promotion');
    }

    markInEligibleForPromotion(userId: any): Observable<any> {
        return this.apiCommonService.put('/admin/make-ineligible-for-Promotion', userId);
    }

    makeUsersEligibleForPromotion(selectedUsers: any): Observable<any> {
        return this.apiCommonService.post('/admin/make-eligible-for-Promotion', selectedUsers);
    }

}

