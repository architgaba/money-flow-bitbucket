import { Component, OnInit, ViewChild } from '@angular/core';
import { AlertService } from 'src/app/core/services/alert.service';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { DashboardService } from '../../../app/dashboard/dashboard.service';
import { DataTable, SelectItem } from 'primeng/primeng';
import { ModalDirective } from 'ng-uikit-pro-standard';
import { ManagePromotionsService, Person } from 'src/app/manage-promotions/manage-promotions.service';
@Component({
  selector: 'app-manage-promotions',
  templateUrl: './manage-promotions.component.html',
  styleUrls: ['./manage-promotions.component.scss']
})
export class ManagePromotionsComponent implements OnInit {
  selectedUsers = [];
  userIdToDelete = null;
  selectedUser: any;
  @ViewChild("dt1") dt: DataTable;
  @ViewChild('deletionFrame') deletionFrame: ModalDirective;
  @ViewChild("frame") frame: ModalDirective;
  constructor(
    private alertService: AlertService,
    private spinnerService: SpinnerService,
    private managePromotionsService: ManagePromotionsService,
    private dashboardService: DashboardService
  ) {
  }
  tableColumns = [
    { field: "firstName", header: "First Name", width: "100px" },
    { field: "lastName", header: "Last Name", width: "100px" },
    { field: "contactNumber", header: "Contact", width: "100px" },
    { field: "email", header: "Email", width: "100px" },
    { field: "referrerName", header: "Sponsor", width: "120px" },
    { field: "appAmbassadorPlusCount", header: "AAP", width: "70px" },
    { field: "p2PYEACount", header: "YEA", width: "70px" },
    { field: "teamSizeCount", header: "Team", width: "70px" },
    { field: "recognitionCategory", header: "Title", width: "90px" },
    { field: "creationDate", header: "Enrollment Date", width: "110px" },
    { field: "action", header: "Actions", width: "100px" }
  ];
  sponsorName: SelectItem[] = [];
  rankType: SelectItem[] = [];
  usersList = [];
  eligibleUsers = [];
  ngOnInit() {
    this.dashboardService.clearMessage();
    this.dashboardService.sendMessage(13);
    this.getAllUsers();
    this.getAllUserEligibleForPromotion();
    this.rankType = [
      { label: 'N.A.', value: 'N.A' },
      { label: 'Bronze', value: 'Bronze' },
      { label: 'Diamond', value: 'Diamond' },
      { label: 'Platinum', value: 'Platinum' },

    ];
  }
  getAllUsers() {
    this.usersList = [];
    this.spinnerService.showLoader();
    this.managePromotionsService.getAllUsers().subscribe(
      res => {
        res.forEach(element => {
          this.usersList = [...this.usersList, {
            userId: element.userId,
            name: element.name,
            type: element.type
          }];
        });
        this.spinnerService.hideLoader();
      },
      err => { 
        this.spinnerService.hideLoader();
      },
      () => {
        this.dt.reset();
        console.log(this.usersList);
      }
    );
  }

  makeUsersEligibleForPromotion() {
    console.log(this.selectedUsers);
    if (this.selectedUsers.length == 0) {
      return;
    }
    this.spinnerService.showLoader();
    this.managePromotionsService.makeUsersEligibleForPromotion(this.selectedUsers).subscribe(
      res => {
        this.frame.hide();
        this.getAllUserEligibleForPromotion();
        this.getAllUsers();
        this.spinnerService.hideLoader();
        this.alertService.sendMessage(res.message,"success")
      },
      (err) => {
        this.alertService.sendMessage(err.message,"error")
        this.spinnerService.hideLoader();
       },
      () => {
        this.dt.reset();
        this.selectedUsers = [];
      }
    );
  }
  reset(){
    this.selectedUsers=[];
  }
  getAllUserEligibleForPromotion() {
    this.eligibleUsers = [];
    this.spinnerService.showLoader();
    this.managePromotionsService.getAllUserEligibleForPromotion().subscribe(
      res => {
        res.forEach((element, index) => {
          this.eligibleUsers.push(element);
          if (!this.sponsorName.some(x => x.value === this.eligibleUsers[index].referrerName)) {
            this.sponsorName.push({ label: this.eligibleUsers[index].referrerName, value: this.eligibleUsers[index].referrerName })
          }
        });
        this.spinnerService.hideLoader();
      },
      err => { this.spinnerService.hideLoader();},
      () => {
        
        this.dt.reset();
        console.log(this.usersList);
      }
    );
  }

  setUserToDelete(rowData: any) {
    this.userIdToDelete = rowData.userRegistrationId;
    this.selectedUser = rowData
    console.log(this.userIdToDelete);
    console.log(this.selectedUser);
  }

  markInEligibleForPromotion() {
    this.spinnerService.showLoader()
    this.managePromotionsService.markInEligibleForPromotion([this.userIdToDelete]).subscribe(
      res => {
        this.deletionFrame.hide();
        this.getAllUserEligibleForPromotion();
        this.getAllUsers();
        this.spinnerService.hideLoader();
        this.alertService.sendMessage(res.message,"success")
      },
      (err) => { 
        this.alertService.sendMessage(err.message,"error")
        this.spinnerService.hideLoader();
      },
      () => {
        this.spinnerService.hideLoader();
        this.dt.reset();
        console.log(this.usersList);
      }
    );
  }

}
