import { Component, OnInit, ViewChild } from "@angular/core";
import { BudgetService } from "./budget.service";
import { Validators, FormGroup, FormBuilder, AbstractControl } from "@angular/forms";
import { ModalDirective } from "ng-uikit-pro-standard";
import { ActivatedRoute } from "@angular/router";
import { SpinnerService } from "src/app/core/services/spinner.service";
import { AlertService } from "src/app/core/services/alert.service";
import { DateFormatterService } from "src/app/core/services/date-formatter.service";
import { MoneyflowmanagerService } from "../moneyflowmanager.service";
import { DashboardService } from "src/app/dashboard/dashboard.service";
@Component({
  selector: "app-budget",
  templateUrl: "./budget.component.html",
  styleUrls: ["./budget.component.scss"]
})
export class BudgetComponent implements OnInit {
  @ViewChild("frame") frame: ModalDirective;
  @ViewChild("deleteFrame") deleteFrame: ModalDirective;
  budgetList: any[];
  budgetForm: FormGroup;
  flowId: any;
  formType = "add";
  fromDate: any;
  toDate: any;
  selectedBudgetId: any;
  filterFromDate: Date =new Date(new Date().getFullYear(), 0, 1);
  filterToDate: Date = new Date();
  tableColumns = [
    { field: "amount", header: "Amount" },
    { field: "fromDate", header: "From Date" },
    { field: "toDate", header: "To Date" },
    { field: "action", header: "Action" }
  ];
  constructor(
    private budgetService: BudgetService,
    private _fb: FormBuilder,
    private route: ActivatedRoute,
    private spinnerService: SpinnerService,
    private alert: AlertService,
    private dateFormatter: DateFormatterService,
    private mfmService: MoneyflowmanagerService,
    private dashboardService: DashboardService
  ) {
    // this.filterFromDate.setDate(this.filterFromDate.getDate() - 30);
    this.flowId = this.mfmService.getMoneyFlowId();
    this.getBudgetList();
  }
  setDate() {
    this.budgetForm.controls.fromDate.setValue(new Date());
    this.budgetForm.controls.toDate.setValue(new Date());
  }
  getBudgetList() {
    this.spinnerService.showLoader();
    this.budgetList = [];
    var fromDate = this.dateFormatter.getFormattedDate(this.filterFromDate);
    var toDate = this.dateFormatter.getFormattedDate(this.filterToDate);
    this.budgetService.getBudgetList(this.flowId, fromDate, toDate).subscribe(
      res => {
        this.budgetList = res;
        console.log(this.budgetList);
      },
      err => { },
      () => {
        this.spinnerService.hideLoader();
      }
    );
  }
  createNewBudget() {
    if (this.budgetForm.valid) {
      this.spinnerService.showLoader();
      let toDate;
      let fromDate;
      toDate = this.dateFormatter.getFormattedDate(
        this.budgetForm.controls.toDate.value
      );
      fromDate = this.dateFormatter.getFormattedDate(
        this.budgetForm.controls.fromDate.value
      );
      const body = {
        amount: this.budgetForm.controls.amount.value,
        fromDate: fromDate,
        toDate: toDate
      };
      return this.budgetService.createNewBudget(this.flowId, body).subscribe(
        res => {
          console.log("<------Created Budget------->");
          console.log(res);
          this.alert.clearMessage();
          this.alert.sendMessage(res.message, "success");
        },
        err => {
          this.spinnerService.hideLoader();
          this.alert.clearMessage();
          this.alert.sendMessage(err.message, "error");
        },
        () => {
          this.spinnerService.hideLoader();
          this.getBudgetList();
          this.resetForm();
          this.frame.hide();
        }
      );
    } else {
      this.budgetForm.controls.amount.markAsDirty();
      this.budgetForm.controls.toDate.markAsDirty();
      this.budgetForm.controls.fromDate.markAsDirty();
    }
  }
  setBudgetId(rowData: any) {
    this.selectedBudgetId = rowData.moneyFlowBudgetId;
  }
  viewAndEditBudget(data) {
    this.budgetForm.controls.amount.setValue(data.amount);
    this.budgetForm.controls.fromDate.setValue(data.fromDate);
    this.budgetForm.controls.toDate.setValue(data.toDate);
    this.selectedBudgetId = data.moneyFlowBudgetId;
    this.formType = "edit";
    var fromdate = new Date(data.fromDate);
    var todate = new Date(data.toDate);
    this.budgetForm.controls.toDate.setValue(todate);
    this.budgetForm.controls.fromDate.setValue(fromdate);
    console.log(this.budgetForm);
    console.log(this.selectedBudgetId);
  }
  updateBudget() {
    if (this.budgetForm.valid) {
      this.spinnerService.showLoader();
      var toDate;
      var fromDate;
      toDate = this.dateFormatter.getFormattedDate(
        this.budgetForm.controls.toDate.value
      );
      fromDate = this.dateFormatter.getFormattedDate(
        this.budgetForm.controls.fromDate.value
      );
      const body = {
        amount: this.budgetForm.controls.amount.value,
        toDate: toDate,
        fromDate: fromDate
      };
      return this.budgetService
        .updateBudget(this.selectedBudgetId, body)
        .subscribe(
          res => {
            console.log("<------Updated Budget------->");
            console.log(res);
            this.alert.clearMessage();
            this.alert.sendMessage(res.message, "success");
          },
          err => {
            this.spinnerService.hideLoader();
            this.alert.clearMessage();
            this.alert.sendMessage(err.message, "error");
          },
          () => {
            this.spinnerService.hideLoader();
            this.getBudgetList();
            this.resetForm();
            this.frame.hide();
          }
        );
    }
  }
  deleteBudget() {
    this.spinnerService.showLoader();
    this.budgetService.deleteBudget(this.selectedBudgetId).subscribe(
      res => {
        this.spinnerService.hideLoader();
        this.deleteFrame.hide();
        this.alert.clearMessage();
        this.alert.sendMessage(res.message, "success");
      },
      err => {
        this.alert.clearMessage();
        this.alert.sendMessage(err.message, "error");
      },
      () => {
        this.selectedBudgetId = null;
        this.getBudgetList();
        this.spinnerService.hideLoader();
        this.deleteFrame.hide();
      }
    );
  }
  resetForm() {
    this.budgetForm.reset();
  }
  ngOnInit() {
    this.dashboardService.clearMessage();
    this.dashboardService.sendMessage(1);
    this.budgetForm = this._fb.group({
      amount: [null, [Validators.required, this.nonZero]],
      fromDate: [null, [Validators.required]],
      toDate: ["", [Validators.required]]
    });
  }
  nonZero(control: AbstractControl): { [key: string]: boolean } | null {
    if (Number(control.value) < 0) {
      return { nonZero: true };
    } else {
      return null;
    }
  }
}
