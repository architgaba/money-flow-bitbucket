import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiCommonService } from 'src/app/core/services/api-common.service';
@Injectable({
  providedIn: 'root'
})
export class BudgetService {
  constructor(private apiCommonService: ApiCommonService) { }
  getBudgetList(id, fromDate: any, toDate: any): Observable<any> {
    return this.apiCommonService.get('/money-flow/budget/details/' + id + '?fromDate=' + fromDate + '&toDate=' + toDate);
  }
  createNewBudget(id, body): Observable<any> {
    return this.apiCommonService.post('/money-flow/budget/create/' + id, body);
  }
  deleteBudget(id): Observable<any> {
    return this.apiCommonService.delete('/money-flow/budget/delete/' + id);
  }
  updateBudget(id, body): Observable<any> {
    return this.apiCommonService.put('/money-flow/budget/update/' + id, body);
  }
}
