import { Component, OnInit } from '@angular/core';
import { MFMDashboardService } from './dashboard.service';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { DateFormatterService } from 'src/app/core/services/date-formatter.service';
import { MoneyflowmanagerService } from '../moneyflowmanager.service';
import { TabService } from '../tab.service';
import { DashboardService } from 'src/app/dashboard/dashboard.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  fromDate: Date = new Date();
  toDate: Date = new Date();
  flowId: any;
  budget: any;
  expense: any;
  income: any;
  profit_loss: any;
  class: string;
  percent: any;
  legendColor: string;
  constructor(private spinnerService: SpinnerService,
    private dashboardService: MFMDashboardService,
    private dateFormatter: DateFormatterService,
    private mfmService: MoneyflowmanagerService,
    private tabService: TabService,
    private mydashboardService: DashboardService
  ) {
    this.fromDate.setDate(this.fromDate.getDate() - 30);
    this.flowId = this.mfmService.getMoneyFlowId();
    this.getDashboard();
  }
  settings = {
    bigBanner: false,
    timePicker: false,
    format: 'dd-MM-yyyy',
    defaultOpen: false,
  };
  ngOnInit() {
    this.tabService.clearMessage();
    this.tabService.sendMessage(0)
    this.mydashboardService.clearMessage();
    this.mydashboardService.sendMessage(1);
  }
  getDashboard() {
    this.spinnerService.showLoader();
    var fromDate = this.dateFormatter.getFormattedDate(this.fromDate);
    var toDate = this.dateFormatter.getFormattedDate(this.toDate);
    this.dashboardService.getDashboard(this.flowId, fromDate, toDate).subscribe(res => {
      console.log(res);
      this.budget = res.budget;
      this.expense = res.expense;
      this.income = res.income;
      this.profit_loss = res.profit_loss;
      if (this.profit_loss < 0) {
        this.class = 'loss';
        console.log('loss');
      } else {
        this.class = 'profit';
        console.log('profit');
      }
      this.percent = (this.expense / this.budget) * 100;
      if (this.percent >= 95) {
        this.legendColor = 'red';
      } else if (this.percent >= 85 && this.percent < 95) {
        this.legendColor = 'yellow';
      } else {
        this.legendColor = 'green';
      }
      console.log(this.percent);
      console.log(this.legendColor);
    }, (err) => {
    }, () => {
      this.spinnerService.hideLoader();
    });
  }
}
