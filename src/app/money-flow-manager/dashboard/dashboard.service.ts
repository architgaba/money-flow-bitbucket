import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Principal } from '../../core/services/principal.service';
import { AuthServerProvider } from '../../core/services/auth-jwt.service';
import { Observable } from 'rxjs';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { ApiCommonService } from 'src/app/core/services/api-common.service';
@Injectable({ providedIn: 'root' })
export class MFMDashboardService {
    constructor(private apiCommonService: ApiCommonService, private router: Router, private principal: Principal,
        private authServerProvider: AuthServerProvider, private spinnerService: SpinnerService) { }
    getDashboard(flowId: any, fromDate: any, toDate: any): Observable<any> {
        return this.apiCommonService.get('/money-flow/dashboard/' + flowId + '?fromDate=' + fromDate + '&toDate=' + toDate);
    }
}
