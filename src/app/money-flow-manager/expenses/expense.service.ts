import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiCommonService } from 'src/app/core/services/api-common.service';
@Injectable({ providedIn: 'root' })
export class ExpenseService {
    constructor(private apiCommonService: ApiCommonService) { }
    getExpenseType(): Observable<any> {
        return this.apiCommonService.get('/money-flow/expense/fetch/expense-types');
    }
    createNewExpense(id, body): Observable<any> {
        return this.apiCommonService.postWithFormData('/money-flow/expense/create/' + id, body);
    }
    createNewExpenseType(body): Observable<any> {
        return this.apiCommonService.post('/money-flow/expense/create/type', body);
    }
    getExpenseList(id, fromDate: any, toDate: any): Observable<any> {
        return this.apiCommonService.get('/money-flow/expense/details/' + id + '?fromDate=' + fromDate + '&toDate=' + toDate);
    }
    deleteExpense(id): Observable<any> {
        return this.apiCommonService.delete('/money-flow/expense/delete/' + id);
    }
    deleteReceipt(id): Observable<any> {
        return this.apiCommonService.delete('/money-flow/receipts/delete-receipts?moneyFlowExpenseId=' + id);
    }
    updateExpense(id, body): Observable<any> {
        return this.apiCommonService.putWithFormData('/money-flow/expense/update/' + id, body);
    }
}
