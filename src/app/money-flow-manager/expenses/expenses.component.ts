import {
  Component,
  OnInit,
  ViewChild,
  ElementRef
} from "@angular/core";
import { ExpenseService } from "./expense.service";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { ModalDirective } from "ng-uikit-pro-standard";
import { SpinnerService } from "src/app/core/services/spinner.service";
import { Ng2ImgMaxService } from "ng2-img-max";
import { DomSanitizer } from "@angular/platform-browser";
import { AlertService } from "src/app/core/services/alert.service";
import { DateFormatterService } from "src/app/core/services/date-formatter.service";
import { MoneyflowmanagerService } from "../moneyflowmanager.service";
import { DashboardService } from "src/app/dashboard/dashboard.service";
@Component({
  selector: "app-expenses",
  templateUrl: "./expenses.component.html",
  styleUrls: ["./expenses.component.scss"]
})
export class ExpensesComponent implements OnInit {
  @ViewChild("frame") frame: ModalDirective;
  @ViewChild("deleteFrame") deleteFrame: ModalDirective;
  @ViewChild("fileInput") myInputVariable: ElementRef;
  expenseForm: FormGroup;
  filterFromDate: Date = new Date(new Date().getFullYear(), 0, 1);
  filterToDate: Date = new Date();
  newExpenseType = new FormControl();
  flowId: any;
  expenseType: any[];
  expenseList: any = [];
  showAddNewType = false;
  previous: any = [];
  firstItemIndex;
  lastItemIndex;
  selectedExpenseId: any;
  selectedFile: File = null;
  selectedFileName = "";
  url = "";
  imageBasic: any[] = [];
  tableColumns = [
    { field: "expenseType", header: "Expense Type" },
    { field: "amount", header: "Amount" },
    { field: "date", header: "Date" },
    { field: "recurring", header: "Recurring" },
    { field: "action", header: "Actions" }
  ];
  constructor(
    public sanitizer: DomSanitizer,
    private ng2ImgMax: Ng2ImgMaxService,
    private spinnerService: SpinnerService,
    private route: ActivatedRoute,
    private _fb: FormBuilder,
    private expenseService: ExpenseService,
    private alertService: AlertService,
    private dateFormatter: DateFormatterService,
    private mfmService: MoneyflowmanagerService,
    private dashboardService: DashboardService
  ) {
    // this.filterFromDate.setDate(this.filterFromDate.getDate() - 30);
    this.flowId = this.mfmService.getMoneyFlowId();
  }
  setDate() {
    this.expenseForm.controls.date.setValue(new Date());
    this.expenseForm.controls.recurrenceDate.setValue(new Date());
  }
  getExpenseTypes() {
    this.expenseType = [];
    this.expenseService.getExpenseType().subscribe(
      res => {
        console.log(res);
        this.expenseType = res;
      },
      err => { },
      () => { }
    );
  }
  createNewExpenseType() {
    if (
      this.newExpenseType.value !== null ||
      this.newExpenseType.value !== ""
    ) {
      const body = {
        custom: true,
        name: this.newExpenseType.value,
        typeId: 0
      };
      this.expenseService.createNewExpenseType(body).subscribe(
        res => {
          this.getExpenseTypes();
        },
        err => {
          this.getExpenseTypes();
        }
      );
    }
  }
  createNewExpense() {
    if (this.expenseForm.valid && this.newExpenseType.valid) {
      if (this.showAddNewType === true) {
        this.createNewExpenseType();
        this.expenseForm.controls.expenseType.setValue(
          this.newExpenseType.value
        );
      }
      this.spinnerService.showLoader();
      let expenceDate;
      let recurrenceDate;
      expenceDate = this.dateFormatter.getFormattedDate(
        this.expenseForm.controls.date.value
      );
      if (this.expenseForm.controls.recurring.value) {
        recurrenceDate = this.dateFormatter.getFormattedDate(
          this.expenseForm.controls.recurrenceDate.value
        );
      }
      const body = new FormData();
      body.append("amount", this.expenseForm.controls.amount.value);
      body.append("date", expenceDate);
      body.append("type", this.expenseForm.controls.expenseType.value);
      body.append("recurrenceInterval", this.expenseForm.controls.recurrenceInterval.value);
      body.append(
        "notes",
        this.expenseForm.controls.notes.value
          ? this.expenseForm.controls.notes.value
          : ""
      );
      body.append("recurring", this.expenseForm.controls.recurring.value);
      body.append(
        "recurrenceDate",
        recurrenceDate ? recurrenceDate : ""
      );
      body.append(
        "source",
        this.expenseForm.controls.source.value
          ? this.expenseForm.controls.source.value
          : ""
      );
      body.append("file", this.selectedFile);
      console.log(body);
      this.expenseService.createNewExpense(this.flowId, body).subscribe(
        res => {
          this.getExpenseList();
          this.showAddNewType = false;
          this.frame.hide();
          this.alertService.clearMessage();
          this.alertService.sendMessage(res.message, "success");
        },
        err => {
          this.getExpenseList();
          this.frame.hide();
          this.alertService.clearMessage();
          this.alertService.sendMessage(err.message, "error");
        },
        () => {
          this.spinnerService.hideLoader();
          this.getExpenseTypes();
          this.getExpenseList();
          this.frame.hide();
        }
      );
    } else {
      this.expenseForm.controls.recurrenceInterval.markAsDirty();
      this.expenseForm.controls.expenseType.markAsDirty();
      this.expenseForm.controls.date.markAsDirty();
      this.expenseForm.controls.amount.markAsDirty();
      this.newExpenseType.markAsDirty();
      if (this.showAddNewType) this.newExpenseType.markAsDirty();
    }
  }
  updateExpense() {
    if (this.expenseForm.valid && this.newExpenseType.valid) {
      if (this.showAddNewType === true) {
        this.createNewExpenseType();
        this.expenseForm.controls.expenseType.setValue(
          this.newExpenseType.value
        );
      }
      this.spinnerService.showLoader();
      let expenceDate;
      let recurrenceDate;
      expenceDate = this.dateFormatter.getFormattedDate(
        this.expenseForm.controls.date.value
      );
      if (this.expenseForm.controls.recurring.value) {
        recurrenceDate = this.dateFormatter.getFormattedDate(
          this.expenseForm.controls.recurrenceDate.value
        );
      }
      const body = new FormData();
      body.append("amount", this.expenseForm.controls.amount.value);
      body.append("date", expenceDate);
      body.append("type", this.expenseForm.controls.expenseType.value);
      body.append("recurrenceInterval", this.expenseForm.controls.recurrenceInterval.value);
      body.append(
        "notes",
        this.expenseForm.controls.notes.value
          ? this.expenseForm.controls.notes.value
          : ""
      );
      body.append("recurring", this.expenseForm.controls.recurring.value);
      body.append(
        "recurrenceDate",
        recurrenceDate
      );
      body.append(
        "source",
        this.expenseForm.controls.source.value
          ? this.expenseForm.controls.source.value
          : ""
      );
      body.append("file", this.selectedFile);
      console.log(body);
      this.expenseService
        .updateExpense(this.expenseForm.controls.moneyFlowExpenseId.value, body)
        .subscribe(
        res => {
          this.getExpenseList();
          this.showAddNewType = false;
          this.resetForm();
          this.frame.hide();
          this.alertService.clearMessage();
          this.alertService.sendMessage(res.message, "success");
        },
        err => {
          this.getExpenseList();
          this.frame.hide();
          this.alertService.clearMessage();
          this.alertService.sendMessage(err.message, "error");
        },
        () => {
          this.spinnerService.hideLoader();
          this.getExpenseTypes();
          this.getExpenseList();
          this.frame.hide();
        }
        );
    } else {
      this.expenseForm.controls.recurrenceInterval.markAsDirty();
      this.expenseForm.controls.expenseType.markAsDirty();
      this.expenseForm.controls.date.markAsDirty();
      this.expenseForm.controls.amount.markAsDirty();
      if (this.showAddNewType) this.newExpenseType.markAsDirty();
    }
  }

  selectedInterval(interval: any) {
    this.expenseForm.controls.recurrenceInterval.setValue(interval);

  }
  getExpenseList() {
    this.expenseList = [];
    var fromDate = this.dateFormatter.getFormattedDate(this.filterFromDate);
    var toDate = this.dateFormatter.getFormattedDate(this.filterToDate);
    this.spinnerService.showLoader();
    this.expenseService.getExpenseList(this.flowId, fromDate, toDate).subscribe(
      res => {
        this.expenseList = res;
      },
      err => {
        this.alertService.clearMessage();
        this.alertService.sendMessage(err.message, "error");
      },
      () => {
        this.spinnerService.hideLoader();
      }
    );
  }
  resetForm() {
    this.expenseForm.reset();
    this.expenseForm.controls.recurring.setValue(false);
    this.expenseForm.controls.recurrenceInterval.clearValidators();
    this.expenseForm.controls.recurrenceInterval.updateValueAndValidity();
    this.expenseForm.markAsUntouched();
    this.expenseForm.controls.expenseType.enable();
    this.selectedFile = null;
    this.selectedFileName = "";
    this.url = "";
  }
  setFormData(data) {
    this.url = data.receiptKey;
    var date = new Date(data.date);
    this.expenseForm.controls.expenseType.setValue(data.expenseType);
    this.expenseForm.controls.moneyFlowExpenseId.setValue(
      data.moneyFlowExpenseId
    );
    this.expenseForm.controls.date.setValue(date);
    this.expenseForm.controls.amount.setValue(data.amount);
    this.expenseForm.controls.source.setValue(data.source);
    this.expenseForm.controls.notes.setValue(data.notes);
    this.expenseForm.controls.recurring.setValue(data.recurring);
    this.expenseForm.controls.recurrenceInterval.setValue(data.recurrenceInterval);
    if (data.recurring) {
      date = new Date(data.recurrenceDate);
      this.expenseForm.controls.recurrenceDate.setValue(data.recurring ? date : "");
    }
    this.expenseForm.controls.expenseType.enable();
    this.showAddNewType = false;
  }
  setSelectedExpenseId(formData: any) {
    this.selectedExpenseId = formData.moneyFlowExpenseId;
  }
  removeExpense() {
    this.spinnerService.showLoader();
    this.expenseService.deleteExpense(this.selectedExpenseId).subscribe(
      res => {
        this.deleteFrame.hide();
        this.alertService.clearMessage();
        this.alertService.sendMessage(res.message, "success");
      },
      err => {
        this.alertService.clearMessage();
        this.alertService.sendMessage(err.message, "error");
      },
      () => {
        this.spinnerService.hideLoader();
        this.selectedExpenseId = null;
        this.getExpenseList();
        this.deleteFrame.hide();
      }
    );
  }
  downloadImage() {
    window.location.href = this.url;
  }
  ngOnInit() {
    this.dashboardService.clearMessage();
    this.dashboardService.sendMessage(1);
    this.getExpenseList();
    this.expenseForm = this._fb.group({
      moneyFlowExpenseId: [null],
      expenseType: [null, [Validators.required]],
      date: ["", [Validators.required]],
      amount: [null, [Validators.required]],
      source: [""],
      notes: [""],
      recurring: [false],
      recurrenceDate: [""],
      expenseFile: [null],
      recurrenceInterval: [""],
    });
    this.getExpenseTypes();
  }
  showFile() {
    return this.selectedFileName;
  }
  selectFile(imageInput) {
    console.log("File Upload function");
    this.spinnerService.showLoader();
    this.selectedFile = null;
    this.selectedFileName = "";
    this.url = "";
    this.selectedFileName = imageInput.files[0].name;
    this.ng2ImgMax.compressImage(imageInput.files[0], 2).subscribe(
      result => {
        this.selectedFile = new File([result], result.name);
        this.getImagePreview(this.selectedFile);
        this.spinnerService.hideLoader();
      },
      error => {
        this.spinnerService.hideLoader();
      }
    );
  }
  getImagePreview(file: File) {
    const reader: FileReader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (event: any) => {
      this.url = event.target.result;
    };
  }
  deleteReceipt() {
    if (this.expenseForm.controls.moneyFlowExpenseId.value != null) {
      this.spinnerService.showLoader();
      this.expenseService
        .deleteReceipt(this.expenseForm.controls.moneyFlowExpenseId.value)
        .subscribe(
        res => {
          this.alertService.clearMessage();
          this.alertService.sendMessage(res.message, "success");
        },
        err => {
          this.alertService.clearMessage();
          this.alertService.sendMessage(err.message, "error");
        },
        () => {
          this.spinnerService.hideLoader();
          this.getExpenseList();
        }
        );
    }
  }
  resetFileInput() {
    this.myInputVariable.nativeElement.value = "";
    this.url = "";
    this.selectedFile = null;
    this.selectedFileName = "";
    this.deleteReceipt();
  }
  setValidation() {
    console.log(this.expenseForm.controls.recurring.value);
    if (this.expenseForm.controls.recurring.value === false) {
      this.expenseForm.controls.recurrenceDate.clearValidators();
      this.expenseForm.controls.recurrenceInterval.clearValidators();
    } else if (this.expenseForm.controls.recurring.value === true) {
      this.expenseForm.controls.recurrenceDate.setValidators([
        Validators.required
      ]);
      this.expenseForm.controls.recurrenceDate.setValue(new Date());
      this.expenseForm.controls.recurrenceInterval.setValidators([
        Validators.required
      ]);
    }
    this.expenseForm.controls.recurrenceDate.updateValueAndValidity();
    this.expenseForm.controls.recurrenceInterval.updateValueAndValidity();
  }
  setNewTypeValidation() {
    if (this.showAddNewType) {
      this.newExpenseType.setValidators([Validators.required]);
    } else {
      this.newExpenseType.clearValidators();
    }
    this.newExpenseType.reset();
    this.newExpenseType.markAsUntouched();
    this.newExpenseType.updateValueAndValidity();
  }
}
