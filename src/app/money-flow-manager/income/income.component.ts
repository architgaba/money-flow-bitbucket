import {
  Component,
  OnInit,
  ViewChild,
  ElementRef
} from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";
import { ModalDirective } from "ng-uikit-pro-standard";
import { IncomeService } from "./income.service";
import { SpinnerService } from "src/app/core/services/spinner.service";
import { Ng2ImgMaxService } from "ng2-img-max";
import { DomSanitizer } from "@angular/platform-browser";
import { AlertService } from "src/app/core/services/alert.service";
import { DateFormatterService } from "src/app/core/services/date-formatter.service";
import { MoneyflowmanagerService } from "../moneyflowmanager.service";
import { DashboardService } from "src/app/dashboard/dashboard.service";
@Component({
  selector: "app-income",
  templateUrl: "./income.component.html",
  styleUrls: ["./income.component.scss"]
})
export class IncomeComponent implements OnInit {
  @ViewChild("frame") frame: ModalDirective;
  @ViewChild("deleteFrame") deleteFrame: ModalDirective;
  @ViewChild("fileInput")
  @ViewChild("fileInput")
  myInputVariable: ElementRef;
  recurrence = false;
  filterFromDate: Date = new Date(new Date().getFullYear(), 0, 1);
  filterToDate: Date = new Date();
  incomeForm: FormGroup;
  imageBasic: any[] = [];
  selectedFile: File = null;
  selectedFileName = "";
  url = "";
  downloadURL = "";
  newIncomeType = new FormControl();
  incomeType = [];
  incomeList = [];
  flowId: any;
  httpMethod: any;
  description: any;
  date: Date;
  showAddNewType: Boolean;
  mfm_Income_selectedIncomeId: any;
  selectedIncomeId: any;
  tableColumns = [
    { field: "incomeType", header: "Income Type" },
    { field: "amount", header: "Amount" },
    { field: "date", header: "Date" },
    { field: "recurring", header: "Recurring" },
    { field: "action", header: "Actions" }
  ];
  constructor(
    public sanitizer: DomSanitizer,
    private ng2ImgMax: Ng2ImgMaxService,
    private spinnerService: SpinnerService,
    private _fb: FormBuilder,
    private incomeService: IncomeService,
    private alertService: AlertService,
    private dateFormatter: DateFormatterService,
    private mfmService: MoneyflowmanagerService,
    private dashboardService: DashboardService
  ) {
    // this.filterFromDate.setDate(this.filterFromDate.getDate() - 30);
    this.flowId = this.mfmService.getMoneyFlowId();
    this.showAddNewType = false;
  }
  setDate() {
    this.incomeForm.controls.date.setValue(new Date());
    this.incomeForm.controls.recurrenceDate.setValue(new Date());
  }
  resetForm() {
    this.incomeForm.reset();
    this.incomeForm.markAsUntouched();
    this.showAddNewType = false;
    this.incomeForm.controls.recurring.setValue(false);
    this.incomeForm.controls.recurrenceInterval.clearValidators();
    this.incomeForm.controls.recurrenceInterval.updateValueAndValidity();
    this.incomeForm.controls.type.enable();
    this.selectedFile = null;
    this.selectedFileName = "";
    this.url = "";
    this.downloadURL = "";
  }

  selectedInterval(interval: any) {
    this.incomeForm.controls.recurrenceInterval.setValue(interval);

  }
  ngOnInit() {
    this.dashboardService.clearMessage();
    this.dashboardService.sendMessage(1);
    this.incomeForm = this._fb.group({
      moneyFlowIncomeId: [null],
      type: [null, [Validators.required]],
      date: ["", [Validators.required]],
      amount: [null, [Validators.required]],
      source: [""],
      notes: [""],
      recurring: [false],
      recurrenceDate: [""],
      incomeFile: [null],
      recurrenceInterval: [""]
    });
    this.getIncomeTypes();
    this.getIncomeList();
  }
  downloadImage() {
    window.location.href = this.url;
  }
  createNewIncomeType() {
    const body = {
      name: this.newIncomeType.value,
      typeId: 0
    };
    this.incomeService.createNewIncomeType(body).subscribe(
      res => {
        console.log(res);
        this.getIncomeTypes();
      },
      err => { },
      () => {
        this.getIncomeTypes();
      }
    );
  }
  selectFile(imageInput) {
    console.log(imageInput);
    console.log("File Upload function");
    this.spinnerService.showLoader();
    this.selectedFile = null;
    this.selectedFileName = "";
    this.url = "";
    this.selectedFileName = imageInput.files[0].name;
    this.ng2ImgMax.compressImage(imageInput.files[0], 2).subscribe(
      result => {
        this.selectedFile = new File([result], result.name);
        this.getImagePreview(this.selectedFile);
        this.spinnerService.hideLoader();
      },
      error => {
        this.spinnerService.hideLoader();
      }
    );
  }
  getImagePreview(file: File) {
    const reader: FileReader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (event: any) => {
      this.url = event.target.result;
    };
  }
  resetFileInput() {
    this.myInputVariable.nativeElement.value = "";
    this.url = "";
    this.selectedFile = null;
    this.selectedFileName = "";
    this.deleteReceipt();
  }
  getIncomeTypes() {
    this.incomeType = [];
    this.spinnerService.showLoader();
    this.incomeService.getIncomeType().subscribe(
      res => {
        console.log(res);
        this.incomeType = res;
      },
      err => {
        this.alertService.clearMessage();
        this.alertService.sendMessage(err.message, "error");
      },
      () => {
        this.spinnerService.hideLoader();
      }
    );
  }
  convert(dateToFormat) {
    var oTempDate = new Date(dateToFormat),
      sMonth = '' + (oTempDate.getMonth() + 1),
      sDay = '' + oTempDate.getDate(),
      iYear = oTempDate.getFullYear();
    if (sMonth.length < 2) { sMonth = '0' + sMonth; }
    if (sDay.length < 2) { sDay = '0' + sDay; }
    return [sDay, sMonth, iYear].join('-');
  }
  createNewIncome() {
    if (this.incomeForm.valid && this.newIncomeType.valid) {
      this.spinnerService.showLoader();
      if (this.showAddNewType) {
        this.createNewIncomeType();
      }
      var incomeDate;
      incomeDate = this.dateFormatter.getFormattedDate(
        this.incomeForm.controls.date.value
      );
      console.log(incomeDate);
      if (this.incomeForm.controls.recurring.value) {
        var recurrenceDate;
        recurrenceDate = this.dateFormatter.getFormattedDate(
          this.incomeForm.controls.recurrenceDate.value
        );
      }
      const body = new FormData();
      body.append("amount", this.incomeForm.controls.amount.value);
      body.append("date", incomeDate);
      body.append("recurrenceInterval", this.incomeForm.controls.recurrenceInterval.value);
      body.append(
        "type",
        this.showAddNewType
          ? this.newIncomeType.value
          : this.incomeForm.controls.type.value
      );
      body.append(
        "notes",
        this.incomeForm.controls.notes.value
          ? this.incomeForm.controls.notes.value
          : ""
      );
      body.append("recurring", this.incomeForm.controls.recurring.value);
      body.append("recurrenceDate", recurrenceDate ? recurrenceDate : "");
      body.append(
        "source",
        this.incomeForm.controls.source.value
          ? this.incomeForm.controls.source.value
          : ""
      );
      body.append("file", this.selectedFile);
      console.log(body);
      this.incomeService.createNewIncome(this.flowId, body).subscribe(
        res => {
          this.frame.hide();
          this.alertService.clearMessage();
          this.alertService.sendMessage(res.message, "success");
        },
        err => {
          this.frame.hide();
          this.alertService.clearMessage();
          this.alertService.sendMessage(err.message, "error");
        },
        () => {
          this.spinnerService.hideLoader();
          this.getIncomeList();
          this.getIncomeTypes();
          this.frame.hide();
        }
      );
    } else {
      Object.keys(this.incomeForm.controls).forEach(key => {
        this.incomeForm.get(key).markAsDirty();
      });
      this.newIncomeType.markAsDirty();
      if (this.showAddNewType) this.newIncomeType.markAsDirty();
    }
  }
  updateIncome() {
    if (this.incomeForm.valid && this.newIncomeType.valid) {
      this.spinnerService.showLoader();
      if (this.showAddNewType) {
        this.createNewIncomeType();
      }
      var incomeDate;
      incomeDate = this.dateFormatter.getFormattedDate(
        this.incomeForm.controls.date.value
      );
      if (this.incomeForm.controls.recurring.value) {
        var recurrenceDate;
        recurrenceDate = this.dateFormatter.getFormattedDate(
          this.incomeForm.controls.recurrenceDate.value
        );
      }
      const body = new FormData();
      body.append("amount", this.incomeForm.controls.amount.value);
      body.append("date", incomeDate);
      body.append("recurrenceInterval", this.incomeForm.controls.recurrenceInterval.value);
      body.append(
        "type",
        this.showAddNewType
          ? this.newIncomeType.value
          : this.incomeForm.controls.type.value
      );
      body.append(
        "notes",
        this.incomeForm.controls.notes.value
          ? this.incomeForm.controls.notes.value
          : ""
      );
      body.append("recurring", this.incomeForm.controls.recurring.value);
      body.append("recurrenceDate", recurrenceDate ? recurrenceDate : "");
      body.append(
        "source",
        this.incomeForm.controls.source.value
          ? this.incomeForm.controls.source.value
          : ""
      );
      body.append("file", this.selectedFile);
      console.log(body);
      this.incomeService
        .updateIncome(this.incomeForm.controls.moneyFlowIncomeId.value, body)
        .subscribe(
        res => {
          this.frame.hide();
          this.getIncomeList();
          this.resetForm();
          this.alertService.clearMessage();
          this.alertService.sendMessage(res.message, "success");
        },
        err => {
          this.frame.hide();
          this.getIncomeList();
          this.alertService.clearMessage();
          this.alertService.sendMessage(err.message, "error");
        },
        () => {
          this.frame.hide();
          this.getIncomeList();
          this.getIncomeTypes();
          this.spinnerService.hideLoader();
        }
        );
    } else {
      Object.keys(this.incomeForm.controls).forEach(key => {
        this.incomeForm.get(key).markAsDirty();
      });
      if (this.showAddNewType) this.newIncomeType.markAsDirty();
    }
  }
  getIncomeList() {
    var fromDate = this.dateFormatter.getFormattedDate(this.filterFromDate);
    var toDate = this.dateFormatter.getFormattedDate(this.filterToDate);
    this.spinnerService.showLoader();
    this.incomeList = [];
    this.incomeService.getIncomeList(this.flowId, fromDate, toDate).subscribe(
      res => {
        console.log(res);
        this.incomeList = res;
      },
      err => {
        this.alertService.clearMessage();
        this.alertService.sendMessage(err.message, "error");
      },
      () => {
        this.spinnerService.hideLoader();
      }
    );
  }
  showFile() {
    return this.selectedFileName;
  }
  deleteIncome() {
    this.spinnerService.showLoader();
    this.incomeService.deleteIncome(this.selectedIncomeId).subscribe(
      res => {
        console.log(res);
        this.getIncomeList();
        this.alertService.clearMessage();
        this.alertService.sendMessage(res.message, "success");
      },
      err => {
        this.alertService.clearMessage();
        this.alertService.sendMessage(err.message, "error");
      },
      () => {
        this.spinnerService.hideLoader();
        this.selectedIncomeId = null;
        this.getIncomeList();
        this.deleteFrame.hide();
      }
    );
  }
  setSelectedIncomeId(rowData: any) {
    this.selectedIncomeId = rowData.moneyFlowIncomeId;
  }
  deleteReceipt() {
    if (this.incomeForm.controls.moneyFlowIncomeId.value != null) {
      this.spinnerService.showLoader();
      this.incomeService
        .deleteReceipt(this.incomeForm.controls.moneyFlowIncomeId.value)
        .subscribe(
        res => {
          this.alertService.clearMessage();
          this.alertService.sendMessage(res.message, "success");
        },
        err => {
          this.alertService.clearMessage();
          this.alertService.sendMessage(err.message, "error");
        },
        () => {
          this.spinnerService.hideLoader();
          this.getIncomeList();
        }
        );
    }
  }
  selectIncome(row: any) {
    console.log(row);
    var date = new Date(row.date);
    this.incomeForm.controls.amount.setValue(row.amount);
    this.incomeForm.controls.date.setValue(date);
    this.incomeForm.controls.type.setValue(row.incomeType);
    this.incomeForm.controls.recurring.setValue(row.recurring);
    this.incomeForm.controls.notes.setValue(row.notes);
    this.incomeForm.controls.recurrenceInterval.setValue(row.recurrenceInterval);
    if (row.recurring) {
      date = new Date(row.recurrenceDate);
      this.incomeForm.controls.recurrenceDate.setValue(row.recurring ? date : "");
    }
    this.incomeForm.controls.source.setValue(row.source);
    this.downloadURL = row.receiptKey;
    this.url = row.receiptKey;
    this.incomeForm.controls.moneyFlowIncomeId.setValue(row.moneyFlowIncomeId);
    this.incomeForm.controls.type.enable();
    this.showAddNewType = false;
    console.log(this.incomeForm.value);
    Object.keys(this.incomeForm.controls).forEach(key => {
      this.incomeForm.get(key).markAsUntouched();
    });
  }
  setValidation() {
    if (this.incomeForm.controls.recurring.value === false) {
      this.incomeForm.controls.recurrenceDate.clearValidators();
      this.incomeForm.controls.recurrenceInterval.clearValidators();
    } else if (this.incomeForm.controls.recurring.value === true) {
      this.incomeForm.controls.recurrenceDate.setValidators([
        Validators.required
      ]);
      this.incomeForm.controls.recurrenceDate.setValue(new Date());
      this.incomeForm.controls.recurrenceInterval.setValidators([
        Validators.required
      ]);
    }
    this.incomeForm.controls.recurrenceDate.updateValueAndValidity();
    this.incomeForm.controls.recurrenceInterval.updateValueAndValidity();
  }
  setNewTypeValidation() {
    if (this.showAddNewType) {
      this.newIncomeType.setValidators([Validators.required]);
    } else {
      this.newIncomeType.clearValidators();
    }
    this.newIncomeType.reset();
    this.newIncomeType.markAsUntouched();
    this.newIncomeType.updateValueAndValidity();
  }
}
