import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiCommonService } from 'src/app/core/services/api-common.service';
@Injectable({ providedIn: 'root' })
export class IncomeService {
    constructor(private apiCommonService: ApiCommonService) { }
    getIncomeType(): Observable<any> {
        return this.apiCommonService.get('/money-flow/income/fetch/income-types');
    }
    createNewIncome(id, body): Observable<any> {
        return this.apiCommonService.postWithFormData('/money-flow/income/create/' + id, body);
    }
    getIncomeList(id, fromDate: any, toDate: any): Observable<any> {
        return this.apiCommonService.get('/money-flow/income/details/' + id + '?fromDate=' + fromDate + '&toDate=' + toDate);
    }
    createNewIncomeType(body): Observable<any> {
        return this.apiCommonService.post('/money-flow/income/create/type', body);
    }
    deleteIncome(id): Observable<any> {
        return this.apiCommonService.delete('/money-flow/income/delete/' + id);
    }
    deleteReceipt(id): Observable<any> {
        return this.apiCommonService.delete('/money-flow/receipts/delete-receipts?moneyFlowIncomeId=' + id);
    }
    updateIncome(id, body): Observable<any> {
        return this.apiCommonService.putWithFormData('/money-flow/income/update/' + id, body);
    }
}
