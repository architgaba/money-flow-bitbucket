import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewMileageComponent } from './view-mileage/view-mileage.component';
import { VehiclesComponent } from './vehicles/vehicles.component';
import { TrackerComponent } from './tracker/tracker.component';
import { TripsComponent } from './trips/trips.component';
const routes: Routes = [
  { path: '',
  component: ViewMileageComponent,
  children: [
    {
      path: '',
      component: VehiclesComponent
    },
    {
      path: 'tracker',
      component: TrackerComponent
    },
    {
      path: 'trips',
      component: TripsComponent
    },
  ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MileageTrackerRoutingModule { }
