import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MileageTrackerRoutingModule } from './mileage-tracker-routing.module';
import { VehiclesComponent } from './vehicles/vehicles.component';
import { ViewMileageComponent } from './view-mileage/view-mileage.component';
import {
  ButtonsModule,
  WavesModule,
  CollapseModule,
  IconsModule,
  TooltipModule,
  ModalModule,
  InputsModule,
  TimePickerModule
} from 'ng-uikit-pro-standard';
import { TableModule } from 'primeng/table';
import { TrackerComponent } from './tracker/tracker.component';
import { TripsComponent } from './trips/trips.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CalendarModule, TabMenuModule } from 'primeng/primeng';
import {DropdownModule} from 'primeng/dropdown';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { PipeModule } from 'src/app/core/pipes/pipe.module';
@NgModule({
  declarations: [VehiclesComponent, ViewMileageComponent, TrackerComponent, TripsComponent],
  imports: [
    CommonModule,
    MileageTrackerRoutingModule,
    ButtonsModule,
    WavesModule,
    InputsModule,
    CollapseModule,
    IconsModule,
    TableModule,
    TooltipModule,
    ModalModule,
    ReactiveFormsModule,
    CalendarModule,
    DropdownModule,
    TimePickerModule,
    GooglePlaceModule,
    FormsModule,
    TabMenuModule,
    PipeModule
  ]
})
export class MileageTrackerModule {}
