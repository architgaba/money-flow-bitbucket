/// <reference types="@types/googlemaps" />
import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { TrackerService } from "./tracker.service";
import { MileageService } from "../mileage.service";
import { AlertService } from "src/app/core/services/alert.service";
import { SpinnerService } from "src/app/core/services/spinner.service";
import { DateFormatterService } from "src/app/core/services/date-formatter.service";
import { Address } from "ngx-google-places-autocomplete/objects/address";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { LatLng } from "@agm/core";
declare const google: any;
@Component({
  selector: "app-tracker",
  templateUrl: "./tracker.component.html",
  styleUrls: ["./tracker.component.scss"]
})
export class TrackerComponent implements OnInit {
  manualMileage: FormGroup;
  formatedFromAddress: string;
  formatedToAddress: string;
  flowId: any;
  @ViewChild("placesRef") placesRef: GooglePlaceDirective;
  vehicleList = [];
  tripTypes = [
    { label: "Business", value: "Business" },
    { label: "Personal", value: "Personal" },
    { label: "Commute", value: "Commute" }
  ];
  fromLat: number;
  fromLong: number;
  toLat: any;
  toLong: any;
  flowList: any[];
  constructor(
    private fb: FormBuilder,
    private trackerService: TrackerService,
    private mileageService: MileageService,
    private alertService: AlertService,
    private loaderService: SpinnerService,
    private dateFormatter: DateFormatterService
  ) {
    this.flowId = this.mileageService.getFlowId();
  }
  addMileage() {
    this.manualMileage.controls.tripType.setValue('Business');
    if (this.manualMileage.valid) {
      this.loaderService.showLoader();
      console.log(this.manualMileage.value);
      this.manualMileage.controls.tripDate.setValue(
        this.dateFormatter.getFormattedDate(
          this.manualMileage.controls.tripDate.value
        )
      );
      this.manualMileage.controls.fromAddress.setValue(
        this.formatedFromAddress
      );
      this.manualMileage.controls.manual.setValue(true);
      this.manualMileage.controls.toAddress.setValue(this.formatedToAddress);
      const body = this.manualMileage.getRawValue();
      console.log(body);
      body.startTime = this.dateFormatter.getFormattedTime(
        this.manualMileage.controls.startTime.value
      );
      body.endTime = this.dateFormatter.getFormattedTime(
        this.manualMileage.controls.endTime.value
      );
      console.log(body);
      this.trackerService.addMileage(this.manualMileage.controls.flowId.value, body).subscribe(
        res => {
          this.alertService.clearMessage();
          this.alertService.sendMessage(res.message, "success");
        },
        err => { },
        () => {
          this.manualMileage.reset();
          this.manualMileage.controls.tripDate.setValue(new Date());
          this.manualMileage.controls.mileageRate.setValue(0.58);
          this.loaderService.hideLoader();
          Object.keys(this.manualMileage.controls).forEach(key => {
            this.manualMileage.get(key).markAsUntouched();
          });
        }
      );
    } else {
      Object.keys(this.manualMileage.controls).forEach(key => {
        this.manualMileage.get(key).markAsDirty();
      });
    }
  }
  getFlowList() {
    this.flowList = [];
    this.trackerService.getMoneyFlowTableData().subscribe(res => {
      res.forEach(element => {
        this.flowList.push({
          label: element.moneyFlowName,
          value: element.moneyFlowAccountId
        });
      }, (err) => {
      }, () => {
      });
      console.log(this.flowList);
    });
  }
  getVehicleList() {
    this.trackerService.getVehicleList(this.flowId).subscribe(
      res => {
        res.forEach(element => {
          this.vehicleList.push({
            label: element.year + " " + element.make + " " + element.model,
            value: element.id
          });
        });
        if (this.vehicleList.length === 0) {
          this.vehicleList.push({
            label: "No Vehicles Available",
            value: " "
          });
        }
        console.log(this.vehicleList);
      },
      err => { },
      () => { }
    );
  }
  setRateData(data: number) {
    const tripCost = data * this.manualMileage.controls.milesDriven.value;
    this.manualMileage.controls.tripCost.setValue(
      Math.round((tripCost + 0.00001) * 100) / 100
    );
    this.manualMileage.controls.totalExpense.setValue(
      this.manualMileage.controls.tripCost.value
    );
  }
  onParkingInputChange(data: number) {
    const total = (+data) + (+this.manualMileage.controls.tripCost.value) + (+this.manualMileage.controls.tolls.value);
    this.manualMileage.controls.totalExpense.setValue(
      Math.round((total + 0.00001) * 100) / 100
    );
  }
  onTollInputChange(data) {
    const total = (+data) + (+this.manualMileage.controls.tripCost.value) + (+this.manualMileage.controls.parking.value);
    this.manualMileage.controls.totalExpense.setValue(
      Math.round((total + 0.00001) * 100) / 100
    );
  }
  setFromLocation(address: Address) {
    this.formatedFromAddress = address.formatted_address;
    console.log(address.formatted_address);
    console.log(address.geometry.location.lng());
    console.log(address.geometry.location.lat());
    this.fromLat = address.geometry.location.lat();
    this.fromLong = address.geometry.location.lng();
    this.calculateDistance(
      this.fromLong,
      this.fromLat,
      this.toLong,
      this.toLat
    );
    this.setRateData(this.manualMileage.controls.mileageRate.value);
  }
  setToLocation(address: Address) {
    this.formatedToAddress = address.formatted_address;
    console.log(address.formatted_address);
    console.log(address.geometry.location.lng());
    console.log(address.geometry.location.lat());
    this.toLat = address.geometry.location.lat();
    this.toLong = address.geometry.location.lng();
    this.calculateDistance(
      this.fromLong,
      this.fromLat,
      this.toLong,
      this.toLat
    );
    this.setRateData(this.manualMileage.controls.mileageRate.value);
  }
  calculateDistance(fromLong, fromLat, toLong, toLat) {
    const fromAddress: LatLng = new google.maps.LatLng(fromLat, fromLong);
    const toAddress: LatLng = new google.maps.LatLng(toLat, toLong);
    const distance = google.maps.geometry.spherical.computeDistanceBetween(
      fromAddress,
      toAddress
    );
    this.manualMileage.controls.milesDriven.setValue(
      Math.round(((distance / 1000) * 0.621371 + 0.00001) * 100) / 100
    );
  }
  checkValidity(data) {
  }
  setFlow(event: any) {
    console.log(event);
    this.vehicleList = [];
    this.loaderService.showLoader();
    this.trackerService.getVehicleListOfAFlow(event.value).subscribe(res => {
      res.forEach(element => {
        this.vehicleList.push({
          label: element.year + " " + element.make + " " + element.model,
          value: element.id
        });
      });
      console.log(res);
    }, (err) => {
    }, () => {
      this.loaderService.hideLoader();
    });
  }
  ngOnInit() {
    this.getFlowList();
    this.manualMileage = this.fb.group({
      id: [null],
      vehicleId: [null, [Validators.required]],
      tripType: ['Business', [Validators.required]],
      flowId: [null, [Validators.required]],
      tripDate: [null, [Validators.required]],
      startTime: [null, [Validators.required]],
      endTime: [null, [Validators.required]],
      fromAddress: [null, [Validators.required]],
      toAddress: [null, [Validators.required]],
      mileageRate: [{ value: '', disabled: true }, [Validators.required]],
      tripCost: [{ value: '', disabled: true }, [Validators.required]],
      milesDriven: [{ value: '', disabled: true }],
      parking: [0],
      tolls: [0],
      totalExpense: [{ value: '', disabled: true }, [Validators.required]],
      manual: [true]
    });
    this.manualMileage.controls.tripDate.setValue(new Date());
    this.manualMileage.controls.milesDriven.setValue(0);
    this.manualMileage.controls.mileageRate.setValue(0.58);
  }
}
