import { Injectable } from '@angular/core';
import { ApiCommonService } from 'src/app/core/services/api-common.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class TrackerService {
  constructor(private apiCommonService: ApiCommonService, private http: HttpClient) { }
  addMileage(id, body): Observable<any> {
    return this.apiCommonService.post('/money-flow/mileage-tracker/add-mileage/' + '?flowId=' + id, body);
  }
  getVehicleList(id): Observable<any> {
    return this.apiCommonService.get('/money-flow/mileage-tracker/vehicle-list?flowId=' + id);
  }
  getMoneyFlowTableData(): Observable<any> {
    return this.apiCommonService.get('/money-flow/details');
  }
  getVehicleListOfAFlow(flowId: any) {
    return this.apiCommonService.get('/money-flow/mileage-tracker/vehicle-list?flowId=' + flowId);
  }
}
