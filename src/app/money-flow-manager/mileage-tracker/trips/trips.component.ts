/// <reference types="@types/googlemaps" />
import { Component, OnInit, ViewChild } from "@angular/core";
import { TripsService } from "./trips.service";
import { MileageService } from "../mileage.service";
import { SpinnerService } from "src/app/core/services/spinner.service";
import { AlertService } from "src/app/core/services/alert.service";
import { ModalDirective } from "ng-uikit-pro-standard";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { DateFormatterService } from "src/app/core/services/date-formatter.service";
import { Address } from "ngx-google-places-autocomplete/objects/address";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { LatLng } from "@agm/core";
import { SelectItem } from "primeng/api";
import { MoneyFlowReportsService } from "src/app/reports/money-flow-reports/money-flow-reports.service";
import { ApiCommonService } from "src/app/core/services/api-common.service";
declare const google: any;
@Component({
  selector: "app-trips",
  templateUrl: "./trips.component.html",
  styleUrls: ["./trips.component.scss"]
})
export class TripsComponent implements OnInit {
  @ViewChild("mergeFrame") mergeFrame: ModalDirective;
  @ViewChild("deleteFrame") deleteFrame: ModalDirective;
  @ViewChild("detailsFrame") detailsFrame: ModalDirective;
  @ViewChild("placesRef2") placesRef: GooglePlaceDirective;
  tripData: FormGroup;
  tableColumns = [
    { field: "fromAddress", header: "From",width:"200px" },
    { field: "toAddress", header: "To",width:"200px" },
    { field: "tripDate", header: "Date",width:"120px" },
    { field: "flowName", header: "Money Flow Account",width:"120px" },
    { field: "merged", header: "Merged Trip" ,width:"90px"},
    { field: "vehicleName", header: "Vehicle",width:"70px" }, 
    { field: "milesDriven", header: "Miles",width:"70px" },
    { field: "totalExpense", header: "Expense ($)" ,width:"70px"},
    { field: "action", header: "Action",width:"200px" }
  ];
  secondTableColumns = [
    { field: "fromAddress", header: "From" },
    { field: "toAddress", header: "To" },
    { field: "tripDate", header: "Date" },
    { field: "tripType", header: "Trip Type" },
    { field: "totalExpense", header: "Expense ($)" }
  ];
  tripType: any = [
    { label: "All", value: "all" },
    { label: "Automatic", value: "automatic" },
    { label: "Manual", value: "manual" },
  ];
  tripDropdownTypes = [
    { label: "Business", value: "Business" },
    { label: "Personal", value: "Personal" },
    { label: "Commute", value: "Commute" }
  ];
  tripTypeFilter = [
    { label: "All", value: "" },
    { label: "Business", value: "Business" },
    { label: "Personal", value: "Personal" },
    { label: "Commute", value: "Commute" }
  ];
  mergedFilter = [
    { label: "All", value: "" },
    { label: "Merged", value: "true" },
    { label: "Unmerged", value: "false" }
  ];
  viewTrip = false;
  flowId: any;
  autoTripsData: any = [];
  flowList = [];
  manualTripsData: any = [];
  filteredArray: any = [];
  tripsData: any = [];
  selectedTrips: any = [];
  selectedType = "all";
  selectedTrip: any;
  vehicleList: SelectItem[];
  vehicleFilterList: any[] = [
    {
      label: "All",
      value: " "
    }
  ];
  formatedFromAddress: string;
  formatedToAddress: any;
  fromLat: number;
  fromLong: number;
  toLat: any;
  toLong: any;
  date1: Date = new Date();
  constructor(
    private tripService: TripsService,
    private mileageService: MileageService,
    private loaderService: SpinnerService,
    private alertService: AlertService,
    private fb: FormBuilder,
    private dateFormatter: DateFormatterService,
    private mfservice: MoneyFlowReportsService,
    private apiCommonService: ApiCommonService
  ) {
    this.vehicleList = [];
    this.flowId = this.mileageService.getFlowId();
  }
  filterTrips(date) {
    this.getTripData(date);
  }
  getTripData(date) {
    const filterDate = this.dateFormatter.getFormattedDate(date);
    this.loaderService.showLoader();
    this.tripService.getTrips(this.flowId, filterDate).subscribe(
      res => {
        this.autoTripsData = res.automaticTrips;
        this.manualTripsData = res.manualTrips;
        this.setTableData("all");
        console.log(this.autoTripsData)
        console.log(this.manualTripsData)
      },
      err => { },
      () => {
        this.loaderService.hideLoader();
      }
    );
  }
  setTableData(selectedType) {
    this.tripsData = [];
    if (selectedType === "all") {
      this.autoTripsData.forEach(element => {
        this.tripsData.push(element)
      });
      this.manualTripsData.forEach(element => {
        this.tripsData.push(element)
      });
    }
    if (selectedType === "automatic") {
      this.tripsData = this.autoTripsData;
    } else if (selectedType === "manual") {
      this.tripsData = this.manualTripsData;
    }
    console.log(this.tripsData);
  }
  getFlowList() {
    this.flowList = [];
    this.tripService.getMoneyFlowTableData().subscribe(res => {
      res.forEach(element => {
        this.flowList.push({
          label: element.moneyFlowName,
          value: element.moneyFlowAccountId
        });
      }, (err) => {
      }, () => {
      });
      console.log(this.flowList);
    });
  }
  setFlow(event: any) {
    console.log(event);
    this.vehicleList = [];
    this.loaderService.showLoader();
    this.tripService.getVehicleListOfAFlow(event.value).subscribe(res => {
      res.forEach(element => {
        this.vehicleList.push({
          label: element.year + " " + element.make + " " + element.model,
          value: element.id
        });
      });
      console.log(res);
    }, (err) => {
    }, () => {
      this.loaderService.hideLoader();
    });
  }
  setTripDetails(data) {
    this.apiCommonService.getLatLong('https://maps.googleapis.com/maps/api/geocode/json?address=' + this.tripData.controls.fromAddress.value + '&key=AIzaSyDRNeu5OsRcsOGm9ce2V6uBWJv0cESDRGo').subscribe(res => {
      this.fromLat = res.results[0].geometry.location.lat
      this.fromLong = res.results[0].geometry.location.lng
      this.formatedFromAddress = res.results[0].formatted_address
    }, (err) => {
    }, () => {
    });
    this.apiCommonService.getLatLong('https://maps.googleapis.com/maps/api/geocode/json?address=' + this.tripData.controls.toAddress.value + '&key=AIzaSyDRNeu5OsRcsOGm9ce2V6uBWJv0cESDRGo').subscribe(res => {
      console.log(res)
      this.toLat = res.results[0].geometry.location.lat
      this.toLong = res.results[0].geometry.location.lng
      this.formatedToAddress = res.results[0].formatted_address
    }, (err) => {
    }, () => {
    });
    this.viewTrip = true;
    this.tripData.controls.toAddress.markAsTouched();
    this.tripData.controls.fromAddress.markAsTouched();
    console.log(data);
    this.tripData.controls.tripDate.setValue(data.tripDate);
    this.tripData.controls.toAddress.setValue(data.toAddress);
    this.tripData.controls.fromAddress.setValue(data.fromAddress);
    this.selectedTrip = data;
    var startDateArr = [];
    startDateArr = data.startTime.split(":");
    var startTime = new Date();
    startTime.setHours(startDateArr[0]);
    startTime.setMinutes(startDateArr[1]);
    var endDateArr = [];
    endDateArr = data.endTime.split(":");
    var endTime = new Date();
    endTime.setHours(endDateArr[0]);
    endTime.setMinutes(endDateArr[1]);
    console.log(this.flowList);
    var flow = this.flowList.filter(flow => flow.label == data.flowName);
    this.loaderService.showLoader();
    this.mfservice.getVehicleList(flow[0].value).subscribe(res => {
      res.forEach(element => {
        this.vehicleList.push({
          label: element.year + " " + element.make + " " + element.model,
          value: element.id
        });
      });
      if (this.vehicleList.length === 0) {
        this.vehicleList.push({
          label: "No Vehicles Available",
          value: " "
        });
      }
      this.tripData.patchValue({
        id: data.id,
        flowId: flow[0].value,
        vehicleId: data.vehicleId,
        tripType: data.tripType,
        startTime: startTime,
        endTime: endTime,
        mileageRate: data.mileageRate,
        tripCost: Math.round((data.tripCost + 0.00001) * 100) / 100 ,
        milesDriven:  Math.round((data.milesDriven + 0.00001) * 100) / 100,
        parking:  Math.round((data.parking + 0.00001) * 100) / 100 ,
        tolls:  Math.round((data.tolls + 0.00001) * 100) / 100 ,
        totalExpense: Math.round((data.totalExpense + 0.00001) * 100) / 100,
        manual: true
      });
    }, (err) => {
    }, () => {
      this.loaderService.hideLoader();
    });
  }
  deleteTrip() {
    this.loaderService.showLoader();
    this.tripService.deleteTrip(this.selectedTrip.id).subscribe(
      res => {
        this.alertService.clearMessage();
        this.alertService.sendMessage(res.message, "success");
      },
      err => { },
      () => {
        this.selectedType = "all";
        this.getTripData(this.date1);
        this.loaderService.hideLoader();
        this.deleteFrame.hide();
      }
    );
  }
  filterMergeData(data) {
    let newArray = [];
    this.selectedTrips = [];
    this.selectedTrip = data;
    newArray = this.tripsData.filter(
      trip => trip.tripType === data.tripType
    );
    newArray = newArray.filter(trip => trip.vehicle === data.vehicle);
    newArray = newArray.filter(trip => trip.tripDate === data.tripDate);
    newArray = newArray.filter(trip => trip.id !== data.id);
    console.log(newArray);
    this.filteredArray = newArray;
  }
  mergeTrips() {
    console.log(this.selectedTrips[0].id);
    console.log(this.selectedTrip);
    if (this.selectedTrips.length > 1) {
      this.alertService.clearMessage();
      this.alertService.sendMessage("Please Select One trip Only", "error");
    } else {
      if (this.selectedTrips.length !== 0) {
        this.loaderService.showLoader();
        this.tripService
          .mergeTrip(this.selectedTrip.id, this.selectedTrips[0].id)
          .subscribe(
            res => {
              this.alertService.clearMessage();
              this.alertService.sendMessage(res.message, "success");
            },
            err => { },
            () => {
              this.selectedType = "all";
              this.getTripData(this.date1);
              this.loaderService.hideLoader();
              this.mergeFrame.hide();
            }
          );
      }
    }
  }
  updateTrip() {
    if (this.tripData.valid) {
      this.loaderService.showLoader();
      console.log("update Block");
      if (this.viewTrip === true) {
        this.tripData.controls.fromAddress.setValue(this.formatedFromAddress);
        this.tripData.controls.toAddress.setValue(this.formatedToAddress);
      }
      this.tripData.controls.manual.setValue(true);
      console.log(this.tripData.value);
      const body = this.tripData.getRawValue();
      console.log(body);
      body.startTime = this.dateFormatter.getFormattedTime(
        this.tripData.controls.startTime.value
      );
      body.endTime = this.dateFormatter.getFormattedTime(
        this.tripData.controls.endTime.value
      );
      this.tripService
        .updateTrip(this.tripData.controls.id.value, this.tripData.controls.flowId.value, body)
        .subscribe(
          res => {
            this.alertService.clearMessage();
            this.alertService.sendMessage(res.message, "success");
          },
          err => { },
          () => {
            this.tripData.reset();
            this.tripData.controls.tripDate.setValue(new Date());
            this.tripData.controls.mileageRate.setValue(0.58);
            this.getTripData(this.date1);
            this.viewTrip = false;
            this.detailsFrame.hide();
            this.loaderService.hideLoader();
          }
        );
    } else {
      Object.keys(this.tripData.controls).forEach(key => {
        this.tripData.get(key).markAsDirty();
      });
    }
  }
  onParkingInputChange(data: number) {
    const total = (+data) + (+this.tripData.controls.tripCost.value) + (+this.tripData.controls.tolls.value);
    this.tripData.controls.totalExpense.setValue(
      Math.round((total + 0.00001) * 100) / 100
    );
  }
  onTollInputChange(data) {
    const total = (+data) + (+this.tripData.controls.tripCost.value) + (+this.tripData.controls.parking.value);
    this.tripData.controls.totalExpense.setValue(
      Math.round((total + 0.00001) * 100) / 100
    );
  }
  setFromLocation(address: Address) {
    console.log(address);
    this.viewTrip = true;
    this.formatedFromAddress = address.formatted_address;
    console.log(address.formatted_address);
    console.log(address.geometry.location.lng());
    console.log(address.geometry.location.lat());
    this.fromLat = address.geometry.location.lat();
    this.fromLong = address.geometry.location.lng();
    this.calculateDistance(
      this.fromLong,
      this.fromLat,
      this.toLong,
      this.toLat
    );
    this.setRateData(this.tripData.controls.mileageRate.value);
  }
  setToLocation(address: Address) {
    this.viewTrip = true;
    this.formatedToAddress = address.formatted_address;
    console.log(address.formatted_address);
    console.log(address.geometry.location.lng());
    console.log(address.geometry.location.lat());
    this.toLat = address.geometry.location.lat();
    this.toLong = address.geometry.location.lng();
    this.calculateDistance(
      this.fromLong,
      this.fromLat,
      this.toLong,
      this.toLat
    );
    this.setRateData(this.tripData.controls.mileageRate.value);
  }
  setRateData(data: number) {
    const tripCost = data * this.tripData.controls.milesDriven.value;
    this.tripData.controls.tripCost.setValue(
      Math.round((tripCost + 0.00001) * 100) / 100
    );
    this.tripData.controls.totalExpense.setValue(
      this.tripData.controls.tripCost.value
    );
  }
  calculateDistance(fromLong, fromLat, toLong, toLat) {
    if (this.tripData.controls.fromAddress.untouched) {
      this.alertService.clearMessage();
      this.alertService.sendMessage(
        "Please Select From Address first",
        "error"
      );
      this.tripData.controls.toAddress.setValue(
        this.tripData.controls.toAddress.value
      );
    } else {
      const fromAddress: LatLng = new google.maps.LatLng(fromLat, fromLong);
      const toAddress: LatLng = new google.maps.LatLng(toLat, toLong);
      const distance = google.maps.geometry.spherical.computeDistanceBetween(
        fromAddress,
        toAddress
      );
      this.tripData.controls.milesDriven.setValue(
        Math.round(((distance / 1000) * 0.621371 + 0.00001) * 100) / 100
      );
    }
  }
  formatTime(data, type) {
  }
  formatDate() {
    this.tripData.controls.tripDate.setValue(
      this.dateFormatter.getFormattedDate(this.tripData.controls.tripDate.value)
    );
  }
  ngOnInit() {
    this.getFlowList();
    this.getTripData(new Date());
    this.tripData = this.fb.group({
      id: [null],
      vehicleId: [null, [Validators.required]],
      flowId: [null, [Validators.required]],
      tripType: [null, [Validators.required]],
      tripDate: [null, [Validators.required]],
      startTime: [null, [Validators.required]],
      endTime: [null, [Validators.required]],
      fromAddress: [null, [Validators.required]],
      toAddress: [null, [Validators.required]],
      mileageRate: [{ value: "", disabled: true }, [Validators.required]],
      tripCost: [{ value: "", disabled: true }, [Validators.required]],
      milesDriven: [{ value: "", disabled: true }],
      parking: [0],
      tolls: [0],
      totalExpense: [{ value: "", disabled: true }, [Validators.required]],
      manual: [true]
    });
  }
}
