import { Injectable } from "@angular/core";
import { ApiCommonService } from "src/app/core/services/api-common.service";
import { Observable } from "rxjs";
@Injectable({
  providedIn: "root"
})
export class TripsService {
  constructor(private apiCommonService: ApiCommonService) { }
  getTrips(id, date): Observable<any> {
    return this.apiCommonService.get("/money-flow/mileage-tracker/mileage-list?flowId=" + id + "&date=" + date);
  }
  getMoneyFlowTableData(): Observable<any> {
    return this.apiCommonService.get('/money-flow/details');
  }
  getVehicleListOfAFlow(flowId: any) {
    return this.apiCommonService.get('/money-flow/mileage-tracker/vehicle-list?flowId=' + flowId);
  }
  deleteTrip(id): Observable<any> {
    return this.apiCommonService.delete('/money-flow/mileage-tracker/delete-mileage?mileageId=' + id);
  }
  mergeTrip(id1, id2) {
    return this.apiCommonService.put(`/money-flow/mileage-tracker/merge-mileages?tripId1=${id1}&tripId2=${id2}`, {});
  }
  getVehicleList(id): Observable<any> {
    return this.apiCommonService.get('/money-flow/mileage-tracker/vehicle-list?flowId=' + id);
  }
  updateTrip(id, newFlowId, body): Observable<any> {
    return this.apiCommonService.put('/money-flow/mileage-tracker/update-mileage?mileageId=' + id + '&newFlowId=' + newFlowId, body);
  }
}
