import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators, AbstractControl } from "@angular/forms";
import { VehiclesService } from "./vehicles.service";
import { SpinnerService } from "src/app/core/services/spinner.service";
import { AlertService } from "src/app/core/services/alert.service";
import { ModalDirective } from "ng-uikit-pro-standard";
import { MileageService } from "../mileage.service";
@Component({
  selector: "app-vehicles",
  templateUrl: "./vehicles.component.html",
  styleUrls: ["./vehicles.component.scss"]
})
export class VehiclesComponent implements OnInit {
  @ViewChild("frame") frame: ModalDirective;
  @ViewChild("deleteFrame") deleteFrame: ModalDirective;
  tableColumns = [
    { field: "make", header: "Company" },
    { field: "model", header: "Model" },
    { field: "year", header: "Year" },
    { field: "odoReading", header: "Current Odometer Reading" },
    { field: "plate", header: "Plate" },
    { field: "action", header: "Action" }
  ];
  yearList = [];
  companyList = [];
  vehicleList: any = [];
  vehicleGroup: FormGroup;
  flowId: any;
  selectedVehicleId: any;
  modelName: string;
  constructor(
    private _fb: FormBuilder,
    private vehicleService: VehiclesService,
    private mileageService: MileageService,
    private loaderService: SpinnerService,
    private alertService: AlertService
  ) { }
  getYearList() {
    const d = new Date();
    let currentYear = d.getFullYear();
    this.yearList.push({ label: currentYear, value: currentYear });
    for (let i = 50; i > 0; i--) {
      this.yearList.push({ label: currentYear - 1, value: currentYear - 1 });
      currentYear = currentYear - 1;
    }
  }
  getMakeList() {
    this.vehicleService.getMakeList().subscribe(res => {
      res.forEach(element => {
        this.companyList.push({ label: element.name, value: element.name });
      });
      console.log(this.companyList);
    });
  }
  getVehicleList() {
    this.loaderService.showLoader();
    this.vehicleService.getVehicleList(this.flowId).subscribe(
      res => {
        this.vehicleList = res;
      },
      err => { },
      () => {
        this.loaderService.hideLoader();
      }
    );
  }
  addVehicle() {
    if (this.vehicleGroup.valid) {
      this.loaderService.showLoader();
      const body = this.vehicleGroup.value;
      this.vehicleService.addNewVehicle(this.flowId, body).subscribe(
        res => {
          this.alertService.clearMessage();
          this.alertService.sendMessage(res.message, "success");
        },
        err => { },
        () => {
          this.loaderService.hideLoader();
          this.getVehicleList();
          this.frame.hide();
        }
      );
    } else {
      Object.keys(this.vehicleGroup.controls).forEach(key => {
        this.vehicleGroup.get(key).markAsDirty();
      });
    }
  }
  updateVehicle() {
    if (this.vehicleGroup.valid) {
      this.loaderService.showLoader();
      const body = this.vehicleGroup.value;
      this.vehicleService
        .updateVehicle(this.vehicleGroup.controls.vehicleId.value, body)
        .subscribe(
          res => {
            this.alertService.clearMessage();
            this.alertService.sendMessage(res.message, "success");
          },
          err => { },
          () => {
            this.loaderService.hideLoader();
            this.getVehicleList();
            this.frame.hide();
          }
        );
    } else {
      Object.keys(this.vehicleGroup.controls).forEach(key => {
        this.vehicleGroup.get(key).markAsDirty();
      });
    }
  }
  setVehicleId(data) {
    this.selectedVehicleId = data.id;
  }
  deleteVehicle() {
    this.loaderService.showLoader();
    this.vehicleService.deleteVehicle(this.selectedVehicleId).subscribe(
      res => {
        this.alertService.clearMessage();
        this.alertService.sendMessage(res.message, "success");
      },
      err => { },
      () => {
        this.getVehicleList();
        this.loaderService.hideLoader();
        this.deleteFrame.hide();
      }
    );
  }
  setVehicleDetails(data) {
    console.log(data);
    this.vehicleGroup.controls.vehicleId.setValue(data.id);
    this.vehicleGroup.controls.year.setValue(data.year);
    this.vehicleGroup.controls.model.setValue(data.model);
    this.vehicleGroup.controls.make.setValue(data.make);
    this.vehicleGroup.controls.odoReading.setValue(data.odoReading);
    this.vehicleGroup.controls.plate.setValue(data.plate);
  }
  ngOnInit() {
    this.flowId = this.mileageService.getFlowId();
    this.vehicleGroup = this._fb.group({
      vehicleId: [null],
      year: [null, [Validators.required]],
      model: [null, [Validators.required]],
      make: [null, [Validators.required]],
      odoReading: [null, this.nonZero],
      plate: [null]
    });
    this.getYearList();
    this.getMakeList();
    this.getVehicleList();
  }
  nonZero(control: AbstractControl): { [key: string]: boolean } | null {
    if (Number(control.value) < 0) {
      return { nonZero: true };
    } else {
      return null;
    }
  }
}
