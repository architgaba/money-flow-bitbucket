import { Injectable } from '@angular/core';
import { ApiCommonService } from 'src/app/core/services/api-common.service';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class VehiclesService {
  constructor(private apiCommonService: ApiCommonService) { }
  getMakeList(): Observable<any> {
    return this.apiCommonService.get('/money-flow/mileage-tracker/make-list');
  }
  getVehicleList(id): Observable<any> {
    return this.apiCommonService.get('/money-flow/mileage-tracker/vehicle-list?flowId=' + id);
  }
  addNewVehicle(id, body): Observable<any> {
    return this.apiCommonService.post('/money-flow/mileage-tracker/add-vehicle?flowId=' + id, body);
  }
  deleteVehicle(id): Observable<any> {
    return this.apiCommonService.delete('/money-flow/mileage-tracker/delete-vehicle?vehicleId=' + id);
  }
  updateVehicle(id, body): Observable<any> {
    return this.apiCommonService.put('/money-flow/mileage-tracker/update-vehicle?vehicleId=' + id, body);
}
}