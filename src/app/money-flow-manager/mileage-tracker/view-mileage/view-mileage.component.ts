import { Component, OnInit } from "@angular/core";
import { MileageService } from "../mileage.service";
import { MoneyflowmanagerService } from "../../moneyflowmanager.service";
import { DashboardService } from "src/app/dashboard/dashboard.service";
@Component({
  selector: "app-view-mileage",
  templateUrl: "./view-mileage.component.html",
  styleUrls: ["./view-mileage.component.scss"]
})
export class ViewMileageComponent implements OnInit {
  selectedIndex: any = 0;
  flowId: any;
  constructor(
    private mileageService: MileageService,
    private mfmService: MoneyflowmanagerService,
    private dashboardService: DashboardService
  ) {
    this.flowId = this.mfmService.getMoneyFlowId();
    this.mileageService.setFlowId(this.flowId);
  }
  ngOnInit() {
    this.dashboardService.clearMessage();
    this.dashboardService.sendMessage(1);
  }
  setSelectedIndex(index: any) {
    this.selectedIndex = index;
  }
}
