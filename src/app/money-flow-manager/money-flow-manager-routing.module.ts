import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MoneyFlowManagerComponent } from './money-flow-manager/money-flow-manager.component';
import { ViewMoneyFlowComponent } from './view-money-flow/view-money-flow.component';
import { MoneyFlowTableComponent } from './money-flow-table/money-flow-table.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { IncomeComponent } from './income/income.component';
import { ExpensesComponent } from './expenses/expenses.component';
import { RecurringComponent } from './recurring/recurring.component';
import { UnassignedReceiptComponent } from './unassigned-receipt/unassigned-receipt.component';
import { BudgetComponent } from './budget/budget.component';
import { NgxPermissionsGuard } from 'ngx-permissions';
const routes: Routes = [
  {
    path: '',
    canActivate: [NgxPermissionsGuard],
    component: MoneyFlowManagerComponent,
    children: [
      {
        path: '',
        canActivate: [NgxPermissionsGuard],
        component: MoneyFlowTableComponent
      },
      {
        path: 'money-flow-table',
        canActivate: [NgxPermissionsGuard],
        component: MoneyFlowTableComponent
      },
      {
        path: 'view-money-flow',
        canActivate: [NgxPermissionsGuard],
        component: ViewMoneyFlowComponent,
        children: [
          {
            path: '',
            canActivate: [NgxPermissionsGuard],
            component: DashboardComponent
          },
          {
            path: 'dashboard',
            canActivate: [NgxPermissionsGuard],
            component: DashboardComponent
          },
          {
            path: 'income',
            canActivate: [NgxPermissionsGuard],
            component: IncomeComponent
          },
          {
            path: 'expenses',
            component: ExpensesComponent
          },
          {
            path: 'recurring',
            canActivate: [NgxPermissionsGuard],
            component: RecurringComponent
          },
          {
            path: 'unassigned-receipt',
            canActivate: [NgxPermissionsGuard],
            component: UnassignedReceiptComponent
          },
          {
            path: 'budget',
            canActivate: [NgxPermissionsGuard],
            component: BudgetComponent
          },
          {
            path: 'mileage-tracker',
            canActivate: [NgxPermissionsGuard],
            canLoad: [NgxPermissionsGuard],
            loadChildren: 'src/app/money-flow-manager/mileage-tracker/mileage-tracker.module#MileageTrackerModule',
            data: {
              permissions: {
                only: ['ROLE_CUSTOMER_REP', 'ROLE_CUSTOMER', 'ROLE_SUPER_ADMIN', 'ROLE_PROSPECT'],
                redirectTo: '/home'
              }
            }
          }
        ]
      },
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MoneyFlowManagerRoutingModule { }
