import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableModule } from 'primeng/table';
import {
  AccordionModule, ModalModule,
  TooltipModule, PopoverModule,
  WavesModule, NavbarModule, ButtonsModule,
  CardsFreeModule, TabsModule,
  InputsModule, IconsModule, CheckboxModule,
  DatepickerModule, SelectModule, InputUtilitiesModule
} from 'ng-uikit-pro-standard';
import { NgxPermissionsModule } from 'ngx-permissions';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MoneyFlowManagerRoutingModule } from './money-flow-manager-routing.module';
import { MoneyFlowManagerComponent } from './money-flow-manager/money-flow-manager.component';
import { ViewMoneyFlowComponent } from './view-money-flow/view-money-flow.component';
import { MoneyFlowTableComponent } from './money-flow-table/money-flow-table.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { IncomeComponent } from './income/income.component';
import { ExpensesComponent } from './expenses/expenses.component';
import { RecurringComponent } from './recurring/recurring.component';
import { UnassignedReceiptComponent } from './unassigned-receipt/unassigned-receipt.component';
import { DatePipe } from '@angular/common';
import { Ng2ImgMaxModule } from 'ng2-img-max';
import { BudgetComponent } from './budget/budget.component';
import {CalendarModule} from 'primeng/calendar';
import { TabMenuModule } from 'primeng/tabmenu';
import { PipeModule } from '../core/pipes/pipe.module';
@NgModule({
  declarations: [MoneyFlowManagerComponent,
    ViewMoneyFlowComponent,
    MoneyFlowTableComponent,
    DashboardComponent,
    IncomeComponent,
    ExpensesComponent,
    RecurringComponent,
    UnassignedReceiptComponent,
    BudgetComponent,
  ],
  imports: [
    CommonModule,
    TabMenuModule,
    Ng2ImgMaxModule,
    MoneyFlowManagerRoutingModule,
    CommonModule,
    NgxPermissionsModule.forChild(),
    AccordionModule,
    WavesModule,
    NavbarModule,
    ButtonsModule,
    CardsFreeModule,
    TableModule,
    FormsModule,
    InputsModule,
    IconsModule,
    TabMenuModule,
    TabsModule,
    ReactiveFormsModule,
    CheckboxModule,
    ModalModule,
    TooltipModule,
    PopoverModule,
    DatepickerModule,
    SelectModule,
    InputUtilitiesModule,
    CalendarModule,
    PipeModule
  ],
  providers: [DatePipe ]
})
export class MoneyFlowManagerModule { }
