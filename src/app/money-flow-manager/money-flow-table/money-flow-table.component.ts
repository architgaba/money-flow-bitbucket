import { Component, OnInit, ViewChild } from '@angular/core';
import { MoneyFlowTableService } from './money-flow.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { ModalDirective } from 'ng-uikit-pro-standard';
import { AlertService } from 'src/app/core/services/alert.service';
import { DateFormatterService } from 'src/app/core/services/date-formatter.service';
import { MoneyflowmanagerService } from '../moneyflowmanager.service';
import { DashboardService } from 'src/app/dashboard/dashboard.service';
@Component({
  selector: 'app-money-flow-table',
  templateUrl: './money-flow-table.component.html',
  styleUrls: ['./money-flow-table.component.scss']
})
export class MoneyFlowTableComponent implements OnInit {
  @ViewChild('frame') frame: ModalDirective;
  @ViewChild('deleteFrame') deleteFrame: ModalDirective;
  filterFromDate: Date = new Date(new Date().getFullYear(), 0, 1);
  filterToDate: Date = new Date();
  newMoneyFlow: FormGroup;
  table: any = [];
  tableColumns: any = [
    { field: 'moneyFlowName', header: 'Account Name' },
    { field: 'creationDate', header: 'Creation Date' },
    { field: 'lastModifiedDate', header: 'Last Modified Date' },
    { field: 'moneyFlowType', header: 'Account Type' },
    { field: 'action', header: 'Actions' },
  ];
  id: any;
  mfm_firstItemIndex;
  mfm_lastItemIndex;
  mfm_previous: any = [];
  selectedFlowId: any;
  searchText = '';
  constructor(
    private router: Router,
    private moneyFlowTableService: MoneyFlowTableService,
    private _fb: FormBuilder, private spinnerService: SpinnerService,
    private dateFormatter: DateFormatterService,
    private alert: AlertService,
    private mfmService: MoneyflowmanagerService,
    private dashboardService: DashboardService) {
  }
  ngOnInit() {
    this.dashboardService.clearMessage();
    this.dashboardService.sendMessage(1);
    this.newMoneyFlow = this._fb.group({
      "moneyFlowAccountId": [],
      "moneyFlowName": [null, [Validators.required]],
      "moneyFlowType": [null, [Validators.required]]
    });
    console.log(this.table);
    // this.filterFromDate.setDate(new Date().getFullYear(), 0, 1);
    this.getMoneyFlowList();
  }
  closeDialog() {
  }
  submitForNavigation(id) {
    this.mfmService.setMoneyFlowId(id);
    this.router.navigate(['./dashboard/money-flow-manager/view-money-flow/']);
  }
  removeMoneyFlow() {
    this.spinnerService.showLoader();
    this.moneyFlowTableService.deleteMoneyFlow(this.selectedFlowId).subscribe(res => {
      console.log(res);
      this.alert.clearMessage();
      this.alert.sendMessage(res.message, 'success');
    }, (err) => {
      this.alert.clearMessage();
      this.alert.sendMessage(err.message, 'error');
    }, () => {
      this.selectedFlowId = null;
      this.getMoneyFlowList();
      this.deleteFrame.hide();
      this.spinnerService.hideLoader();
    });
  }
  resetForm() {
    this.newMoneyFlow.reset();
    this.newMoneyFlow.controls.moneyFlowType.setValue('BUSINESS');
  }
  setSelectedFlowId(id: any) {
    this.selectedFlowId = id;
  }
  getMoneyFlowList() {
    this.spinnerService.showLoader();
    this.table = [];
    var fromDate = this.dateFormatter.getFormattedDate(this.filterFromDate);
    var toDate = this.dateFormatter.getFormattedDate(this.filterToDate);
    this.moneyFlowTableService.getMoneyFlowTableData(fromDate, toDate).subscribe(res => {
      this.spinnerService.hideLoader();
      console.log(res);
      this.table = res;
    }, (err) => {
      this.spinnerService.hideLoader();
    }, () => {
    });
  }
  chooseMoneyFlow(row: any) {
    this.newMoneyFlow.controls.moneyFlowAccountId.setValue(row.moneyFlowAccountId);
    this.newMoneyFlow.controls.moneyFlowName.setValue(row.moneyFlowName);
    this.newMoneyFlow.controls.moneyFlowType.setValue(row.moneyFlowType);
  }
  createNewMoneyFlow() {
    if (this.newMoneyFlow.valid) {
      this.spinnerService.showLoader();
      const body = {
        "moneyFlowAccountId": 0,
        "moneyFlowName": this.newMoneyFlow.controls.moneyFlowName.value,
        "moneyFlowType": this.newMoneyFlow.controls.moneyFlowType.value,
      };
      this.moneyFlowTableService.addNewMoneyFlow(body).subscribe(res => {
        this.spinnerService.hideLoader();
        this.frame.hide();
        console.log(res);
        this.alert.clearMessage();
        this.alert.sendMessage(res.message, 'success');
      }, (err) => {
        this.spinnerService.hideLoader();
        this.frame.hide();
      }, () => {
        this.closeDialog();
        this.getMoneyFlowList();
        this.frame.hide();
      }
      );
    }
    else {
      Object.keys(this.newMoneyFlow.controls).forEach(key => {
        this.newMoneyFlow.get(key).markAsDirty();
      });
    }
  }
  selectedPlan(type: any) {
    this.newMoneyFlow.controls.moneyFlowType.setValue(type);
  }
  updateMoneyFlow() {
    if (this.newMoneyFlow.valid) {
      this.spinnerService.showLoader();
      const body = {
        "moneyFlowAccountId": this.newMoneyFlow.controls.moneyFlowAccountId.value,
        "moneyFlowName": this.newMoneyFlow.controls.moneyFlowName.value,
        "moneyFlowType": this.newMoneyFlow.controls.moneyFlowType.value,
      };
      this.moneyFlowTableService.updateMoneyFlow(this.newMoneyFlow.controls.moneyFlowAccountId.value, body).subscribe(res => {
        this.alert.clearMessage();
        this.alert.sendMessage(res.message, 'success');
        console.log(res);
        this.frame.hide();
      }, (err) => {
        this.spinnerService.hideLoader();
        this.alert.clearMessage();
        this.alert.sendMessage(err.message, 'error');
        this.frame.hide();
      }, () => {
        this.spinnerService.hideLoader();
        this.closeDialog();
        this.getMoneyFlowList();
        this.frame.hide();
      }
      );
    }
    else {
      Object.keys(this.newMoneyFlow.controls).forEach(key => {
        this.newMoneyFlow.get(key).markAsDirty();
      });
    }
  }
}
