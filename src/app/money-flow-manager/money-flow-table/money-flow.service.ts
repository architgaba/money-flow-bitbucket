import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiCommonService } from 'src/app/core/services/api-common.service';
@Injectable({ providedIn: 'root' })
export class MoneyFlowTableService {
    constructor(private apiCommonService: ApiCommonService
    ) { }
    getMoneyFlowTableData(fromDate: any, toDate: any): Observable<any> {
        return this.apiCommonService.get('/money-flow/details?fromDate=' + fromDate + '&toDate=' + toDate);
    }
    addNewMoneyFlow(body): Observable<any> {
        return this.apiCommonService.post('/money-flow/create', body);
    }
    deleteMoneyFlow(id): Observable<any> {
        return this.apiCommonService.delete('/money-flow/delete/' + id);
    }
    updateMoneyFlow(id, body): Observable<any> {
        return this.apiCommonService.put('/money-flow/update/' + id, body);
    }
}
