import { Injectable } from "@angular/core";
@Injectable({
  providedIn: "root"
})
export class MoneyflowmanagerService {
  flowId: any;
  constructor() { }
  setMoneyFlowId(id: any) {
    window.localStorage.clear();
    window.localStorage.setItem("flowId", id);
    this.flowId = id;
  }
  getMoneyFlowId() {
    return window.localStorage.getItem("flowId");
  }
}
