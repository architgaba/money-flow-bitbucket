import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiCommonService } from 'src/app/core/services/api-common.service';
@Injectable({
  providedIn: 'root'
})
export class RecurrenceService {
  constructor(private apiCommonService: ApiCommonService) { }
  getRecurringData(id, fromDate: any, toDate: any): Observable<any> {
    return this.apiCommonService.get('/money-flow/recurring-entries/' + id+ '?fromDate=' + fromDate + '&toDate=' + toDate);
  }
}
