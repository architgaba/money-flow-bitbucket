import { Component, OnInit, ViewChild } from '@angular/core';
import { MdbTablePaginationComponent, MdbTableService } from 'ng-uikit-pro-standard';
import { RecurrenceService } from './recurrence.service';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { DateFormatterService } from 'src/app/core/services/date-formatter.service';
import { MoneyflowmanagerService } from '../moneyflowmanager.service';
import { DashboardService } from 'src/app/dashboard/dashboard.service';
@Component({
  selector: 'app-recurring',
  templateUrl: './recurring.component.html',
  styleUrls: ['./recurring.component.scss']
})
export class RecurringComponent implements OnInit {
  @ViewChild(MdbTablePaginationComponent) mdbTablePagination: MdbTablePaginationComponent;
  firstItemIndex: number;
  lastItemIndex: any;
  flowId: any;
  recurrenceList: any = [];
  previous: any = [];
  searchText = '';
  filterFromDate: Date = new Date(new Date().getFullYear(), 0, 1);
  filterToDate: Date = new Date();
  tableColumns = [
    { field: 'type', header: 'Type' },
    { field: 'amount', header: 'Amount' },
    { field: 'recurringType', header: 'Category' },
    { field: 'recurrenceDate', header: 'Recurrence Date' },
    { field: 'source', header: 'Source' }
  ];
  constructor(private tableService: MdbTableService,
    private recurrenceService: RecurrenceService,
    private spinnerService: SpinnerService,
    private dateFormatter: DateFormatterService,
    private mfmService: MoneyflowmanagerService,
    private dashboardService: DashboardService) {
    // this.filterFromDate.setDate(this.filterFromDate.getDate() - 30);
    this.flowId = this.mfmService.getMoneyFlowId();
    this.getRecurrenceList();
  }
  getRecurrenceList() {
    this.spinnerService.showLoader();
    this.recurrenceList = [];
    var fromDate = this.dateFormatter.getFormattedDate(this.filterFromDate);
    var toDate = this.dateFormatter.getFormattedDate(this.filterToDate);
    this.recurrenceService.getRecurringData(this.flowId, fromDate, toDate).subscribe(res => {
      console.log(res);
      this.recurrenceList = res;
      this.tableService.setDataSource(this.recurrenceList);
      this.recurrenceList = this.tableService.getDataSource();
      this.previous = this.tableService.getDataSource();
      this.spinnerService.hideLoader();
    },
      (err) => {
        this.spinnerService.hideLoader();
      },
      () => {
        this.spinnerService.hideLoader();
      });
  }
  ngOnInit() {
    this.dashboardService.clearMessage();
    this.dashboardService.sendMessage(1);
  }
}
