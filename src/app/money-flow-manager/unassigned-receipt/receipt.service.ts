import { Injectable } from '@angular/core';
import { ApiCommonService } from 'src/app/core/services/api-common.service';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ReceiptService {
  constructor(private apiCommonService: ApiCommonService) { }
  getExpenseReceiptType(): Observable<any> {
    return this.apiCommonService.get('/money-flow/expense/fetch/expense-types');
  }
  getIncomeReceiptType(): Observable<any> {
    return this.apiCommonService.get('/money-flow/income/fetch/income-types');
  }
  createNewExpenseReceipt(id, body): Observable<any> {
    return this.apiCommonService.postWithFormData('/money-flow/expense/create/' + id, body);
  }
  createNewIncomeReceipt(id, body): Observable<any> {
    return this.apiCommonService.postWithFormData('/money-flow/income/create/' + id, body);
  }
  createNewExpenseReceiptType(body): Observable<any> {
    return this.apiCommonService.post('/money-flow/expense/create/type', body);
  }
  createNewIncomeReceiptType(body): Observable<any> {
    return this.apiCommonService.post('/money-flow/income/create/type', body);
  }
  getReceiptList(id, fromDate: any, toDate: any): Observable<any> {
    return this.apiCommonService.get('/money-flow/unassigned-receipts/' + id + '?fromDate=' + fromDate + '&toDate=' + toDate);
  }
  deleteExpenseReceipt(id): Observable<any> {
    return this.apiCommonService.delete('/money-flow/expense/delete/' + id);
  }
  deleteIncomeReceipt(id): Observable<any> {
    return this.apiCommonService.delete('/money-flow/income/delete/' + id);
  }
  updateExpenseReceipt(id, body): Observable<any> {
    return this.apiCommonService.putWithFormData('/money-flow/expense/update/' + id, body);
  }
  updateIncomeReceipt(id, body): Observable<any> {
    return this.apiCommonService.putWithFormData('/money-flow/income/update/' + id, body);
  }
  deleteReceipt(id: any, type: any): Observable<any> {
    if (type === 'Income') {
      return this.apiCommonService.delete('/money-flow/receipts/delete-receipts?moneyFlowIncomeId=' + id);
    }
    else {
      return this.apiCommonService.delete('/money-flow/receipts/delete-receipts?moneyFlowExpenseId=' + id);
    }
  }
}
