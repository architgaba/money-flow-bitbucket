import {
  Component,
  OnInit,
  ViewChild,
  ElementRef
} from "@angular/core";
import {
  ModalDirective
} from "ng-uikit-pro-standard";
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators
} from "@angular/forms";
import { DomSanitizer } from "@angular/platform-browser";
import { Ng2ImgMaxService } from "ng2-img-max";
import { SpinnerService } from "src/app/core/services/spinner.service";
import { ReceiptService } from "./receipt.service";
import { AlertService } from "src/app/core/services/alert.service";
import { DateFormatterService } from "src/app/core/services/date-formatter.service";
import { MoneyflowmanagerService } from "../moneyflowmanager.service";
import { DashboardService } from "src/app/dashboard/dashboard.service";
@Component({
  selector: "app-unassigned-receipt",
  templateUrl: "./unassigned-receipt.component.html",
  styleUrls: ["./unassigned-receipt.component.scss"]
})
export class UnassignedReceiptComponent implements OnInit {
  @ViewChild("frame") frame: ModalDirective;
  @ViewChild("deleteFrame") deleteFrame: ModalDirective;
  @ViewChild("fileInput") myInputVariable: ElementRef;
  dialogType = "";
  receiptForm: FormGroup;
  newReceiptType = new FormControl();
  flowId: any;
  receiptType: any[];
  receiptList: any = [];
  showAddNewType = false;
  previous: any = [];
  firstItemIndex;
  lastItemIndex;
  selectedReceiptId: any;
  selectedFile: File = null;
  selectedFileName = "";
  url = "";
  imageBasic: any[] = [];
  expenseReceiptType: any[];
  incomeReceiptType: any[];
  unassigned = true;
  tableColumns = [
    { field: "type", header: "Receipt Type" },
    { field: "amount", header: "Amount" },
    { field: "date", header: "Date" },
    { field: "recurring", header: "Recurring" },
    { field: "action", header: "Action" }
  ];
  filterFromDate: Date = new Date(new Date().getFullYear(), 0, 1);
  filterToDate: Date = new Date();
  constructor(
    private alert: AlertService,
    public sanitizer: DomSanitizer,
    private ng2ImgMax: Ng2ImgMaxService,
    private spinnerService: SpinnerService,
    private _fb: FormBuilder,
    private receiptService: ReceiptService,
    private dateFormatter: DateFormatterService,
    private mfmService: MoneyflowmanagerService,
    private dashboardService: DashboardService
  ) {
    // this.filterFromDate.setDate(this.filterFromDate.getDate() - 30);
    this.flowId = this.mfmService.getMoneyFlowId();
    this.getReceiptTypes();
    this.getReceiptList();
  }
  setDate() {
    this.receiptForm.controls.date.setValue(new Date());
    this.receiptForm.controls.recurrenceDate.setValue(new Date());
  }
  deleteReceipt() {
    if (this.receiptForm.controls.moneyFlowReceiptId.value != null) {
      this.spinnerService.showLoader();
      this.receiptService
        .deleteReceipt(this.receiptForm.controls.moneyFlowReceiptId.value, this.dialogType)
        .subscribe(
          res => {
            this.alert.clearMessage();
            this.alert.sendMessage(res.message, "success");
          },
          err => {
            this.alert.clearMessage();
            this.alert.sendMessage(err.message, "error");
          },
          () => {
            this.spinnerService.hideLoader();
            this.getReceiptList();
          }
        );
    }
  }
  setReceiptType(type) {
    console.log(type);
    this.dialogType = type;
    if (type === "Income") {
      this.receiptType = this.incomeReceiptType;
      console.log(this.receiptType);
    } else if (type === "Expense") {
      this.receiptType = this.expenseReceiptType;
      console.log(this.receiptType);
    }
    this.receiptForm.controls.receiptType.setValue("");
  }
  getReceiptTypes() {
    this.expenseReceiptType = [];
    this.incomeReceiptType = [];
    this.receiptService.getExpenseReceiptType().subscribe(
      res => {
        console.log(res);
        this.expenseReceiptType = res;
      },
      err => { },
      () => { }
    );
    this.receiptService.getIncomeReceiptType().subscribe(
      res => {
        this.incomeReceiptType = res;
      },
      err => {
        this.alert.clearMessage();
        this.alert.sendMessage(err.message, "error");
      },
      () => { }
    );
  }
  createNewReceiptType() {
    if (this.dialogType === "Expense") {
      if (
        this.newReceiptType.value !== null ||
        this.newReceiptType.value !== ""
      ) {
        const body = {
          custom: true,
          name: this.newReceiptType.value,
          typeId: 0
        };
        this.receiptService.createNewExpenseReceiptType(body).subscribe(
          res => {
            this.getReceiptTypes();
            this.alert.clearMessage();
            this.alert.sendMessage(res.message, "success");
          },
          err => {
            this.getReceiptTypes();
            this.alert.clearMessage();
            this.alert.sendMessage(err.message, "error");
          }
        );
      }
    } else if (this.dialogType === "Income") {
      if (
        this.newReceiptType.value !== null ||
        this.newReceiptType.value !== ""
      ) {
        const body = {
          custom: true,
          name: this.newReceiptType.value,
          typeId: 0
        };
        this.receiptService.createNewIncomeReceiptType(body).subscribe(
          res => {
            this.getReceiptTypes();
            this.alert.clearMessage();
            this.alert.sendMessage(res.message, "success");
          },
          err => {
            this.getReceiptTypes();
            this.alert.clearMessage();
            this.alert.sendMessage(err.message, "error");
          }
        );
      }
    }
  }
  createNewReceipt() {
    if (this.receiptForm.valid && this.newReceiptType.valid) {
      if (this.showAddNewType === true) {
        this.createNewReceiptType();
        this.receiptForm.controls.receiptType.setValue(
          this.newReceiptType.value
        );
      }
      this.spinnerService.showLoader();
      let expenseDate;
      let recurrenceDate;
      expenseDate = this.dateFormatter.getFormattedDate(
        this.receiptForm.controls.date.value
      );
      if (this.receiptForm.controls.recurring.value) {
        recurrenceDate = this.dateFormatter.getFormattedDate(
          this.receiptForm.controls.recurrenceDate.value
        );
      }
      this.receiptForm.controls.unassigned.setValue(true);
      const body = new FormData();
      body.append("amount", this.receiptForm.controls.amount.value);
      body.append("date", expenseDate);
      body.append("type", this.receiptForm.controls.receiptType.value);
      body.append(
        "notes",
        this.receiptForm.controls.notes.value
          ? this.receiptForm.controls.notes.value
          : ""
      );
      body.append(
        "recurring",
        this.receiptForm.controls.recurring.value
          ? this.receiptForm.controls.recurring.value
          : false
      );
      body.append("recurrenceDate", recurrenceDate ? recurrenceDate : "");
      body.append(
        "source",
        this.receiptForm.controls.source.value
          ? this.receiptForm.controls.source.value
          : ""
      );
      body.append("file", this.selectedFile);
      body.append("unassigned", this.receiptForm.controls.unassigned.value);
      if (this.dialogType === "Expense") {
        this.receiptService
          .createNewExpenseReceipt(this.flowId, body)
          .subscribe(
            res => {
              this.getReceiptList();
              this.showAddNewType = false;
              this.frame.hide();
              this.alert.clearMessage();
              this.alert.sendMessage(res.message, "success");
            },
            err => {
              this.getReceiptList();
              this.frame.hide();
              this.alert.clearMessage();
              this.alert.sendMessage(err.message, "error");
            },
            () => {
              this.spinnerService.hideLoader();
              this.getReceiptList();
              this.frame.hide();
            }
          );
      } else if (this.dialogType === "Income") {
        this.receiptService.createNewIncomeReceipt(this.flowId, body).subscribe(
          res => {
            this.getReceiptList();
            this.showAddNewType = false;
            this.frame.hide();
            this.alert.clearMessage();
            this.alert.sendMessage(res.message, "success");
          },
          err => {
            this.getReceiptList();
            this.frame.hide();
            this.alert.clearMessage();
            this.alert.sendMessage(err.message, "error");
          },
          () => {
            this.spinnerService.hideLoader();
            this.getReceiptList();
            this.frame.hide();
          }
        );
      }
    } else {
      this.receiptForm.controls.receiptType.markAsDirty();
      this.receiptForm.controls.date.markAsDirty();
      this.receiptForm.controls.amount.markAsDirty();
      if (this.showAddNewType) {
        this.newReceiptType.markAsDirty();
      }
    }
  }
  updateReceipt() {
    if (this.receiptForm.valid && this.newReceiptType.valid) {
      if (this.showAddNewType === true) {
        this.createNewReceiptType();
        this.receiptForm.controls.receiptType.setValue(
          this.newReceiptType.value
        );
      }
      this.spinnerService.showLoader();
      let expenceDate;
      let recurrenceDate;
      expenceDate = this.dateFormatter.getFormattedDate(
        this.receiptForm.controls.date.value
      );
      if (this.receiptForm.controls.recurring.value) {
        recurrenceDate = this.dateFormatter.getFormattedDate(
          this.receiptForm.controls.recurrenceDate.value
        );
      }
      const body = new FormData();
      body.append("amount", this.receiptForm.controls.amount.value);
      body.append("date", expenceDate);
      body.append("type", this.receiptForm.controls.receiptType.value);
      body.append(
        "notes",
        this.receiptForm.controls.notes.value
          ? this.receiptForm.controls.notes.value
          : ""
      );
      body.append("recurring", this.receiptForm.controls.recurring.value);
      body.append("recurrenceDate", recurrenceDate ? recurrenceDate : "");
      body.append(
        "source",
        this.receiptForm.controls.source.value
          ? this.receiptForm.controls.source.value
          : ""
      );
      body.append("file", this.selectedFile);
      body.append("unassigned", "true");
      if (this.dialogType === "Expense") {
        this.receiptService
          .updateExpenseReceipt(
            this.receiptForm.controls.moneyFlowReceiptId.value,
            body
          )
          .subscribe(
            res => {
              this.showAddNewType = false;
              this.frame.hide();
              this.resetForm();
              this.alert.clearMessage();
              this.alert.sendMessage(res.message, "success");
            },
            err => {
              this.alert.clearMessage();
              this.alert.sendMessage(err.message, "error");
            },
            () => {
              this.spinnerService.hideLoader();
              this.getReceiptList();
              this.frame.hide();
            }
          );
      } else if (this.dialogType === "Income") {
        this.receiptService
          .updateIncomeReceipt(
            this.receiptForm.controls.moneyFlowReceiptId.value,
            body
          )
          .subscribe(
            res => {
              this.getReceiptList();
              this.showAddNewType = false;
              this.resetForm();
              this.alert.clearMessage();
              this.alert.sendMessage(res.message, "success");
            },
            err => {
              this.alert.clearMessage();
              this.alert.sendMessage(err.message, "error");
            },
            () => {
              this.spinnerService.hideLoader();
              this.getReceiptList();
              this.frame.hide();
            }
          );
      }
    } else {
      this.receiptForm.controls.receiptType.markAsDirty();
      this.receiptForm.controls.date.markAsDirty();
      this.receiptForm.controls.amount.markAsDirty();
      if (this.showAddNewType) {
        this.newReceiptType.markAsDirty();
      }
    }
  }
  getReceiptList() {
    this.receiptList = [];
    this.spinnerService.showLoader();
    var fromDate = this.dateFormatter.getFormattedDate(this.filterFromDate);
    var toDate = this.dateFormatter.getFormattedDate(this.filterToDate);
    this.receiptService.getReceiptList(this.flowId, fromDate, toDate).subscribe(
      res => {
        this.receiptList = res;
        console.log(res);
      },
      err => {
        this.alert.clearMessage();
        this.alert.sendMessage(err.message, "error");
      },
      () => {
        this.spinnerService.hideLoader();
      }
    );
  }
  resetForm() {
    this.receiptForm.reset();
    this.receiptForm.controls.receiptType.enable();
    this.selectedFile = null;
    this.selectedFileName = "";
    this.url = "";
  }
  setFormData(data, rowId) {
    console.log(this.dialogType);
    if (this.dialogType === "Income") {
      this.dialogType = "Income";
      this.receiptType = this.incomeReceiptType;
    } else if (this.dialogType === "Expense") {
      this.dialogType = "Expense";
      this.receiptType = this.expenseReceiptType;
    }
    this.url = data.receiptKey;
    let receiptDate = [];
    var date = new Date(data.date);
    let recurrenceDate = [];
    this.receiptForm.controls.date.setValue(date);
    this.receiptForm.controls.receiptType.setValue(data.recurringType);
    this.receiptForm.controls.moneyFlowReceiptId.setValue(rowId);
    this.receiptForm.controls.date.setValue(date);
    this.receiptForm.controls.amount.setValue(data.amount);
    this.receiptForm.controls.source.setValue(data.source);
    this.receiptForm.controls.notes.setValue(data.notes);
    this.receiptForm.controls.recurring.setValue(data.recurring);
    this.receiptForm.controls.unassigned.setValue(true);
    console.log(this.receiptForm.value);
    if (data.recurring) {
      date = new Date(data.recurrenceDate);
      this.receiptForm.controls.recurrenceDate.setValue(data.recurring ? date : "");
    }
    this.receiptForm.controls.receiptType.enable();
    this.showAddNewType = false;
  }
  setSelectedReceiptId(id) {
    this.selectedReceiptId = id;
  }
  removeReceipt() {
    this.spinnerService.showLoader();
    if (this.dialogType === "Expense") {
      this.receiptService
        .deleteExpenseReceipt(this.selectedReceiptId)
        .subscribe(
          res => {
            this.alert.clearMessage();
            this.alert.sendMessage(res.message, "success");
          },
          err => {
            this.alert.clearMessage();
            this.alert.sendMessage(err.message, "error");
          },
          () => {
            this.spinnerService.hideLoader();
            this.getReceiptList();
            this.deleteFrame.hide();
          }
        );
    } else if (this.dialogType === "Income") {
      this.receiptService.deleteIncomeReceipt(this.selectedReceiptId).subscribe(
        res => {
          this.alert.clearMessage();
          this.alert.sendMessage(res.message, "success");
        },
        err => {
          this.alert.clearMessage();
          this.alert.sendMessage(err.message, "error");
        },
        () => {
          this.spinnerService.hideLoader();
          this.getReceiptList();
          this.deleteFrame.hide();
        }
      );
    }
  }
  downloadImage() {
    window.location.href = this.url;
  }
  ngOnInit() {
    this.dashboardService.clearMessage();
    this.dashboardService.sendMessage(1);
    this.receiptForm = this._fb.group({
      moneyFlowReceiptId: [null],
      receiptType: [null, Validators.required],
      date: ["", Validators.required],
      amount: [null, Validators.required],
      source: [""],
      notes: [""],
      recurring: [false],
      recurrenceDate: [""],
      receiptFile: [null],
      unassigned: [true]
    });
    this.receiptForm.controls.recurring.setValue(false);
    this.receiptForm.controls.unassigned.setValue(true);
  }
  showFile() {
    return this.selectedFileName;
  }
  selectFile(imageInput) {
    console.log("File Upload function");
    this.spinnerService.showLoader();
    this.selectedFile = null;
    this.selectedFileName = "";
    this.url = "";
    this.selectedFileName = imageInput.files[0].name;
    this.ng2ImgMax.compressImage(imageInput.files[0], 2).subscribe(
      result => {
        this.selectedFile = new File([result], result.name);
        this.getImagePreview(this.selectedFile);
        this.spinnerService.hideLoader();
      },
      error => {
        this.spinnerService.hideLoader();
      },
      () => {
        this.spinnerService.hideLoader();
      }
    );
  }
  getImagePreview(file: File) {
    const reader: FileReader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (event: any) => {
      this.url = event.target.result;
    };
  }
  resetFileInput() {
    this.myInputVariable.nativeElement.value = "";
    this.url = "";
    this.selectedFile = null;
    this.selectedFileName = "";
    this.deleteReceipt();
  }
  setValidation() {
    console.log(this.receiptForm.controls.recurring.value);
    if (
      this.receiptForm.controls.recurring.value === false ||
      this.receiptForm.controls.recurring.value === null
    ) {
      this.receiptForm.controls.recurrenceDate.setValidators([
        Validators.required
      ]);
    } else if (this.receiptForm.controls.recurring.value === true) {
      this.receiptForm.controls.recurrenceDate.clearValidators();
    }
    this.receiptForm.controls.recurrenceDate.updateValueAndValidity();
  }
  setNewTypeValidation() {
    if (this.showAddNewType) {
      this.newReceiptType.setValidators([Validators.required]);
    } else {
      this.newReceiptType.clearValidators();
    }
    this.newReceiptType.reset();
    this.newReceiptType.markAsUntouched();
    this.newReceiptType.updateValueAndValidity();
  }
}
