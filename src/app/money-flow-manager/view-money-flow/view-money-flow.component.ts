import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { TabService } from '../tab.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-view-money-flow',
  templateUrl: './view-money-flow.component.html',
  styleUrls: ['./view-money-flow.component.scss']
})
export class ViewMoneyFlowComponent implements OnInit {
  currentTab = {};
  items: MenuItem[];
  id: any;
  httpMethod: any;
  description: any;
  messageSubscription: Subscription;
  constructor(private route: ActivatedRoute,
    private tabService: TabService) {
    this.route.params.subscribe(params => {
      this.id = params.id;
    });
    console.log('The id for the flow is' + this.id);
    console.log(this.route.queryParams);
  }
  ngOnInit() {
    this.items = [
      { label: 'Dashboard', icon: 'fa fa-fw fa-user', routerLink: './dashboard' },
      { label: 'Income', icon: 'fa fa-fw fa-money', routerLink: './income' },
      { label: 'Expense', icon: 'fa fa-fw fa-briefcase', routerLink: './expenses' },
      { label: 'Recurring', icon: 'fa fa-fw fa-repeat', routerLink: './recurring' },
      { label: 'Unassigned Receipts', icon: 'fa fa-fw fa-question-circle', routerLink: './unassigned-receipt' },
      { label: 'Budget', icon: 'fa fa-fw fa-expeditedssl', routerLink: './budget' },
      { label: 'Mileage Tracker', icon: 'fa fa-fw fa-tachometer', routerLink: './mileage-tracker' }
    ];
    if (this.currentTab != {}) {
      this.messageSubscription = this.tabService.getMessage().subscribe(message => {
        this.currentTab = this.items[message];
      });
    }
  }
}
