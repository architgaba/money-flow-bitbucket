import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiCommonService } from 'src/app/core/services/api-common.service';
@Injectable({
  providedIn: 'root'
})
export class AdminReportService {
  constructor(private apiCommonService: ApiCommonService) {
    console.log("Constructor Initialize");
  }
  getAdminReports(): Observable<any> {
    return this.apiCommonService.get('/admin/report');
  }
}
