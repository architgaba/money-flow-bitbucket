import { Component, OnInit, ViewChild } from "@angular/core";
import { DashboardService } from "src/app/dashboard/dashboard.service";
import { AdminReportService } from "./admin-report.service";
import { SpinnerService } from "src/app/core/services/spinner.service";
import { UIChart } from 'primeng/primeng';
@Component({
  selector: "app-admin-reports",
  templateUrl: "./admin-reports.component.html",
  styleUrls: ["./admin-reports.component.scss"]
})
export class AdminReportsComponent implements OnInit {
  currentDateCustomer: any;
  previousDateCustomer: any;
  currentWeekCustomer: any;
  previousWeekCustomer: any;
  currentMonthCustomer: any;
  previousMonthCustomer: any;
  currentYearCustomer: any;
  previousYearCustomer: any;
  totalCustomers: any;

  currentDateYEA: any;
  previousDateYEA: any;
  currentWeekYEA: any;
  previousWeekYEA: any;
  currentMonthYEA: any;
  previousMonthYEA: any;
  currentYearYEA: any;
  previousYearYEA: any;
  totalYEA: any;

  currentDateCustomerRaps: any;
  previousDateCustomerRaps: any;
  currentWeekCustomerRaps: any;
  previousWeekCustomerRaps: any;
  currentMonthCustomerRaps: any;
  previousMonthCustomerRaps: any;
  currentYearCustomerRaps: any;
  previousYearCustomerRaps: any;
  totalCustomersReps: any;
  currentDateGuestUser: any;
  previousDateGuestUser: any;
  currentWeekGuestUser: any;
  previousWeekGuestUser: any;
  currentMonthGuestUser: any;
  previousMonthGuestUser: any;
  currentYearGuestUser: any;
  previousYearGuestUser: any;
  totalProspectUsers: any;
  lastPeriod: any;
  lastYear: any;
  thisPeriod: any;
  thisYear: any;
  previousYearMonthlyCommisssion: any[]
  thisYearMonthlyCommisssion: any[];
  options: any
  @ViewChild("barChartCommission") chartCommission: UIChart;
  commissionGraphData: any;
  cols = [
    { field: "startPeriod", header: "Start Period" },
    { field: "finishperiod", header: "Finish period" },
    { field: "matrix", header: "Matrix" },
    { field: "matchingbonus", header: "Matching bonus" },
    { field: "retail", header: "Retail" },
    { field: "adjustment", header: "Adjustment" },
    { field: "fees", header: "Fees" },
    { field: "total", header: "Total" },
    { field: "action", header: "Action" }
  ];

  constructor(private spinnerService: SpinnerService, private dashboardService: DashboardService, private adminReportService: AdminReportService) {

    this.previousYearMonthlyCommisssion = []
    this.thisYearMonthlyCommisssion = [];
  }
  ngOnInit() {
    this.dashboardService.clearMessage();
    this.dashboardService.sendMessage(4);
    this.getAdminReports();
  }
  getAdminReports() {
    
    this.spinnerService.showLoader();
    this.thisYearMonthlyCommisssion = [];
    this.previousYearMonthlyCommisssion = [];
    this.adminReportService.getAdminReports().subscribe(test => {
      console.log("Object Customer ::: " + test.customers);
      console.log("Object Referrer ::: " + test.distributors);
      console.log("Object Referrer ::: " + test.monthlyCommission);
      this.currentDateCustomer = test.customers.today;
      this.previousDateCustomer = test.customers.yesterday;
      this.currentWeekCustomer = test.customers.thisWeek;
      this.previousWeekCustomer = test.customers.lastWeek;
      this.currentMonthCustomer = test.customers.thisMonth;
      this.previousMonthCustomer = test.customers.lastMonth;
      this.currentYearCustomer = test.customers.thisYear;
      this.previousYearCustomer = test.customers.lastYear;
      this.totalCustomers = test.customers.totalCustomers;
      this.currentDateCustomerRaps = test.customerReps.today;
      this.previousDateCustomerRaps = test.customerReps.yesterday;
      this.currentWeekCustomerRaps = test.customerReps.thisWeek;
      this.previousWeekCustomerRaps = test.customerReps.lastWeek;
      this.currentMonthCustomerRaps = test.customerReps.thisMonth;
      this.previousMonthCustomerRaps = test.customerReps.lastMonth;
      this.currentYearCustomerRaps = test.customerReps.thisYear;
      this.previousYearCustomerRaps = test.customerReps.lastYear;
      this.totalCustomersReps = test.customerReps.totalCustomersReps;
      this.currentDateGuestUser = test.appAmbassadorPlusEligibleMembers.today;
      this.previousDateGuestUser = test.appAmbassadorPlusEligibleMembers.yesterday;
      this.currentWeekGuestUser = test.appAmbassadorPlusEligibleMembers.thisWeek;
      this.previousWeekGuestUser = test.appAmbassadorPlusEligibleMembers.lastWeek;
      this.currentMonthGuestUser = test.appAmbassadorPlusEligibleMembers.thisMonth;
      this.previousMonthGuestUser = test.appAmbassadorPlusEligibleMembers.lastMonth;
      this.currentYearGuestUser = test.appAmbassadorPlusEligibleMembers.thisYear;
      this.previousYearGuestUser = test.appAmbassadorPlusEligibleMembers.lastYear;
      this.totalProspectUsers = test.appAmbassadorPlusEligibleMembers.totalAppAmbassadorPlusEligibleMembers;
      this.lastPeriod = test.monthlyCommission.monthBeforePreviousMonthCommission;
      this.lastYear = test.yearlyCommission.previousYearCommission;
      this.thisPeriod = test.monthlyCommission.previousMonthCommission;
      this.thisYear = test.yearlyCommission.thisYearCommission;

      this.currentDateYEA = test.p2PYoungEntrepreneurs.today;
      this.previousDateYEA = test.p2PYoungEntrepreneurs.yesterday;
      this.currentWeekYEA = test.p2PYoungEntrepreneurs.thisWeek;
      this.previousWeekYEA = test.p2PYoungEntrepreneurs.lastWeek;
      this.currentMonthYEA = test.p2PYoungEntrepreneurs.thisMonth;
      this.previousMonthYEA = test.p2PYoungEntrepreneurs.lastMonth;
      this.currentYearYEA = test.p2PYoungEntrepreneurs.thisYear;
      this.previousYearYEA = test.p2PYoungEntrepreneurs.lastYear;
      this.totalYEA = test.p2PYoungEntrepreneurs.totalP2PYoungEntrepreneurs;


      this.thisYearMonthlyCommisssion = test.yearlyCommissionAmountBreakUp.thisYearMonthlyCommisssion;
      this.previousYearMonthlyCommisssion = test.yearlyCommissionAmountBreakUp.previousYearMonthlyCommisssion;
      this.commissionGraphData = {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        datasets: [
          {
            label: 'Current Year',
            data: this.thisYearMonthlyCommisssion,
            fill: false,
            borderColor: '#4bc0c0'
          },
          {
            label: 'Previous Year',
            data: this.previousYearMonthlyCommisssion,
            fill: false,
            borderColor: '#565656'
          }
        ]
      }
    }, err => {
    }, () => {
      this.chartCommission.refresh();
      this.spinnerService.hideLoader();
    })
  }
}
