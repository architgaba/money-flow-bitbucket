import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { AlertService } from 'src/app/core/services/alert.service';
import { MoneyFlowReportsService } from '../money-flow-reports/money-flow-reports.service';
import { SpinnerService } from 'src/app/core/services/spinner.service';
@Component({
  selector: 'app-mileage-report',
  templateUrl: './mileage-report.component.html',
  styleUrls: ['./mileage-report.component.scss']
})
export class MileageReportComponent implements OnInit {
  loading:boolean=false;
  moneyFLowManagerList: SelectItem[];
  yearList: SelectItem[];
  vehicleList:SelectItem[];
  selectedMoneyFlowManager: string;
  selectedVehicle:string;
  selectedYear:string;
  tableData: any[];
  tableColumns: any[];
  rowGroupMetadata: any;
  constructor(private cdRef:ChangeDetectorRef,private spinnerService: SpinnerService,private alertService: AlertService,private moneyFlowReportsService: MoneyFlowReportsService) {
    this.moneyFLowManagerList = [];
    this.vehicleList=[];
    this.yearList = [];
    this.tableColumns = [
      { field: 'type', header: 'Type',width:"150px" },
      { field: 'subtype', header: 'Sub Type' ,width:"150px"},
      { field: 'january', header: 'Jan',width:"100px" },
      { field: 'february', header: 'Feb' ,width:"100px"},
      { field: 'march', header: 'Mar' ,width:"100px"},
      { field: 'april', header: 'Apr',width:"100px" },
      { field: 'may', header: 'May',width:"100px" },
      { field: 'june', header: 'Jun' ,width:"100px"},
      { field: 'july', header: 'Jul' ,width:"100px"},
      { field: 'august', header: 'Aug',width:"100px" },
      { field: 'september', header: 'Sep' ,width:"100px"},
      { field: 'october', header: 'Oct',width:"100px" },
      { field: 'november', header: 'Nov',width:"100px" },
      { field: 'december', header: 'Dec' ,width:"100px"},
      { field: 'total', header: 'Total',width:"100px" },
    ];
    this.tableData = [];
  }
  ngOnInit() {
    this.getMoneyFlowAccounts();
    this.getYearList();
  }
  getYearList() {
    let date = new Date,
      year = date.getFullYear();
      this.yearList.push({label:(year-5).toString(),value:(year-5).toString()});
      this.yearList.push({label:(year-4).toString(),value:(year-4).toString()});
      this.yearList.push({label:(year-3).toString(),value:(year-3).toString()});
      this.yearList.push({label:(year-2).toString(),value:(year-2).toString()});
      this.yearList.push({label:(year-1).toString(),value:(year-1).toString()});
    for (let i = year; i < year + 5; i++) {
      this.yearList.push({label:i.toString(),value:i.toString()});
    }
    this.selectedYear=year.toString();
  }
  getMoneyFlowAccounts(): any {
    this.spinnerService.showLoader();
    this.moneyFlowReportsService.getMoneyFlowAccounts().subscribe((res) => {
      res.forEach(element => {
        this.moneyFLowManagerList.push({ label: element.moneyFlowName, value: element.moneyFlowAccountId });
      });
      this.spinnerService.hideLoader();
    },
    (error)=>{
      this.spinnerService.hideLoader();
    },
    ()=>{
      this.spinnerService.hideLoader();
    })
  }
  getVehicleList(id): any {
    this.spinnerService.showLoader();
    this.vehicleList=[]
    this.moneyFlowReportsService.getVehicleList(id).subscribe(
      res => {
        res.forEach(element => {
          this.vehicleList.push({
            label: element.year + " " + element.make + " " + element.model,
            value: element.id
          });
        });
        if (this.vehicleList.length === 0) {
          this.vehicleList.push({
            label: "No Vehicles Available",
            value: " "
          });
        }
        this.spinnerService.hideLoader();
        console.log(this.vehicleList);
      },
      err => {
        this.spinnerService.hideLoader();
      },
      () => {
        this.spinnerService.hideLoader();
      }
    );
  }
  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    if (this.tableData) {
      for (let i = 0; i < this.tableData.length; i++) {
        let rowData = this.tableData[i];
        let type = rowData.type;
        if (i === 0) {
          this.rowGroupMetadata[type] = { index: 0, size: 1 };
        } else {
          let previousRowData = this.tableData[i - 1];
          let previousRowGroup = previousRowData.type;
          if (type === previousRowGroup) {
            this.rowGroupMetadata[type].size++;
          } else {
            this.rowGroupMetadata[type] = { index: i, size: 1 };
          }
        }
      }
    }
  }
  getMileageReport() {
    if(this.selectedMoneyFlowManager==null||this.selectedYear==null||this.selectedVehicle==null){
        this.alertService.sendMessage('Please choose a money flow account, vehicle and an year.','error')
        return;
    }
    this.loading=true;
    this.tableData = []
    console.log(this.selectedVehicle);
    this.moneyFlowReportsService.getVehicleReport(this.selectedMoneyFlowManager, this.selectedVehicle,this.selectedYear).subscribe(res => {
      console.log(res);
      res.business.forEach(element => {
        this.tableData.push({ type: 'Business', subtype: element.typeName , january: element.amount[0].amount, february: element.amount[1].amount, march: element.amount[2].amount, april: element.amount[3].amount, may: element.amount[4].amount, june: element.amount[5].amount, july: element.amount[6].amount, august: element.amount[7].amount, september: element.amount[8].amount, october: element.amount[9].amount, november: element.amount[10].amount, december: element.amount[11].amount, total: element.total });
      });
      res.businessTotal?(this.tableData.push({ type: 'Business Trips Total ($)', subtype: '', january: res.businessTotal.amount[0].amount, february: res.businessTotal.amount[1].amount, march: res.businessTotal.amount[2].amount, april: res.businessTotal.amount[3].amount, may: res.businessTotal.amount[4].amount, june: res.businessTotal.amount[5].amount, july: res.businessTotal.amount[6].amount, august: res.businessTotal.amount[7].amount, september: res.businessTotal.amount[8].amount, october: res.businessTotal.amount[9].amount, november: res.businessTotal.amount[10].amount, december: res.businessTotal.amount[11].amount, total: res.businessTotal.total })):null;
      res.personal.forEach(element => {
        this.tableData.push({ type: 'Personal', subtype: element.typeName , january: element.amount[0].amount, february: element.amount[1].amount, march: element.amount[2].amount, april: element.amount[3].amount, may: element.amount[4].amount, june: element.amount[5].amount, july: element.amount[6].amount, august: element.amount[7].amount, september: element.amount[8].amount, october: element.amount[9].amount, november: element.amount[10].amount, december: element.amount[11].amount, total: element.total })
      });
      res.personalTotal?(this.tableData.push({ type: 'Personal Trips Total ($)', subtype: '', january: res.personalTotal.amount[0].amount, february: res.personalTotal.amount[1].amount, march: res.personalTotal.amount[2].amount, april: res.personalTotal.amount[3].amount, may: res.personalTotal.amount[4].amount, june: res.personalTotal.amount[5].amount, july: res.personalTotal.amount[6].amount, august: res.personalTotal.amount[7].amount, september: res.personalTotal.amount[8].amount, october: res.personalTotal.amount[9].amount, november: res.personalTotal.amount[10].amount, december: res.personalTotal.amount[11].amount, total: res.personalTotal.total })):null;
      res.commute.forEach(element => {
        this.tableData.push({ type: 'Commute', subtype: element.typeName , january: element.amount[0].amount, february: element.amount[1].amount, march: element.amount[2].amount, april: element.amount[3].amount, may: element.amount[4].amount, june: element.amount[5].amount, july: element.amount[6].amount, august: element.amount[7].amount, september: element.amount[8].amount, october: element.amount[9].amount, november: element.amount[10].amount, december: element.amount[11].amount, total: element.total })
      });
      res.commuteTotal?(this.tableData.push({ type: 'Commute Trips Total ($)', subtype: '', january: res.commuteTotal.amount[0].amount, february: res.commuteTotal.amount[1].amount, march: res.commuteTotal.amount[2].amount, april: res.commuteTotal.amount[3].amount, may: res.commuteTotal.amount[4].amount, june: res.commuteTotal.amount[5].amount, july: res.commuteTotal.amount[6].amount, august: res.commuteTotal.amount[7].amount, september: res.commuteTotal.amount[8].amount, october: res.commuteTotal.amount[9].amount, november: res.commuteTotal.amount[10].amount, december: res.commuteTotal.amount[11].amount, total: res.commuteTotal.total })):null;
      this.updateRowGroupMetaData();
      this.loading=false;
    }, (err) => {
      this.loading=false;
    }, () => {
     this.cdRef.detectChanges();
    });
  }
}
