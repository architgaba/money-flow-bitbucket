import { Component, OnInit } from "@angular/core";
import { MenuItem } from "primeng/api";
import { TabService } from "../tab.service";
import { Subscription } from "rxjs";
@Component({
  selector: 'app-money-flow-reports',
  templateUrl: './money-flow-reports.component.html',
  styleUrls: ['./money-flow-reports.component.scss']
})
export class MoneyFlowReportsComponent implements OnInit {
  items: MenuItem[];
  currentTab: {};
  messageSubscription: Subscription;
  constructor(
    private tabService: TabService) { }
  ngOnInit() {
    window.scroll(0, 0);
    this.items = [
      {
        label: "Profit Loss Report",
        icon: "fa fa-fw fa-money",
        routerLink: "./profit-loss-report"
      },
      {
        label: "Mileage Report",
        icon: "fa fa-fw fa-car",
        routerLink: "./mileage-report"
      }
    ];
    if (this.currentTab != {}) {
      this.messageSubscription = this.tabService.getMessage().subscribe(message => {
        this.currentTab = this.items[message];
      });
    }
  }
  onGetActiveTab(event: any) {
    console.log(event);
  }
}

