import { Injectable } from '@angular/core';
import { ApiCommonService } from 'src/app/core/services/api-common.service';
import { Observable } from "rxjs";
@Injectable({
  providedIn: 'root'
})
export class MoneyFlowReportsService {
  getVehicleReport(selectedMoneyFlowManager: string, selectedVehicle: string, selectedYear: string): any {
    return this.apiCommonService.get('/money-flow/reports/mileage?flowId=' + selectedMoneyFlowManager + '&vehicle=' + selectedVehicle + '&year=' + selectedYear);
  }
  getVehicleList(id): Observable<any> {
    return this.apiCommonService.get('/money-flow/mileage-tracker/vehicle-list?flowId=' + id);
  }
  constructor(private apiCommonService: ApiCommonService) {
  }
  getMoneyFlowReports(id: any, year: any): Observable<any> {
    return this.apiCommonService.get('/money-flow/reports/profit-loss?MoneyFlowAccountId=' + id + '&year=' + year);
  }
  getMoneyFlowAccounts(): Observable<any> {
    return this.apiCommonService.get('/money-flow/details?fromDate');
  }
}
