import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { MoneyFlowReportsService } from '../money-flow-reports/money-flow-reports.service';
import { SelectItem } from 'primeng/api';
import { AlertService } from 'src/app/core/services/alert.service';
import { DashboardService } from 'src/app/dashboard/dashboard.service';
import { TabService } from '../tab.service';
import { SpinnerService } from 'src/app/core/services/spinner.service';
@Component({
  selector: 'app-profit-loss-report',
  templateUrl: './profit-loss-report.component.html',
  styleUrls: ['./profit-loss-report.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfitLossReportComponent implements OnInit {
  loading: boolean = false;
  moneyFLowManagerList: SelectItem[];
  yearList: SelectItem[];
  selectedMoneyFlowManager: string;
  selectedYear: string;
  tableData: any[];
  budgetTableData: any[];
  tableColumns: any[];
  budgetTableColumns: any[];
  rowGroupMetadata: any;
  constructor(private dashboardService: DashboardService,
    private tabService: TabService,
    private cdRef: ChangeDetectorRef,private spinnerService: SpinnerService, private alertService: AlertService, private moneyFlowReportsService: MoneyFlowReportsService) {
    this.moneyFLowManagerList = [];
    this.yearList = [];
    this.budgetTableColumns = [
      { field: 'type', header: 'Type',width:"150px" },
      { field: 'january', header: 'Jan' ,width:"100px"},
      { field: 'february', header: 'Feb',width:"100px" },
      { field: 'march', header: 'Mar',width:"100px" },
      { field: 'april', header: 'Apr',width:"100px" },
      { field: 'may', header: 'May' ,width:"100px"},
      { field: 'june', header: 'Jun' ,width:"100px"},
      { field: 'july', header: 'Jul' ,width:"100px"},
      { field: 'august', header: 'Aug' ,width:"100px"},
      { field: 'september', header: 'Sep',width:"100px" },
      { field: 'october', header: 'Oct',width:"100px" },
      { field: 'november', header: 'Nov' ,width:"100px"},
      { field: 'december', header: 'Dec' ,width:"100px"},
      { field: 'total', header: 'Total',width:"100px" },
    ]
    this.tableColumns = [
      { field: 'type', header: 'Type',width:"150px" },
      { field: 'subtype', header: 'Sub Type' ,width:"150px"},
      { field: 'january', header: 'Jan',width:"100px" },
      { field: 'february', header: 'Feb' ,width:"100px"},
      { field: 'march', header: 'Mar' ,width:"100px"},
      { field: 'april', header: 'Apr',width:"100px" },
      { field: 'may', header: 'May',width:"100px" },
      { field: 'june', header: 'Jun' ,width:"100px"},
      { field: 'july', header: 'Jul' ,width:"100px"},
      { field: 'august', header: 'Aug',width:"100px" },
      { field: 'september', header: 'Sep' ,width:"100px"},
      { field: 'october', header: 'Oct',width:"100px" },
      { field: 'november', header: 'Nov',width:"100px" },
      { field: 'december', header: 'Dec' ,width:"100px"},
      { field: 'total', header: 'Total',width:"100px" },
    ];
    this.tableData = [];
    this.budgetTableData = [];
  }
  ngOnInit() {
    this.tabService.clearMessage();
    this.tabService.sendMessage(0)
    this.getMoneyFlowAccounts();
    this.getYearList();
    this.dashboardService.clearMessage();
    this.dashboardService.sendMessage(4);
  }
  getYearList() {
    let date = new Date,
      year = date.getFullYear();
    this.yearList.push({ label: (year - 5).toString(), value: (year - 5).toString() });
    this.yearList.push({ label: (year - 4).toString(), value: (year - 4).toString() });
    this.yearList.push({ label: (year - 3).toString(), value: (year - 3).toString() });
    this.yearList.push({ label: (year - 2).toString(), value: (year - 2).toString() });
    this.yearList.push({ label: (year - 1).toString(), value: (year - 1).toString() });
    for (let i = year; i < year + 5; i++) {
      this.yearList.push({ label: i.toString(), value: i.toString() });
    }
    this.selectedYear = year.toString();
  }
  getMoneyFlowAccounts(): any {
    this.spinnerService.showLoader();
    this.moneyFlowReportsService.getMoneyFlowAccounts().subscribe((res) => {
      res.forEach(element => {
        this.moneyFLowManagerList.push({ label: element.moneyFlowName, value: element.moneyFlowAccountId });
      });
      this.spinnerService.hideLoader();
    },
      (error) => {
        this.spinnerService.hideLoader();
      },
      () => {
        this.spinnerService.hideLoader();
      })
  }
  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    if (this.tableData) {
      for (let i = 0; i < this.tableData.length; i++) {
        let rowData = this.tableData[i];
        let type = rowData.type;
        if (i === 0) {
          this.rowGroupMetadata[type] = { index: 0, size: 1 };
        } else {
          let previousRowData = this.tableData[i - 1];
          let previousRowGroup = previousRowData.type;
          if (type === previousRowGroup) {
            this.rowGroupMetadata[type].size++;
          } else {
            this.rowGroupMetadata[type] = { index: i, size: 1 };
          }
        }
      }
    }
  }
  getProfitLossReport() {
    if (this.selectedMoneyFlowManager == null || this.selectedYear == null) {
      this.alertService.sendMessage('Please choose a money flow account and an year.', 'error')
      return;
    }
    this.loading = true;
    this.tableData = []
    this.budgetTableData = []
    this.moneyFlowReportsService.getMoneyFlowReports(this.selectedMoneyFlowManager, this.selectedYear).subscribe(res => {
      console.log(res);
      res.income.forEach(element => {
        this.tableData.push({ type: 'Income', subtype: element.typeName + '($)', january: element.amount[0].amount, february: element.amount[1].amount, march: element.amount[2].amount, april: element.amount[3].amount, may: element.amount[4].amount, june: element.amount[5].amount, july: element.amount[6].amount, august: element.amount[7].amount, september: element.amount[8].amount, october: element.amount[9].amount, november: element.amount[10].amount, december: element.amount[11].amount, total: element.total });
      });
      res.incomeTotal ? (this.tableData.push({ type: 'Total Income($)', subtype: '', january: res.incomeTotal.amount[0].amount, february: res.incomeTotal.amount[1].amount, march: res.incomeTotal.amount[2].amount, april: res.incomeTotal.amount[3].amount, may: res.incomeTotal.amount[4].amount, june: res.incomeTotal.amount[5].amount, july: res.incomeTotal.amount[6].amount, august: res.incomeTotal.amount[7].amount, september: res.incomeTotal.amount[8].amount, october: res.incomeTotal.amount[9].amount, november: res.incomeTotal.amount[10].amount, december: res.incomeTotal.amount[11].amount, total: res.incomeTotal.total })) : null;
      res.expense.forEach(element => {
        this.tableData.push({ type: 'Expense', subtype: element.typeName + '($)', january: element.amount[0].amount, february: element.amount[1].amount, march: element.amount[2].amount, april: element.amount[3].amount, may: element.amount[4].amount, june: element.amount[5].amount, july: element.amount[6].amount, august: element.amount[7].amount, september: element.amount[8].amount, october: element.amount[9].amount, november: element.amount[10].amount, december: element.amount[11].amount, total: element.total })
      });
      res.mileageTotal ? (this.tableData.push({ type: 'Expense', subtype: 'Mileage Expense($)', january: res.mileageTotal.amount[0].amount, february: res.mileageTotal.amount[1].amount, march: res.mileageTotal.amount[2].amount, april: res.mileageTotal.amount[3].amount, may: res.mileageTotal.amount[4].amount, june: res.mileageTotal.amount[5].amount, july: res.mileageTotal.amount[6].amount, august: res.mileageTotal.amount[7].amount, september: res.mileageTotal.amount[8].amount, october: res.mileageTotal.amount[9].amount, november: res.mileageTotal.amount[10].amount, december: res.mileageTotal.amount[11].amount, total: res.mileageTotal.total })) : null;
      res.expenseTotal ? (this.tableData.push({ type: 'Total Expense($)', subtype: '', january: res.expenseTotal.amount[0].amount, february: res.expenseTotal.amount[1].amount, march: res.expenseTotal.amount[2].amount, april: res.expenseTotal.amount[3].amount, may: res.expenseTotal.amount[4].amount, june: res.expenseTotal.amount[5].amount, july: res.expenseTotal.amount[6].amount, august: res.expenseTotal.amount[7].amount, september: res.expenseTotal.amount[8].amount, october: res.expenseTotal.amount[9].amount, november: res.expenseTotal.amount[10].amount, december: res.expenseTotal.amount[11].amount, total: res.expenseTotal.total })) : null;
      res.expenseTotal ? (this.budgetTableData.push({ type: 'Total Expense($)', january: res.expenseTotal.amount[0].amount, february: res.expenseTotal.amount[1].amount, march: res.expenseTotal.amount[2].amount, april: res.expenseTotal.amount[3].amount, may: res.expenseTotal.amount[4].amount, june: res.expenseTotal.amount[5].amount, july: res.expenseTotal.amount[6].amount, august: res.expenseTotal.amount[7].amount, september: res.expenseTotal.amount[8].amount, october: res.expenseTotal.amount[9].amount, november: res.expenseTotal.amount[10].amount, december: res.expenseTotal.amount[11].amount, total: res.expenseTotal.total })) : null;
      res.profitLossTotal ? (this.tableData.push({ type: 'Profit/Loss($)', subtype: '', january: res.profitLossTotal.amount[0].amount, february: res.profitLossTotal.amount[1].amount, march: res.profitLossTotal.amount[2].amount, april: res.profitLossTotal.amount[3].amount, may: res.profitLossTotal.amount[4].amount, june: res.profitLossTotal.amount[5].amount, july: res.profitLossTotal.amount[6].amount, august: res.profitLossTotal.amount[7].amount, september: res.profitLossTotal.amount[8].amount, october: res.profitLossTotal.amount[9].amount, november: res.profitLossTotal.amount[10].amount, december: res.profitLossTotal.amount[11].amount, total: res.profitLossTotal.total })) : null;
      res.budgetTotal ? (this.budgetTableData.push({ type: 'Budget($)', january: res.budgetTotal.amount[0].amount, february: res.budgetTotal.amount[1].amount, march: res.budgetTotal.amount[2].amount, april: res.budgetTotal.amount[3].amount, may: res.budgetTotal.amount[4].amount, june: res.budgetTotal.amount[5].amount, july: res.budgetTotal.amount[6].amount, august: res.budgetTotal.amount[7].amount, september: res.budgetTotal.amount[8].amount, october: res.budgetTotal.amount[9].amount, november: res.budgetTotal.amount[10].amount, december: res.budgetTotal.amount[11].amount, total: res.budgetTotal.total })) : null;
      res.deviationTotal ? (this.budgetTableData.push({ type: 'Variance', january: res.deviationTotal.amount[0].amount, february: res.deviationTotal.amount[1].amount, march: res.deviationTotal.amount[2].amount, april: res.deviationTotal.amount[3].amount, may: res.deviationTotal.amount[4].amount, june: res.deviationTotal.amount[5].amount, july: res.deviationTotal.amount[6].amount, august: res.deviationTotal.amount[7].amount, september: res.deviationTotal.amount[8].amount, october: res.deviationTotal.amount[9].amount, november: res.deviationTotal.amount[10].amount, december: res.deviationTotal.amount[11].amount, total: res.deviationTotal.total })) : null;
      this.updateRowGroupMetaData();
      this.loading = false;
    }, (err) => {
      this.loading = false;
    }, () => {
      this.cdRef.detectChanges();
    });
  }
  getColor(value1, value2) {
    let cssClasses;
    if (value1 === "Profit/Loss($)" && +value2 < 0) {
      cssClasses = {
        "text-color-red": true,
        "text-color-black": false,
        "green-cell": false,
        "amber-cell": false,
        "red-cell": false
      };
    } else if (value1 === "Variance") {
      if (+value2 >= 95) {
        cssClasses = {
          "text-color-red": false,
          "text-color-black": false,
          "green-cell": false,
          "amber-cell": false,
          "red-cell": true
        };
      } else if (+value2 >= 85 && +value2 < 95) {
        cssClasses = {
          "text-color-red": false,
          "text-color-black": false,
          "green-cell": false,
          "amber-cell": true,
          "red-cell": false
        };
      } else if (+value2 < 85) {
        cssClasses = {
          "text-color-red": false,
          "text-color-black": false,
          "green-cell": true,
          "amber-cell": false,
          "red-cell": false
        };
      } else {
        cssClasses = {
          "text-color-red": false,
          "text-color-black": true,
          "green-cell": false,
          "amber-cell": false,
          "red-cell": false
        };
      }
    } else {
      cssClasses = {
        "text-color-red": false,
        "text-color-black": true,
        "green-cell": false,
        "amber-cell": false,
        "red-cell": false
      };
    }
    return cssClasses;
  }
}
