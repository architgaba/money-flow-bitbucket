import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MoneyFlowReportsComponent } from './money-flow-reports/money-flow-reports.component';
import { ProfitLossReportComponent } from './profit-loss-report/profit-loss-report.component';
import { MileageReportComponent } from './mileage-report/mileage-report.component';
import { AdminReportsComponent } from './admin-reports/admin-reports.component';
import { NgxPermissionsGuard } from 'ngx-permissions';
const routes: Routes = [
  {
    path: '',
    canActivate: [NgxPermissionsGuard],
    component: MoneyFlowReportsComponent,
    children: [
      {
        path: '',
        component: ProfitLossReportComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['ROLE_CUSTOMER_REP', 'ROLE_CUSTOMER']
          }
        }
      },
      {
        path: 'profit-loss-report',
        component: ProfitLossReportComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['ROLE_CUSTOMER_REP', 'ROLE_CUSTOMER']
          }
        }
      },
      {
        path: 'mileage-report',
        component: MileageReportComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['ROLE_CUSTOMER_REP', 'ROLE_CUSTOMER']
          }
        }
      },
      {
        path: 'admin-report',
        component: AdminReportsComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['ROLE_ADMIN', 'ROLE_SUPER_ADMIN']
          }
        }
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
