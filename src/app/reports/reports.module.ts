import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableModule } from 'primeng/table';
import { ReportsRoutingModule } from './reports-routing.module';
import { MoneyFlowReportsComponent } from './money-flow-reports/money-flow-reports.component';
import {
  AccordionModule, ModalModule,
  TooltipModule, PopoverModule,
  WavesModule, NavbarModule, ButtonsModule,
  CardsFreeModule, TabsModule,
  InputsModule, IconsModule, CheckboxModule,
  DatepickerModule, SelectModule, InputUtilitiesModule, ChartsModule, ChartSimpleModule
} from 'ng-uikit-pro-standard';
import { NgxPermissionsModule } from "ngx-permissions";
import { ProfitLossReportComponent } from './profit-loss-report/profit-loss-report.component';
import { MileageReportComponent } from './mileage-report/mileage-report.component';
import { TabMenuModule } from 'primeng/tabmenu';
import { FormsModule } from '@angular/forms';
import {DropdownModule} from 'primeng/dropdown';
import { AdminReportsComponent } from './admin-reports/admin-reports.component';
import {ChartModule} from 'primeng/chart';
@NgModule({
  declarations: [MoneyFlowReportsComponent, ProfitLossReportComponent, MileageReportComponent, AdminReportsComponent],
  imports: [
    CommonModule,
    NgxPermissionsModule.forChild(),
    ReportsRoutingModule,
    AccordionModule,
    FormsModule,
    WavesModule,
    NavbarModule,
    ButtonsModule,
    CardsFreeModule,
    TableModule,
    InputsModule,
    IconsModule,
    TabsModule,
    CheckboxModule,
    ModalModule,
    TooltipModule,
    PopoverModule,
    DatepickerModule,
    SelectModule,
    InputUtilitiesModule,
    TabMenuModule,
    DropdownModule,
    TabMenuModule,
    ChartSimpleModule,
    ChartsModule,
    ChartModule
  ]
})
export class ReportsModule { }
