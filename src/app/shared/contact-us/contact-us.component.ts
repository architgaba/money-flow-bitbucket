import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { ApiCommonService } from 'src/app/core/services/api-common.service';
import { AlertService } from 'src/app/core/services/alert.service';
@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
  contactForm: FormGroup;
  constructor(private fb: FormBuilder, private spinnerService: SpinnerService, private apiCommonService: ApiCommonService, private alertService: AlertService) {
  }
  ngOnInit() {
    window.scroll(0, 0);
    this.contactForm = this.fb.group({
      'formName': [null, Validators.required],
      'formEmail': [null, [Validators.required, Validators.email]],
      'formSubject': [null, [Validators.required]],
      'formMessage': [null, [Validators.required]],
      'formReferred': [null, [Validators.required]]
    });
  }
  createContactEnquiry() {
    if (this.contactForm.valid) {
      this.spinnerService.showLoader();
      const body = {
        "emailId": this.contactForm.controls.formEmail.value,
        "message": this.contactForm.controls.formMessage.value,
        "name": this.contactForm.controls.formName.value,
        "subject": this.contactForm.controls.formSubject.value,
        "referredBy": this.contactForm.controls.formReferred.value
      }
      this.apiCommonService.post('/admin/contact-enquiries', body).subscribe(res => {
        console.log(res);
        this.alertService.clearMessage();
        this.alertService.sendMessage(res.message, 'success');
        console.log(res)
      }, (err) => {

      }, () => {
        this.contactForm.reset();
        this.spinnerService.hideLoader();
      });
    }
    else {
    }
  }
}
