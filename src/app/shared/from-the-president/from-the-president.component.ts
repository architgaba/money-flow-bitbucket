import { Component, OnInit } from '@angular/core';
import { Principal } from 'src/app/core/services/principal.service';
@Component({
  selector: 'app-from-the-president',
  templateUrl: './from-the-president.component.html',
  styleUrls: ['./from-the-president.component.scss']
})
export class FromThePresidentComponent implements OnInit {
  image = [
    { url: "../assets/images/mfm-logo.png", show: false },
    { url: "./assets/images/p2p-ceo.jpg", show: false },
    { url: "./assets/images/money-flow-dwain.png", show: false },
    { url: "./assets/images/nileDevelopers.png", show: false }
  ]
  constructor(private principal:Principal) {
    this.principal.changeMessage(false);
   }
  ngOnInit() {
    window.scroll(0 , 0);
  }
}
