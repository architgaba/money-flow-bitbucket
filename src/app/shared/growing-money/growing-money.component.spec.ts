import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrowingMoneyComponent } from './growing-money.component';

describe('GrowingMoneyComponent', () => {
  let component: GrowingMoneyComponent;
  let fixture: ComponentFixture<GrowingMoneyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrowingMoneyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrowingMoneyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
