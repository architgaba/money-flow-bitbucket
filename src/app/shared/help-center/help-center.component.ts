import { Component, OnInit } from '@angular/core';
import { Principal } from 'src/app/core/services/principal.service';

@Component({
  selector: 'app-help-center',
  templateUrl: './help-center.component.html',
  styleUrls: ['./help-center.component.scss']
})
export class HelpCenterComponent implements OnInit {

  constructor(private principal:Principal) {
    this.principal.changeMessage(false);
   }

  ngOnInit(): void {
  }

}
