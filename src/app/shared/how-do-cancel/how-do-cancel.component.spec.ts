import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HowDoCancelComponent } from './how-do-cancel.component';

describe('HowDoCancelComponent', () => {
  let component: HowDoCancelComponent;
  let fixture: ComponentFixture<HowDoCancelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HowDoCancelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HowDoCancelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
