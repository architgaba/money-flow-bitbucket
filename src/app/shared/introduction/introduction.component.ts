import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Principal } from 'src/app/core/services/principal.service';
@Component({
  selector: 'app-introduction',
  templateUrl: './introduction.component.html',
  styleUrls: ['./introduction.component.scss'],
  // encapsulation:ViewEncapsulation.None
})
export class IntroductionComponent implements OnInit {
  constructor(private principal:Principal) {
    this.principal.changeMessage(false);
   }
    cards = [
    {
      img: 'assets/images/module1.png'
    },
    {
      img: 'assets/images/module2.png'
    },
    {
      img: 'assets/images/module3.png'
    },
    {
      img: 'assets/images/module4.png'
    },
    {
      img: 'assets/images/module5.png'
    },
    {
      img: 'assets/images/module1.png'
    }
  ];
  images = [
    { img: 'https://mfmprod-bucket.s3.us-east-2.amazonaws.com/static-academy-images/Kids+on+cell.jpg', thumb:
    'https://mfmprod-bucket.s3.us-east-2.amazonaws.com/static-academy-images/Kids+on+cell.jpg', description: 'Image 1' },
    { img: 'https://mfmprod-bucket.s3.us-east-2.amazonaws.com/static-academy-images/CREDIT+SCORE.jpg', thumb:
    'https://mfmprod-bucket.s3.us-east-2.amazonaws.com/static-academy-images/CREDIT+SCORE.jpg', description: 'Image 2' },
    { img: 'https://mfmprod-bucket.s3.us-east-2.amazonaws.com/static-academy-images/Game+Of+Money.jpg', thumb:
    'https://mfmprod-bucket.s3.us-east-2.amazonaws.com/static-academy-images/Game+Of+Money.jpg', description: 'Image 3' },
    { img: 'https://mfmprod-bucket.s3.us-east-2.amazonaws.com/static-academy-images/Green+Bridge.jpg', thumb:
    'https://mfmprod-bucket.s3.us-east-2.amazonaws.com/static-academy-images/Green+Bridge.jpg', description: 'Image 4' },
    { img: 'https://mfmprod-bucket.s3.us-east-2.amazonaws.com/static-academy-images/Growing+Your+Money.jpg', thumb:
    'https://mfmprod-bucket.s3.us-east-2.amazonaws.com/static-academy-images/Growing+Your+Money.jpg', description: 'Image 5' },
    { img: 'https://mfmprod-bucket.s3.us-east-2.amazonaws.com/static-academy-images/Audit+Protect.jpg', thumb:
    'https://mfmprod-bucket.s3.us-east-2.amazonaws.com/static-academy-images/Audit+Protect.jpg', description: 'Image 6' },
    { img: 'https://mfmprod-bucket.s3.us-east-2.amazonaws.com/static-academy-images/Lifestyle+House.jpg', thumb:
    'https://mfmprod-bucket.s3.us-east-2.amazonaws.com/static-academy-images/Lifestyle+House.jpg', description: 'Image 7' },
    { img: 'https://mfmprod-bucket.s3.us-east-2.amazonaws.com/static-academy-images/Tax+Refund.jpg', thumb:
    'https://mfmprod-bucket.s3.us-east-2.amazonaws.com/static-academy-images/Tax+Refund.jpg', description: 'Image 8' },
    { img: 'https://mfmprod-bucket.s3.us-east-2.amazonaws.com/static-academy-images/Stimulus.jpg', thumb:
    'https://mfmprod-bucket.s3.us-east-2.amazonaws.com/static-academy-images/Stimulus.jpg', description: 'Image 9' }
  ];
  slides: any = [[]];
  chunk(arr: any, chunkSize:any) {
    let R = [];
    for (let i = 0, len = arr.length; i < len; i += chunkSize) {
      R.push(arr.slice(i, i + chunkSize));
    }
    return R;
  }
  ngOnInit() {
    this.slides = this.chunk(this.cards, 3);
    window.scroll(0, 0);
  }
  openStore(url) {
    window.open(url);
  }
}
