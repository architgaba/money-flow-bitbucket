import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ApiCommonService } from '../../core/services/api-common.service';
import { Validators } from '@angular/forms';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AlertService } from '../../core/services/alert.service';
import { SpinnerService } from '../../core/services/spinner.service';
import { Principal } from 'src/app/core/services/principal.service';
@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit,AfterViewInit {
  guestUserForm: FormGroup;
  image = [
    { url: "../assets/images/banner.jpg", show: false },
    { url: "../assets/images/google-store-icon.png", show: false },
    { url: "../assets/images/apple-store-icon.png", show: false },
    { url: "../assets/images/banner-sm-1.jpg", show: false },
    { url: "../assets/images/banner-sm-2.jpg", show: false },
    { url: "../assets/images/banner-sm-3.jpg", show: false },
    { url: "../assets/images/banner2.jpg", show: false },
    { url: "../assets/images/mfm-logo.png", show: false },
    { url: "../assets/images/APP_BENEFITS.jpg", show: false }
  ];
  constructor(private principal:Principal,private alertService: AlertService, private spinnerService: SpinnerService, private apiCommonService: ApiCommonService, private _fb: FormBuilder) { 
    this.principal.changeMessage(true);
  }
  @ViewChild('carousel') carousel: any;

  ngAfterViewInit() {
    this.carousel.el.nativeElement.dispatchEvent(new Event('mouseleave'));
  }
  openStore(url) {
    window.open(url);
  }
  ngOnInit() {
    window.scroll(0, 0);
    this.guestUserForm = this._fb.group({
      "emailId": ['', [Validators.required,Validators.email]],
      "name": ['', [Validators.required]]
    });
  }
  addGuestUser() {
    if (this.guestUserForm.valid) {
      console.log("if");
      this.spinnerService.showLoader();
      const body = {
        "emailId": this.guestUserForm.controls.emailId.value,
        "name": this.guestUserForm.controls.name.value
      };
      this.apiCommonService.post('/marketing/guest-users', body).subscribe(res => {
        console.log(res);
        this.guestUserForm.reset();
      }, (err) => {
      }, () => {
        this.spinnerService.hideLoader();
        this.alertService.clearMessage();
        this.alertService.sendMessage("We appreciate you contacting us.One of our respresentatives will be getting back to you shortly.", "success");
      });
    } else {
      console.log("else");
      Object.keys(this.guestUserForm.controls).forEach(key => {
        this.guestUserForm.get(key).markAsDirty();
      });
    }
  }
}
