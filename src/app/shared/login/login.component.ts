import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from './login.service';
import { StateStorageService } from 'src/app/core/services/state-storage.service';
import { NgxRolesService } from 'ngx-permissions';
import { AlertService } from 'src/app/core/services/alert.service';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { RolesService } from 'src/app/core/services/roles.service';
import { Principal } from 'src/app/core/services/principal.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  image = [
    { url: "assets/images/APP_BENEFITS.jpg", show: false },
    { url: "assets/images/darkgreen.png", show: false },
  ]
  @ViewChild('alert') alert: ElementRef;
  loginForm: FormGroup;
  authenticationError: boolean;
  showLogin: boolean;
  forgotForm: FormGroup;
  otpCode: any;
  showOtpBox: boolean;
  messageFromService: any
  resetForm: FormGroup;
  showResetPassword: any
  isCustomer: boolean;
  isCustomerRep: boolean;
  isProspect: boolean;
  isSubscriptionExpired: boolean;
  isAdmin: boolean;
  isSuperAdmin: boolean;
  show_button1: Boolean = false;
  show_button2: Boolean = false;
  show_eye1: Boolean = false;
  show_eye2: Boolean = false;
  constructor(
    private router: Router, private stateStorageService: StateStorageService,
    private loginService: LoginService, private _fb: FormBuilder,
    private alertService: AlertService, private spinnerService: SpinnerService,
    private roleService: NgxRolesService,
    private rolesService: RolesService, private principal: Principal) {
    this.showLogin = true;
    this.showResetPassword = false
    this.showOtpBox = false;
    this.loginForm = this._fb.group({
      "password": [null, [Validators.required]],
      "userName": [null, [Validators.required]]
    });
    this.forgotForm = this._fb.group({
      "email": [null, [Validators.required, Validators.email]]
    });
    this.resetForm = this._fb.group({
      "oldEmail": [null],
      "otp": [null, [Validators.required]],
      "password": [null, [Validators.required]],
      "confirmPassword": [null, [Validators.required]],
    });
    this.principal.changeMessage(true);
  }
  ngOnInit() {
    window.scroll(0, 0);
  }
  login() {
    if (this.loginForm.valid) {
      console.log(this.loginForm.controls.userName.value);
      this.loginService
        .login({
          userName: this.loginForm.controls.userName.value,
          password: this.loginForm.controls.password.value,
        })
        .then((res) => {
          console.log(res);
          this.alertService.clearMessage();
          this.alertService.sendMessage('Logged In Successfully', 'success');
          console.log('Then block');
          this.authenticationError = false;
          let roles = this.roleService.getRoles();
          console.log(roles);
          var role = this.rolesService.getRole();
          var index = role.indexOf('ROLE_PROSPECT');
          (index < 0) ? this.isProspect = false : this.isProspect = true;
          var index = role.indexOf('SUBSCRIPTION CANCELLED');
          (index < 0) ? this.isSubscriptionExpired = false : this.isSubscriptionExpired = true;
          var index = role.indexOf('ROLE_ADMIN');
          (index < 0) ? this.isAdmin = false : this.isAdmin = true;
          var index = role.indexOf('ROLE_SUPER_ADMIN');
          (index < 0) ? this.isSuperAdmin = false : this.isSuperAdmin = true;
          var index = role.indexOf('ROLE_CUSTOMER_REP');
          (index < 0) ? this.isCustomerRep = false : this.isCustomerRep = true;
          var index = role.indexOf('ROLE_CUSTOMER');
          (index < 0) ? this.isCustomer = false : this.isCustomer = true;
          if (this.isProspect || this.isSubscriptionExpired) {
            this.router.navigate(['./dashboard/profile']);
            return;
          }
          if (this.isAdmin || this.isSuperAdmin) {
            this.router.navigate(['./dashboard/money-flow-reports/admin-report']);
            return;
          }
          if (this.isCustomer || this.isCustomerRep) {
            this.router.navigate(['./dashboard/money-flow-reports']);
            return;
          }
          const redirect = this.stateStorageService.getUrl();
          if (redirect) {
            this.stateStorageService.storeUrl(null);
            this.router.navigate([redirect]);
          }
        })
        .catch(() => {
          console.log("catch ...");
          this.authenticationError = true;
        });
    } else {
      this.loginForm.controls.userName.markAsDirty();
      this.loginForm.controls.password.markAsDirty();
    }
  }
  showPassword(val: any) {
    if (val === 'first') {
      this.show_button1 = !this.show_button1;
      this.show_eye1 = !this.show_eye1;
    } else {
      this.show_button2 = !this.show_button2;
      this.show_eye2 = !this.show_eye2;
    }
  }
  getNewPassword() {
    this.resetForm.reset();
    if (this.forgotForm.valid) {
      this.spinnerService.showLoader();
      console.log(this.forgotForm.controls.email.value);
      let emailAddress = this.forgotForm.controls.email.value;
      this.loginService.forgotPassword(emailAddress).subscribe((res) => {
        this.spinnerService.hideLoader();
        this.messageFromService = res.message
        console.log(res)
        console.log(this.messageFromService)
        this.alertService.clearMessage();
        if (res.status == 'OK') {
          this.alertService.sendMessage(this.messageFromService, 'success');
          this.resetForm.controls.oldEmail.setValue(emailAddress);
          // this.showResetPassword = false;
          // this.showOtpBox = true;
        } else {
          this.alertService.sendMessage(this.messageFromService, 'error');
          this.showResetPassword = true;
        }
      }, err => {
        this.spinnerService.hideLoader();
      }, () => { this.spinnerService.hideLoader(); })
    } else {
      this.forgotForm.controls.email.markAsDirty();
    }
  }
  changeState(state) {
    if (state == 'showResetPassword') {
      this.forgotForm.reset();
      this.showResetPassword = true; this.showLogin = false; this.showOtpBox = false;
    }
    if (state == 'showLogin') {
      this.loginForm.reset();
      this.showLogin = true; this.showResetPassword = false; this.showOtpBox = false;
    }
    if (state == 'showOtpBox') {
      this.resetForm.reset();
      this.showOtpBox = true; this.showResetPassword = false; this.showLogin = false;
    }
  }
  resetPassword() {
    if (this.resetForm.valid) {
      this.spinnerService.showLoader();
      if (this.resetForm.controls.password.value === this.resetForm.controls.confirmPassword.value) {
        const body = {
          "emailAddress": this.resetForm.controls.oldEmail.value,
          "otp": this.resetForm.controls.otp.value,
          "password": this.resetForm.controls.password.value
        }
        this.loginService.resetPassword(body).subscribe((res) => {
          this.spinnerService.hideLoader();
          this.messageFromService = res.message
          console.log(res)
          console.log(this.messageFromService)
          this.alertService.clearMessage();
          if (res.status == 'OK') {
            this.alertService.sendMessage(this.messageFromService, 'success');
            this.showLogin = true;
            this.showResetPassword = false;
            this.showOtpBox = false;
          } else {
            this.alertService.sendMessage(this.messageFromService, 'error');
            this.showResetPassword = true;
            this.showLogin = false;
            this.showOtpBox = false;
          }
        }, err => {
          this.spinnerService.hideLoader();
        }, () => { this.spinnerService.hideLoader(); })
      } else {
        this.spinnerService.hideLoader();
        this.alertService.clearMessage();
        this.alertService.sendMessage("Passwords doesn't match. Please provide the same password in both fields.", 'error');
      }
    } else {
      this.spinnerService.hideLoader();
      this.resetForm.controls.password.markAsDirty();
      this.resetForm.controls.confirmPassword.markAsDirty();
    }
  }
}
