import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Principal } from '../../core/services/principal.service';
import { AuthServerProvider } from '../../core/services/auth-jwt.service';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { ApiCommonService } from 'src/app/core/services/api-common.service';
import { Observable } from 'rxjs';
@Injectable({ providedIn: 'root' })
export class LoginService {
    resetPassword(body: { "emailAddress": any; "otp": any; "password": any; }): any {
        return this.apiCommonService.put("/user/reset-password/", body);
    }
    forgotPassword(email: any): Observable<any> {
        return this.apiCommonService.get("/user/forgot-password/" + email)
    }
    constructor(private apiCommonService: ApiCommonService, private router: Router, private principal: Principal,
        private authServerProvider: AuthServerProvider, private spinnerService: SpinnerService) {
    }
    login(credentials, callback?) {
        this.spinnerService.showLoader();
        const cb = callback || function () { };
        return new Promise((resolve, reject) => {
            this.authServerProvider.login(credentials).subscribe(
                data => {
                    this.spinnerService.hideLoader();
                    console.log(data);
                    this.principal.identity(true).then(account => {
                        resolve(data);
                    });
                    return cb();
                },
                err => {
                    this.spinnerService.hideLoader();
                    reject(err);
                    return cb(err);
                },
                () => {
                }
            );
        });
    }
}
