import { Component, OnInit, Renderer2, ViewChild, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { AlertService } from 'src/app/core/services/alert.service';
import { Principal } from 'src/app/core/services/principal.service';
import { LeaderTeamService } from 'src/app/dashboard/leaders-team/leaders-team.service';
import { ImageModalComponent } from 'ng-uikit-pro-standard';
@Component({
  selector: 'app-mileage-tracker',
  templateUrl: './mileage-tracker.component.html',
  styleUrls: ['./mileage-tracker.component.scss']
})
export class MileageTrackerComponent implements OnInit {
  @ViewChild('ImageModalComponent') ImageModalComponent: ImageModalComponent;
  @ViewChild('ImageModalComponent2') ImageModalComponent2: ImageModalComponent;
  @ViewChild('ImageModalComponent3') ImageModalComponent3: ImageModalComponent;
  @ViewChild('ImageModalComponent4') ImageModalComponent4: ImageModalComponent;
  constructor(private principal: Principal, private spinnerService: SpinnerService, private alertService: AlertService, private leaderService: LeaderTeamService, private renderer: Renderer2, private cd: ChangeDetectorRef) {
    this.principal.changeMessage(false);
  }
  bronzeLeaders = [];
  diamondLeaders = [];
  platinumLeaders = [];
  foundingMembers = [];
  ngOnInit() {
    window.scroll(0, 0);
    this.getLeaders()
  }
  getLeaders() {
    this.spinnerService.showLoader();
    this.leaderService.getLeaders().subscribe(res => {
      res.Bronze.forEach((element, index) => {
        this.bronzeLeaders.push({ img: element.imageUrl, thumb: element.imageUrl, description: 'Image' + '' + index })
      });
      res.Diamond.forEach((element, index) => {
        this.diamondLeaders.push({ img: element.imageUrl, thumb: element.imageUrl, description: 'Image' + '' + index })
      });
      res.Platinum.forEach((element, index) => {
        this.platinumLeaders.push({ img: element.imageUrl, thumb: element.imageUrl, description: 'Image' + '' + index })
      });
      res["Founding Members"].forEach((element, index) => {
        this.foundingMembers.push({ img: element.imageUrl, thumb: element.imageUrl, description: 'Image' + '' + index })
      });
      this.spinnerService.hideLoader();
      this.cd.detectChanges();
    },
      (err) => {
        console.log(err)
        this.alertService.clearMessage();
        this.alertService.sendMessage(err.message, "error");
      },
      () => {
        for (const child of this.ImageModalComponent.element.nativeElement.children) {
          for (const col of child.children) {
            if (!col.classList.contains('ng-gallery-content') && !col.classList.contains('top-bar')) {
              this.renderer.removeClass(col, 'col-md-4');
              this.renderer.addClass(col, 'col-md-3');
            }
          }
        }
        for (const child of this.ImageModalComponent2.element.nativeElement.children) {
          for (const col of child.children) {
            if (!col.classList.contains('ng-gallery-content') && !col.classList.contains('top-bar')) {
              this.renderer.removeClass(col, 'col-md-4');
              this.renderer.addClass(col, 'col-md-3');
            }
          }
        }
        for (const child of this.ImageModalComponent3.element.nativeElement.children) {
          for (const col of child.children) {
            if (!col.classList.contains('ng-gallery-content') && !col.classList.contains('top-bar')) {
              this.renderer.removeClass(col, 'col-md-4');
              this.renderer.addClass(col, 'col-md-3');
            }
          }
        }
        for (const child of this.ImageModalComponent4.element.nativeElement.children) {
          for (const col of child.children) {
            if (!col.classList.contains('ng-gallery-content') && !col.classList.contains('top-bar')) {
              this.renderer.removeClass(col, 'col-md-4');
              this.renderer.addClass(col, 'col-md-3');
            }
          }
        }
        this.spinnerService.hideLoader();
      })
  }


  openStore(url) {
    window.open(url);
  }

}
