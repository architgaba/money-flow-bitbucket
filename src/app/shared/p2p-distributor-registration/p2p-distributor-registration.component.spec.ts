import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { P2pDistributorRegistrationComponent } from './p2p-distributor-registration.component';

describe('P2pDistributorRegistrationComponent', () => {
  let component: P2pDistributorRegistrationComponent;
  let fixture: ComponentFixture<P2pDistributorRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ P2pDistributorRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(P2pDistributorRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
