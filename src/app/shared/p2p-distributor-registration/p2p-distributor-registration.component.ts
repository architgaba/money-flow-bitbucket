import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { AlertService } from 'src/app/core/services/alert.service';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { ReferrerService } from 'src/app/dashboard/referrers/view-referrers/referrer.service';
import { Router } from '@angular/router';
import { LoginService } from '../login/login.service';


@Component({
  selector: 'app-p2p-distributor-registration',
  templateUrl: './p2p-distributor-registration.component.html',
  styleUrls: ['./p2p-distributor-registration.component.scss']
})
export class P2pDistributorRegistrationComponent implements OnInit {
  addReferrerForm: FormGroup;
  show_button1: Boolean = false;
  show_button2: Boolean = false;
  show_eye1: Boolean = false;
  show_eye2: Boolean = false;
  constructor(private _fb: FormBuilder,
    private alertService: AlertService,
    private spinnerService: SpinnerService,
    private loginService: LoginService, private router: Router,
    private referrerService: ReferrerService) { }

  ngOnInit() {
    this.addReferrerForm = this._fb.group({
      referrerId: [""],
      contactNumber: ['', [Validators.pattern(/^\(\d{3}\)\s\d{3}-\d{4}$/)]],
      email: ["", [Validators.required, Validators.email]],
      firstName: ["", [Validators.required]],
      lastName: [""],
      test: ["3", [Validators.required]],
      p2pDistributer: [""],
      referralCode: [null],
      userName: ["", Validators.required],
      password: ["", Validators.required],
      confirmPassword: ["", Validators.required],
    });
  }
  createNewReferrer() {
    if (this.addReferrerForm.valid) {
      if (this.addReferrerForm.controls.confirmPassword.value != this.addReferrerForm.controls.password.value) {
        this.alertService.clearMessage();
        this.alertService.sendMessage("Password don't match.", "error");
        return;
      }
      this.spinnerService.showLoader();
      const body = {
        contactNumber: this.addReferrerForm.controls.contactNumber.value,
        email: this.addReferrerForm.controls.email.value,
        firstName: this.addReferrerForm.controls.firstName.value,
        lastName: this.addReferrerForm.controls.lastName.value,
        accountType: this.addReferrerForm.controls.test.value,
        referralCode: this.addReferrerForm.controls.referralCode.value,
        userName: this.addReferrerForm.controls.userName.value,
        password: this.addReferrerForm.controls.password.value,
      };
      this.referrerService.p2pDistributorSelfRegister(body).subscribe(
        res => {
          console.log(res);
          this.alertService.clearMessage();
          this.alertService.sendMessage(res.message, "success");
          this.loginService.login({
            userName: this.addReferrerForm.controls.userName.value,
            password: this.addReferrerForm.controls.password.value,
          }).then(() => {
            this.alertService.clearMessage();
            this.alertService.sendMessage('Logged In Successfully', 'success');
            this.router.navigate(['./dashboard/profile']);
          });
        },
        err => {
        },
        () => {
          this.spinnerService.hideLoader();
        }
      );
    } else {
      Object.keys(this.addReferrerForm.controls).forEach(key => {
        this.addReferrerForm.get(key).markAsDirty();
      });
    }
  }

  showPassword(val: any) {
    if (val === 'first') {
      this.show_button1 = !this.show_button1;
      this.show_eye1 = !this.show_eye1;
    } else {
      this.show_button2 = !this.show_button2;
      this.show_eye2 = !this.show_eye2;
    }
  }

}
