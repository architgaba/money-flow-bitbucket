import { Component, OnInit } from '@angular/core';
import { Principal } from 'src/app/core/services/principal.service';
@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss']
})
export class PageNotFoundComponent implements OnInit {
  constructor(private principal:Principal) { 
    this.principal.changeMessage(false);
  }
  ngOnInit() {
  }
}
