import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiCommonService } from 'src/app/core/services/api-common.service';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { AlertService } from 'src/app/core/services/alert.service';
import { LoginService } from '../login/login.service';
import { Principal } from 'src/app/core/services/principal.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  id: any;
  resetPasswordForm: FormGroup;
  show_button1: Boolean = false;
  show_button2: Boolean = false;
  show_eye1: Boolean = false;
  showSuccessMessage = false;
  show_eye2: Boolean = false;
  constructor(private principal: Principal, private loginService: LoginService, private router: Router, private alertService: AlertService, private spinnerService: SpinnerService, private apiCommonService: ApiCommonService, private route: ActivatedRoute, private _fb: FormBuilder) {
    this.route.params.subscribe(params => {
      this.id = params.id
    });
    this.principal.changeMessage(false);
    this.resetPasswordForm = this._fb.group({
      "password": ['', [Validators.required]],
      "confirmPassword": ['', [Validators.required]],
    });
  }
  ngOnInit() {
    console.log(this.id);
  }
  resetPassword() {
    if (this.resetPasswordForm.valid) {
      if (this.resetPasswordForm.controls.confirmPassword.value != this.resetPasswordForm.controls.password.value) {
        this.alertService.clearMessage();
        this.alertService.sendMessage("Password don't match.", "error");
        return;
      }
      this.spinnerService.showLoader();

      const body = {
        "confirmPassword": this.resetPasswordForm.controls.confirmPassword.value,
        "password": this.resetPasswordForm.controls.password.value,
        "passwordResetKey": this.id
      }
      this.apiCommonService.put('/user/reset-password', body).subscribe(res => {
        console.log(res);
        this.alertService.clearMessage();
        this.alertService.sendMessage(res.message, "success");
        this.showSuccessMessage = true;
      }, (err) => {
        this.spinnerService.hideLoader();
      }, () => {
        this.spinnerService.hideLoader();
      });
    }
    else {
      Object.keys(this.resetPasswordForm.controls).forEach(key => {
        this.resetPasswordForm.get(key).markAsDirty();
      });
    }
  }
  showPassword(val: any) {
    if (val === 'first') {
      this.show_button1 = !this.show_button1;
      this.show_eye1 = !this.show_eye1;
    } else {
      this.show_button2 = !this.show_button2;
      this.show_eye2 = !this.show_eye2;
    }
  }
  navigateToLogin() {
    this.router.navigate(['./login']);
  }
}



