import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { NgxPermissionsModule } from 'ngx-permissions';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IntroductionComponent } from './introduction/introduction.component';
import { UserActivationComponent } from './user-activation/user-activation.component';
import { FooterComponent } from '../layouts/footer/footer.component';
import { CheckboxModule, WavesModule, ButtonsModule, CardsFreeModule,CarouselModule,SmoothscrollModule,
  AccordionModule,IconsModule,ModalModule,LightBoxModule,
  CollapseModule, InputsModule } from 'ng-uikit-pro-standard'
import { FromThePresidentComponent } from './from-the-president/from-the-president.component';
import { MileageTrackerComponent } from './mileage-tracker/mileage-tracker.component';
import { DeferLoadModule } from '@trademe/ng-defer-load';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { P2pDistributorRegistrationComponent } from './p2p-distributor-registration/p2p-distributor-registration.component';
import { PipeModule } from '../core/pipes/pipe.module';
import { SharedComponentAuthGuard } from '../core/services/shared.component.guard';
import { AccountSetupComponent } from './account-setup/account-setup.component';
import { AppFeaturesComponent } from './app-features/app-features.component';
import { FaqComponent } from './faq/faq.component';
import { GrowingMoneyComponent } from './growing-money/growing-money.component';
import { HowDoCancelComponent } from './how-do-cancel/how-do-cancel.component';
import { HelpCenterComponent } from './help-center/help-center.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
@NgModule({
  declarations: [LandingPageComponent, ContactUsComponent,
    LoginComponent, SignupComponent, ContactUsComponent, IntroductionComponent,
    UserActivationComponent, FooterComponent, AccountSetupComponent,
    AppFeaturesComponent,
   FaqComponent,
   GrowingMoneyComponent ,
   HowDoCancelComponent ,HelpCenterComponent,FromThePresidentComponent, MileageTrackerComponent, PageNotFoundComponent, P2pDistributorRegistrationComponent, ResetPasswordComponent],
  imports: [
    CommonModule,
    FormsModule,
    WavesModule,
    InputsModule,
    PipeModule,
    ModalModule,
    ReactiveFormsModule,
    RouterModule,
    SmoothscrollModule,
    AccordionModule,
    CollapseModule,
    IconsModule,
    CheckboxModule, WavesModule, ButtonsModule, LightBoxModule,CardsFreeModule,CarouselModule,
    NgxPermissionsModule.forChild(),
    DeferLoadModule
  ],
  exports: [
    NgxPermissionsModule
  ],
  providers:[
    SharedComponentAuthGuard
  ]})
export class SharedModule { }
