import { Component, OnInit } from '@angular/core';
import { ApiCommonService } from 'src/app/core/services/api-common.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService } from 'src/app/core/services/alert.service';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { SignUpService } from './signup.service';
import { LoginService } from '../login/login.service';
import { Router } from '@angular/router';
import { Principal } from 'src/app/core/services/principal.service';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;
  show_button1: Boolean = false;
  show_button2: Boolean = false;
  show_eye1: Boolean = false;
  show_eye2: Boolean = false;
  image = [
    { url: "assets/images/APP_BENEFITS.jpg", show: false },
    { url: "assets/images/darkgreen.png", show: false },
  ]
  constructor(private loginService: LoginService, private router: Router, private spinnerService: SpinnerService, private signUpService: SignUpService, private _fb: FormBuilder, private apiCommonService: ApiCommonService,
    private alertService: AlertService, private principal: Principal) {
    this.principal.changeMessage(true);
  }
  ngOnInit() {
    window.scroll(0, 0);
    this.signupForm = this._fb.group({
      "email": [null, [Validators.required, Validators.email]],
      "emailUpdates": [false],
      "firstName": [null, [Validators.required]],
      "lastName": [null],
      "password": [null, [Validators.required]],
      "confirmPassword": [null, [Validators.required]],
      "userName": [null, [Validators.required]],
      "customerRepCode": [null,[Validators.required]]
    });
  }
  savePersonalInfo() {
    const body = {
      "email": this.signupForm.controls.email.value,
      "emailUpdates": this.signupForm.controls.emailUpdates.value,
      "firstName": this.signupForm.controls.firstName.value,
      "lastName": this.signupForm.controls.lastName.value,
      "password": this.signupForm.controls.password.value,
      "userName": this.signupForm.controls.userName.value,
      "customerRepCode": this.signupForm.controls.customerRepCode.value
    };
    console.log(body);
    if (this.signupForm.valid) {
      this.spinnerService.showLoader();
      if (this.signupForm.controls.password.value === this.signupForm.controls.confirmPassword.value) {
        this.signUpService.signUp(body).subscribe(res => {
          this.spinnerService.hideLoader();
          console.log(res);
          this.alertService.clearMessage();
          // this.alertService.sendMessage('Your account has been created successfully.Please check your email for verification link to activate your account.', 'success');
          // this.signupForm.reset();

          this.alertService.clearMessage();
          this.alertService.sendMessage(res.message, "success");
          this.loginService.login({
            userName: this.signupForm.controls.userName.value,
            password: this.signupForm.controls.password.value,
          }).then(() => {
            this.alertService.clearMessage();
            this.alertService.sendMessage('Logged In Successfully', 'success');
            this.router.navigate(['./dashboard/profile']);
          });

        },
          (err) => {
            this.spinnerService.hideLoader();
          },
          () => {
            this.spinnerService.hideLoader();
          });
      } else {
        this.spinnerService.hideLoader();
        this.alertService.clearMessage();
        this.alertService.sendMessage("Passwords don't match.Please provide same password in both fields.", 'error');
      }
    } else {
      this.spinnerService.hideLoader();
      this.signupForm.controls.email.markAsDirty();
      this.signupForm.controls.firstName.markAsDirty();
      this.signupForm.controls.password.markAsDirty();
      this.signupForm.controls.confirmPassword.markAsDirty();
      this.signupForm.controls.userName.markAsDirty();
      this.signupForm.controls.customerRepCode.markAsDirty();
    }
  }
  showPassword(val: any) {
    if (val === 'first') {
      this.show_button1 = !this.show_button1;
      this.show_eye1 = !this.show_eye1;
    } else {
      this.show_button2 = !this.show_button2;
      this.show_eye2 = !this.show_eye2;
    }
  }
}
