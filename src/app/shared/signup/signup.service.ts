import { Injectable } from '@angular/core';
import { ApiCommonService } from 'src/app/core/services/api-common.service';
import { Observable } from 'rxjs';
@Injectable({ providedIn: 'root' })
export class SignUpService {
    constructor (private apiCommonService: ApiCommonService) {
      }
    signUp(requestBody):Observable<any>{
       return this.apiCommonService.post('/user/register', requestBody);
    }
}