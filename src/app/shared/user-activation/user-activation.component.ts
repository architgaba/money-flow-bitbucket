import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiCommonService } from 'src/app/core/services/api-common.service';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { AlertService } from 'src/app/core/services/alert.service';
import { LoginService } from '../login/login.service';
import { Principal } from 'src/app/core/services/principal.service';
@Component({
  selector: 'app-user-activation',
  templateUrl: './user-activation.component.html',
  styleUrls: ['./user-activation.component.scss']
})
export class UserActivationComponent implements OnInit {
  id: any;
  userActivationForm: FormGroup;
  show_button1: Boolean = false;
  show_button2: Boolean = false;
  show_eye1: Boolean = false;
  show_eye2: Boolean = false;
  constructor(private principal:Principal,private loginService: LoginService, private router: Router, private alertService: AlertService, private spinnerService: SpinnerService, private apiCommonService: ApiCommonService, private route: ActivatedRoute, private _fb: FormBuilder) {
    this.route.params.subscribe(params => {
      this.id = params.id
    });
    this.userActivationForm = this._fb.group({
      "userName": ['', [Validators.required]],
      "password": ['', [Validators.required]],
      "confirmPassword": ['', [Validators.required]],
    });
    this.principal.changeMessage(false);
  }
  ngOnInit() {
    console.log(this.id);
  }
  activateUser() {
    if (this.userActivationForm.valid) {
      if (this.userActivationForm.controls.confirmPassword.value != this.userActivationForm.controls.password.value) {
        this.alertService.clearMessage();
        this.alertService.sendMessage("Password don't match.", "error");
        return;
      }
      this.spinnerService.showLoader();
      const body = {
        "password": this.userActivationForm.controls.password.value,
        "userName": this.userActivationForm.controls.userName.value
      }
      this.apiCommonService.put('/customer/activate/account/' + this.id, body).subscribe(res => {
        console.log(res);
        this.alertService.clearMessage();
        this.alertService.sendMessage(res.message, "success");
        this.loginService.login({
          userName: this.userActivationForm.controls.userName.value,
          password: this.userActivationForm.controls.password.value,
        }).then(() => {
          this.alertService.clearMessage();
          this.alertService.sendMessage('Logged In Successfully', 'success');
          this.router.navigate(['./dashboard/profile']);
        });
      }, (err) => {
      }, () => {
        this.spinnerService.hideLoader();
      });
    }
    else {
      Object.keys(this.userActivationForm.controls).forEach(key => {
        this.userActivationForm.get(key).markAsDirty();
      });
    }
  }
  showPassword(val: any) {
    if (val === 'first') {
      this.show_button1 = !this.show_button1;
      this.show_eye1 = !this.show_eye1;
    } else {
      this.show_button2 = !this.show_button2;
      this.show_eye2 = !this.show_eye2;
    }
  }
}
